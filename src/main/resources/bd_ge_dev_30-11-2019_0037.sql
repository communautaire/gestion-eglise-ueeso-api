-- --------------------------------------------------------
-- Hôte :                        bd-ueeso-cocody.coswj31nm5fw.us-east-2.rds.amazonaws.com
-- Version du serveur:           5.6.40-log - Source distribution
-- SE du serveur:                Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour bd_ge
DROP DATABASE IF EXISTS `bd_ge`;
CREATE DATABASE IF NOT EXISTS `bd_ge` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd_ge`;

-- Export de la structure de la table bd_ge. adresse
DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ville` varchar(255) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `quartier` varchar(255) DEFAULT NULL,
  `secteur` varchar(255) DEFAULT NULL,
  `adresse_postale` varchar(255) DEFAULT NULL,
  `id_user` smallint(11) unsigned DEFAULT NULL,
  `id_pays_rsd` smallint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_pays_rsd` (`id_pays_rsd`),
  KEY `ville` (`ville`),
  KEY `commune` (`commune`),
  KEY `quartier` (`quartier`),
  CONSTRAINT `adresse_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user_tab` (`id`),
  CONSTRAINT `adresse_ibfk_2` FOREIGN KEY (`id_pays_rsd`) REFERENCES `pays` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.adresse : ~0 rows (environ)
/*!40000 ALTER TABLE `adresse` DISABLE KEYS */;
/*!40000 ALTER TABLE `adresse` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. engagement
DROP TABLE IF EXISTS `engagement`;
CREATE TABLE IF NOT EXISTS `engagement` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `id_projet` smallint(5) unsigned DEFAULT NULL,
  `id_user` smallint(5) unsigned DEFAULT NULL,
  `date_engagement` date DEFAULT NULL,
  `montant` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_projet` (`id_projet`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `engagement_ibfk_1` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id`),
  CONSTRAINT `engagement_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user_tab` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.engagement : ~4 rows (environ)
/*!40000 ALTER TABLE `engagement` DISABLE KEYS */;
INSERT INTO `engagement` (`id`, `id_projet`, `id_user`, `date_engagement`, `montant`) VALUES
	(1, NULL, NULL, '2018-04-13', 1000000),
	(2, NULL, NULL, NULL, 2500000),
	(3, NULL, NULL, NULL, 2500000),
	(4, NULL, NULL, NULL, 2500000);
/*!40000 ALTER TABLE `engagement` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. groupe_const
DROP TABLE IF EXISTS `groupe_const`;
CREATE TABLE IF NOT EXISTS `groupe_const` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `description` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.groupe_const : ~0 rows (environ)
/*!40000 ALTER TABLE `groupe_const` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupe_const` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. migration_versions
DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table bd_ge.migration_versions : ~0 rows (environ)
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. mission
DROP TABLE IF EXISTS `mission`;
CREATE TABLE IF NOT EXISTS `mission` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `description` mediumtext,
  `adresse` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.mission : ~0 rows (environ)
/*!40000 ALTER TABLE `mission` DISABLE KEYS */;
/*!40000 ALTER TABLE `mission` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. paiement
DROP TABLE IF EXISTS `paiement`;
CREATE TABLE IF NOT EXISTS `paiement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `montant` int(11) DEFAULT NULL,
  `id_eng` smallint(11) unsigned DEFAULT NULL,
  `date_paiement` datetime DEFAULT NULL,
  `id_tresorier` smallint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_eng` (`id_eng`),
  KEY `date_paiement` (`date_paiement`),
  KEY `id_tresorier` (`id_tresorier`),
  KEY `montant` (`montant`),
  CONSTRAINT `paiement_ibfk_1` FOREIGN KEY (`id_eng`) REFERENCES `engagement` (`id`),
  CONSTRAINT `paiement_ibfk_2` FOREIGN KEY (`date_paiement`) REFERENCES `periode` (`date_paiement`),
  CONSTRAINT `paiement_ibfk_3` FOREIGN KEY (`id_tresorier`) REFERENCES `tresorier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.paiement : ~0 rows (environ)
/*!40000 ALTER TABLE `paiement` DISABLE KEYS */;
/*!40000 ALTER TABLE `paiement` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. pays
DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.pays : ~0 rows (environ)
/*!40000 ALTER TABLE `pays` DISABLE KEYS */;
/*!40000 ALTER TABLE `pays` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. periode
DROP TABLE IF EXISTS `periode`;
CREATE TABLE IF NOT EXISTS `periode` (
  `date_paiement` datetime NOT NULL,
  `dayOfweek` smallint(5) unsigned DEFAULT NULL,
  `dayOfmonth` smallint(5) unsigned DEFAULT NULL,
  `dayOfyear` smallint(5) unsigned DEFAULT NULL,
  `weekOfmonth` smallint(5) unsigned DEFAULT NULL,
  `weekOfyear` smallint(5) unsigned DEFAULT NULL,
  `month` smallint(5) unsigned DEFAULT NULL,
  `trimester` smallint(5) unsigned DEFAULT NULL,
  `semester` smallint(5) unsigned DEFAULT NULL,
  `year` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`date_paiement`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.periode : ~0 rows (environ)
/*!40000 ALTER TABLE `periode` DISABLE KEYS */;
/*!40000 ALTER TABLE `periode` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. profession
DROP TABLE IF EXISTS `profession`;
CREATE TABLE IF NOT EXISTS `profession` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.profession : ~0 rows (environ)
/*!40000 ALTER TABLE `profession` DISABLE KEYS */;
/*!40000 ALTER TABLE `profession` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. projet
DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) DEFAULT NULL,
  `description` mediumtext,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `cout` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.projet : ~0 rows (environ)
/*!40000 ALTER TABLE `projet` DISABLE KEYS */;
/*!40000 ALTER TABLE `projet` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. tresorier
DROP TABLE IF EXISTS `tresorier`;
CREATE TABLE IF NOT EXISTS `tresorier` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `adresse_email` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.tresorier : ~0 rows (environ)
/*!40000 ALTER TABLE `tresorier` DISABLE KEYS */;
/*!40000 ALTER TABLE `tresorier` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. type_user
DROP TABLE IF EXISTS `type_user`;
CREATE TABLE IF NOT EXISTS `type_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `code` int(10) unsigned DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.type_user : ~0 rows (environ)
/*!40000 ALTER TABLE `type_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_user` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. user_gc
DROP TABLE IF EXISTS `user_gc`;
CREATE TABLE IF NOT EXISTS `user_gc` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `fonction` varchar(20) DEFAULT NULL,
  `id_user` smallint(5) unsigned DEFAULT NULL,
  `id_gc` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_gc` (`id_gc`),
  CONSTRAINT `user_gc_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user_tab` (`id`),
  CONSTRAINT `user_gc_ibfk_2` FOREIGN KEY (`id_gc`) REFERENCES `groupe_const` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.user_gc : ~0 rows (environ)
/*!40000 ALTER TABLE `user_gc` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_gc` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. user_mission
DROP TABLE IF EXISTS `user_mission`;
CREATE TABLE IF NOT EXISTS `user_mission` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `fonction` varchar(20) DEFAULT NULL,
  `id_user` smallint(5) unsigned DEFAULT NULL,
  `id_mission` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_mission` (`id_mission`),
  CONSTRAINT `user_mission_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user_tab` (`id`),
  CONSTRAINT `user_mission_ibfk_2` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.user_mission : ~0 rows (environ)
/*!40000 ALTER TABLE `user_mission` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_mission` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. user_tab
DROP TABLE IF EXISTS `user_tab`;
CREATE TABLE IF NOT EXISTS `user_tab` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `adresse_email` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `image` text,
  `sitation_prof` enum('etudiant','travailleur','recherche emploi','retraite') DEFAULT NULL,
  `is_local` enum('yes','no') DEFAULT NULL,
  `nom_conjoint` varchar(20) DEFAULT NULL,
  `date_mariage` date DEFAULT NULL,
  `is_home_respo` enum('yes','no') DEFAULT NULL,
  `home_respo_name` varchar(20) DEFAULT NULL,
  `date_arrive_ueeso` date DEFAULT NULL,
  `date_conversion` date DEFAULT NULL,
  `bapteme_immersion` enum('yes','no') DEFAULT NULL,
  `date_bapteme` date DEFAULT NULL,
  `eglise_bapteme` varchar(65) DEFAULT NULL,
  `eglise_origine` varchar(65) DEFAULT NULL,
  `type_user` smallint(5) unsigned DEFAULT NULL,
  `id_pays_nat` smallint(5) unsigned DEFAULT NULL,
  `id_prof` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_user` (`type_user`),
  KEY `id_pays_nat` (`id_pays_nat`),
  KEY `id_prof` (`id_prof`),
  CONSTRAINT `user_tab_ibfk_1` FOREIGN KEY (`type_user`) REFERENCES `type_user` (`id`),
  CONSTRAINT `user_tab_ibfk_2` FOREIGN KEY (`id_pays_nat`) REFERENCES `pays` (`id`),
  CONSTRAINT `user_tab_ibfk_3` FOREIGN KEY (`id_prof`) REFERENCES `profession` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.user_tab : ~0 rows (environ)
/*!40000 ALTER TABLE `user_tab` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tab` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
