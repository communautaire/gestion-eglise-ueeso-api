-- --------------------------------------------------------
-- Hôte :                        bd-ueeso-cocody.coswj31nm5fw.us-east-2.rds.amazonaws.com
-- Version du serveur:           5.6.40-log - Source distribution
-- SE du serveur:                Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour bd_ge
DROP DATABASE IF EXISTS `bd_ge`;
CREATE DATABASE IF NOT EXISTS `bd_ge` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd_ge`;

-- Export de la structure de la table bd_ge. adresse
DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `id_pays` int(11) DEFAULT NULL COMMENT 'Pays de residence',
  `id_ville` int(11) DEFAULT NULL,
  `id_commune` int(11) DEFAULT NULL,
  `id_quartier` int(11) DEFAULT NULL,
  `adresse_postale` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_pays_rsd` (`id_pays`),
  KEY `ville` (`id_ville`),
  KEY `commune` (`id_commune`),
  KEY `quartier` (`id_quartier`),
  CONSTRAINT `FK_adresse_commune` FOREIGN KEY (`id_commune`) REFERENCES `commune` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_adresse_pays` FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_adresse_quartier` FOREIGN KEY (`id_quartier`) REFERENCES `quartier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_adresse_ville` FOREIGN KEY (`id_ville`) REFERENCES `ville` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `adresse_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.adresse : ~18 rows (environ)
/*!40000 ALTER TABLE `adresse` DISABLE KEYS */;
INSERT INTO `adresse` (`id`, `id_user`, `id_pays`, `id_ville`, `id_commune`, `id_quartier`, `adresse_postale`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 2, 1, 84, NULL, NULL, 'test)adresse', '2020-01-18 20:31:14', NULL, NULL, NULL, NULL, NULL, b'0'),
	(2, 3, 1, 84, NULL, NULL, 'testadresse', '2020-01-18 20:47:10', NULL, '2020-01-23 15:14:16', NULL, NULL, 1, b'1'),
	(3, 5, 1, 84, NULL, NULL, 'testadresse', '2020-01-18 20:49:49', NULL, '2020-01-23 15:14:17', NULL, NULL, 1, b'1'),
	(4, 6, 1, 84, NULL, NULL, 'testadresse', '2020-01-21 11:12:21', NULL, '2020-01-23 15:14:19', NULL, NULL, 1, b'1'),
	(5, 7, 1, 84, NULL, NULL, 'testadresse', '2020-01-21 11:13:38', NULL, '2020-01-23 15:14:21', NULL, NULL, 1, b'1'),
	(6, 8, 1, 84, NULL, NULL, 'testadresse', '2020-01-21 15:27:39', NULL, '2020-01-23 15:14:22', NULL, NULL, 1, b'1'),
	(7, 9, 1, 84, NULL, NULL, 'testadresse', '2020-01-21 15:31:36', NULL, '2020-01-23 15:14:24', NULL, NULL, 1, b'1'),
	(8, 10, 1, 79, NULL, NULL, 'Yopougon', '2020-01-23 11:46:01', NULL, NULL, NULL, NULL, NULL, b'0'),
	(9, 11, 1, 79, NULL, NULL, 'Yopougon', '2020-01-23 15:16:36', NULL, NULL, NULL, NULL, NULL, b'0'),
	(10, 12, 39, 79, NULL, NULL, 'COCODY, Plateau Dokui', '2020-01-24 23:55:23', NULL, NULL, NULL, NULL, NULL, b'0'),
	(11, 13, 235, 86, NULL, NULL, 'test', '2020-01-25 20:23:40', NULL, NULL, NULL, NULL, NULL, b'0'),
	(12, 14, 236, 86, NULL, NULL, 'test', '2020-01-25 20:35:10', NULL, NULL, NULL, NULL, NULL, b'0'),
	(13, 15, 231, 86, NULL, NULL, 'test', '2020-01-25 22:01:59', NULL, NULL, NULL, NULL, NULL, b'0'),
	(14, 18, 231, 86, NULL, NULL, 'test', '2020-03-14 16:56:12', NULL, NULL, NULL, NULL, NULL, b'0'),
	(15, 30, 39, 79, 9, 7, '7éme tranche', '2020-04-04 18:16:04', NULL, NULL, NULL, NULL, NULL, b'0'),
	(16, 38, 39, 79, 19, 17, 'ananeraie', '2020-04-05 00:20:25', NULL, NULL, NULL, NULL, NULL, b'0'),
	(17, 39, 39, 79, 19, 17, 'ananeraie', '2020-04-05 09:55:33', NULL, NULL, NULL, NULL, NULL, b'0'),
	(18, 40, 39, 79, 9, 7, 'Angré, 7ème Tranche', '2020-04-11 17:49:03', NULL, NULL, NULL, NULL, NULL, b'0');
/*!40000 ALTER TABLE `adresse` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. commune
DROP TABLE IF EXISTS `commune`;
CREATE TABLE IF NOT EXISTS `commune` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `superficie` double DEFAULT NULL,
  `id_ville` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `id_pays` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_commune_ville` (`id_ville`),
  KEY `FK_pays` (`id_pays`),
  CONSTRAINT `FK_commune_ville` FOREIGN KEY (`id_ville`) REFERENCES `ville` (`id`),
  CONSTRAINT `FK_pays` FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.commune : ~3 rows (environ)
/*!40000 ALTER TABLE `commune` DISABLE KEYS */;
INSERT INTO `commune` (`id`, `code`, `libelle`, `longitude`, `latitude`, `superficie`, `id_ville`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`, `id_pays`) VALUES
	(9, NULL, 'Cocody', NULL, NULL, NULL, 79, '2020-04-04 18:16:04', NULL, NULL, 1, NULL, NULL, b'0', 39),
	(19, NULL, 'yopougon', NULL, NULL, NULL, 79, '2020-04-05 00:20:24', NULL, NULL, 1, NULL, NULL, b'0', 39);
/*!40000 ALTER TABLE `commune` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. departement
DROP TABLE IF EXISTS `departement`;
CREATE TABLE IF NOT EXISTS `departement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) CHARACTER SET utf8 NOT NULL,
  `id_region` int(11) NOT NULL,
  `chef_lieu` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_region` (`id_region`),
  CONSTRAINT `FK_region_departement` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=big5 COMMENT='la listes des departements de la Côte d''Ivoire';

-- Export de données de la table bd_ge.departement : ~0 rows (environ)
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. engagement
DROP TABLE IF EXISTS `engagement`;
CREATE TABLE IF NOT EXISTS `engagement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_projet` int(11) unsigned DEFAULT NULL,
  `id_user` int(11) unsigned DEFAULT NULL,
  `date_engagement` datetime DEFAULT NULL,
  `montant_engagement` int(11) DEFAULT NULL,
  `montant_restant_a_payer` int(11) unsigned DEFAULT NULL,
  `is_settle` bit(1) DEFAULT NULL,
  `settle_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_projet` (`id_projet`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `engagement_ibfk_1` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id`),
  CONSTRAINT `engagement_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.engagement : ~16 rows (environ)
/*!40000 ALTER TABLE `engagement` DISABLE KEYS */;
INSERT INTO `engagement` (`id`, `id_projet`, `id_user`, `date_engagement`, `montant_engagement`, `montant_restant_a_payer`, `is_settle`, `settle_at`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(5, 1, 1, '2019-10-12 00:00:00', 1000000, 1000000, b'1', '2019-10-12 00:00:00', '2020-01-26 23:28:58', '2020-01-26 23:30:10', NULL, 1, 1, NULL, b'0'),
	(6, 1, 1, '2019-11-01 00:00:00', 1500000, 1500000, b'1', '2019-11-01 00:00:00', '2020-01-26 23:28:59', NULL, NULL, 1, NULL, NULL, b'0'),
	(7, 1, 1, '2019-08-01 00:00:00', 8000000, 8000000, b'1', '2019-08-01 00:00:00', '2020-01-26 23:28:59', NULL, NULL, 1, NULL, NULL, b'0'),
	(8, 1, 1, '2019-07-01 00:00:00', 7000000, 7000000, b'1', '2019-07-01 00:00:00', '2020-01-26 23:29:00', NULL, '2020-01-26 23:30:43', 1, NULL, 1, b'0'),
	(9, 1, 1, '2019-10-12 00:00:00', 500000, 500000, b'1', '2019-10-12 00:00:00', '2020-02-04 10:01:08', NULL, NULL, 1, NULL, NULL, b'0'),
	(10, 1, 1, '2019-11-01 00:00:00', 900000, 900000, b'1', '2019-11-01 00:00:00', '2020-02-04 10:01:09', NULL, NULL, 1, NULL, NULL, b'0'),
	(11, 1, 1, '2019-08-01 00:00:00', 8500000, 8500000, b'1', '2019-08-01 00:00:00', '2020-02-04 10:01:10', NULL, NULL, 1, NULL, NULL, b'0'),
	(12, 1, 1, '2019-07-01 00:00:00', 170000, 170000, b'1', '2019-07-01 00:00:00', '2020-02-04 10:01:11', NULL, NULL, 1, NULL, NULL, b'0'),
	(13, 1, 2, '2019-10-12 00:00:00', 500000, 500000, b'1', '2019-10-12 00:00:00', '2020-02-13 17:05:10', NULL, NULL, 2, NULL, NULL, b'0'),
	(14, 1, 2, '2019-11-01 00:00:00', 350000, 350000, b'1', '2019-11-01 00:00:00', '2020-02-13 17:05:12', NULL, NULL, 2, NULL, NULL, b'0'),
	(15, 1, 2, '2019-08-01 00:00:00', 125000, 125000, b'1', '2019-08-01 00:00:00', '2020-02-13 17:05:13', NULL, NULL, 2, NULL, NULL, b'0'),
	(16, 1, 2, '2019-07-01 00:00:00', 550000, 550000, b'1', '2019-07-01 00:00:00', '2020-02-13 17:05:13', NULL, NULL, 2, NULL, NULL, b'0'),
	(17, 1, 2, '2020-03-12 00:00:00', 50000, 50000, b'1', '2019-10-12 00:00:00', '2020-03-17 16:13:17', NULL, NULL, 2, NULL, NULL, b'0'),
	(18, 1, 2, '2020-03-17 00:00:00', 475000, 475000, b'1', '2020-09-01 00:00:00', '2020-03-17 16:13:17', NULL, NULL, 2, NULL, NULL, b'0'),
	(19, 1, 2, '2020-03-05 00:00:00', 150000, 150000, b'1', '2020-07-15 00:00:00', '2020-03-17 16:13:17', NULL, NULL, 2, NULL, NULL, b'0'),
	(20, 1, 2, '2020-03-01 00:00:00', 700000, 700000, b'1', '2020-06-11 00:00:00', '2020-03-17 16:13:18', NULL, NULL, 2, NULL, NULL, b'0');
/*!40000 ALTER TABLE `engagement` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. fonction
DROP TABLE IF EXISTS `fonction`;
CREATE TABLE IF NOT EXISTS `fonction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.fonction : ~9 rows (environ)
/*!40000 ALTER TABLE `fonction` DISABLE KEYS */;
INSERT INTO `fonction` (`id`, `libelle`, `code`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'Responsable', NULL, '2020-01-23 11:17:40', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'Trésorière', NULL, '2020-01-23 11:17:41', NULL, NULL, 1, NULL, NULL, b'0'),
	(3, 'Intercesseux(se)', NULL, '2020-01-23 11:17:41', NULL, NULL, 1, NULL, NULL, b'0'),
	(4, 'Commissaire au compte', NULL, '2020-01-23 11:28:47', NULL, '2020-01-23 11:34:47', 1, NULL, 1, b'1'),
	(5, 'Membre', NULL, '2020-01-23 11:28:48', '2020-01-23 11:33:34', NULL, 1, 1, NULL, b'0'),
	(6, 'Vice-Président', NULL, '2020-01-23 11:28:48', NULL, NULL, 1, NULL, NULL, b'0'),
	(7, 'Président', NULL, '2020-04-11 14:16:01', NULL, NULL, NULL, NULL, NULL, b'0'),
	(8, 'Sécrétaire', NULL, '2020-04-11 14:17:54', NULL, NULL, NULL, NULL, NULL, b'0'),
	(9, 'Sécrétaire Adjoint', NULL, '2020-04-11 14:18:54', NULL, NULL, NULL, NULL, NULL, b'0');
/*!40000 ALTER TABLE `fonction` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. functionality
DROP TABLE IF EXISTS `functionality`;
CREATE TABLE IF NOT EXISTS `functionality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.functionality : ~2 rows (environ)
/*!40000 ALTER TABLE `functionality` DISABLE KEYS */;
INSERT INTO `functionality` (`id`, `code`, `libelle`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'ALL', 'ALL', '2020-01-23 14:30:40', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'VIEW_DATA', 'VIEW_DATA', '2020-01-23 14:30:40', '2020-01-23 14:31:28', NULL, 1, 1, NULL, b'0'),
	(3, 'CREATE_USER', 'CREATE_USER', '2020-01-23 14:32:08', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `functionality` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. groupe_constitue
DROP TABLE IF EXISTS `groupe_constitue`;
CREATE TABLE IF NOT EXISTS `groupe_constitue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.groupe_constitue : ~18 rows (environ)
/*!40000 ALTER TABLE `groupe_constitue` DISABLE KEYS */;
INSERT INTO `groupe_constitue` (`id`, `code`, `libelle`, `description`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'SDB', 'Servantes de Bethanie(Femme)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(2, 'GDH', 'Groupe des Hommes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(3, 'DEM', 'Disciples d\'Emmaüs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(4, 'ECODIM', 'Ecole du Dimanche', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(5, 'GMU', 'Groupe Musical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(6, 'CHRL', 'Chorale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(7, 'CODEC', 'Comité de Développement', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(8, 'ASO', 'Action Sociale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(9, 'DME', 'Département Mission et Evangélisation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(10, 'FLL', 'Flambeau & Lumière', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(11, 'ACI', 'Accueil et Intégration', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(12, 'CELCOM', 'Cellule de Communication', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(13, 'CODOF', 'Comité des dîmes et des offrandes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(14, 'GMED', 'Groupe des médecins', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(15, 'GHN', 'Groupes d\'Hygène et de nettoyage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(16, 'COPAS', 'Corps Pastorale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(17, 'CANC', 'Conseil et Anciens', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(18, 'SEC', 'Secrétariat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(19, 'AUT', 'Autres...', NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0');
/*!40000 ALTER TABLE `groupe_constitue` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. member_status
DROP TABLE IF EXISTS `member_status`;
CREATE TABLE IF NOT EXISTS `member_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.member_status : ~3 rows (environ)
/*!40000 ALTER TABLE `member_status` DISABLE KEYS */;
INSERT INTO `member_status` (`id`, `code`, `libelle`, `comment`) VALUES
	(1, 'old_member', 'Ancien(ne) Membre', 'Vous n\'êtes plus dans la communauté UEESO de Cocody'),
	(2, 'cur_member', 'Membre Actuel(le)', 'Vous féquentez la communauté UEESO de Cocody actuellement'),
	(3, 'hon_member', 'Membre Honoraire', 'Vous êtes membre de la communauté UEESO mais résidez à l\'étranger');
/*!40000 ALTER TABLE `member_status` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. mission
DROP TABLE IF EXISTS `mission`;
CREATE TABLE IF NOT EXISTS `mission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `description` text,
  `adresse` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.mission : ~1 rows (environ)
/*!40000 ALTER TABLE `mission` DISABLE KEYS */;
INSERT INTO `mission` (`id`, `code`, `libelle`, `description`, `adresse`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'Don', 'Don dans aux démunis', 'Don dans un quartier défavorisé', 'Port-bouet', '2020-04-05 01:04:37', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `mission` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. paiement
DROP TABLE IF EXISTS `paiement`;
CREATE TABLE IF NOT EXISTS `paiement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `montant` int(11) DEFAULT NULL,
  `id_engagement` int(11) unsigned DEFAULT NULL,
  `id_tresorier` int(11) unsigned DEFAULT NULL,
  `id_projet` int(11) unsigned DEFAULT NULL,
  `date_paiement` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_eng` (`id_engagement`),
  KEY `id_tresorier` (`id_tresorier`),
  KEY `montant` (`montant`),
  KEY `fk_projet` (`id_projet`),
  CONSTRAINT `fk_projet` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `paiement_ibfk_1` FOREIGN KEY (`id_engagement`) REFERENCES `engagement` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `paiement_ibfk_3` FOREIGN KEY (`id_tresorier`) REFERENCES `tresorier` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.paiement : ~12 rows (environ)
/*!40000 ALTER TABLE `paiement` DISABLE KEYS */;
INSERT INTO `paiement` (`id`, `montant`, `id_engagement`, `id_tresorier`, `id_projet`, `date_paiement`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(15, 10000, 5, 1, NULL, NULL, '2020-02-06 10:55:13', NULL, NULL, 1, NULL, NULL, b'0'),
	(16, 143000, 6, 1, NULL, '2020-02-11', '2020-02-06 10:55:14', NULL, NULL, 1, NULL, NULL, b'0'),
	(17, 500000, NULL, 2, 1, NULL, '2020-02-06 10:55:15', NULL, NULL, 1, NULL, NULL, b'0'),
	(33, 13000, 5, 1, NULL, '2020-02-09', '2020-02-13 15:57:50', NULL, NULL, 1, NULL, NULL, b'0'),
	(34, 14000, 6, 1, NULL, '2020-02-11', '2020-02-13 15:57:51', NULL, NULL, 1, NULL, NULL, b'0'),
	(35, 50000, NULL, 2, 1, '2020-02-13', '2020-02-13 15:57:53', NULL, NULL, 1, NULL, NULL, b'0'),
	(36, 2300, 14, 1, NULL, '2020-01-11', '2020-02-13 17:07:24', NULL, NULL, 1, NULL, NULL, b'0'),
	(37, 7600, 15, 1, NULL, '2020-01-31', '2020-02-13 17:07:25', NULL, NULL, 1, NULL, NULL, b'0'),
	(38, 4500, 16, 2, NULL, '2020-01-12', '2020-02-13 17:07:31', NULL, NULL, 1, NULL, NULL, b'0'),
	(41, 78500, 6, 1, NULL, '2020-03-09', '2020-03-17 15:50:06', NULL, NULL, 1, NULL, NULL, b'0'),
	(42, 14500, 6, 1, NULL, '2020-03-11', '2020-03-17 15:50:07', NULL, NULL, 1, NULL, NULL, b'0'),
	(43, 1970, 6, 1, NULL, '2020-03-13', '2020-03-17 15:50:08', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `paiement` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. pays
DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.pays : ~237 rows (environ)
/*!40000 ALTER TABLE `pays` DISABLE KEYS */;
INSERT INTO `pays` (`id`, `code`, `libelle`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`, `is_deleted`) VALUES
	(1, 'AF', 'Afghanistan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(2, 'ZA', 'Afrique du Sud', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(3, 'AL', 'Albanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(4, 'DZ', 'Algérie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(5, 'DE', 'Allemagne', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(6, 'AD', 'Andorre', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(7, 'AO', 'Angola', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(8, 'AI', 'Anguilla', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(9, 'AQ', 'Antarctique', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(10, 'AG', 'Antigua-et-Barbuda', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(11, 'AN', 'Antilles néerlandaises', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(12, 'SA', 'Arabie saoudite', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(13, 'AR', 'Argentine', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(14, 'AM', 'Arménie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(15, 'AW', 'Aruba', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(16, 'AU', 'Australie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(17, 'AT', 'Autriche', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(18, 'AZ', 'Azerbaïdjan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(19, 'BJ', 'Bénin', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(20, 'BS', 'Bahamas', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(21, 'BH', 'Bahreïn', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(22, 'BD', 'Bangladesh', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(23, 'BB', 'Barbade', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(24, 'PW', 'Belau', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(25, 'BE', 'Belgique', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(26, 'BZ', 'Belize', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(27, 'BM', 'Bermudes', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(28, 'BT', 'Bhoutan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(29, 'BY', 'Biélorussie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(30, 'MM', 'Birmanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(31, 'BO', 'Bolivie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(32, 'BA', 'Bosnie-Herzégovine', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(33, 'BW', 'Botswana', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(34, 'BR', 'Brésil', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(35, 'BN', 'Brunei', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(36, 'BG', 'Bulgarie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(37, 'BF', 'Burkina Faso', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(38, 'BI', 'Burundi', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(39, 'CI', 'Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(40, 'KH', 'Cambodge', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(41, 'CM', 'Cameroun', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(42, 'CA', 'Canada', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(43, 'CV', 'Cap-Vert', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(44, 'CL', 'Chili', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(45, 'CN', 'Chine', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(46, 'CY', 'Chypre', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(47, 'CO', 'Colombie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(48, 'KM', 'Comores', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(49, 'CG', 'Congo', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(50, 'KP', 'Corée du Nord', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(51, 'KR', 'Corée du Sud', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(52, 'CR', 'Costa Rica', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(53, 'HR', 'Croatie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(54, 'CU', 'Cuba', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(55, 'DK', 'Danemark', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(56, 'DJ', 'Djibouti', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(57, 'DM', 'Dominique', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(58, 'EG', 'Égypte', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(59, 'AE', 'Émirats arabes unis', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(60, 'EC', 'Équateur', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(61, 'ER', 'Érythrée', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(62, 'ES', 'Espagne', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(63, 'EE', 'Estonie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(64, 'US', 'États-Unis', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(65, 'ET', 'Éthiopie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(66, 'FI', 'Finlande', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(67, 'FR', 'France', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(68, 'GE', 'Géorgie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(69, 'GA', 'Gabon', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(70, 'GM', 'Gambie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(71, 'GH', 'Ghana', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(72, 'GI', 'Gibraltar', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(73, 'GR', 'Grèce', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(74, 'GD', 'Grenade', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(75, 'GL', 'Groenland', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(76, 'GP', 'Guadeloupe', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(77, 'GU', 'Guam', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(78, 'GT', 'Guatemala', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(79, 'GN', 'Guinée', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(80, 'GQ', 'Guinée équatoriale', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(81, 'GW', 'Guinée-Bissao', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(82, 'GY', 'Guyana', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(83, 'GF', 'Guyane française', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(84, 'HT', 'Haïti', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(85, 'HN', 'Honduras', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(86, 'HK', 'Hong Kong', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(87, 'HU', 'Hongrie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(88, 'BV', 'Ile Bouvet', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(89, 'CX', 'Ile Christmas', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(90, 'NF', 'Ile Norfolk', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(91, 'KY', 'Iles Cayman', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(92, 'CK', 'Iles Cook', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(93, 'FO', 'Iles Féroé', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(94, 'FK', 'Iles Falkland', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(95, 'FJ', 'Iles Fidji', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(96, 'GS', 'Iles Géorgie du Sud et Sandwich du Sud', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(97, 'HM', 'Iles Heard et McDonald', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(98, 'MH', 'Iles Marshall', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(99, 'PN', 'Iles Pitcairn', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(100, 'SB', 'Iles Salomon', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(101, 'SJ', 'Iles Svalbard et Jan Mayen', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(102, 'TC', 'Iles Turks-et-Caicos', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(103, 'VI', 'Iles Vierges américaines', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(104, 'VG', 'Iles Vierges britanniques', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(105, 'CC', 'Iles des Cocos (Keeling)', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(106, 'UM', 'Iles mineures éloignées des États-Unis', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(107, 'IN', 'Inde', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(108, 'ID', 'Indonésie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(109, 'IR', 'Iran', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(110, 'IQ', 'Iraq', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(111, 'IE', 'Irlande', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(112, 'IS', 'Islande', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(113, 'IL', 'Israël', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(114, 'IT', 'Italie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(115, 'JM', 'Jamaïque', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(116, 'JP', 'Japon', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(117, 'JO', 'Jordanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(118, 'KZ', 'Kazakhstan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(119, 'KE', 'Kenya', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(120, 'KG', 'Kirghizistan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(121, 'KI', 'Kiribati', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(122, 'KW', 'Koweït', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(123, 'LA', 'Laos', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(124, 'LS', 'Lesotho', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(125, 'LV', 'Lettonie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(126, 'LB', 'Liban', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(127, 'LR', 'Liberia', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(128, 'LY', 'Libye', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(129, 'LI', 'Liechtenstein', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(130, 'LT', 'Lituanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(131, 'LU', 'Luxembourg', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(132, 'MO', 'Macao', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(133, 'MG', 'Madagascar', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(134, 'MY', 'Malaisie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(135, 'MW', 'Malawi', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(136, 'MV', 'Maldives', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(137, 'ML', 'Mali', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(138, 'MT', 'Malte', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(139, 'MP', 'Mariannes du Nord', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(140, 'MA', 'Maroc', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(141, 'MQ', 'Martinique', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(142, 'MU', 'Maurice', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(143, 'MR', 'Mauritanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(144, 'YT', 'Mayotte', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(145, 'MX', 'Mexique', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(146, 'FM', 'Micronésie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(147, 'MD', 'Moldavie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(148, 'MC', 'Monaco', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(149, 'MN', 'Mongolie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(150, 'MS', 'Montserrat', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(151, 'MZ', 'Mozambique', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(152, 'NP', 'Népal', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(153, 'NA', 'Namibie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(154, 'NR', 'Nauru', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(155, 'NI', 'Nicaragua', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(156, 'NE', 'Niger', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(157, 'NG', 'Nigeria', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(158, 'NU', 'Nioué', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(159, 'NO', 'Norvège', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(160, 'NC', 'Nouvelle-Calédonie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(161, 'NZ', 'Nouvelle-Zélande', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(162, 'OM', 'Oman', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(163, 'UG', 'Ouganda', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(164, 'UZ', 'Ouzbékistan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(165, 'PE', 'Pérou', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(166, 'PK', 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(167, 'PA', 'Panama', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(168, 'PG', 'Papouasie-Nouvelle-Guinée', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(169, 'PY', 'Paraguay', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(170, 'NL', 'Pays-Bas', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(171, 'PH', 'Philippines', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(172, 'PL', 'Pologne', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(173, 'PF', 'Polynésie française', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(174, 'PR', 'Porto Rico', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(175, 'PT', 'Portugal', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(176, 'QA', 'Qatar', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(177, 'CF', 'République centrafricaine', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(178, 'CD', 'République démocratique du Congo', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(179, 'DO', 'République dominicaine', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(180, 'CZ', 'République tchèque', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(181, 'RE', 'Réunion', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(182, 'RO', 'Roumanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(183, 'GB', 'Royaume-Uni', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(184, 'RU', 'Russie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(185, 'RW', 'Rwanda', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(186, 'SN', 'Sénégal', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(187, 'EH', 'Sahara occidental', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(188, 'KN', 'Saint-Christophe-et-Niévès', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(189, 'SM', 'Saint-Marin', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(190, 'PM', 'Saint-Pierre-et-Miquelon', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(191, 'VA', 'Saint-Siège', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(192, 'VC', 'Saint-Vincent-et-les-Grenadines', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(193, 'SH', 'Sainte-Hélène', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(194, 'LC', 'Sainte-Lucie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(195, 'SV', 'Salvador', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(196, 'WS', 'Samoa', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(197, 'AS', 'Samoa américaines', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(198, 'ST', 'Sao Tomé-et-Principe', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(199, 'SC', 'Seychelles', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(200, 'SL', 'Sierra Leone', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(201, 'SG', 'Singapour', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(202, 'SI', 'Slovénie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(203, 'SK', 'Slovaquie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(204, 'SO', 'Somalie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(205, 'SD', 'Soudan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(206, 'LK', 'Sri Lanka', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(207, 'SE', 'Suède', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(208, 'CH', 'Suisse', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(209, 'SR', 'Suriname', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(210, 'SZ', 'Swaziland', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(211, 'SY', 'Syrie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(212, 'TW', 'Taïwan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(213, 'TJ', 'Tadjikistan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(214, 'TZ', 'Tanzanie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(215, 'TD', 'Tchad', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(216, 'TF', 'Terres australes françaises', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(217, 'IO', 'Territoire britannique de l\'Océan Indien', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(218, 'TH', 'Thaïlande', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(219, 'TL', 'Timor Oriental', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(220, 'TG', 'Togo', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(221, 'TK', 'Tokélaou', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(222, 'TO', 'Tonga', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(223, 'TT', 'Trinité-et-Tobago', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(224, 'TN', 'Tunisie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(225, 'TM', 'Turkménistan', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(226, 'TR', 'Turquie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(227, 'TV', 'Tuvalu', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(228, 'UA', 'Ukraine', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(229, 'UY', 'Uruguay', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(230, 'VU', 'Vanuatu', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(231, 'VE', 'Venezuela', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(232, 'VN', 'Viêt Nam', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(233, 'WF', 'Wallis-et-Futuna', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(234, 'YE', 'Yémen', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(235, 'YU', 'Yougoslavie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(236, 'ZM', 'Zambie', NULL, NULL, NULL, NULL, NULL, NULL, b'0'),
	(237, 'ZW', 'Zimbabwe', NULL, NULL, NULL, NULL, NULL, NULL, b'0');
/*!40000 ALTER TABLE `pays` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. periode
DROP TABLE IF EXISTS `periode`;
CREATE TABLE IF NOT EXISTS `periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paiement` int(11) unsigned DEFAULT NULL,
  `day_ofweek` int(11) unsigned DEFAULT NULL,
  `day_ofmonth` int(11) unsigned DEFAULT NULL,
  `day_ofyear` int(11) unsigned DEFAULT NULL,
  `week_ofmonth` int(11) unsigned DEFAULT NULL,
  `week_ofyear` int(11) unsigned DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `trimester` int(11) unsigned DEFAULT NULL,
  `semester` int(11) unsigned DEFAULT NULL,
  `year` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_paiement` (`id_paiement`),
  CONSTRAINT `fk_paiement` FOREIGN KEY (`id_paiement`) REFERENCES `paiement` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.periode : ~12 rows (environ)
/*!40000 ALTER TABLE `periode` DISABLE KEYS */;
INSERT INTO `periode` (`id`, `id_paiement`, `day_ofweek`, `day_ofmonth`, `day_ofyear`, `week_ofmonth`, `week_ofyear`, `month`, `trimester`, `semester`, `year`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(7, 15, 3, 12, 316, 2, 46, 11, 4, 2, 2019, '2020-02-06 10:55:13', NULL, NULL, NULL, NULL, NULL, b'0'),
	(8, 16, 4, 15, 15, 3, 3, 1, 1, 1, 2020, '2020-02-06 10:55:14', NULL, NULL, NULL, NULL, NULL, b'0'),
	(9, 17, 7, 25, 25, 4, 4, 1, 1, 1, 2020, '2020-02-06 10:55:16', NULL, NULL, NULL, NULL, NULL, b'0'),
	(25, 33, 1, 9, 40, 1, 6, 2, 1, 1, 2020, '2020-02-13 15:57:51', NULL, NULL, NULL, NULL, NULL, b'0'),
	(26, 34, 3, 11, 42, 2, 7, 2, 1, 1, 2020, '2020-02-13 15:57:52', NULL, NULL, NULL, NULL, NULL, b'0'),
	(27, 35, 5, 13, 44, 2, 7, 2, 1, 1, 2020, '2020-02-13 15:57:54', NULL, NULL, NULL, NULL, NULL, b'0'),
	(28, 36, 7, 11, 11, 2, 2, 1, 1, 1, 2020, '2020-02-13 17:07:25', NULL, NULL, NULL, NULL, NULL, b'0'),
	(29, 37, 6, 31, 31, 5, 5, 1, 1, 1, 2020, '2020-02-13 17:07:26', NULL, NULL, NULL, NULL, NULL, b'0'),
	(30, 38, 1, 12, 12, 2, 2, 1, 1, 1, 2020, '2020-02-13 17:07:31', NULL, NULL, NULL, NULL, NULL, b'0'),
	(31, 41, 2, 9, 69, 2, 11, 3, 1, 1, 2020, '2020-03-17 15:50:07', NULL, NULL, NULL, NULL, NULL, b'0'),
	(32, 42, 4, 11, 71, 2, 11, 3, 1, 1, 2020, '2020-03-17 15:50:08', NULL, NULL, NULL, NULL, NULL, b'0'),
	(33, 43, 6, 13, 73, 2, 11, 3, 1, 1, 2020, '2020-03-17 15:50:09', NULL, NULL, NULL, NULL, NULL, b'0');
/*!40000 ALTER TABLE `periode` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. profession
DROP TABLE IF EXISTS `profession`;
CREATE TABLE IF NOT EXISTS `profession` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `is_mission_related` bit(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.profession : ~12 rows (environ)
/*!40000 ALTER TABLE `profession` DISABLE KEYS */;
INSERT INTO `profession` (`id`, `code`, `libelle`, `is_mission_related`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'INFO', 'Informaticien', b'0', '2020-01-24 21:56:32', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'ETUDT', 'Etudiant', b'0', '2020-01-24 21:56:20', NULL, NULL, 1, NULL, NULL, b'0'),
	(3, 'ELVE', 'Elève', b'0', '2020-01-24 21:56:49', NULL, NULL, 1, NULL, NULL, b'0'),
	(4, 'MISSIONNAIRE', 'Missionnaire', b'1', '2020-03-14 20:10:01', NULL, NULL, 1, NULL, NULL, b'0'),
	(5, 'TCF', 'Thérapeute conjugal et familial', b'0', '2020-04-11 14:35:26', NULL, NULL, 1, NULL, NULL, b'0'),
	(6, 'AVT', 'Avocat', b'0', '2020-04-11 14:37:04', NULL, NULL, 1, NULL, NULL, b'0'),
	(7, 'HJ', 'Huissier de justice', b'0', '2020-04-11 14:38:40', NULL, NULL, 1, NULL, NULL, b'0'),
	(8, 'MED', 'Médecin', b'0', '2020-04-11 14:39:24', NULL, NULL, 1, NULL, NULL, b'0'),
	(9, 'CO', 'Conseiller(ère) d\'Orientation ', b'0', '2020-04-11 14:41:01', NULL, NULL, 1, NULL, NULL, b'0'),
	(10, 'ENS', 'Enseignant(e)', b'0', '2020-04-11 14:42:18', NULL, NULL, 1, NULL, NULL, b'0'),
	(11, 'ENSC', 'Enseignant(e) Chercheur', b'0', '2020-04-11 14:43:39', NULL, NULL, 1, NULL, NULL, b'0'),
	(12, 'INST', 'Instituteur(trice)', b'0', '2020-04-11 14:46:15', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `profession` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. projet
DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `description` text,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `cout` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.projet : ~2 rows (environ)
/*!40000 ALTER TABLE `projet` DISABLE KEYS */;
INSERT INTO `projet` (`id`, `code`, `libelle`, `description`, `start_date`, `end_date`, `cout`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'INVEST2019', 'Projet Investissement 2019', 'TEST', '2019-07-01', '2019-12-31', 48000000, '2020-01-26 22:59:15', '2020-01-26 23:02:20', NULL, 1, 1, NULL, b'0'),
	(2, 'CODEC', 'CODEC', '', '2019-07-01', '2019-12-31', 10000000, '2020-01-26 23:01:00', NULL, '2020-01-26 23:02:45', 1, NULL, 1, b'1');
/*!40000 ALTER TABLE `projet` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. quartier
DROP TABLE IF EXISTS `quartier`;
CREATE TABLE IF NOT EXISTS `quartier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `superficie` double DEFAULT NULL,
  `id_commune` int(11) DEFAULT NULL,
  `id_village` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quartier_ibfk_2` (`id_village`),
  KEY `FK_quartier_commune` (`id_commune`),
  CONSTRAINT `FK_quartier_commune` FOREIGN KEY (`id_commune`) REFERENCES `commune` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `quartier_ibfk_2` FOREIGN KEY (`id_village`) REFERENCES `village` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.quartier : ~3 rows (environ)
/*!40000 ALTER TABLE `quartier` DISABLE KEYS */;
INSERT INTO `quartier` (`id`, `code`, `libelle`, `longitude`, `latitude`, `superficie`, `id_commune`, `id_village`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(7, '2Plateaux', '2 plateaux', NULL, NULL, NULL, 9, NULL, '2020-04-04 18:16:04', NULL, NULL, 1, NULL, NULL, b'0'),
	(17, 'Ananeraie', 'ananeraie', NULL, NULL, NULL, 19, NULL, '2020-04-05 00:20:24', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `quartier` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. region
DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) CHARACTER SET utf8 NOT NULL,
  `chef_lieu` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=big5 COMMENT='la liste des regions de la Côte d''Ivoire';

-- Export de données de la table bd_ge.region : ~0 rows (environ)
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `is_default_param` bit(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.role : ~3 rows (environ)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `code`, `libelle`, `is_default_param`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'SUPER_ADMIN', 'SUPER_ADMIN', b'1', '2020-01-23 13:00:04', NULL, '2020-01-23 13:04:13', 1, NULL, 1, b'0'),
	(2, 'FDLE', 'fidèle', b'0', '2020-01-23 13:00:04', '2020-01-25 22:02:20', NULL, 1, 1, NULL, b'0'),
	(3, 'TEST', 'TEST', b'0', '2020-01-23 13:01:29', NULL, '2020-01-23 13:01:58', 1, NULL, 1, b'0');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. role_functionality
DROP TABLE IF EXISTS `role_functionality`;
CREATE TABLE IF NOT EXISTS `role_functionality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) DEFAULT NULL,
  `id_fonctionality` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__role` (`id_role`),
  KEY `FK__functionality` (`id_fonctionality`),
  CONSTRAINT `FK__functionality` FOREIGN KEY (`id_fonctionality`) REFERENCES `functionality` (`id`),
  CONSTRAINT `FK__role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.role_functionality : ~2 rows (environ)
/*!40000 ALTER TABLE `role_functionality` DISABLE KEYS */;
INSERT INTO `role_functionality` (`id`, `id_role`, `id_fonctionality`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 1, 1, '2020-01-23 15:06:07', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 1, 2, '2020-01-23 15:06:09', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `role_functionality` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. situation_matrimoniale
DROP TABLE IF EXISTS `situation_matrimoniale`;
CREATE TABLE IF NOT EXISTS `situation_matrimoniale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `has_wedded_life` bit(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.situation_matrimoniale : ~6 rows (environ)
/*!40000 ALTER TABLE `situation_matrimoniale` DISABLE KEYS */;
INSERT INTO `situation_matrimoniale` (`id`, `code`, `libelle`, `has_wedded_life`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'CELBTRE', 'Célibataire', b'0', '2019-12-02 02:03:46', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'FIANCE', 'Fiancé(e)', b'1', '2019-12-02 02:03:46', NULL, NULL, 1, NULL, NULL, b'0'),
	(3, 'MARIE', 'Marié(e)', b'1', '2019-12-02 02:03:46', NULL, NULL, 1, NULL, NULL, b'0'),
	(4, 'CONCUBIN', 'Concubin(e)', b'1', '2019-12-02 02:03:46', NULL, NULL, 1, NULL, NULL, b'0'),
	(5, 'DIVORCE', 'Divorcé(e)', b'0', '2019-12-02 02:03:46', NULL, NULL, 1, NULL, NULL, b'0'),
	(6, 'VEUF', 'Veuf/Veuve', b'0', '2019-12-02 02:03:46', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `situation_matrimoniale` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. situation_professionnelle
DROP TABLE IF EXISTS `situation_professionnelle`;
CREATE TABLE IF NOT EXISTS `situation_professionnelle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libele` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.situation_professionnelle : ~4 rows (environ)
/*!40000 ALTER TABLE `situation_professionnelle` DISABLE KEYS */;
INSERT INTO `situation_professionnelle` (`id`, `code`, `libelle`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'TRA', 'Travailleur', '2020-01-24 21:50:28', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'RETRT', 'Retraité', '2020-01-24 21:53:38', NULL, NULL, 1, NULL, NULL, b'0'),
	(3, 'CHMGE', 'Chaumage', '2020-01-24 21:54:34', NULL, NULL, 1, NULL, NULL, b'0'),
	(4, 'ETUDT', 'Etudiant', '2020-01-24 21:55:05', NULL, NULL, 1, NULL, NULL, b'0'),
	(5, 'ELVE', 'Elève', '2020-01-24 21:55:24', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `situation_professionnelle` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. smtp_server
DROP TABLE IF EXISTS `smtp_server`;
CREATE TABLE IF NOT EXISTS `smtp_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `smtp_user` varchar(255) DEFAULT NULL,
  `auth` bit(1) DEFAULT NULL COMMENT 'authentifier l''utilisateur',
  `enable_ssl` bit(1) DEFAULT NULL COMMENT 'utilisez SSL pour vous connecter et utiliser le port SSL par défaut. La valeur par défaut est false pour le protocole "smtp" et true pour le protocole "smtps"',
  `ssl_protocols` text COMMENT 'Spécifie les protocoles SSL qui seront activés pour les connexions SSL (Ex: "SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2")',
  `enable_starttls` bit(1) DEFAULT NULL COMMENT 'permet l''utilisation de la commande STARTTLS (si elle est prise en charge par le serveur) pour basculer la connexion vers une connexion protégée par TLS avant d''émettre des commandes de connexion. Notez qu''un magasin de confiance approprié doit être configuré pour que le client fasse confiance au certificat du serveur',
  `is_starttls_required` bit(1) DEFAULT NULL COMMENT 'Utiliser forcément la commande STARTTLS. Si le serveur ne supporte pas cette commande ou si elle echoue, la connection echouera',
  `hosts_to_trust` text COMMENT 'Ensemble de host qui faire confiance',
  `is_smtps_protocol` bit(1) DEFAULT NULL COMMENT 'Utiliser le protocole SMTPS',
  `is_active` bit(1) DEFAULT NULL COMMENT 'Serveur a utiliser pour les envoi de mail',
  `sender_username` varchar(255) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `test_email` varchar(255) DEFAULT NULL COMMENT 'Adresse email sur lequel un email de test sera envoye apres chaque modifcation pour s''assurer que les parametres du serveur conviennent',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.smtp_server : ~4 rows (environ)
/*!40000 ALTER TABLE `smtp_server` DISABLE KEYS */;
INSERT INTO `smtp_server` (`id`, `description`, `host`, `login`, `password`, `port`, `smtp_user`, `auth`, `enable_ssl`, `ssl_protocols`, `enable_starttls`, `is_starttls_required`, `hosts_to_trust`, `is_smtps_protocol`, `is_active`, `sender_username`, `sender_email`, `test_email`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'Serveur de messagerie de google', 'smtp.gmail.com', 'smile.backend@gmail.com', 'smile#2017', 587, NULL, b'1', b'0', '', b'1', b'1', '*', b'0', b'0', NULL, 'smile.backend@gmail.com', 'smile.backend@gmail.com', '2018-11-13 11:52:26', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'Serveur de messagerie de google', 'smtp.gmail.com', 'smartt.backend@gmail.com', 'smartt#2018', 587, NULL, b'1', b'0', NULL, b'1', b'1', '*', b'0', b'0', NULL, 'smartt.backend@gmail.com', 'smartt.backend@gmail.com', '2018-11-13 11:52:26', NULL, NULL, 1, NULL, NULL, b'0'),
	(3, 'Serveur de messagerie de google', 'smtp.gmail.com', 'backend.email.sender@gmail.com', 'emailsender#2020', 587, NULL, b'1', b'0', NULL, b'1', b'1', '*', b'0', b'0', 'Backend Email Sender', 'backend.email.sender@gmail.com', 'backend.email.sender@gmail.com', '2020-01-29 12:47:02', NULL, NULL, 1, NULL, NULL, b'0'),
	(4, 'Serveur de messagerie de google', 'smtp.gmail.com', 'ueeso.cocody19@gmail.com', 'egliseueeso_cocody', 587, NULL, b'1', b'0', NULL, b'1', b'1', '*', b'0', b'1', 'Eglise UEESO-COCODY', 'ueeso.cocody19@gmail.com', 'ueeso.cocody19@gmail.com', '2020-03-12 14:45:29', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `smtp_server` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. sous_prefecture
DROP TABLE IF EXISTS `sous_prefecture`;
CREATE TABLE IF NOT EXISTS `sous_prefecture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) CHARACTER SET utf8 NOT NULL,
  `id_departement` int(11) NOT NULL,
  `chef_lieu` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_departement_sous-prefecture` (`id_departement`),
  CONSTRAINT `FK_departement_sous-prefecture` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=big5 COMMENT='la liste des sous-prefectures de la Côte d''ivoire';

-- Export de données de la table bd_ge.sous_prefecture : ~0 rows (environ)
/*!40000 ALTER TABLE `sous_prefecture` DISABLE KEYS */;
/*!40000 ALTER TABLE `sous_prefecture` ENABLE KEYS */;

-- Export de la structure de la procédure bd_ge. sp_add_default_column
DROP PROCEDURE IF EXISTS `sp_add_default_column`;
DELIMITER //
CREATE DEFINER=`ueesococody`@`%` PROCEDURE `sp_add_default_column`(
	IN `v_db_name` VARCHAR(255)
)
BEGIN

  DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_table_name VARCHAR(500) DEFAULT "";
    DECLARE v_table_schema VARCHAR(500) DEFAULT "";
    DECLARE stmt VARCHAR(500) DEFAULT "";
    
   #declare cursor
  DECLARE column_cursor CURSOR 
    FOR SELECT table_schema, table_name FROM information_schema.tables WHERE table_schema = v_db_name AND table_type = 'base table';
        
     #declare NOT FOUND handler
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
  
    OPEN column_cursor;
    
    alter_tables: LOOP
 
    FETCH column_cursor INTO v_table_schema, v_table_name;
    IF v_finished = 1 THEN 
      LEAVE alter_tables;
    END IF;
 
    #build query
    SET @prepstmt = CONCAT('ALTER TABLE ', v_table_schema, '.', v_table_name,
	' ADD COLUMN  created_at DATETIME DEFAULT NULL, 
	  ADD COLUMN  updated_at DATETIME DEFAULT NULL,
	  ADD COLUMN  deleted_at DATETIME DEFAULT NULL, 
	  ADD COLUMN  created_by INT(11) DEFAULT NULL, 
	  ADD COLUMN  updated_by INT(11) DEFAULT NULL, 
	  ADD COLUMN  deleted_by INT(11) DEFAULT NULL, 
	  ADD COLUMN  is_deleted BIT DEFAULT NULL;');
        
    PREPARE stmt FROM @prepstmt;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
   
  END LOOP alter_tables;
   
  CLOSE column_cursor;
END//
DELIMITER ;

-- Export de la structure de la table bd_ge. tresorier
DROP TABLE IF EXISTS `tresorier`;
CREATE TABLE IF NOT EXISTS `tresorier` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `mobile_account_1` varchar(255) NOT NULL,
  `mobile_account_2` varchar(255) DEFAULT NULL,
  `mobile_account_3` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tresorier_user` (`id_user`),
  CONSTRAINT `FK_tresorier_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.tresorier : ~2 rows (environ)
/*!40000 ALTER TABLE `tresorier` DISABLE KEYS */;
INSERT INTO `tresorier` (`id`, `id_user`, `mobile_account_1`, `mobile_account_2`, `mobile_account_3`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 13, '07346534', '04563898', '41329245', '2020-01-27 00:02:22', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 14, '47980734', '05674351', '', '2020-01-27 00:02:23', NULL, NULL, 1, NULL, NULL, b'0'),
	(3, 15, '89096534', '41254634', '', '2020-01-27 00:02:24', '2020-01-27 00:07:13', NULL, 1, 1, NULL, b'0');
/*!40000 ALTER TABLE `tresorier` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. type_user
DROP TABLE IF EXISTS `type_user`;
CREATE TABLE IF NOT EXISTS `type_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `libelle` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.type_user : ~2 rows (environ)
/*!40000 ALTER TABLE `type_user` DISABLE KEYS */;
INSERT INTO `type_user` (`id`, `code`, `libelle`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'FDLE', 'Fidèle', '2020-01-24 19:46:32', NULL, NULL, 1, NULL, NULL, b'0'),
	(2, 'DNTR', 'Généreux Donnateur', '2020-01-24 19:51:49', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `type_user` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `pre_generated_login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `date_naissance` datetime DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `id_situation_professionnelle` int(11) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  `is_local_resident` bit(1) DEFAULT NULL,
  `id_situation_matrimoniale` int(11) DEFAULT NULL,
  `nom_conjoint` varchar(255) DEFAULT NULL,
  `date_vie_conjugale` datetime DEFAULT NULL,
  `is_home_respo` bit(1) DEFAULT NULL,
  `home_respo_name` varchar(255) DEFAULT NULL,
  `date_arrive_ueeso` datetime DEFAULT NULL,
  `date_conversion` datetime DEFAULT NULL,
  `bapteme_immersion` bit(1) DEFAULT NULL,
  `date_bapteme` datetime DEFAULT NULL,
  `eglise_bapteme` varchar(255) DEFAULT NULL,
  `eglise_origine` varchar(255) DEFAULT NULL,
  `id_type_user` int(11) unsigned DEFAULT NULL,
  `id_pays` int(11) DEFAULT NULL COMMENT 'Pays d''orgine',
  `id_profession` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `locked` bit(1) DEFAULT NULL,
  `is_connected` bit(1) DEFAULT NULL,
  `is_validated` bit(1) DEFAULT NULL COMMENT 'compte utilisateur validé ou pas par l''administrateur',
  `lieu_naissance` varchar(255) DEFAULT NULL COMMENT 'lieu de naissance',
  `domaine_competence` varchar(255) DEFAULT NULL COMMENT 'liste des domaine de competence séparés par des virgules',
  `id_member_status` int(11) DEFAULT NULL,
  `civilite` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_user` (`id_type_user`),
  KEY `id_pays_nat` (`id_pays`),
  KEY `id_prof` (`id_profession`),
  KEY `FK_user_situation_professionnelle` (`id_situation_professionnelle`),
  KEY `FK_user_situation_matrimoniale` (`id_situation_matrimoniale`),
  KEY `nom` (`nom`),
  KEY `prenom` (`prenom`),
  KEY `FK_user_role` (`id_role`),
  KEY `id_member_status` (`id_member_status`),
  CONSTRAINT `FK_user_pays` FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id`),
  CONSTRAINT `FK_user_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_user_situation_matrimoniale` FOREIGN KEY (`id_situation_matrimoniale`) REFERENCES `situation_matrimoniale` (`id`),
  CONSTRAINT `FK_user_situation_professionnelle` FOREIGN KEY (`id_situation_professionnelle`) REFERENCES `situation_professionnelle` (`id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_type_user`) REFERENCES `type_user` (`id`),
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`id_profession`) REFERENCES `profession` (`id`),
  CONSTRAINT `user_ibfk_4` FOREIGN KEY (`id_member_status`) REFERENCES `member_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.user : ~19 rows (environ)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `telephone`, `login`, `pre_generated_login`, `password`, `date_naissance`, `photo`, `id_situation_professionnelle`, `id_role`, `is_local_resident`, `id_situation_matrimoniale`, `nom_conjoint`, `date_vie_conjugale`, `is_home_respo`, `home_respo_name`, `date_arrive_ueeso`, `date_conversion`, `bapteme_immersion`, `date_bapteme`, `eglise_bapteme`, `eglise_origine`, `id_type_user`, `id_pays`, `id_profession`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`, `locked`, `is_connected`, `is_validated`, `lieu_naissance`, `domaine_competence`, `id_member_status`, `civilite`) VALUES
	(1, 'admin', 'ADMIN', 'admin@gmail.com', '78232313', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-18 21:05:58', NULL, NULL, 1, b'0', b'0', b'0', b'0', NULL, NULL, NULL, 'Monsieur'),
	(2, 'kam', 'kam', 'kam@email.com', '73232313', NULL, NULL, NULL, '2019-12-12 00:00:00', '', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-18 20:31:14', '2020-01-18 21:08:26', NULL, 1, 1, NULL, b'0', b'0', b'0', b'1', 'Abidjan', NULL, NULL, 'Monsieur'),
	(3, 'markou', 'markou', 'markou@email.com', '03232313', NULL, NULL, NULL, '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/FILES/photo.jpeg', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-18 20:47:10', NULL, '2020-01-23 15:14:16', 1, NULL, 1, b'1', b'0', b'0', b'0', 'Abidjan', NULL, NULL, 'Monsieur'),
	(5, 'daouda', 'daouda', 'daouda@email.com', '93232313', NULL, NULL, NULL, '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/FILES/photo.jpeg', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-18 20:49:49', NULL, '2020-01-23 15:14:17', 1, NULL, 1, b'1', b'0', b'0', b'0', 'Oume', NULL, NULL, 'Monsieur'),
	(6, 'daouda', 'daouda', 'test1@email.com', '99232313', NULL, NULL, NULL, '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/imagesGE/photo.jpeg_20200121111217891.jpg', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-21 11:12:21', NULL, '2020-01-23 15:14:19', 1, NULL, 1, b'1', b'0', b'0', b'0', 'Oume', NULL, NULL, 'Monsieur'),
	(7, 'daouda', 'daouda', 'test3@email.com', '49232313', NULL, NULL, NULL, '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/imagesGE/photo.jpeg_20200121111335841.jpg', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-21 11:13:38', NULL, '2020-01-23 15:14:21', 1, NULL, 1, b'1', b'0', b'0', b'0', 'Oume', NULL, NULL, 'Monsieur'),
	(8, 'markou', 'markou', 'markou3@email.com', '79455423', NULL, NULL, NULL, '2019-12-12 00:00:00', '', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-21 15:27:38', NULL, '2020-01-23 15:14:22', 1, NULL, 1, b'1', b'0', b'0', b'0', 'Oume', NULL, NULL, 'Monsieur'),
	(9, 'k2K', 'k2k', 'markou34@email.com', '19455423', NULL, NULL, NULL, '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/imagesGE/photo.jpeg_20200121153136111.jpg', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-21 15:31:36', NULL, '2020-01-23 15:14:24', 1, NULL, 1, b'1', b'0', b'0', b'0', 'Oume', NULL, NULL, 'Monsieur'),
	(10, 'm1', 'm2p', 'm2p@email.com', '94554237', NULL, NULL, NULL, '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/imagesGE/photo.jpeg_20200123114600342.jpg', 1, NULL, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-23 11:46:00', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', b'0', 'Allepe', NULL, NULL, 'Monsieur'),
	(11, 'super_admin', 'super_admin', 'super_admin@email.com', '99564321', 'admin', NULL, 'd033e22ae348aeb5660fc2140aec35850c4da997', '2019-12-12 00:00:00', '/Users/kam/Documents/PERSO/UEESO/imagesGE/photo.jpeg_20200123151635278.jpg', 1, 1, b'1', 1, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 1, '2020-01-23 15:16:35', '2020-04-11 17:49:33', NULL, 1, NULL, NULL, b'0', b'0', b'1', b'0', 'Abidjan', NULL, NULL, 'Monsieur'),
	(12, 'YAO', 'Lazare Kouakou', 'kouakoulazarepro@gmail.com', '49185990', NULL, NULL, NULL, '1990-01-01 00:00:00', '', 1, NULL, b'1', 1, NULL, NULL, b'1', NULL, '2020-01-10 00:00:00', NULL, b'0', NULL, NULL, 'CMA', 1, 39, 1, '2020-01-24 23:55:23', '2020-03-08 01:08:00', NULL, 1, 1, NULL, b'0', b'0', b'0', NULL, 'KORHOGO', 'Informatiq coding', 1, 'Monsieur'),
	(13, 'tresorier1', 'tresorier1', 'tresorier1@mail.com', '+22507238520', NULL, NULL, NULL, '2020-01-25 00:00:00', 'test.jpg', 3, 1, b'1', 1, NULL, NULL, b'1', NULL, '0009-01-01 00:00:00', '1990-01-01 00:00:00', b'1', '1998-01-01 00:00:00', 'TEST', 'TEST', 1, 231, 1, '2020-01-25 20:23:40', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'test', 'test', 1, 'M/Mmme'),
	(14, 'tresorier2', 'tresorier2', 'tresorier2@mail.com', '+22507238521', NULL, NULL, NULL, '2020-01-25 00:00:00', 'test.jpg', 1, 1, b'1', 1, NULL, NULL, b'1', NULL, '0009-01-01 00:00:00', '1990-01-01 00:00:00', b'1', '1998-01-01 00:00:00', 'TEST', 'TEST', 1, 224, 2, '2020-01-25 20:35:10', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'test', 'test', 1, 'M/Mmme'),
	(15, 'tresorier3', 'tresorier3', 'tresorier3@mail.com', '+22507238522', NULL, NULL, NULL, '2020-01-25 00:00:00', 'test.jpg', 3, 2, b'1', 6, NULL, NULL, b'1', NULL, '0007-01-01 00:00:00', '1970-10-20 00:00:00', b'1', '2019-01-01 00:00:00', 'TEST', 'TEST', 1, 234, 2, '2020-01-25 22:01:59', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'test', 'test', 1, 'M/Mmme'),
	(18, 'test', 'test', 'test@mail.com', '09000090', NULL, NULL, NULL, '1990-01-01 00:00:00', '', 5, 2, b'0', 2, 'test', '1990-01-01 00:00:00', b'1', NULL, NULL, '0004-01-01 00:00:00', b'1', '0002-01-01 00:00:00', 'test', 'test', 2, 190, 1, '2020-03-14 16:56:12', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'test', 'test', NULL, 'Monsieur'),
	(30, 'Anjipu', 'Leclaire', 'leclairanjipu@gmail.com', '(+225) 09 93 75 38', NULL, NULL, NULL, '1990-01-01 00:00:00', 'https://ueeso-file.s3.us-east-2.amazonaws.com/photo_2018-10-15_16-35-46.jpg', 1, 2, b'0', 1, NULL, NULL, b'1', NULL, '2018-01-01 00:00:00', '2018-01-01 00:00:00', b'1', '2018-01-01 00:00:00', 'UEESO', 'UEESO', 1, 41, 1, '2020-04-04 18:16:04', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'Yaoudé', 'TIC', 2, 'Monsieur'),
	(38, 'Fidele 1', 'fidele', 'test1@gmail.com', '09011090', NULL, NULL, NULL, '2018-02-08 00:00:00', 'https://ueeso-file.s3.us-east-2.amazonaws.com/photo_2018-10-15_16-36-43.jpg', 1, 2, b'0', 1, NULL, NULL, b'1', NULL, '2020-01-01 00:00:00', '2020-01-01 00:00:00', b'1', '2020-01-01 00:00:00', 'UEESO', 'UEESO', 1, 39, NULL, '2020-04-05 00:20:25', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'Abidjan', 'volontariat', 2, 'Monsieur'),
	(39, 'FraTest', 'Super', 'testeur@gmail.com', '09000612', 'testeur@gmail.com', NULL, 'f1964bb723c798ffdb3a711d3f6f8a4bea06ac2d', '1992-06-17 00:00:00', 'https://ueeso-file.s3.us-east-2.amazonaws.com/pexels-photo-1236809.jpeg', 1, 2, b'0', 1, NULL, '2020-04-05 00:00:00', b'1', NULL, '2018-10-19 00:00:00', '2019-01-10 00:00:00', b'1', '2018-03-08 00:00:00', 'UEESO', 'UEESO', 1, 39, 4, '2020-04-05 09:55:33', '2020-04-05 12:51:45', NULL, 1, 11, NULL, b'0', b'0', b'0', b'1', 'Yaoudé', 'volontariat', 2, 'Monsieur'),
	(40, 'ANJIPU MFONCHA', 'Enock Leclaire', 'leclaire.anjipu@@smile.ci', '+22558300487', NULL, NULL, NULL, '1992-01-15 00:00:00', 'https://ueeso-file.s3.us-east-2.amazonaws.com/Leclaire-pic3.jpg', 1, 2, b'0', 1, NULL, NULL, b'1', NULL, '2018-06-17 00:00:00', '2005-03-01 00:00:00', b'1', '2018-06-03 00:00:00', 'Eglise Evangélique de Dakar', 'Eglise Evangélique du Cameroun', 1, 41, 1, '2020-04-11 17:49:03', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', NULL, 'NJISSE (Foumban)', 'Mathématiques', 2, 'Monsieur');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. user_groupe_constitue
DROP TABLE IF EXISTS `user_groupe_constitue`;
CREATE TABLE IF NOT EXISTS `user_groupe_constitue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_fonction` int(11) DEFAULT NULL,
  `id_user` int(11) unsigned DEFAULT NULL,
  `id_groupe_constitue` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_gc` (`id_groupe_constitue`),
  KEY `fk_fonction` (`id_fonction`),
  CONSTRAINT `fk_fonction` FOREIGN KEY (`id_fonction`) REFERENCES `fonction` (`id`),
  CONSTRAINT `user_groupe_constitue_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `user_groupe_constitue_ibfk_2` FOREIGN KEY (`id_groupe_constitue`) REFERENCES `groupe_constitue` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.user_groupe_constitue : ~9 rows (environ)
/*!40000 ALTER TABLE `user_groupe_constitue` DISABLE KEYS */;
INSERT INTO `user_groupe_constitue` (`id`, `id_fonction`, `id_user`, `id_groupe_constitue`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(3, 1, 10, 1, '2020-01-23 11:46:06', NULL, NULL, NULL, NULL, NULL, b'0'),
	(4, 2, 10, 2, '2020-01-23 11:46:07', NULL, NULL, NULL, NULL, NULL, b'0'),
	(5, 1, 11, 1, '2020-01-23 15:16:36', NULL, NULL, NULL, NULL, NULL, b'0'),
	(6, 2, 11, 2, '2020-01-23 15:16:37', NULL, NULL, NULL, NULL, NULL, b'0'),
	(7, 1, 30, 7, '2020-04-04 18:16:04', NULL, NULL, 1, NULL, NULL, b'0'),
	(9, 5, 38, 8, '2020-04-05 00:20:25', NULL, NULL, 1, NULL, NULL, b'0'),
	(10, 1, 39, 7, '2020-04-05 09:55:33', NULL, NULL, 1, NULL, NULL, b'0'),
	(11, 9, 40, 15, '2020-04-11 17:49:03', NULL, NULL, 1, NULL, NULL, b'0'),
	(12, 5, 40, 12, '2020-04-11 17:49:04', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `user_groupe_constitue` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. user_mission
DROP TABLE IF EXISTS `user_mission`;
CREATE TABLE IF NOT EXISTS `user_mission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_fonction` int(11) DEFAULT NULL,
  `id_user` int(11) unsigned DEFAULT NULL,
  `id_mission` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_mission` (`id_mission`),
  KEY `FK_user_mission_fonction` (`id_fonction`),
  CONSTRAINT `FK_user_mission_fonction` FOREIGN KEY (`id_fonction`) REFERENCES `fonction` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `user_mission_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `user_mission_ibfk_2` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Export de données de la table bd_ge.user_mission : ~1 rows (environ)
/*!40000 ALTER TABLE `user_mission` DISABLE KEYS */;
INSERT INTO `user_mission` (`id`, `id_fonction`, `id_user`, `id_mission`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 5, 39, 1, '2020-04-05 09:55:33', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `user_mission` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. village
DROP TABLE IF EXISTS `village`;
CREATE TABLE IF NOT EXISTS `village` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) CHARACTER SET utf8 NOT NULL,
  `chef_lieu` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=big5 COMMENT='la liste des villages de la Côte d''ivoire';

-- Export de données de la table bd_ge.village : ~0 rows (environ)
/*!40000 ALTER TABLE `village` DISABLE KEYS */;
/*!40000 ALTER TABLE `village` ENABLE KEYS */;

-- Export de la structure de la table bd_ge. ville
DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `superficie` double DEFAULT NULL,
  `id_pays` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `libele` (`libelle`),
  KEY `fk_id_pays` (`id_pays`),
  CONSTRAINT `fk_id_pays` FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table bd_ge.ville : ~81 rows (environ)
/*!40000 ALTER TABLE `ville` DISABLE KEYS */;
INSERT INTO `ville` (`id`, `libelle`, `longitude`, `latitude`, `superficie`, `id_pays`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `is_deleted`) VALUES
	(1, 'Abidjan', -4.03, 5.33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Bouake', -5.03, 7.69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Daloa', -6.45, 6.89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Yamoussoukro', -5.28, 6.82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Korhogo', -5.65, 9.45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'Divo', -5.37, 5.85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'San-pedro', -6.64, 4.77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'Anyama', -4.05, 5.5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'Man', -7.55, 7.4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'Gagnoa', -5.88, 6.15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'Abengourou', -3.49, 6.73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'Dabou', -4.39, 5.32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'Bouafle', -5.75, 6.98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'Grand bassam', -3.75, 5.2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 'Dimbokro', -4.71, 6.65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 'Agboville', -4.28, 5.94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 'Sinfra', -5.92, 6.62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 'Bingerville', -3.9, 5.36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 'Danane', -8.16, 7.26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 'Bondoukou', -2.8, 8.03, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 'Ferkessedougou', -5.2, 9.6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 'Katiola', -5.11, 8.15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 'Issia', -6.59, 6.49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 'Oume', -5.43, 6.38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 'Odienne', -7.58, 9.51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 'Toumodi', -5.03, 6.55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 'Adzope', -3.87, 6.1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 'Soubre', -6.61, 5.79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 'Duekoue', -7.35, 6.74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 'Seguela', -6.67, 7.95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 'Agnibilekrou', -3.2, 7.13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 'Daoukro', -3.96, 7.06, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 'Aboisso', -3.2, 5.47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 'Tiassale', -4.83, 5.88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 'Akoupe', -3.9, 6.38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, 'Lakota', -5.69, 5.85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 'Tingrela', -6.4, 10.49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 'Guiglo', -7.49, 6.55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, 'Boundiali', -6.49, 9.53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, 'Bonoua', -3.61, 5.28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, 'Arrah', -3.97, 6.67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, 'Zuenoula', -6.05, 7.44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, 'Vavoua', -6.49, 7.38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, 'Affery', -3.95, 6.32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, 'Bongouanou', -4.2, 6.65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, 'Hire', -5.29, 6.19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, 'Mbatto', -4.37, 6.46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, 'Touba', -7.69, 8.29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, 'Ouragahio', -5.94, 6.32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, 'Bouna', -3, 9.27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, 'Sassandra', -6.08, 4.95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, 'Ndouci', -4.77, 5.87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, 'Biankouma', -7.62, 7.74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, 'Tanda', -3.17, 7.8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, 'Tiebissou', -5.29, 7.16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, 'Tabou', -7.36, 4.42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, 'Mankono', -6.19, 8.06, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, 'Beoumi', -5.58, 7.68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, 'Bangolo', -7.48, 7.02, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, 'Adiake', -3.3, 5.28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, 'Dikodougou', -5.78, 9.07, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, 'Dabakala', -4.42, 8.36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, 'Rubino', -4.32, 6.07, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, 'Bako', -7.61, 9.14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, 'Sakassou', -5.3, 7.45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(66, 'Kani', -6.61, 8.48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, 'Tafire', -5.17, 9.07, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, 'M\'bahiakro', -4.46, 7.48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, 'Toulepleu', -8.42, 6.58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, 'Fresco', -5.57, 5.08, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, 'Jacqueville', -4.42, 5.2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, 'Guiberoua', -6.17, 6.24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, 'Ayame', -3.16, 5.62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, 'Botro', -5.32, 7.85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, 'Alepe', -3.65, 5.5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, 'Ndiekro', -4.77, 7.44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, 'Bocanda', -4.5, 7.06, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, 'Grand-lahou', -5.03, 5.13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, 'Abidjan', NULL, NULL, NULL, 39, '2020-01-18 19:24:25', NULL, NULL, 1, NULL, NULL, b'0'),
	(80, 'Yamoussoukro', NULL, NULL, NULL, 39, '2020-01-18 20:20:50', NULL, NULL, 1, NULL, NULL, b'0'),
	(84, 'testVille', NULL, NULL, NULL, 1, '2020-01-18 20:31:12', NULL, NULL, NULL, NULL, NULL, b'0'),
	(86, 'test', NULL, NULL, NULL, 235, '2020-01-25 20:23:39', NULL, NULL, 1, NULL, NULL, b'0');
/*!40000 ALTER TABLE `ville` ENABLE KEYS */;

-- Export de la structure de la vue bd_ge. v_etat_engagement_projet
DROP VIEW IF EXISTS `v_etat_engagement_projet`;
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `v_etat_engagement_projet` (
	`id` INT(11) UNSIGNED NOT NULL,
	`nom_et_prenoms` VARCHAR(511) NULL COLLATE 'latin1_swedish_ci',
	`contact` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`projet` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`email` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`mois` INT(11) NULL,
	`montant_engagement` INT(11) NULL,
	`montant_restant_a_payer` DECIMAL(33,0) NULL
) ENGINE=MyISAM;

-- Export de la structure de la vue bd_ge. v_paiement
DROP VIEW IF EXISTS `v_paiement`;
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `v_paiement` (
	`id_user` INT(11) UNSIGNED NULL,
	`id_engagement` INT(11) UNSIGNED NOT NULL,
	`id_paiement` INT(11) UNSIGNED NOT NULL,
	`mois` INT(11) NULL,
	`projet` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`montant_paiement` INT(11) NULL,
	`montant_engagement` INT(11) NULL
) ENGINE=MyISAM;

-- Export de la structure de la vue bd_ge. v_reste_a_payer
DROP VIEW IF EXISTS `v_reste_a_payer`;
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `v_reste_a_payer` (
	`id_eng` INT(11) UNSIGNED NOT NULL,
	`montant_restant_a_payer` DECIMAL(33,0) NULL
) ENGINE=MyISAM;

-- Export de la structure de la vue bd_ge. v_etat_engagement_projet
DROP VIEW IF EXISTS `v_etat_engagement_projet`;
-- Suppression de la table temporaire et création finale de la structure d'une vue
DROP TABLE IF EXISTS `v_etat_engagement_projet`;
CREATE ALGORITHM=UNDEFINED DEFINER=`ueesococody`@`%` SQL SECURITY DEFINER VIEW `v_etat_engagement_projet` AS select `u`.`id` AS `id`,concat(`u`.`nom`,' ',`u`.`prenom`) AS `nom_et_prenoms`,`u`.`telephone` AS `contact`,`vp`.`projet` AS `projet`,`u`.`email` AS `email`,`vp`.`mois` AS `mois`,`vp`.`montant_engagement` AS `montant_engagement`,`vr`.`montant_restant_a_payer` AS `montant_restant_a_payer` from ((`user` `u` join `v_paiement` `vp`) join `v_reste_a_payer` `vr`) where ((`u`.`id` = `vp`.`id_user`) and (`vp`.`id_engagement` = `vr`.`id_eng`)) group by `u`.`id`,concat(`u`.`nom`,' ',`u`.`prenom`),`u`.`telephone`,`u`.`email`,`vp`.`mois`,`vp`.`projet`;

-- Export de la structure de la vue bd_ge. v_paiement
DROP VIEW IF EXISTS `v_paiement`;
-- Suppression de la table temporaire et création finale de la structure d'une vue
DROP TABLE IF EXISTS `v_paiement`;
CREATE ALGORITHM=UNDEFINED DEFINER=`ueesococody`@`%` SQL SECURITY DEFINER VIEW `v_paiement` AS select `en`.`id_user` AS `id_user`,`en`.`id` AS `id_engagement`,`pa`.`id` AS `id_paiement`,`pe`.`month` AS `mois`,`pr`.`code` AS `projet`,`pa`.`montant` AS `montant_paiement`,`en`.`montant_engagement` AS `montant_engagement` from (((`paiement` `pa` join `periode` `pe`) join `engagement` `en`) join `projet` `pr`) where ((`pa`.`id` = `pe`.`id_paiement`) and (`pa`.`id_engagement` = `en`.`id`) and (`en`.`id_projet` = `pr`.`id`));

-- Export de la structure de la vue bd_ge. v_reste_a_payer
DROP VIEW IF EXISTS `v_reste_a_payer`;
-- Suppression de la table temporaire et création finale de la structure d'une vue
DROP TABLE IF EXISTS `v_reste_a_payer`;
CREATE ALGORITHM=UNDEFINED DEFINER=`ueesococody`@`%` SQL SECURITY DEFINER VIEW `v_reste_a_payer` AS select `en`.`id` AS `id_eng`,(`en`.`montant_engagement` - sum(`pa`.`montant`)) AS `montant_restant_a_payer` from (`engagement` `en` join `paiement` `pa`) where (`en`.`id` = `pa`.`id_engagement`) group by `en`.`id`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
