package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.SmtpServer;
import ci.ueeso.gestion.eglise.helper.dto.SmtpServerDto;

/**
 * Repository customize : SmtpServer.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _SmtpServerRepository {
	default List<String> _generateCriteria(SmtpServerDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	
	@Query("select e from SmtpServer e where e.host = :host and e.isDeleted = :isDeleted")
	SmtpServer findSmtpServerByHost(@Param("host")String host, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select e from SmtpServer e where e.isActive = :isActive and e.isDeleted = :isDeleted")
	SmtpServer findSmtpServerByIsActive(@Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);
	
	@Query(value="select * from smtp_server s where s.is_active = true and s.is_deleted = false order by s.id asc limit 1", nativeQuery=true)
	SmtpServer getServer();
}
