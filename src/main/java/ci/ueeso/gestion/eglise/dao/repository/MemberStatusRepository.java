

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.MemberStatus;
import ci.ueeso.gestion.eglise.dao.repository.customize._MemberStatusRepository;
import ci.ueeso.gestion.eglise.helper.CriteriaUtils;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.dto.MemberStatusDto;

/**
 * Repository : MemberStatus.
 */
@Repository
public interface MemberStatusRepository extends JpaRepository<MemberStatus, Integer>, _MemberStatusRepository {
	/**
	 * Finds MemberStatus by using id as a search criteria.
	 *
	 * @param id
	 * @return An Object MemberStatus whose id is equals to the given id. If
	 *         no MemberStatus is found, this method returns null.
	 */
	@Query("select e from MemberStatus e where e.id = :id")
	MemberStatus findOne(@Param("id")Integer id);

	/**
	 * Finds MemberStatus by using code as a search criteria.
	 *
	 * @param code
	 * @return An Object MemberStatus whose code is equals to the given code. If
	 *         no MemberStatus is found, this method returns null.
	 */
	@Query("select e from MemberStatus e where e.code = :code")
	MemberStatus findByCode(@Param("code")String code);
	/**
	 * Finds MemberStatus by using libelle as a search criteria.
	 *
	 * @param libelle
	 * @return An Object MemberStatus whose libelle is equals to the given libelle. If
	 *         no MemberStatus is found, this method returns null.
	 */
	@Query("select e from MemberStatus e where e.libelle = :libelle")
	MemberStatus findByLibelle(@Param("libelle")String libelle);
	/**
	 * Finds MemberStatus by using comment as a search criteria.
	 *
	 * @param comment
	 * @return An Object MemberStatus whose comment is equals to the given comment. If
	 *         no MemberStatus is found, this method returns null.
	 */
	@Query("select e from MemberStatus e where e.comment = :comment")
	List<MemberStatus> findByComment(@Param("comment")String comment);



	/**
	 * Finds List of MemberStatus by using memberStatusDto as a search criteria.
	 *
	 * @param request, em
	 * @return A List of MemberStatus
	 * @throws DataAccessException,ParseException
	 */
	public default List<MemberStatus> getByCriteria(Request<MemberStatusDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from MemberStatus e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<MemberStatus> query = em.createQuery(req, MemberStatus.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of MemberStatus by using memberStatusDto as a search criteria.
	 *
	 * @param request, em
	 * @return Number of MemberStatus
	 *
	 */
	public default Long count(Request<MemberStatusDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from MemberStatus e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<MemberStatusDto> request, HashMap<String, Object> param, Locale locale) throws Exception {
		// main query
		MemberStatusDto dto = request.getData() != null ? request.getData() : new MemberStatusDto();
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (MemberStatusDto elt : request.getDatas()) {
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param req
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(MemberStatusDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCode())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getComment())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("comment", dto.getComment(), "e.comment", "String", dto.getCommentParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
