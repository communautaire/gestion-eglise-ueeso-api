

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._CommuneRepository;

/**
 * Repository : Commune.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface CommuneRepository extends JpaRepository<Commune, Integer>, _CommuneRepository {
	/**
	 * Finds Commune by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Commune whose id is equals to the given id. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.id = :id and e.isDeleted = :isDeleted")
	Commune findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Commune by using code as a search criteria.
	 * 
	 * @param code
	 * @return An Object Commune whose code is equals to the given code. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.code = :code and e.isDeleted = :isDeleted")
	Commune findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Commune whose libelle is equals to the given libelle. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Commune findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using longitude as a search criteria.
	 * 
	 * @param longitude
	 * @return An Object Commune whose longitude is equals to the given longitude. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.longitude = :longitude and e.isDeleted = :isDeleted")
	List<Commune> findByLongitude(@Param("longitude")Double longitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using latitude as a search criteria.
	 * 
	 * @param latitude
	 * @return An Object Commune whose latitude is equals to the given latitude. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.latitude = :latitude and e.isDeleted = :isDeleted")
	List<Commune> findByLatitude(@Param("latitude")Double latitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using superficie as a search criteria.
	 * 
	 * @param superficie
	 * @return An Object Commune whose superficie is equals to the given superficie. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.superficie = :superficie and e.isDeleted = :isDeleted")
	List<Commune> findBySuperficie(@Param("superficie")Double superficie, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Commune whose createdAt is equals to the given createdAt. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Commune> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Commune whose updatedAt is equals to the given updatedAt. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Commune> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Commune whose deletedAt is equals to the given deletedAt. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Commune> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Commune whose createdBy is equals to the given createdBy. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Commune> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Commune whose updatedBy is equals to the given updatedBy. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Commune> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Commune whose deletedBy is equals to the given deletedBy. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Commune> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Commune by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Commune whose isDeleted is equals to the given isDeleted. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.isDeleted = :isDeleted")
	List<Commune> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Commune by using idPays as a search criteria.
	 * 
	 * @param idPays
	 * @return An Object Commune whose idPays is equals to the given idPays. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
	List<Commune> findByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Commune by using idPays as a search criteria.
   *
   * @param idPays
   * @return An Object Commune whose idPays is equals to the given idPays. If
   *         no Commune is found, this method returns null.
   */
  @Query("select e from Commune e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
  Commune findCommuneByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Commune by using idVille as a search criteria.
	 * 
	 * @param idVille
	 * @return An Object Commune whose idVille is equals to the given idVille. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.ville.id = :idVille and e.isDeleted = :isDeleted")
	List<Commune> findByIdVille(@Param("idVille")Integer idVille, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Commune by using idVille as a search criteria.
   *
   * @param idVille
   * @return An Object Commune whose idVille is equals to the given idVille. If
   *         no Commune is found, this method returns null.
   */
  @Query("select e from Commune e where e.ville.id = :idVille and e.isDeleted = :isDeleted")
  Commune findCommuneByIdVille(@Param("idVille")Integer idVille, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Commune by using communeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Commune
	 * @throws DataAccessException,ParseException
	 */
	public default List<Commune> getByCriteria(Request<CommuneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Commune e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Commune> query = em.createQuery(req, Commune.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Commune by using communeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Commune
	 * 
	 */
	public default Long count(Request<CommuneDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Commune e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<CommuneDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		CommuneDto dto = request.getData() != null ? request.getData() : new CommuneDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (CommuneDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(CommuneDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCode()) || Utilities.searchParamIsNotEmpty(dto.getCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLibelle()) || Utilities.searchParamIsNotEmpty(dto.getLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getLongitude() != null || Utilities.searchParamIsNotEmpty(dto.getLongitudeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("longitude", dto.getLongitude(), "e.longitude", "Double", dto.getLongitudeParam(), param, index, locale));
			}
			if (dto.getLatitude() != null || Utilities.searchParamIsNotEmpty(dto.getLatitudeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("latitude", dto.getLatitude(), "e.latitude", "Double", dto.getLatitudeParam(), param, index, locale));
			}
			if (dto.getSuperficie() != null || Utilities.searchParamIsNotEmpty(dto.getSuperficieParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("superficie", dto.getSuperficie(), "e.superficie", "Double", dto.getSuperficieParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdPays() != null || Utilities.searchParamIsNotEmpty(dto.getIdPaysParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idPays", dto.getIdPays(), "e.pays.id", "Integer", dto.getIdPaysParam(), param, index, locale));
			}
						if (dto.getIdVille() != null || Utilities.searchParamIsNotEmpty(dto.getIdVilleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idVille", dto.getIdVille(), "e.ville.id", "Integer", dto.getIdVilleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysCode()) || Utilities.searchParamIsNotEmpty(dto.getPaysCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysCode", dto.getPaysCode(), "e.pays.code", "String", dto.getPaysCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysLibelle()) || Utilities.searchParamIsNotEmpty(dto.getPaysLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysLibelle", dto.getPaysLibelle(), "e.pays.libelle", "String", dto.getPaysLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getVilleLibelle()) || Utilities.searchParamIsNotEmpty(dto.getVilleLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeLibelle", dto.getVilleLibelle(), "e.ville.libelle", "String", dto.getVilleLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
