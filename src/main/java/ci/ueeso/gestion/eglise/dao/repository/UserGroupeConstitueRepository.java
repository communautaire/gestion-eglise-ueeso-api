

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._UserGroupeConstitueRepository;

/**
 * Repository : UserGroupeConstitue.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface UserGroupeConstitueRepository extends JpaRepository<UserGroupeConstitue, Integer>, _UserGroupeConstitueRepository {
	/**
	 * Finds UserGroupeConstitue by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object UserGroupeConstitue whose id is equals to the given id. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.id = :id and e.isDeleted = :isDeleted")
	UserGroupeConstitue findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds UserGroupeConstitue by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object UserGroupeConstitue whose createdAt is equals to the given createdAt. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserGroupeConstitue by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object UserGroupeConstitue whose updatedAt is equals to the given updatedAt. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserGroupeConstitue by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object UserGroupeConstitue whose deletedAt is equals to the given deletedAt. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserGroupeConstitue by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object UserGroupeConstitue whose createdBy is equals to the given createdBy. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserGroupeConstitue by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object UserGroupeConstitue whose updatedBy is equals to the given updatedBy. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserGroupeConstitue by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object UserGroupeConstitue whose deletedBy is equals to the given deletedBy. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds UserGroupeConstitue by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object UserGroupeConstitue whose isDeleted is equals to the given isDeleted. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds UserGroupeConstitue by using idUser as a search criteria.
	 * 
	 * @param idUser
	 * @return An Object UserGroupeConstitue whose idUser is equals to the given idUser. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.user.id = :idUser and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByIdUser(@Param("idUser")Integer idUser, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one UserGroupeConstitue by using idUser as a search criteria.
   *
   * @param idUser
   * @return An Object UserGroupeConstitue whose idUser is equals to the given idUser. If
   *         no UserGroupeConstitue is found, this method returns null.
   */
  @Query("select e from UserGroupeConstitue e where e.user.id = :idUser and e.isDeleted = :isDeleted")
  UserGroupeConstitue findUserGroupeConstitueByIdUser(@Param("idUser")Integer idUser, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds UserGroupeConstitue by using idFonction as a search criteria.
	 * 
	 * @param idFonction
	 * @return An Object UserGroupeConstitue whose idFonction is equals to the given idFonction. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.fonction.id = :idFonction and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByIdFonction(@Param("idFonction")Integer idFonction, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one UserGroupeConstitue by using idFonction as a search criteria.
   *
   * @param idFonction
   * @return An Object UserGroupeConstitue whose idFonction is equals to the given idFonction. If
   *         no UserGroupeConstitue is found, this method returns null.
   */
  @Query("select e from UserGroupeConstitue e where e.fonction.id = :idFonction and e.isDeleted = :isDeleted")
  UserGroupeConstitue findUserGroupeConstitueByIdFonction(@Param("idFonction")Integer idFonction, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds UserGroupeConstitue by using idGroupeConstitue as a search criteria.
	 * 
	 * @param idGroupeConstitue
	 * @return An Object UserGroupeConstitue whose idGroupeConstitue is equals to the given idGroupeConstitue. If
	 *         no UserGroupeConstitue is found, this method returns null.
	 */
	@Query("select e from UserGroupeConstitue e where e.groupeConstitue.id = :idGroupeConstitue and e.isDeleted = :isDeleted")
	List<UserGroupeConstitue> findByIdGroupeConstitue(@Param("idGroupeConstitue")Integer idGroupeConstitue, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one UserGroupeConstitue by using idGroupeConstitue as a search criteria.
   *
   * @param idGroupeConstitue
   * @return An Object UserGroupeConstitue whose idGroupeConstitue is equals to the given idGroupeConstitue. If
   *         no UserGroupeConstitue is found, this method returns null.
   */
  @Query("select e from UserGroupeConstitue e where e.groupeConstitue.id = :idGroupeConstitue and e.isDeleted = :isDeleted")
  UserGroupeConstitue findUserGroupeConstitueByIdGroupeConstitue(@Param("idGroupeConstitue")Integer idGroupeConstitue, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of UserGroupeConstitue by using userGroupeConstitueDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of UserGroupeConstitue
	 * @throws DataAccessException,ParseException
	 */
	public default List<UserGroupeConstitue> getByCriteria(Request<UserGroupeConstitueDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from UserGroupeConstitue e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<UserGroupeConstitue> query = em.createQuery(req, UserGroupeConstitue.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of UserGroupeConstitue by using userGroupeConstitueDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of UserGroupeConstitue
	 * 
	 */
	public default Long count(Request<UserGroupeConstitueDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from UserGroupeConstitue e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<UserGroupeConstitueDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		UserGroupeConstitueDto dto = request.getData() != null ? request.getData() : new UserGroupeConstitueDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (UserGroupeConstitueDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;

		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(UserGroupeConstitueDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdUser() != null || Utilities.searchParamIsNotEmpty(dto.getIdUserParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idUser", dto.getIdUser(), "e.user.id", "Integer", dto.getIdUserParam(), param, index, locale));
			}
						if (dto.getIdFonction() != null || Utilities.searchParamIsNotEmpty(dto.getIdFonctionParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idFonction", dto.getIdFonction(), "e.fonction.id", "Integer", dto.getIdFonctionParam(), param, index, locale));
			}
						if (dto.getIdGroupeConstitue() != null || Utilities.searchParamIsNotEmpty(dto.getIdGroupeConstitueParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idGroupeConstitue", dto.getIdGroupeConstitue(), "e.groupeConstitue.id", "Integer", dto.getIdGroupeConstitueParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserNom()) || Utilities.searchParamIsNotEmpty(dto.getUserNomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserPrenom()) || Utilities.searchParamIsNotEmpty(dto.getUserPrenomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserLogin()) || Utilities.searchParamIsNotEmpty(dto.getUserLoginParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getFonctionLibelle()) || Utilities.searchParamIsNotEmpty(dto.getFonctionLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fonctionLibelle", dto.getFonctionLibelle(), "e.fonction.libelle", "String", dto.getFonctionLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getGroupeConstitueCode()) || Utilities.searchParamIsNotEmpty(dto.getGroupeConstitueCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("groupeConstitueCode", dto.getGroupeConstitueCode(), "e.groupeConstitue.code", "String", dto.getGroupeConstitueCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getGroupeConstitueLibelle()) || Utilities.searchParamIsNotEmpty(dto.getGroupeConstitueLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("groupeConstitueLibelle", dto.getGroupeConstitueLibelle(), "e.groupeConstitue.libelle", "String", dto.getGroupeConstitueLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
