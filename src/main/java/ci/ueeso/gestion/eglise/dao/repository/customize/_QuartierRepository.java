package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.Quartier;
import ci.ueeso.gestion.eglise.helper.dto.QuartierDto;

/**
 * Repository customize : Quartier.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _QuartierRepository {
	default List<String> _generateCriteria(QuartierDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	@Query("select e from Quartier e where e.libelle = :libelle and e.commune.id = :idCommune and e.isDeleted = :isDeleted")
	Quartier findByLibelleAndIdCommune(@Param("libelle")String libelle, @Param("idCommune")Integer idCommune, @Param("isDeleted")Boolean isDeleted);

	@Query("select e from Quartier e where e.code = :code and e.commune.id = :idCommune and e.isDeleted = :isDeleted")
	Quartier findByCodeAndIdCommune(@Param("code")String code, @Param("idCommune")Integer idCommune, @Param("isDeleted")Boolean isDeleted);


	
	@Query("select e from Quartier e where e.libelle = :libelle and e.village.id = :idVillage and e.isDeleted = :isDeleted")
	Quartier findByLibelleAndIdVillage(@Param("libelle")String libelle, @Param("idVillage")Integer idVillage, @Param("isDeleted")Boolean isDeleted);

	@Query("select e from Quartier e where e.code = :code and e.village.id = :idVillage and e.isDeleted = :isDeleted")
	Quartier findByCodeAndIdVillage(@Param("code")String code, @Param("idVillage")Integer idVillage, @Param("isDeleted")Boolean isDeleted);

	
	
}
