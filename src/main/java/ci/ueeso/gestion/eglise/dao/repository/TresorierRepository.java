

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._TresorierRepository;

/**
 * Repository : Tresorier.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface TresorierRepository extends JpaRepository<Tresorier, Integer>, _TresorierRepository {
	/**
	 * Finds Tresorier by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Tresorier whose id is equals to the given id. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.id = :id and e.isDeleted = :isDeleted")
	Tresorier findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Tresorier by using mobileAccount1 as a search criteria.
	 * 
	 * @param mobileAccount1
	 * @return An Object Tresorier whose mobileAccount1 is equals to the given mobileAccount1. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.mobileAccount1 = :mobileAccount1 and e.isDeleted = :isDeleted")
	List<Tresorier> findByMobileAccount1(@Param("mobileAccount1")String mobileAccount1, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using mobileAccount2 as a search criteria.
	 * 
	 * @param mobileAccount2
	 * @return An Object Tresorier whose mobileAccount2 is equals to the given mobileAccount2. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.mobileAccount2 = :mobileAccount2 and e.isDeleted = :isDeleted")
	List<Tresorier> findByMobileAccount2(@Param("mobileAccount2")String mobileAccount2, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using mobileAccount3 as a search criteria.
	 * 
	 * @param mobileAccount3
	 * @return An Object Tresorier whose mobileAccount3 is equals to the given mobileAccount3. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.mobileAccount3 = :mobileAccount3 and e.isDeleted = :isDeleted")
	List<Tresorier> findByMobileAccount3(@Param("mobileAccount3")String mobileAccount3, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Tresorier whose createdAt is equals to the given createdAt. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Tresorier> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Tresorier whose updatedAt is equals to the given updatedAt. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Tresorier> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Tresorier whose deletedAt is equals to the given deletedAt. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Tresorier> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Tresorier whose createdBy is equals to the given createdBy. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Tresorier> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Tresorier whose updatedBy is equals to the given updatedBy. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Tresorier> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Tresorier whose deletedBy is equals to the given deletedBy. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Tresorier> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Tresorier by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Tresorier whose isDeleted is equals to the given isDeleted. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.isDeleted = :isDeleted")
	List<Tresorier> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Tresorier by using idUser as a search criteria.
	 * 
	 * @param idUser
	 * @return An Object Tresorier whose idUser is equals to the given idUser. If
	 *         no Tresorier is found, this method returns null.
	 */
	@Query("select e from Tresorier e where e.user.id = :idUser and e.isDeleted = :isDeleted")
	List<Tresorier> findByIdUser(@Param("idUser")Integer idUser, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Tresorier by using idUser as a search criteria.
   *
   * @param idUser
   * @return An Object Tresorier whose idUser is equals to the given idUser. If
   *         no Tresorier is found, this method returns null.
   */
  @Query("select e from Tresorier e where e.user.id = :idUser and e.isDeleted = :isDeleted")
  Tresorier findTresorierByIdUser(@Param("idUser")Integer idUser, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Tresorier by using tresorierDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Tresorier
	 * @throws DataAccessException,ParseException
	 */
	public default List<Tresorier> getByCriteria(Request<TresorierDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Tresorier e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Tresorier> query = em.createQuery(req, Tresorier.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Tresorier by using tresorierDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Tresorier
	 * 
	 */
	public default Long count(Request<TresorierDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Tresorier e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<TresorierDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		TresorierDto dto = request.getData() != null ? request.getData() : new TresorierDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (TresorierDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(TresorierDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getMobileAccount1()) || Utilities.searchParamIsNotEmpty(dto.getMobileAccount1Param())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("mobileAccount1", dto.getMobileAccount1(), "e.mobileAccount1", "String", dto.getMobileAccount1Param(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getMobileAccount2()) || Utilities.searchParamIsNotEmpty(dto.getMobileAccount2Param())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("mobileAccount2", dto.getMobileAccount2(), "e.mobileAccount2", "String", dto.getMobileAccount2Param(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getMobileAccount3()) || Utilities.searchParamIsNotEmpty(dto.getMobileAccount3Param())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("mobileAccount3", dto.getMobileAccount3(), "e.mobileAccount3", "String", dto.getMobileAccount3Param(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdUser() != null || Utilities.searchParamIsNotEmpty(dto.getIdUserParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idUser", dto.getIdUser(), "e.user.id", "Integer", dto.getIdUserParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserNom()) || Utilities.searchParamIsNotEmpty(dto.getUserNomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserPrenom()) || Utilities.searchParamIsNotEmpty(dto.getUserPrenomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
