

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._SmtpServerRepository;

/**
 * Repository : SmtpServer.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface SmtpServerRepository extends JpaRepository<SmtpServer, Integer>, _SmtpServerRepository {
	/**
	 * Finds SmtpServer by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object SmtpServer whose id is equals to the given id. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.id = :id and e.isDeleted = :isDeleted")
	SmtpServer findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds SmtpServer by using description as a search criteria.
	 * 
	 * @param description
	 * @return An Object SmtpServer whose description is equals to the given description. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.description = :description and e.isDeleted = :isDeleted")
	List<SmtpServer> findByDescription(@Param("description")String description, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using host as a search criteria.
	 * 
	 * @param host
	 * @return An Object SmtpServer whose host is equals to the given host. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.host = :host and e.isDeleted = :isDeleted")
	List<SmtpServer> findByHost(@Param("host")String host, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using login as a search criteria.
	 * 
	 * @param login
	 * @return An Object SmtpServer whose login is equals to the given login. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.login = :login and e.isDeleted = :isDeleted")
	SmtpServer findByLogin(@Param("login")String login, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using password as a search criteria.
	 * 
	 * @param password
	 * @return An Object SmtpServer whose password is equals to the given password. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.password = :password and e.isDeleted = :isDeleted")
	List<SmtpServer> findByPassword(@Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using port as a search criteria.
	 * 
	 * @param port
	 * @return An Object SmtpServer whose port is equals to the given port. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.port = :port and e.isDeleted = :isDeleted")
	List<SmtpServer> findByPort(@Param("port")Integer port, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using smtpUser as a search criteria.
	 * 
	 * @param smtpUser
	 * @return An Object SmtpServer whose smtpUser is equals to the given smtpUser. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.smtpUser = :smtpUser and e.isDeleted = :isDeleted")
	List<SmtpServer> findBySmtpUser(@Param("smtpUser")String smtpUser, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using auth as a search criteria.
	 * 
	 * @param auth
	 * @return An Object SmtpServer whose auth is equals to the given auth. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.auth = :auth and e.isDeleted = :isDeleted")
	List<SmtpServer> findByAuth(@Param("auth")Boolean auth, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using enableSsl as a search criteria.
	 * 
	 * @param enableSsl
	 * @return An Object SmtpServer whose enableSsl is equals to the given enableSsl. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.enableSsl = :enableSsl and e.isDeleted = :isDeleted")
	List<SmtpServer> findByEnableSsl(@Param("enableSsl")Boolean enableSsl, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using sslProtocols as a search criteria.
	 * 
	 * @param sslProtocols
	 * @return An Object SmtpServer whose sslProtocols is equals to the given sslProtocols. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.sslProtocols = :sslProtocols and e.isDeleted = :isDeleted")
	List<SmtpServer> findBySslProtocols(@Param("sslProtocols")String sslProtocols, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using enableStarttls as a search criteria.
	 * 
	 * @param enableStarttls
	 * @return An Object SmtpServer whose enableStarttls is equals to the given enableStarttls. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.enableStarttls = :enableStarttls and e.isDeleted = :isDeleted")
	List<SmtpServer> findByEnableStarttls(@Param("enableStarttls")Boolean enableStarttls, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using isStarttlsRequired as a search criteria.
	 * 
	 * @param isStarttlsRequired
	 * @return An Object SmtpServer whose isStarttlsRequired is equals to the given isStarttlsRequired. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.isStarttlsRequired = :isStarttlsRequired and e.isDeleted = :isDeleted")
	List<SmtpServer> findByIsStarttlsRequired(@Param("isStarttlsRequired")Boolean isStarttlsRequired, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using hostsToTrust as a search criteria.
	 * 
	 * @param hostsToTrust
	 * @return An Object SmtpServer whose hostsToTrust is equals to the given hostsToTrust. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.hostsToTrust = :hostsToTrust and e.isDeleted = :isDeleted")
	List<SmtpServer> findByHostsToTrust(@Param("hostsToTrust")String hostsToTrust, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using isSmtpsProtocol as a search criteria.
	 * 
	 * @param isSmtpsProtocol
	 * @return An Object SmtpServer whose isSmtpsProtocol is equals to the given isSmtpsProtocol. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.isSmtpsProtocol = :isSmtpsProtocol and e.isDeleted = :isDeleted")
	List<SmtpServer> findByIsSmtpsProtocol(@Param("isSmtpsProtocol")Boolean isSmtpsProtocol, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using isActive as a search criteria.
	 * 
	 * @param isActive
	 * @return An Object SmtpServer whose isActive is equals to the given isActive. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.isActive = :isActive and e.isDeleted = :isDeleted")
	List<SmtpServer> findByIsActive(@Param("isActive")Boolean isActive, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using senderUsername as a search criteria.
	 * 
	 * @param senderUsername
	 * @return An Object SmtpServer whose senderUsername is equals to the given senderUsername. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.senderUsername = :senderUsername and e.isDeleted = :isDeleted")
	List<SmtpServer> findBySenderUsername(@Param("senderUsername")String senderUsername, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using senderEmail as a search criteria.
	 * 
	 * @param senderEmail
	 * @return An Object SmtpServer whose senderEmail is equals to the given senderEmail. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.senderEmail = :senderEmail and e.isDeleted = :isDeleted")
	List<SmtpServer> findBySenderEmail(@Param("senderEmail")String senderEmail, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using testEmail as a search criteria.
	 * 
	 * @param testEmail
	 * @return An Object SmtpServer whose testEmail is equals to the given testEmail. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.testEmail = :testEmail and e.isDeleted = :isDeleted")
	List<SmtpServer> findByTestEmail(@Param("testEmail")String testEmail, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object SmtpServer whose createdAt is equals to the given createdAt. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<SmtpServer> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object SmtpServer whose updatedAt is equals to the given updatedAt. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<SmtpServer> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object SmtpServer whose deletedAt is equals to the given deletedAt. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<SmtpServer> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object SmtpServer whose createdBy is equals to the given createdBy. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<SmtpServer> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object SmtpServer whose updatedBy is equals to the given updatedBy. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<SmtpServer> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object SmtpServer whose deletedBy is equals to the given deletedBy. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<SmtpServer> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds SmtpServer by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object SmtpServer whose isDeleted is equals to the given isDeleted. If
	 *         no SmtpServer is found, this method returns null.
	 */
	@Query("select e from SmtpServer e where e.isDeleted = :isDeleted")
	List<SmtpServer> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);



	/**
	 * Finds List of SmtpServer by using smtpServerDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of SmtpServer
	 * @throws DataAccessException,ParseException
	 */
	public default List<SmtpServer> getByCriteria(Request<SmtpServerDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from SmtpServer e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<SmtpServer> query = em.createQuery(req, SmtpServer.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of SmtpServer by using smtpServerDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of SmtpServer
	 * 
	 */
	public default Long count(Request<SmtpServerDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from SmtpServer e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SmtpServerDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		SmtpServerDto dto = request.getData() != null ? request.getData() : new SmtpServerDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SmtpServerDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SmtpServerDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDescription()) || Utilities.searchParamIsNotEmpty(dto.getDescriptionParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("description", dto.getDescription(), "e.description", "String", dto.getDescriptionParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getHost()) || Utilities.searchParamIsNotEmpty(dto.getHostParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("host", dto.getHost(), "e.host", "String", dto.getHostParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLogin()) || Utilities.searchParamIsNotEmpty(dto.getLoginParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("login", dto.getLogin(), "e.login", "String", dto.getLoginParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPassword()) || Utilities.searchParamIsNotEmpty(dto.getPasswordParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("password", dto.getPassword(), "e.password", "String", dto.getPasswordParam(), param, index, locale));
			}
			if (dto.getPort() != null || Utilities.searchParamIsNotEmpty(dto.getPortParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("port", dto.getPort(), "e.port", "Integer", dto.getPortParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSmtpUser()) || Utilities.searchParamIsNotEmpty(dto.getSmtpUserParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("smtpUser", dto.getSmtpUser(), "e.smtpUser", "String", dto.getSmtpUserParam(), param, index, locale));
			}
			if (dto.getAuth() != null || Utilities.searchParamIsNotEmpty(dto.getAuthParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("auth", dto.getAuth(), "e.auth", "Boolean", dto.getAuthParam(), param, index, locale));
			}
			if (dto.getEnableSsl() != null || Utilities.searchParamIsNotEmpty(dto.getEnableSslParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enableSsl", dto.getEnableSsl(), "e.enableSsl", "Boolean", dto.getEnableSslParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSslProtocols()) || Utilities.searchParamIsNotEmpty(dto.getSslProtocolsParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sslProtocols", dto.getSslProtocols(), "e.sslProtocols", "String", dto.getSslProtocolsParam(), param, index, locale));
			}
			if (dto.getEnableStarttls() != null || Utilities.searchParamIsNotEmpty(dto.getEnableStarttlsParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("enableStarttls", dto.getEnableStarttls(), "e.enableStarttls", "Boolean", dto.getEnableStarttlsParam(), param, index, locale));
			}
			if (dto.getIsStarttlsRequired() != null || Utilities.searchParamIsNotEmpty(dto.getIsStarttlsRequiredParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isStarttlsRequired", dto.getIsStarttlsRequired(), "e.isStarttlsRequired", "Boolean", dto.getIsStarttlsRequiredParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getHostsToTrust()) || Utilities.searchParamIsNotEmpty(dto.getHostsToTrustParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("hostsToTrust", dto.getHostsToTrust(), "e.hostsToTrust", "String", dto.getHostsToTrustParam(), param, index, locale));
			}
			if (dto.getIsSmtpsProtocol() != null || Utilities.searchParamIsNotEmpty(dto.getIsSmtpsProtocolParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isSmtpsProtocol", dto.getIsSmtpsProtocol(), "e.isSmtpsProtocol", "Boolean", dto.getIsSmtpsProtocolParam(), param, index, locale));
			}
			if (dto.getIsActive() != null || Utilities.searchParamIsNotEmpty(dto.getIsActiveParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isActive", dto.getIsActive(), "e.isActive", "Boolean", dto.getIsActiveParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSenderUsername()) || Utilities.searchParamIsNotEmpty(dto.getSenderUsernameParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("senderUsername", dto.getSenderUsername(), "e.senderUsername", "String", dto.getSenderUsernameParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSenderEmail()) || Utilities.searchParamIsNotEmpty(dto.getSenderEmailParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("senderEmail", dto.getSenderEmail(), "e.senderEmail", "String", dto.getSenderEmailParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getTestEmail()) || Utilities.searchParamIsNotEmpty(dto.getTestEmailParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("testEmail", dto.getTestEmail(), "e.testEmail", "String", dto.getTestEmailParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
