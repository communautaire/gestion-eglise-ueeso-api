package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.dto.SousPrefectureDto;

/**
 * Repository customize : SousPrefecture.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _SousPrefectureRepository {
	default List<String> _generateCriteria(SousPrefectureDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
}
