

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._AdresseRepository;

/**
 * Repository : Adresse.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Integer>, _AdresseRepository {
	/**
	 * Finds Adresse by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Adresse whose id is equals to the given id. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.id = :id and e.isDeleted = :isDeleted")
	Adresse findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Adresse by using adressePostale as a search criteria.
	 * 
	 * @param adressePostale
	 * @return An Object Adresse whose adressePostale is equals to the given adressePostale. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.adressePostale = :adressePostale and e.isDeleted = :isDeleted")
	List<Adresse> findByAdressePostale(@Param("adressePostale")String adressePostale, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Adresse whose createdAt is equals to the given createdAt. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Adresse> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Adresse whose updatedAt is equals to the given updatedAt. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Adresse> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Adresse whose deletedAt is equals to the given deletedAt. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Adresse> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Adresse whose createdBy is equals to the given createdBy. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Adresse> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Adresse whose updatedBy is equals to the given updatedBy. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Adresse> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Adresse whose deletedBy is equals to the given deletedBy. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Adresse> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adresse by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Adresse whose isDeleted is equals to the given isDeleted. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.isDeleted = :isDeleted")
	List<Adresse> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Adresse by using idCommune as a search criteria.
	 * 
	 * @param idCommune
	 * @return An Object Adresse whose idCommune is equals to the given idCommune. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.commune.id = :idCommune and e.isDeleted = :isDeleted")
	List<Adresse> findByIdCommune(@Param("idCommune")Integer idCommune, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Adresse by using idCommune as a search criteria.
   *
   * @param idCommune
   * @return An Object Adresse whose idCommune is equals to the given idCommune. If
   *         no Adresse is found, this method returns null.
   */
  @Query("select e from Adresse e where e.commune.id = :idCommune and e.isDeleted = :isDeleted")
  Adresse findAdresseByIdCommune(@Param("idCommune")Integer idCommune, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Adresse by using idUser as a search criteria.
	 * 
	 * @param idUser
	 * @return An Object Adresse whose idUser is equals to the given idUser. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.user.id = :idUser and e.isDeleted = :isDeleted")
	List<Adresse> findByIdUser(@Param("idUser")Integer idUser, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Adresse by using idUser as a search criteria.
   *
   * @param idUser
   * @return An Object Adresse whose idUser is equals to the given idUser. If
   *         no Adresse is found, this method returns null.
   */
  @Query("select e from Adresse e where e.user.id = :idUser and e.isDeleted = :isDeleted")
  Adresse findAdresseByIdUser(@Param("idUser")Integer idUser, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Adresse by using idPays as a search criteria.
	 * 
	 * @param idPays
	 * @return An Object Adresse whose idPays is equals to the given idPays. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
	List<Adresse> findByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Adresse by using idPays as a search criteria.
   *
   * @param idPays
   * @return An Object Adresse whose idPays is equals to the given idPays. If
   *         no Adresse is found, this method returns null.
   */
  @Query("select e from Adresse e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
  Adresse findAdresseByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Adresse by using idVille as a search criteria.
	 * 
	 * @param idVille
	 * @return An Object Adresse whose idVille is equals to the given idVille. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.ville.id = :idVille and e.isDeleted = :isDeleted")
	List<Adresse> findByIdVille(@Param("idVille")Integer idVille, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Adresse by using idVille as a search criteria.
   *
   * @param idVille
   * @return An Object Adresse whose idVille is equals to the given idVille. If
   *         no Adresse is found, this method returns null.
   */
  @Query("select e from Adresse e where e.ville.id = :idVille and e.isDeleted = :isDeleted")
  Adresse findAdresseByIdVille(@Param("idVille")Integer idVille, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Adresse by using idQuartier as a search criteria.
	 * 
	 * @param idQuartier
	 * @return An Object Adresse whose idQuartier is equals to the given idQuartier. If
	 *         no Adresse is found, this method returns null.
	 */
	@Query("select e from Adresse e where e.quartier.id = :idQuartier and e.isDeleted = :isDeleted")
	List<Adresse> findByIdQuartier(@Param("idQuartier")Integer idQuartier, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Adresse by using idQuartier as a search criteria.
   *
   * @param idQuartier
   * @return An Object Adresse whose idQuartier is equals to the given idQuartier. If
   *         no Adresse is found, this method returns null.
   */
  @Query("select e from Adresse e where e.quartier.id = :idQuartier and e.isDeleted = :isDeleted")
  Adresse findAdresseByIdQuartier(@Param("idQuartier")Integer idQuartier, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Adresse by using adresseDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Adresse
	 * @throws DataAccessException,ParseException
	 */
	public default List<Adresse> getByCriteria(Request<AdresseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Adresse e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Adresse> query = em.createQuery(req, Adresse.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Adresse by using adresseDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Adresse
	 * 
	 */
	public default Long count(Request<AdresseDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Adresse e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<AdresseDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		AdresseDto dto = request.getData() != null ? request.getData() : new AdresseDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (AdresseDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(AdresseDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getAdressePostale()) || Utilities.searchParamIsNotEmpty(dto.getAdressePostaleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adressePostale", dto.getAdressePostale(), "e.adressePostale", "String", dto.getAdressePostaleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdCommune() != null || Utilities.searchParamIsNotEmpty(dto.getIdCommuneParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idCommune", dto.getIdCommune(), "e.commune.id", "Integer", dto.getIdCommuneParam(), param, index, locale));
			}
						if (dto.getIdUser() != null || Utilities.searchParamIsNotEmpty(dto.getIdUserParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idUser", dto.getIdUser(), "e.user.id", "Integer", dto.getIdUserParam(), param, index, locale));
			}
						if (dto.getIdPays() != null || Utilities.searchParamIsNotEmpty(dto.getIdPaysParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idPays", dto.getIdPays(), "e.pays.id", "Integer", dto.getIdPaysParam(), param, index, locale));
			}
						if (dto.getIdVille() != null || Utilities.searchParamIsNotEmpty(dto.getIdVilleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idVille", dto.getIdVille(), "e.ville.id", "Integer", dto.getIdVilleParam(), param, index, locale));
			}
						if (dto.getIdQuartier() != null || Utilities.searchParamIsNotEmpty(dto.getIdQuartierParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idQuartier", dto.getIdQuartier(), "e.quartier.id", "Integer", dto.getIdQuartierParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCommuneCode()) || Utilities.searchParamIsNotEmpty(dto.getCommuneCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeCode", dto.getCommuneCode(), "e.commune.code", "String", dto.getCommuneCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCommuneLibelle()) || Utilities.searchParamIsNotEmpty(dto.getCommuneLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeLibelle", dto.getCommuneLibelle(), "e.commune.libelle", "String", dto.getCommuneLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserNom()) || Utilities.searchParamIsNotEmpty(dto.getUserNomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userNom", dto.getUserNom(), "e.user.nom", "String", dto.getUserNomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserPrenom()) || Utilities.searchParamIsNotEmpty(dto.getUserPrenomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userPrenom", dto.getUserPrenom(), "e.user.prenom", "String", dto.getUserPrenomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUserLogin()) || Utilities.searchParamIsNotEmpty(dto.getUserLoginParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("userLogin", dto.getUserLogin(), "e.user.login", "String", dto.getUserLoginParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysCode()) || Utilities.searchParamIsNotEmpty(dto.getPaysCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysCode", dto.getPaysCode(), "e.pays.code", "String", dto.getPaysCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysLibelle()) || Utilities.searchParamIsNotEmpty(dto.getPaysLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysLibelle", dto.getPaysLibelle(), "e.pays.libelle", "String", dto.getPaysLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getVilleLibelle()) || Utilities.searchParamIsNotEmpty(dto.getVilleLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villeLibelle", dto.getVilleLibelle(), "e.ville.libelle", "String", dto.getVilleLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getQuartierCode()) || Utilities.searchParamIsNotEmpty(dto.getQuartierCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("quartierCode", dto.getQuartierCode(), "e.quartier.code", "String", dto.getQuartierCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getQuartierLibelle()) || Utilities.searchParamIsNotEmpty(dto.getQuartierLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("quartierLibelle", dto.getQuartierLibelle(), "e.quartier.libelle", "String", dto.getQuartierLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
