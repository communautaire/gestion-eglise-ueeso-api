

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._PeriodeRepository;

/**
 * Repository : Periode.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface PeriodeRepository extends JpaRepository<Periode, Integer>, _PeriodeRepository {
	/**
	 * Finds Periode by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Periode whose id is equals to the given id. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.id = :id and e.isDeleted = :isDeleted")
	Periode findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Periode by using datePaiement as a search criteria.
	 * 
	 * @param datePaiement
	 * @return An Object Periode whose datePaiement is equals to the given datePaiement. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.datePaiement = :datePaiement and e.isDeleted = :isDeleted")
	List<Periode> findByDatePaiement(@Param("datePaiement")Date datePaiement, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using dayofweek as a search criteria.
	 * 
	 * @param dayofweek
	 * @return An Object Periode whose dayofweek is equals to the given dayofweek. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.dayofweek = :dayofweek and e.isDeleted = :isDeleted")
	List<Periode> findByDayofweek(@Param("dayofweek")Integer dayofweek, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using dayofmonth as a search criteria.
	 * 
	 * @param dayofmonth
	 * @return An Object Periode whose dayofmonth is equals to the given dayofmonth. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.dayofmonth = :dayofmonth and e.isDeleted = :isDeleted")
	List<Periode> findByDayofmonth(@Param("dayofmonth")Integer dayofmonth, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using dayofyear as a search criteria.
	 * 
	 * @param dayofyear
	 * @return An Object Periode whose dayofyear is equals to the given dayofyear. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.dayofyear = :dayofyear and e.isDeleted = :isDeleted")
	List<Periode> findByDayofyear(@Param("dayofyear")Integer dayofyear, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using weekofmonth as a search criteria.
	 * 
	 * @param weekofmonth
	 * @return An Object Periode whose weekofmonth is equals to the given weekofmonth. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.weekofmonth = :weekofmonth and e.isDeleted = :isDeleted")
	List<Periode> findByWeekofmonth(@Param("weekofmonth")Integer weekofmonth, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using weekofyear as a search criteria.
	 * 
	 * @param weekofyear
	 * @return An Object Periode whose weekofyear is equals to the given weekofyear. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.weekofyear = :weekofyear and e.isDeleted = :isDeleted")
	List<Periode> findByWeekofyear(@Param("weekofyear")Integer weekofyear, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using month as a search criteria.
	 * 
	 * @param month
	 * @return An Object Periode whose month is equals to the given month. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.month = :month and e.isDeleted = :isDeleted")
	List<Periode> findByMonth(@Param("month")Integer month, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using trimester as a search criteria.
	 * 
	 * @param trimester
	 * @return An Object Periode whose trimester is equals to the given trimester. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.trimester = :trimester and e.isDeleted = :isDeleted")
	List<Periode> findByTrimester(@Param("trimester")Integer trimester, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using semester as a search criteria.
	 * 
	 * @param semester
	 * @return An Object Periode whose semester is equals to the given semester. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.semester = :semester and e.isDeleted = :isDeleted")
	List<Periode> findBySemester(@Param("semester")Integer semester, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using year as a search criteria.
	 * 
	 * @param year
	 * @return An Object Periode whose year is equals to the given year. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.year = :year and e.isDeleted = :isDeleted")
	List<Periode> findByYear(@Param("year")Integer year, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Periode whose createdAt is equals to the given createdAt. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Periode> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Periode whose updatedAt is equals to the given updatedAt. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Periode> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Periode whose deletedAt is equals to the given deletedAt. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Periode> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Periode whose createdBy is equals to the given createdBy. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Periode> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Periode whose updatedBy is equals to the given updatedBy. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Periode> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Periode whose deletedBy is equals to the given deletedBy. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Periode> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Periode by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Periode whose isDeleted is equals to the given isDeleted. If
	 *         no Periode is found, this method returns null.
	 */
	@Query("select e from Periode e where e.isDeleted = :isDeleted")
	List<Periode> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);



	/**
	 * Finds List of Periode by using periodeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Periode
	 * @throws DataAccessException,ParseException
	 */
	public default List<Periode> getByCriteria(Request<PeriodeDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Periode e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Periode> query = em.createQuery(req, Periode.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Periode by using periodeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Periode
	 * 
	 */
	public default Long count(Request<PeriodeDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Periode e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PeriodeDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		PeriodeDto dto = request.getData() != null ? request.getData() : new PeriodeDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PeriodeDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PeriodeDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (Utilities.isNotBlank(dto.getDatePaiement()) || Utilities.searchParamIsNotEmpty(dto.getDatePaiementParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("datePaiement", dto.getDatePaiement(), "e.datePaiement", "Date", dto.getDatePaiementParam(), param, index, locale));
			}
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getDayofweek() != null || Utilities.searchParamIsNotEmpty(dto.getDayofweekParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dayofweek", dto.getDayofweek(), "e.dayofweek", "Integer", dto.getDayofweekParam(), param, index, locale));
			}
			if (dto.getDayofmonth() != null || Utilities.searchParamIsNotEmpty(dto.getDayofmonthParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dayofmonth", dto.getDayofmonth(), "e.dayofmonth", "Integer", dto.getDayofmonthParam(), param, index, locale));
			}
			if (dto.getDayofyear() != null || Utilities.searchParamIsNotEmpty(dto.getDayofyearParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dayofyear", dto.getDayofyear(), "e.dayofyear", "Integer", dto.getDayofyearParam(), param, index, locale));
			}
			if (dto.getWeekofmonth() != null || Utilities.searchParamIsNotEmpty(dto.getWeekofmonthParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("weekofmonth", dto.getWeekofmonth(), "e.weekofmonth", "Integer", dto.getWeekofmonthParam(), param, index, locale));
			}
			if (dto.getWeekofyear() != null || Utilities.searchParamIsNotEmpty(dto.getWeekofyearParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("weekofyear", dto.getWeekofyear(), "e.weekofyear", "Integer", dto.getWeekofyearParam(), param, index, locale));
			}
			if (dto.getMonth() != null || Utilities.searchParamIsNotEmpty(dto.getMonthParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("month", dto.getMonth(), "e.month", "Integer", dto.getMonthParam(), param, index, locale));
			}
			if (dto.getTrimester() != null || Utilities.searchParamIsNotEmpty(dto.getTrimesterParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("trimester", dto.getTrimester(), "e.trimester", "Integer", dto.getTrimesterParam(), param, index, locale));
			}
			if (dto.getSemester() != null || Utilities.searchParamIsNotEmpty(dto.getSemesterParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("semester", dto.getSemester(), "e.semester", "Integer", dto.getSemesterParam(), param, index, locale));
			}
			if (dto.getYear() != null || Utilities.searchParamIsNotEmpty(dto.getYearParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("year", dto.getYear(), "e.year", "Integer", dto.getYearParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
