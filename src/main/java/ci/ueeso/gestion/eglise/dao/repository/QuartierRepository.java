

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._QuartierRepository;

/**
 * Repository : Quartier.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface QuartierRepository extends JpaRepository<Quartier, Integer>, _QuartierRepository {
	/**
	 * Finds Quartier by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Quartier whose id is equals to the given id. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.id = :id and e.isDeleted = :isDeleted")
	Quartier findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Quartier by using code as a search criteria.
	 * 
	 * @param code
	 * @return An Object Quartier whose code is equals to the given code. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.code = :code and e.isDeleted = :isDeleted")
	Quartier findByCode(@Param("code")String code, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Quartier whose libelle is equals to the given libelle. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Quartier findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using longitude as a search criteria.
	 * 
	 * @param longitude
	 * @return An Object Quartier whose longitude is equals to the given longitude. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.longitude = :longitude and e.isDeleted = :isDeleted")
	List<Quartier> findByLongitude(@Param("longitude")Double longitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using latitude as a search criteria.
	 * 
	 * @param latitude
	 * @return An Object Quartier whose latitude is equals to the given latitude. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.latitude = :latitude and e.isDeleted = :isDeleted")
	List<Quartier> findByLatitude(@Param("latitude")Double latitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using superficie as a search criteria.
	 * 
	 * @param superficie
	 * @return An Object Quartier whose superficie is equals to the given superficie. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.superficie = :superficie and e.isDeleted = :isDeleted")
	List<Quartier> findBySuperficie(@Param("superficie")Double superficie, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Quartier whose createdAt is equals to the given createdAt. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Quartier> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Quartier whose updatedAt is equals to the given updatedAt. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Quartier> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Quartier whose deletedAt is equals to the given deletedAt. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Quartier> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Quartier whose createdBy is equals to the given createdBy. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Quartier> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Quartier whose updatedBy is equals to the given updatedBy. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Quartier> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Quartier whose deletedBy is equals to the given deletedBy. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Quartier> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Quartier by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Quartier whose isDeleted is equals to the given isDeleted. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.isDeleted = :isDeleted")
	List<Quartier> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Quartier by using idVillage as a search criteria.
	 * 
	 * @param idVillage
	 * @return An Object Quartier whose idVillage is equals to the given idVillage. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.village.id = :idVillage and e.isDeleted = :isDeleted")
	List<Quartier> findByIdVillage(@Param("idVillage")Integer idVillage, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Quartier by using idVillage as a search criteria.
   *
   * @param idVillage
   * @return An Object Quartier whose idVillage is equals to the given idVillage. If
   *         no Quartier is found, this method returns null.
   */
  @Query("select e from Quartier e where e.village.id = :idVillage and e.isDeleted = :isDeleted")
  Quartier findQuartierByIdVillage(@Param("idVillage")Integer idVillage, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Quartier by using idCommune as a search criteria.
	 * 
	 * @param idCommune
	 * @return An Object Quartier whose idCommune is equals to the given idCommune. If
	 *         no Quartier is found, this method returns null.
	 */
	@Query("select e from Quartier e where e.commune.id = :idCommune and e.isDeleted = :isDeleted")
	List<Quartier> findByIdCommune(@Param("idCommune")Integer idCommune, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Quartier by using idCommune as a search criteria.
   *
   * @param idCommune
   * @return An Object Quartier whose idCommune is equals to the given idCommune. If
   *         no Quartier is found, this method returns null.
   */
  @Query("select e from Quartier e where e.commune.id = :idCommune and e.isDeleted = :isDeleted")
  Quartier findQuartierByIdCommune(@Param("idCommune")Integer idCommune, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Quartier by using quartierDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Quartier
	 * @throws DataAccessException,ParseException
	 */
	public default List<Quartier> getByCriteria(Request<QuartierDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Quartier e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Quartier> query = em.createQuery(req, Quartier.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Quartier by using quartierDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Quartier
	 * 
	 */
	public default Long count(Request<QuartierDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Quartier e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<QuartierDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		QuartierDto dto = request.getData() != null ? request.getData() : new QuartierDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (QuartierDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(QuartierDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCode()) || Utilities.searchParamIsNotEmpty(dto.getCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("code", dto.getCode(), "e.code", "String", dto.getCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLibelle()) || Utilities.searchParamIsNotEmpty(dto.getLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getLongitude() != null || Utilities.searchParamIsNotEmpty(dto.getLongitudeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("longitude", dto.getLongitude(), "e.longitude", "Double", dto.getLongitudeParam(), param, index, locale));
			}
			if (dto.getLatitude() != null || Utilities.searchParamIsNotEmpty(dto.getLatitudeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("latitude", dto.getLatitude(), "e.latitude", "Double", dto.getLatitudeParam(), param, index, locale));
			}
			if (dto.getSuperficie() != null || Utilities.searchParamIsNotEmpty(dto.getSuperficieParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("superficie", dto.getSuperficie(), "e.superficie", "Double", dto.getSuperficieParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdVillage() != null || Utilities.searchParamIsNotEmpty(dto.getIdVillageParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idVillage", dto.getIdVillage(), "e.village.id", "Integer", dto.getIdVillageParam(), param, index, locale));
			}
						if (dto.getIdCommune() != null || Utilities.searchParamIsNotEmpty(dto.getIdCommuneParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idCommune", dto.getIdCommune(), "e.commune.id", "Integer", dto.getIdCommuneParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getVillageLibelle()) || Utilities.searchParamIsNotEmpty(dto.getVillageLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("villageLibelle", dto.getVillageLibelle(), "e.village.libelle", "String", dto.getVillageLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCommuneCode()) || Utilities.searchParamIsNotEmpty(dto.getCommuneCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeCode", dto.getCommuneCode(), "e.commune.code", "String", dto.getCommuneCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCommuneLibelle()) || Utilities.searchParamIsNotEmpty(dto.getCommuneLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("communeLibelle", dto.getCommuneLibelle(), "e.commune.libelle", "String", dto.getCommuneLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
