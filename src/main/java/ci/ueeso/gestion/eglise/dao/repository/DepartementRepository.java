

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.Departement;
import ci.ueeso.gestion.eglise.dao.repository.customize._DepartementRepository;
import ci.ueeso.gestion.eglise.helper.CriteriaUtils;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.dto.DepartementDto;

/**
 * Repository : Departement.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface DepartementRepository extends JpaRepository<Departement, Integer>, _DepartementRepository {
	/**
	 * Finds Departement by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Departement whose id is equals to the given id. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.id = :id and e.isDeleted = :isDeleted")
	Departement findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Departement by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Departement whose libelle is equals to the given libelle. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Departement findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using chefLieu as a search criteria.
	 * 
	 * @param chefLieu
	 * @return An Object Departement whose chefLieu is equals to the given chefLieu. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.chefLieu = :chefLieu and e.isDeleted = :isDeleted")
	List<Departement> findByChefLieu(@Param("chefLieu")String chefLieu, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Departement whose createdAt is equals to the given createdAt. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Departement> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Departement whose updatedAt is equals to the given updatedAt. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Departement> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Departement whose deletedAt is equals to the given deletedAt. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Departement> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Departement whose createdBy is equals to the given createdBy. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Departement> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Departement whose updatedBy is equals to the given updatedBy. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Departement> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Departement by using deleted as a search criteria.
	 * 
	 * @param deleted
	 * @return An Object Departement whose deleted is equals to the given deleted. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.deleted = :deleted")
	List<Departement> findByDeleted(@Param("deleted")Integer deleted);
	/**
	 * Finds Departement by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Departement whose isDeleted is equals to the given isDeleted. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.isDeleted = :isDeleted")
	List<Departement> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Departement by using idRegion as a search criteria.
	 * 
	 * @param idRegion
	 * @return An Object Departement whose idRegion is equals to the given idRegion. If
	 *         no Departement is found, this method returns null.
	 */
	@Query("select e from Departement e where e.region.id = :idRegion and e.isDeleted = :isDeleted")
	List<Departement> findByIdRegion(@Param("idRegion")Integer idRegion, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Departement by using idRegion as a search criteria.
   *
   * @param idRegion
   * @return An Object Departement whose idRegion is equals to the given idRegion. If
   *         no Departement is found, this method returns null.
   */
  @Query("select e from Departement e where e.region.id = :idRegion and e.isDeleted = :isDeleted")
  Departement findDepartementByIdRegion(@Param("idRegion")Integer idRegion, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Departement by using departementDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Departement
	 * @throws DataAccessException,ParseException
	 */
	public default List<Departement> getByCriteria(Request<DepartementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Departement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Departement> query = em.createQuery(req, Departement.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Departement by using departementDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Departement
	 * 
	 */
	public default Long count(Request<DepartementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Departement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<DepartementDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		DepartementDto dto = request.getData() != null ? request.getData() : new DepartementDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (DepartementDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(DepartementDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLibelle()) || Utilities.searchParamIsNotEmpty(dto.getLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getChefLieu()) || Utilities.searchParamIsNotEmpty(dto.getChefLieuParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("chefLieu", dto.getChefLieu(), "e.chefLieu", "String", dto.getChefLieuParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deleted", dto.getDeleted(), "e.deleted", "Integer", dto.getDeletedParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdRegion() != null || Utilities.searchParamIsNotEmpty(dto.getIdRegionParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idRegion", dto.getIdRegion(), "e.region.id", "Integer", dto.getIdRegionParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getRegionLibelle()) || Utilities.searchParamIsNotEmpty(dto.getRegionLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("regionLibelle", dto.getRegionLibelle(), "e.region.libelle", "String", dto.getRegionLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
