

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._PaiementRepository;

/**
 * Repository : Paiement.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface PaiementRepository extends JpaRepository<Paiement, Integer>, _PaiementRepository {
	/**
	 * Finds Paiement by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Paiement whose id is equals to the given id. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.id = :id and e.isDeleted = :isDeleted")
	Paiement findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Paiement by using montant as a search criteria.
	 * 
	 * @param montant
	 * @return An Object Paiement whose montant is equals to the given montant. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.montant = :montant and e.isDeleted = :isDeleted")
	List<Paiement> findByMontant(@Param("montant")Integer montant, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Paiement whose createdAt is equals to the given createdAt. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Paiement> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Paiement whose updatedAt is equals to the given updatedAt. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Paiement> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Paiement whose deletedAt is equals to the given deletedAt. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Paiement> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Paiement whose createdBy is equals to the given createdBy. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Paiement> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Paiement whose updatedBy is equals to the given updatedBy. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Paiement> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Paiement whose deletedBy is equals to the given deletedBy. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Paiement> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Paiement by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Paiement whose isDeleted is equals to the given isDeleted. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.isDeleted = :isDeleted")
	List<Paiement> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Paiement by using idEngagement as a search criteria.
	 * 
	 * @param idEngagement
	 * @return An Object Paiement whose idEngagement is equals to the given idEngagement. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.engagement.id = :idEngagement and e.isDeleted = :isDeleted")
	List<Paiement> findByIdEngagement(@Param("idEngagement")Integer idEngagement, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Paiement by using idEngagement as a search criteria.
   *
   * @param idEngagement
   * @return An Object Paiement whose idEngagement is equals to the given idEngagement. If
   *         no Paiement is found, this method returns null.
   */
  @Query("select e from Paiement e where e.engagement.id = :idEngagement and e.isDeleted = :isDeleted")
  Paiement findPaiementByIdEngagement(@Param("idEngagement")Integer idEngagement, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Paiement by using idDatePaiement as a search criteria.
	 * 
	 * @param idDatePaiement
	 * @return An Object Paiement whose idDatePaiement is equals to the given idDatePaiement. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.periode.id = :idDatePaiement and e.isDeleted = :isDeleted")
	List<Paiement> findByIdDatePaiement(@Param("idDatePaiement")Integer idDatePaiement, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Paiement by using idDatePaiement as a search criteria.
   *
   * @param idDatePaiement
   * @return An Object Paiement whose idDatePaiement is equals to the given idDatePaiement. If
   *         no Paiement is found, this method returns null.
   */
  @Query("select e from Paiement e where e.periode.id = :idDatePaiement and e.isDeleted = :isDeleted")
  Paiement findPaiementByIdDatePaiement(@Param("idDatePaiement")Integer idDatePaiement, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds Paiement by using idTresorier as a search criteria.
	 * 
	 * @param idTresorier
	 * @return An Object Paiement whose idTresorier is equals to the given idTresorier. If
	 *         no Paiement is found, this method returns null.
	 */
	@Query("select e from Paiement e where e.tresorier.id = :idTresorier and e.isDeleted = :isDeleted")
	List<Paiement> findByIdTresorier(@Param("idTresorier")Integer idTresorier, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Paiement by using idTresorier as a search criteria.
   *
   * @param idTresorier
   * @return An Object Paiement whose idTresorier is equals to the given idTresorier. If
   *         no Paiement is found, this method returns null.
   */
  @Query("select e from Paiement e where e.tresorier.id = :idTresorier and e.isDeleted = :isDeleted")
  Paiement findPaiementByIdTresorier(@Param("idTresorier")Integer idTresorier, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Paiement by using paiementDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Paiement
	 * @throws DataAccessException,ParseException
	 */
	public default List<Paiement> getByCriteria(Request<PaiementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Paiement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Paiement> query = em.createQuery(req, Paiement.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Paiement by using paiementDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Paiement
	 * 
	 */
	public default Long count(Request<PaiementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Paiement e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<PaiementDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		PaiementDto dto = request.getData() != null ? request.getData() : new PaiementDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (PaiementDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(PaiementDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getMontant() != null || Utilities.searchParamIsNotEmpty(dto.getMontantParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("montant", dto.getMontant(), "e.montant", "Integer", dto.getMontantParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdEngagement() != null || Utilities.searchParamIsNotEmpty(dto.getIdEngagementParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idEngagement", dto.getIdEngagement(), "e.engagement.id", "Integer", dto.getIdEngagementParam(), param, index, locale));
			}
						if (dto.getIdDatePaiement() != null || Utilities.searchParamIsNotEmpty(dto.getIdDatePaiementParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idDatePaiement", dto.getIdDatePaiement(), "e.periode.id", "Integer", dto.getIdDatePaiementParam(), param, index, locale));
			}
						if (dto.getIdTresorier() != null || Utilities.searchParamIsNotEmpty(dto.getIdTresorierParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idTresorier", dto.getIdTresorier(), "e.tresorier.id", "Integer", dto.getIdTresorierParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
