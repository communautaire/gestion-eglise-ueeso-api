package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.UserGroupeConstitue;
import ci.ueeso.gestion.eglise.helper.dto.UserGroupeConstitueDto;

/**
 * Repository customize : UserGroupeConstitue.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _UserGroupeConstitueRepository {
	default List<String> _generateCriteria(UserGroupeConstitueDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	@Query("select e from UserGroupeConstitue e where e.user.id = :idUser and e.groupeConstitue.id = :idGroupeConstitue and e.isDeleted = :isDeleted")
	UserGroupeConstitue findByIdUserAndIdGroupeConstitue(@Param("idUser")Integer idUser, @Param("idGroupeConstitue")Integer idGroupeConstitue, @Param("isDeleted")Boolean isDeleted);

}
