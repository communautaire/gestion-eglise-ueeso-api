

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._UserRepository;

/**
 * Repository : User.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>, _UserRepository {
	/**
	 * Finds User by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object User whose id is equals to the given id. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.id = :id and e.isDeleted = :isDeleted")
	User findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using nom as a search criteria.
	 * 
	 * @param nom
	 * @return An Object User whose nom is equals to the given nom. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<User> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using prenom as a search criteria.
	 * 
	 * @param prenom
	 * @return An Object User whose prenom is equals to the given prenom. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.prenom = :prenom and e.isDeleted = :isDeleted")
	List<User> findByPrenom(@Param("prenom")String prenom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using email as a search criteria.
	 * 
	 * @param email
	 * @return An Object User whose email is equals to the given email. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.email = :email and e.isDeleted = :isDeleted")
	User findByEmail(@Param("email")String email, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using telephone as a search criteria.
	 * 
	 * @param telephone
	 * @return An Object User whose telephone is equals to the given telephone. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.telephone = :telephone and e.isDeleted = :isDeleted")
	User findByTelephone(@Param("telephone")String telephone, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using login as a search criteria.
	 * 
	 * @param login
	 * @return An Object User whose login is equals to the given login. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.login = :login and e.isDeleted = :isDeleted")
	User findByLogin(@Param("login")String login, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using preGeneratedLogin as a search criteria.
	 * 
	 * @param preGeneratedLogin
	 * @return An Object User whose preGeneratedLogin is equals to the given preGeneratedLogin. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.preGeneratedLogin = :preGeneratedLogin and e.isDeleted = :isDeleted")
	List<User> findByPreGeneratedLogin(@Param("preGeneratedLogin")String preGeneratedLogin, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using password as a search criteria.
	 * 
	 * @param password
	 * @return An Object User whose password is equals to the given password. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.password = :password and e.isDeleted = :isDeleted")
	List<User> findByPassword(@Param("password")String password, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateNaissance as a search criteria.
	 * 
	 * @param dateNaissance
	 * @return An Object User whose dateNaissance is equals to the given dateNaissance. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateNaissance = :dateNaissance and e.isDeleted = :isDeleted")
	List<User> findByDateNaissance(@Param("dateNaissance")Date dateNaissance, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using photo as a search criteria.
	 * 
	 * @param photo
	 * @return An Object User whose photo is equals to the given photo. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.photo = :photo and e.isDeleted = :isDeleted")
	List<User> findByPhoto(@Param("photo")String photo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isLocalResident as a search criteria.
	 * 
	 * @param isLocalResident
	 * @return An Object User whose isLocalResident is equals to the given isLocalResident. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isLocalResident = :isLocalResident and e.isDeleted = :isDeleted")
	List<User> findByIsLocalResident(@Param("isLocalResident")Boolean isLocalResident, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using nomConjoint as a search criteria.
	 * 
	 * @param nomConjoint
	 * @return An Object User whose nomConjoint is equals to the given nomConjoint. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.nomConjoint = :nomConjoint and e.isDeleted = :isDeleted")
	List<User> findByNomConjoint(@Param("nomConjoint")String nomConjoint, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateVieConjugale as a search criteria.
	 * 
	 * @param dateVieConjugale
	 * @return An Object User whose dateVieConjugale is equals to the given dateVieConjugale. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateVieConjugale = :dateVieConjugale and e.isDeleted = :isDeleted")
	List<User> findByDateVieConjugale(@Param("dateVieConjugale")Date dateVieConjugale, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isHomeRespo as a search criteria.
	 * 
	 * @param isHomeRespo
	 * @return An Object User whose isHomeRespo is equals to the given isHomeRespo. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isHomeRespo = :isHomeRespo and e.isDeleted = :isDeleted")
	List<User> findByIsHomeRespo(@Param("isHomeRespo")Boolean isHomeRespo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using homeRespoName as a search criteria.
	 * 
	 * @param homeRespoName
	 * @return An Object User whose homeRespoName is equals to the given homeRespoName. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.homeRespoName = :homeRespoName and e.isDeleted = :isDeleted")
	List<User> findByHomeRespoName(@Param("homeRespoName")String homeRespoName, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateArriveUeeso as a search criteria.
	 * 
	 * @param dateArriveUeeso
	 * @return An Object User whose dateArriveUeeso is equals to the given dateArriveUeeso. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateArriveUeeso = :dateArriveUeeso and e.isDeleted = :isDeleted")
	List<User> findByDateArriveUeeso(@Param("dateArriveUeeso")Date dateArriveUeeso, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateConversion as a search criteria.
	 * 
	 * @param dateConversion
	 * @return An Object User whose dateConversion is equals to the given dateConversion. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateConversion = :dateConversion and e.isDeleted = :isDeleted")
	List<User> findByDateConversion(@Param("dateConversion")Date dateConversion, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using baptemeImmersion as a search criteria.
	 * 
	 * @param baptemeImmersion
	 * @return An Object User whose baptemeImmersion is equals to the given baptemeImmersion. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.baptemeImmersion = :baptemeImmersion and e.isDeleted = :isDeleted")
	List<User> findByBaptemeImmersion(@Param("baptemeImmersion")Boolean baptemeImmersion, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using dateBapteme as a search criteria.
	 * 
	 * @param dateBapteme
	 * @return An Object User whose dateBapteme is equals to the given dateBapteme. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.dateBapteme = :dateBapteme and e.isDeleted = :isDeleted")
	List<User> findByDateBapteme(@Param("dateBapteme")Date dateBapteme, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using egliseBapteme as a search criteria.
	 * 
	 * @param egliseBapteme
	 * @return An Object User whose egliseBapteme is equals to the given egliseBapteme. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.egliseBapteme = :egliseBapteme and e.isDeleted = :isDeleted")
	List<User> findByEgliseBapteme(@Param("egliseBapteme")String egliseBapteme, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using egliseOrigine as a search criteria.
	 * 
	 * @param egliseOrigine
	 * @return An Object User whose egliseOrigine is equals to the given egliseOrigine. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.egliseOrigine = :egliseOrigine and e.isDeleted = :isDeleted")
	List<User> findByEgliseOrigine(@Param("egliseOrigine")String egliseOrigine, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object User whose createdAt is equals to the given createdAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<User> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object User whose updatedAt is equals to the given updatedAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<User> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object User whose deletedAt is equals to the given deletedAt. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<User> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object User whose createdBy is equals to the given createdBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<User> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object User whose updatedBy is equals to the given updatedBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<User> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object User whose deletedBy is equals to the given deletedBy. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<User> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object User whose isDeleted is equals to the given isDeleted. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isDeleted = :isDeleted")
	List<User> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using locked as a search criteria.
	 * 
	 * @param locked
	 * @return An Object User whose locked is equals to the given locked. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.locked = :locked and e.isDeleted = :isDeleted")
	List<User> findByLocked(@Param("locked")Boolean locked, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isConnected as a search criteria.
	 * 
	 * @param isConnected
	 * @return An Object User whose isConnected is equals to the given isConnected. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isConnected = :isConnected and e.isDeleted = :isDeleted")
	List<User> findByIsConnected(@Param("isConnected")Boolean isConnected, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using isValidated as a search criteria.
	 * 
	 * @param isValidated
	 * @return An Object User whose isValidated is equals to the given isValidated. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.isValidated = :isValidated and e.isDeleted = :isDeleted")
	List<User> findByIsValidated(@Param("isValidated")Boolean isValidated, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using lieuNaissance as a search criteria.
	 * 
	 * @param lieuNaissance
	 * @return An Object User whose lieuNaissance is equals to the given lieuNaissance. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.lieuNaissance = :lieuNaissance and e.isDeleted = :isDeleted")
	List<User> findByLieuNaissance(@Param("lieuNaissance")String lieuNaissance, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using domaineCompetence as a search criteria.
	 * 
	 * @param domaineCompetence
	 * @return An Object User whose domaineCompetence is equals to the given domaineCompetence. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.domaineCompetence = :domaineCompetence and e.isDeleted = :isDeleted")
	List<User> findByDomaineCompetence(@Param("domaineCompetence")String domaineCompetence, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds User by using civilite as a search criteria.
	 * 
	 * @param civilite
	 * @return An Object User whose civilite is equals to the given civilite. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.civilite = :civilite and e.isDeleted = :isDeleted")
	List<User> findByCivilite(@Param("civilite")String civilite, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds User by using idProfession as a search criteria.
	 * 
	 * @param idProfession
	 * @return An Object User whose idProfession is equals to the given idProfession. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.profession.id = :idProfession and e.isDeleted = :isDeleted")
	List<User> findByIdProfession(@Param("idProfession")Integer idProfession, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idProfession as a search criteria.
   *
   * @param idProfession
   * @return An Object User whose idProfession is equals to the given idProfession. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.profession.id = :idProfession and e.isDeleted = :isDeleted")
  User findUserByIdProfession(@Param("idProfession")Integer idProfession, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using idPays as a search criteria.
	 * 
	 * @param idPays
	 * @return An Object User whose idPays is equals to the given idPays. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
	List<User> findByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idPays as a search criteria.
   *
   * @param idPays
   * @return An Object User whose idPays is equals to the given idPays. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
  User findUserByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using idMemberStatus as a search criteria.
	 * 
	 * @param idMemberStatus
	 * @return An Object User whose idMemberStatus is equals to the given idMemberStatus. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.memberStatus.id = :idMemberStatus and e.isDeleted = :isDeleted")
	List<User> findByIdMemberStatus(@Param("idMemberStatus")Integer idMemberStatus, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idMemberStatus as a search criteria.
   *
   * @param idMemberStatus
   * @return An Object User whose idMemberStatus is equals to the given idMemberStatus. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.memberStatus.id = :idMemberStatus and e.isDeleted = :isDeleted")
  User findUserByIdMemberStatus(@Param("idMemberStatus")Integer idMemberStatus, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using idSituationMatrimoniale as a search criteria.
	 * 
	 * @param idSituationMatrimoniale
	 * @return An Object User whose idSituationMatrimoniale is equals to the given idSituationMatrimoniale. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.situationMatrimoniale.id = :idSituationMatrimoniale and e.isDeleted = :isDeleted")
	List<User> findByIdSituationMatrimoniale(@Param("idSituationMatrimoniale")Integer idSituationMatrimoniale, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idSituationMatrimoniale as a search criteria.
   *
   * @param idSituationMatrimoniale
   * @return An Object User whose idSituationMatrimoniale is equals to the given idSituationMatrimoniale. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.situationMatrimoniale.id = :idSituationMatrimoniale and e.isDeleted = :isDeleted")
  User findUserByIdSituationMatrimoniale(@Param("idSituationMatrimoniale")Integer idSituationMatrimoniale, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using idSituationProfessionnelle as a search criteria.
	 * 
	 * @param idSituationProfessionnelle
	 * @return An Object User whose idSituationProfessionnelle is equals to the given idSituationProfessionnelle. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.situationProfessionnelle.id = :idSituationProfessionnelle and e.isDeleted = :isDeleted")
	List<User> findByIdSituationProfessionnelle(@Param("idSituationProfessionnelle")Integer idSituationProfessionnelle, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idSituationProfessionnelle as a search criteria.
   *
   * @param idSituationProfessionnelle
   * @return An Object User whose idSituationProfessionnelle is equals to the given idSituationProfessionnelle. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.situationProfessionnelle.id = :idSituationProfessionnelle and e.isDeleted = :isDeleted")
  User findUserByIdSituationProfessionnelle(@Param("idSituationProfessionnelle")Integer idSituationProfessionnelle, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using idTypeUser as a search criteria.
	 * 
	 * @param idTypeUser
	 * @return An Object User whose idTypeUser is equals to the given idTypeUser. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.typeUser.id = :idTypeUser and e.isDeleted = :isDeleted")
	List<User> findByIdTypeUser(@Param("idTypeUser")Integer idTypeUser, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idTypeUser as a search criteria.
   *
   * @param idTypeUser
   * @return An Object User whose idTypeUser is equals to the given idTypeUser. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.typeUser.id = :idTypeUser and e.isDeleted = :isDeleted")
  User findUserByIdTypeUser(@Param("idTypeUser")Integer idTypeUser, @Param("isDeleted")Boolean isDeleted);


	/**
	 * Finds User by using idRole as a search criteria.
	 * 
	 * @param idRole
	 * @return An Object User whose idRole is equals to the given idRole. If
	 *         no User is found, this method returns null.
	 */
	@Query("select e from User e where e.role.id = :idRole and e.isDeleted = :isDeleted")
	List<User> findByIdRole(@Param("idRole")Integer idRole, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one User by using idRole as a search criteria.
   *
   * @param idRole
   * @return An Object User whose idRole is equals to the given idRole. If
   *         no User is found, this method returns null.
   */
  @Query("select e from User e where e.role.id = :idRole and e.isDeleted = :isDeleted")
  User findUserByIdRole(@Param("idRole")Integer idRole, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of User by using userDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of User
	 * @throws DataAccessException,ParseException
	 */
	public default List<User> getByCriteria(Request<UserDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from User e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<User> query = em.createQuery(req, User.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of User by using userDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of User
	 * 
	 */
	public default Long count(Request<UserDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from User e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<UserDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		UserDto dto = request.getData() != null ? request.getData() : new UserDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (UserDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(UserDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getNom()) || Utilities.searchParamIsNotEmpty(dto.getNomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPrenom()) || Utilities.searchParamIsNotEmpty(dto.getPrenomParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenom", dto.getPrenom(), "e.prenom", "String", dto.getPrenomParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getEmail()) || Utilities.searchParamIsNotEmpty(dto.getEmailParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("email", dto.getEmail(), "e.email", "String", dto.getEmailParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getTelephone()) || Utilities.searchParamIsNotEmpty(dto.getTelephoneParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone", dto.getTelephone(), "e.telephone", "String", dto.getTelephoneParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLogin()) || Utilities.searchParamIsNotEmpty(dto.getLoginParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("login", dto.getLogin(), "e.login", "String", dto.getLoginParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPreGeneratedLogin()) || Utilities.searchParamIsNotEmpty(dto.getPreGeneratedLoginParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("preGeneratedLogin", dto.getPreGeneratedLogin(), "e.preGeneratedLogin", "String", dto.getPreGeneratedLoginParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPassword()) || Utilities.searchParamIsNotEmpty(dto.getPasswordParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("password", dto.getPassword(), "e.password", "String", dto.getPasswordParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDateNaissance()) || Utilities.searchParamIsNotEmpty(dto.getDateNaissanceParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateNaissance", dto.getDateNaissance(), "e.dateNaissance", "Date", dto.getDateNaissanceParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPhoto()) || Utilities.searchParamIsNotEmpty(dto.getPhotoParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("photo", dto.getPhoto(), "e.photo", "String", dto.getPhotoParam(), param, index, locale));
			}
			if (dto.getIsLocalResident() != null || Utilities.searchParamIsNotEmpty(dto.getIsLocalResidentParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isLocalResident", dto.getIsLocalResident(), "e.isLocalResident", "Boolean", dto.getIsLocalResidentParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getNomConjoint()) || Utilities.searchParamIsNotEmpty(dto.getNomConjointParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nomConjoint", dto.getNomConjoint(), "e.nomConjoint", "String", dto.getNomConjointParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDateVieConjugale()) || Utilities.searchParamIsNotEmpty(dto.getDateVieConjugaleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateVieConjugale", dto.getDateVieConjugale(), "e.dateVieConjugale", "Date", dto.getDateVieConjugaleParam(), param, index, locale));
			}
			if (dto.getIsHomeRespo() != null || Utilities.searchParamIsNotEmpty(dto.getIsHomeRespoParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isHomeRespo", dto.getIsHomeRespo(), "e.isHomeRespo", "Boolean", dto.getIsHomeRespoParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getHomeRespoName()) || Utilities.searchParamIsNotEmpty(dto.getHomeRespoNameParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("homeRespoName", dto.getHomeRespoName(), "e.homeRespoName", "String", dto.getHomeRespoNameParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDateArriveUeeso()) || Utilities.searchParamIsNotEmpty(dto.getDateArriveUeesoParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateArriveUeeso", dto.getDateArriveUeeso(), "e.dateArriveUeeso", "Date", dto.getDateArriveUeesoParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDateConversion()) || Utilities.searchParamIsNotEmpty(dto.getDateConversionParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateConversion", dto.getDateConversion(), "e.dateConversion", "Date", dto.getDateConversionParam(), param, index, locale));
			}
			if (dto.getBaptemeImmersion() != null || Utilities.searchParamIsNotEmpty(dto.getBaptemeImmersionParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("baptemeImmersion", dto.getBaptemeImmersion(), "e.baptemeImmersion", "Boolean", dto.getBaptemeImmersionParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDateBapteme()) || Utilities.searchParamIsNotEmpty(dto.getDateBaptemeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateBapteme", dto.getDateBapteme(), "e.dateBapteme", "Date", dto.getDateBaptemeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getEgliseBapteme()) || Utilities.searchParamIsNotEmpty(dto.getEgliseBaptemeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("egliseBapteme", dto.getEgliseBapteme(), "e.egliseBapteme", "String", dto.getEgliseBaptemeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getEgliseOrigine()) || Utilities.searchParamIsNotEmpty(dto.getEgliseOrigineParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("egliseOrigine", dto.getEgliseOrigine(), "e.egliseOrigine", "String", dto.getEgliseOrigineParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getLocked() != null || Utilities.searchParamIsNotEmpty(dto.getLockedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("locked", dto.getLocked(), "e.locked", "Boolean", dto.getLockedParam(), param, index, locale));
			}
			if (dto.getIsConnected() != null || Utilities.searchParamIsNotEmpty(dto.getIsConnectedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isConnected", dto.getIsConnected(), "e.isConnected", "Boolean", dto.getIsConnectedParam(), param, index, locale));
			}
			if (dto.getIsValidated() != null || Utilities.searchParamIsNotEmpty(dto.getIsValidatedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isValidated", dto.getIsValidated(), "e.isValidated", "Boolean", dto.getIsValidatedParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLieuNaissance()) || Utilities.searchParamIsNotEmpty(dto.getLieuNaissanceParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("lieuNaissance", dto.getLieuNaissance(), "e.lieuNaissance", "String", dto.getLieuNaissanceParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDomaineCompetence()) || Utilities.searchParamIsNotEmpty(dto.getDomaineCompetenceParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("domaineCompetence", dto.getDomaineCompetence(), "e.domaineCompetence", "String", dto.getDomaineCompetenceParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCivilite()) || Utilities.searchParamIsNotEmpty(dto.getCiviliteParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("civilite", dto.getCivilite(), "e.civilite", "String", dto.getCiviliteParam(), param, index, locale));
			}
						if (dto.getIdProfession() != null || Utilities.searchParamIsNotEmpty(dto.getIdProfessionParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idProfession", dto.getIdProfession(), "e.profession.id", "Integer", dto.getIdProfessionParam(), param, index, locale));
			}
						if (dto.getIdPays() != null || Utilities.searchParamIsNotEmpty(dto.getIdPaysParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idPays", dto.getIdPays(), "e.pays.id", "Integer", dto.getIdPaysParam(), param, index, locale));
			}
						if (dto.getIdMemberStatus() != null || Utilities.searchParamIsNotEmpty(dto.getIdMemberStatusParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idMemberStatus", dto.getIdMemberStatus(), "e.memberStatus.id", "Integer", dto.getIdMemberStatusParam(), param, index, locale));
			}
						if (dto.getIdSituationMatrimoniale() != null || Utilities.searchParamIsNotEmpty(dto.getIdSituationMatrimonialeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idSituationMatrimoniale", dto.getIdSituationMatrimoniale(), "e.situationMatrimoniale.id", "Integer", dto.getIdSituationMatrimonialeParam(), param, index, locale));
			}
						if (dto.getIdSituationProfessionnelle() != null || Utilities.searchParamIsNotEmpty(dto.getIdSituationProfessionnelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idSituationProfessionnelle", dto.getIdSituationProfessionnelle(), "e.situationProfessionnelle.id", "Integer", dto.getIdSituationProfessionnelleParam(), param, index, locale));
			}
						if (dto.getIdTypeUser() != null || Utilities.searchParamIsNotEmpty(dto.getIdTypeUserParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idTypeUser", dto.getIdTypeUser(), "e.typeUser.id", "Integer", dto.getIdTypeUserParam(), param, index, locale));
			}
						if (dto.getIdRole() != null || Utilities.searchParamIsNotEmpty(dto.getIdRoleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idRole", dto.getIdRole(), "e.role.id", "Integer", dto.getIdRoleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getProfessionCode()) || Utilities.searchParamIsNotEmpty(dto.getProfessionCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("professionCode", dto.getProfessionCode(), "e.profession.code", "String", dto.getProfessionCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getProfessionLibelle()) || Utilities.searchParamIsNotEmpty(dto.getProfessionLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("professionLibelle", dto.getProfessionLibelle(), "e.profession.libelle", "String", dto.getProfessionLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysCode()) || Utilities.searchParamIsNotEmpty(dto.getPaysCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysCode", dto.getPaysCode(), "e.pays.code", "String", dto.getPaysCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysLibelle()) || Utilities.searchParamIsNotEmpty(dto.getPaysLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysLibelle", dto.getPaysLibelle(), "e.pays.libelle", "String", dto.getPaysLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getMemberStatusCode()) || Utilities.searchParamIsNotEmpty(dto.getMemberStatusCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("memberStatusCode", dto.getMemberStatusCode(), "e.memberStatus.code", "String", dto.getMemberStatusCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getMemberStatusLibelle()) || Utilities.searchParamIsNotEmpty(dto.getMemberStatusLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("memberStatusLibelle", dto.getMemberStatusLibelle(), "e.memberStatus.libelle", "String", dto.getMemberStatusLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSituationMatrimonialeCode()) || Utilities.searchParamIsNotEmpty(dto.getSituationMatrimonialeCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("situationMatrimonialeCode", dto.getSituationMatrimonialeCode(), "e.situationMatrimoniale.code", "String", dto.getSituationMatrimonialeCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSituationMatrimonialeLibelle()) || Utilities.searchParamIsNotEmpty(dto.getSituationMatrimonialeLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("situationMatrimonialeLibelle", dto.getSituationMatrimonialeLibelle(), "e.situationMatrimoniale.libelle", "String", dto.getSituationMatrimonialeLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSituationProfessionnelleCode()) || Utilities.searchParamIsNotEmpty(dto.getSituationProfessionnelleCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("situationProfessionnelleCode", dto.getSituationProfessionnelleCode(), "e.situationProfessionnelle.code", "String", dto.getSituationProfessionnelleCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getSituationProfessionnelleLibelle()) || Utilities.searchParamIsNotEmpty(dto.getSituationProfessionnelleLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("situationProfessionnelleLibelle", dto.getSituationProfessionnelleLibelle(), "e.situationProfessionnelle.libelle", "String", dto.getSituationProfessionnelleLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getTypeUserCode()) || Utilities.searchParamIsNotEmpty(dto.getTypeUserCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeUserCode", dto.getTypeUserCode(), "e.typeUser.code", "String", dto.getTypeUserCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getTypeUserLibelle()) || Utilities.searchParamIsNotEmpty(dto.getTypeUserLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("typeUserLibelle", dto.getTypeUserLibelle(), "e.typeUser.libelle", "String", dto.getTypeUserLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getRoleCode()) || Utilities.searchParamIsNotEmpty(dto.getRoleCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleCode", dto.getRoleCode(), "e.role.code", "String", dto.getRoleCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getRoleLibelle()) || Utilities.searchParamIsNotEmpty(dto.getRoleLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("roleLibelle", dto.getRoleLibelle(), "e.role.libelle", "String", dto.getRoleLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
