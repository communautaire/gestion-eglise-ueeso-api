package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.Commune;
import ci.ueeso.gestion.eglise.helper.dto.CommuneDto;

/**
 * Repository customize : Commune.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _CommuneRepository {
	default List<String> _generateCriteria(CommuneDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}

	/**
	 * Finds Commune by using libele as a search criteria.
	 * 
	 * @param libele
	 * @return An Object Commune whose libele is equals to the given libele. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.libelle = :libelle and e.ville.id = :villeId and e.isDeleted = :isDeleted")
	Commune findByLibelleAndVille(@Param("libelle")String libelle,@Param("villeId")Integer villeId, @Param("isDeleted")Boolean isDeleted);

	

	/**
	 * Finds Commune by using libele as a search criteria.
	 * 
	 * @param libele
	 * @return An Object Commune whose libele is equals to the given libele. If
	 *         no Commune is found, this method returns null.
	 */
	@Query("select e from Commune e where e.libelle = :libelle and e.pays.id = :idPays and e.isDeleted = :isDeleted")
	Commune findByLibelleAndPays(@Param("libelle")String libelle,@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);
}

