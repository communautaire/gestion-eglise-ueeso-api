package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.UserMission;
import ci.ueeso.gestion.eglise.helper.dto.UserMissionDto;

/**
 * Repository customize : UserMission.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _UserMissionRepository {
	default List<String> _generateCriteria(UserMissionDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	@Query("select e from UserMission e where e.user.id = :idUser and e.mission.id = :idMission and e.isDeleted = :isDeleted")
	UserMission findByIdUserAndIdMission(@Param("idUser")Integer idUser, @Param("idMission")Integer idMission, @Param("isDeleted")Boolean isDeleted);

}
