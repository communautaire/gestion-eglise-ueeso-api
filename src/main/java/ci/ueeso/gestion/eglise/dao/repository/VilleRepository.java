

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.customize._VilleRepository;

/**
 * Repository : Ville.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface VilleRepository extends JpaRepository<Ville, Integer>, _VilleRepository {
	/**
	 * Finds Ville by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Ville whose id is equals to the given id. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.id = :id and e.isDeleted = :isDeleted")
	Ville findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Ville whose libelle is equals to the given libelle. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Ville findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using longitude as a search criteria.
	 * 
	 * @param longitude
	 * @return An Object Ville whose longitude is equals to the given longitude. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.longitude = :longitude and e.isDeleted = :isDeleted")
	List<Ville> findByLongitude(@Param("longitude")Double longitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using latitude as a search criteria.
	 * 
	 * @param latitude
	 * @return An Object Ville whose latitude is equals to the given latitude. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.latitude = :latitude and e.isDeleted = :isDeleted")
	List<Ville> findByLatitude(@Param("latitude")Double latitude, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using superficie as a search criteria.
	 * 
	 * @param superficie
	 * @return An Object Ville whose superficie is equals to the given superficie. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.superficie = :superficie and e.isDeleted = :isDeleted")
	List<Ville> findBySuperficie(@Param("superficie")Double superficie, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Ville whose createdAt is equals to the given createdAt. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Ville> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Ville whose updatedAt is equals to the given updatedAt. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Ville> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Ville whose deletedAt is equals to the given deletedAt. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Ville> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Ville whose createdBy is equals to the given createdBy. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Ville> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Ville whose updatedBy is equals to the given updatedBy. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Ville> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using deletedBy as a search criteria.
	 * 
	 * @param deletedBy
	 * @return An Object Ville whose deletedBy is equals to the given deletedBy. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.deletedBy = :deletedBy and e.isDeleted = :isDeleted")
	List<Ville> findByDeletedBy(@Param("deletedBy")Integer deletedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Ville by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Ville whose isDeleted is equals to the given isDeleted. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.isDeleted = :isDeleted")
	List<Ville> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Ville by using idPays as a search criteria.
	 * 
	 * @param idPays
	 * @return An Object Ville whose idPays is equals to the given idPays. If
	 *         no Ville is found, this method returns null.
	 */
	@Query("select e from Ville e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
	List<Ville> findByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);

  /**
   * Finds one Ville by using idPays as a search criteria.
   *
   * @param idPays
   * @return An Object Ville whose idPays is equals to the given idPays. If
   *         no Ville is found, this method returns null.
   */
  @Query("select e from Ville e where e.pays.id = :idPays and e.isDeleted = :isDeleted")
  Ville findVilleByIdPays(@Param("idPays")Integer idPays, @Param("isDeleted")Boolean isDeleted);




	/**
	 * Finds List of Ville by using villeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Ville
	 * @throws DataAccessException,ParseException
	 */
	public default List<Ville> getByCriteria(Request<VilleDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Ville e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Ville> query = em.createQuery(req, Ville.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Ville by using villeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Ville
	 * 
	 */
	public default Long count(Request<VilleDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Ville e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<VilleDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		VilleDto dto = request.getData() != null ? request.getData() : new VilleDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (VilleDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(VilleDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLibelle()) || Utilities.searchParamIsNotEmpty(dto.getLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getLongitude() != null || Utilities.searchParamIsNotEmpty(dto.getLongitudeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("longitude", dto.getLongitude(), "e.longitude", "Double", dto.getLongitudeParam(), param, index, locale));
			}
			if (dto.getLatitude() != null || Utilities.searchParamIsNotEmpty(dto.getLatitudeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("latitude", dto.getLatitude(), "e.latitude", "Double", dto.getLatitudeParam(), param, index, locale));
			}
			if (dto.getSuperficie() != null || Utilities.searchParamIsNotEmpty(dto.getSuperficieParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("superficie", dto.getSuperficie(), "e.superficie", "Double", dto.getSuperficieParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeletedBy() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedBy", dto.getDeletedBy(), "e.deletedBy", "Integer", dto.getDeletedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
						if (dto.getIdPays() != null || Utilities.searchParamIsNotEmpty(dto.getIdPaysParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idPays", dto.getIdPays(), "e.pays.id", "Integer", dto.getIdPaysParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysCode()) || Utilities.searchParamIsNotEmpty(dto.getPaysCodeParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysCode", dto.getPaysCode(), "e.pays.code", "String", dto.getPaysCodeParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getPaysLibelle()) || Utilities.searchParamIsNotEmpty(dto.getPaysLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("paysLibelle", dto.getPaysLibelle(), "e.pays.libelle", "String", dto.getPaysLibelleParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
