package ci.ueeso.gestion.eglise.dao.repository.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.Functionality;
import ci.ueeso.gestion.eglise.helper.dto.RoleFunctionalityDto;

/**
 * Repository customize : RoleFunctionality.
 *
 * @author Smile Backend Generator
 *
 */
@Repository
public interface _RoleFunctionalityRepository {
	default List<String> _generateCriteria(RoleFunctionalityDto dto, HashMap<String, Object> param, Integer index, Locale locale) throws Exception {
		List<String> listOfQuery = new ArrayList<String>();

		// PUT YOUR RIGHT CUSTOM CRITERIA HERE

		return listOfQuery;
	}
	
	@Query("select e.functionality from RoleFunctionality e where e.role.id = :roleId and e.isDeleted = :isDeleted")
	List<Functionality> findFunctionalityByRoleId(@Param("roleId")Integer profilId, @Param("isDeleted")Boolean isDeleted);
	
	@Query("select e.functionality from RoleFunctionality e where e.role.id = :roleId and e.functionality.code = :functionalityCode  and e.isDeleted = :isDeleted")
	Functionality isGranted(@Param("roleId") Integer roleId, @Param("functionalityCode") String moduleCode , @Param("isDeleted")Boolean isDeleted);

}
