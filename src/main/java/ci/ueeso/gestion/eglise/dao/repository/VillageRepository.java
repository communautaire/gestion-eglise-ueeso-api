

package ci.ueeso.gestion.eglise.dao.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.ueeso.gestion.eglise.dao.entity.Village;
import ci.ueeso.gestion.eglise.dao.repository.customize._VillageRepository;
import ci.ueeso.gestion.eglise.helper.CriteriaUtils;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.dto.VillageDto;

/**
 * Repository : Village.
 *
 * @author Smile Backend Generator
 */
@Repository
public interface VillageRepository extends JpaRepository<Village, Integer>, _VillageRepository {
	/**
	 * Finds Village by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Village whose id is equals to the given id. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.id = :id and e.isDeleted = :isDeleted")
	Village findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Village by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Village whose libelle is equals to the given libelle. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Village findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using chefLieu as a search criteria.
	 * 
	 * @param chefLieu
	 * @return An Object Village whose chefLieu is equals to the given chefLieu. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.chefLieu = :chefLieu and e.isDeleted = :isDeleted")
	List<Village> findByChefLieu(@Param("chefLieu")String chefLieu, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Village whose createdAt is equals to the given createdAt. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Village> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Village whose updatedAt is equals to the given updatedAt. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Village> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using deletedAt as a search criteria.
	 * 
	 * @param deletedAt
	 * @return An Object Village whose deletedAt is equals to the given deletedAt. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.deletedAt = :deletedAt and e.isDeleted = :isDeleted")
	List<Village> findByDeletedAt(@Param("deletedAt")Date deletedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Village whose createdBy is equals to the given createdBy. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Village> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Village whose updatedBy is equals to the given updatedBy. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Village> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Village by using deleted as a search criteria.
	 * 
	 * @param deleted
	 * @return An Object Village whose deleted is equals to the given deleted. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.deleted = :deleted")
	List<Village> findByDeleted(@Param("deleted")Integer deleted);
	/**
	 * Finds Village by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Village whose isDeleted is equals to the given isDeleted. If
	 *         no Village is found, this method returns null.
	 */
	@Query("select e from Village e where e.isDeleted = :isDeleted")
	List<Village> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);



	/**
	 * Finds List of Village by using villageDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Village
	 * @throws DataAccessException,ParseException
	 */
	public default List<Village> getByCriteria(Request<VillageDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Village e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				TypedQuery<Village> query = em.createQuery(req, Village.class);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Village by using villageDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Village
	 * 
	 */
	public default Long count(Request<VillageDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Village e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<String, Object>();
		req += getWhereExpression(request, param, locale);
				javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<VillageDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		VillageDto dto = request.getData() != null ? request.getData() : new VillageDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (VillageDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		
		//order
		if(Direction.fromOptionalString(dto.getOrderDirection()).orElse(null) != null && Utilities.notBlank(dto.getOrderField())) {
			req += " order by e."+dto.getOrderField()+" "+dto.getOrderDirection();
		}
		else {
			req += " order by  e.id desc";
		}
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(VillageDto dto, HashMap<String, Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId() != null || Utilities.searchParamIsNotEmpty(dto.getIdParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getLibelle()) || Utilities.searchParamIsNotEmpty(dto.getLibelleParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getChefLieu()) || Utilities.searchParamIsNotEmpty(dto.getChefLieuParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("chefLieu", dto.getChefLieu(), "e.chefLieu", "String", dto.getChefLieuParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getCreatedAt()) || Utilities.searchParamIsNotEmpty(dto.getCreatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getUpdatedAt()) || Utilities.searchParamIsNotEmpty(dto.getUpdatedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (Utilities.isNotBlank(dto.getDeletedAt()) || Utilities.searchParamIsNotEmpty(dto.getDeletedAtParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deletedAt", dto.getDeletedAt(), "e.deletedAt", "Date", dto.getDeletedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getCreatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy() != null || Utilities.searchParamIsNotEmpty(dto.getUpdatedByParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("deleted", dto.getDeleted(), "e.deleted", "Integer", dto.getDeletedParam(), param, index, locale));
			}
			if (dto.getIsDeleted() != null || Utilities.searchParamIsNotEmpty(dto.getIsDeletedParam())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}

			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
