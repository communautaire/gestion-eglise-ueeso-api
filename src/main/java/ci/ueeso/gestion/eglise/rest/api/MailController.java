/*
 * Created on 27 oct. 2018 ( Time 23:09:09 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.business.MailBusiness;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.StatusCode;
import ci.ueeso.gestion.eglise.helper.StatusMessage;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;


/**
Controller for mail sending
 *
 * @author YAO Lazare Kouakou, Back-End developper
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value="/mail")
public class MailController {
@Autowired
    private Environment environment;

	@Autowired
	private MailBusiness mailBusiness;

	@Autowired
	private FunctionalError functionalError;

	@Autowired
	private ExceptionUtils			exceptionUtils;

	private Logger slf4jLogger = LoggerFactory.getLogger(getClass());

	@Autowired
	private HttpServletRequest requestBasic;

	@RequestMapping(value="/sendTestEmail",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<Map<String, String>> sendTestEmail(@RequestBody Request<Map<String, String>> request) {
		slf4jLogger.info("start method /mail/sendTestEmail",environment);
		Response<Map<String, String>> response = new Response<Map<String, String>>();

		String languageID = (String)requestBasic.getAttribute("CURRENT_LANGUAGE_IDENTIFIER");
        Locale locale = new Locale(languageID, "");

		try {

			response = Validate.validateList(request, response, functionalError, locale);
			if(response.isHasError()){
				System.out.println(String.format("Erreur| code: %s -  message: %s", response.getStatus().getCode(), response.getStatus().getMessage()));
				return response;
			}

			response = mailBusiness.sendTestEmail(request, locale);
			if(response.isHasError()){
				System.out.println(String.format("Erreur| code: %s -  message: %s", response.getStatus().getCode(), response.getStatus().getMessage()));
				return response;
			}

			response.setStatus(functionalError.SUCCESS("", locale));
			slf4jLogger.info("end method sendTestEmail",environment);
			System.out.println(String.format("code: %s -  message: %s", StatusCode.SUCCESS, StatusMessage.SUCCESS));

		} catch (CannotCreateTransactionException e) {
			exceptionUtils.CANNOT_CREATE_TRANSACTION_EXCEPTION(response, locale, e);
		} catch (TransactionSystemException e) {
			exceptionUtils.TRANSACTION_SYSTEM_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		}
		slf4jLogger.info("end method /mail/sendTestEmail",environment);
		return response;
	}
}
