

/*
 * Java controller for entity table groupe_constitue 
 * Created on 2019-11-30 ( Time 18:43:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "groupe_constitue"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/groupeConstitue")
public class GroupeConstitueController {

	@Autowired
    private ControllerFactory<GroupeConstitueDto> controllerFactory;
	@Autowired
	private GroupeConstitueBusiness groupeConstitueBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<GroupeConstitueDto> create(@RequestBody Request<GroupeConstitueDto> request) {
    	log.info("start method /groupeConstitue/create");
        Response<GroupeConstitueDto> response = controllerFactory.create(groupeConstitueBusiness, request, FunctionalityEnum.CREATE_GROUPE_CONSTITUE);
		log.info("end method /groupeConstitue/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<GroupeConstitueDto> update(@RequestBody Request<GroupeConstitueDto> request) {
    	log.info("start method /groupeConstitue/update");
        Response<GroupeConstitueDto> response = controllerFactory.update(groupeConstitueBusiness, request, FunctionalityEnum.UPDATE_GROUPE_CONSTITUE);
		log.info("end method /groupeConstitue/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<GroupeConstitueDto> delete(@RequestBody Request<GroupeConstitueDto> request) {
    	log.info("start method /groupeConstitue/delete");
        Response<GroupeConstitueDto> response = controllerFactory.delete(groupeConstitueBusiness, request, FunctionalityEnum.DELETE_GROUPE_CONSTITUE);
		log.info("end method /groupeConstitue/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<GroupeConstitueDto> forceDelete(@RequestBody Request<GroupeConstitueDto> request) {
    	log.info("start method /groupeConstitue/forceDelete");
        Response<GroupeConstitueDto> response = controllerFactory.forceDelete(groupeConstitueBusiness, request, FunctionalityEnum.DELETE_GROUPE_CONSTITUE);
		log.info("end method /groupeConstitue/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<GroupeConstitueDto> getByCriteria(@RequestBody Request<GroupeConstitueDto> request) {
    	log.info("start method /groupeConstitue/getByCriteria");
        Response<GroupeConstitueDto> response = controllerFactory.getByCriteria(groupeConstitueBusiness, request, FunctionalityEnum.VIEW_GROUPE_CONSTITUE);
		log.info("end method /groupeConstitue/getByCriteria");
        return response;
    }
}
