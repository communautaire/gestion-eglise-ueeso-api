

/*
 * Java controller for entity table region 
 * Created on 2020-01-18 ( Time 16:24:57 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "region"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/region")
public class RegionController {

	@Autowired
    private ControllerFactory<RegionDto> controllerFactory;
	@Autowired
	private RegionBusiness regionBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RegionDto> create(@RequestBody Request<RegionDto> request) {
    	log.info("start method /region/create");
        Response<RegionDto> response = controllerFactory.create(regionBusiness, request, FunctionalityEnum.CREATE_REGION);
		log.info("end method /region/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RegionDto> update(@RequestBody Request<RegionDto> request) {
    	log.info("start method /region/update");
        Response<RegionDto> response = controllerFactory.update(regionBusiness, request, FunctionalityEnum.UPDATE_REGION);
		log.info("end method /region/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RegionDto> delete(@RequestBody Request<RegionDto> request) {
    	log.info("start method /region/delete");
        Response<RegionDto> response = controllerFactory.delete(regionBusiness, request, FunctionalityEnum.DELETE_REGION);
		log.info("end method /region/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RegionDto> forceDelete(@RequestBody Request<RegionDto> request) {
    	log.info("start method /region/forceDelete");
        Response<RegionDto> response = controllerFactory.forceDelete(regionBusiness, request, FunctionalityEnum.DELETE_REGION);
		log.info("end method /region/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RegionDto> getByCriteria(@RequestBody Request<RegionDto> request) {
    	log.info("start method /region/getByCriteria");
        Response<RegionDto> response = controllerFactory.getByCriteria(regionBusiness, request, FunctionalityEnum.VIEW_REGION);
		log.info("end method /region/getByCriteria");
        return response;
    }
}
