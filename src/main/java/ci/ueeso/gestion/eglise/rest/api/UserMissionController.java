

/*
 * Java controller for entity table user_mission 
 * Created on 2020-03-14 ( Time 21:14:44 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "user_mission"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/userMission")
public class UserMissionController {

	@Autowired
    private ControllerFactory<UserMissionDto> controllerFactory;
	@Autowired
	private UserMissionBusiness userMissionBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserMissionDto> create(@RequestBody Request<UserMissionDto> request) {
    	log.info("start method /userMission/create");
        Response<UserMissionDto> response = controllerFactory.create(userMissionBusiness, request, FunctionalityEnum.CREATE_USER_MISSION);
		log.info("end method /userMission/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserMissionDto> update(@RequestBody Request<UserMissionDto> request) {
    	log.info("start method /userMission/update");
        Response<UserMissionDto> response = controllerFactory.update(userMissionBusiness, request, FunctionalityEnum.UPDATE_USER_MISSION);
		log.info("end method /userMission/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserMissionDto> delete(@RequestBody Request<UserMissionDto> request) {
    	log.info("start method /userMission/delete");
        Response<UserMissionDto> response = controllerFactory.delete(userMissionBusiness, request, FunctionalityEnum.DELETE_USER_MISSION);
		log.info("end method /userMission/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserMissionDto> forceDelete(@RequestBody Request<UserMissionDto> request) {
    	log.info("start method /userMission/forceDelete");
        Response<UserMissionDto> response = controllerFactory.forceDelete(userMissionBusiness, request, FunctionalityEnum.DELETE_USER_MISSION);
		log.info("end method /userMission/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserMissionDto> getByCriteria(@RequestBody Request<UserMissionDto> request) {
    	log.info("start method /userMission/getByCriteria");
        Response<UserMissionDto> response = controllerFactory.getByCriteria(userMissionBusiness, request, FunctionalityEnum.VIEW_USER_MISSION);
		log.info("end method /userMission/getByCriteria");
        return response;
    }
}
