

/*
 * Java controller for entity table functionality 
 * Created on 2019-11-30 ( Time 18:43:45 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "functionality"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/functionality")
public class FunctionalityController {

	@Autowired
    private ControllerFactory<FunctionalityDto> controllerFactory;
	@Autowired
	private FunctionalityBusiness functionalityBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FunctionalityDto> create(@RequestBody Request<FunctionalityDto> request) {
    	log.info("start method /functionality/create");
        Response<FunctionalityDto> response = controllerFactory.create(functionalityBusiness, request, FunctionalityEnum.CREATE_FUNCTIONALITY);
		log.info("end method /functionality/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FunctionalityDto> update(@RequestBody Request<FunctionalityDto> request) {
    	log.info("start method /functionality/update");
        Response<FunctionalityDto> response = controllerFactory.update(functionalityBusiness, request, FunctionalityEnum.UPDATE_FUNCTIONALITY);
		log.info("end method /functionality/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FunctionalityDto> delete(@RequestBody Request<FunctionalityDto> request) {
    	log.info("start method /functionality/delete");
        Response<FunctionalityDto> response = controllerFactory.delete(functionalityBusiness, request, FunctionalityEnum.DELETE_FUNCTIONALITY);
		log.info("end method /functionality/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FunctionalityDto> forceDelete(@RequestBody Request<FunctionalityDto> request) {
    	log.info("start method /functionality/forceDelete");
        Response<FunctionalityDto> response = controllerFactory.forceDelete(functionalityBusiness, request, FunctionalityEnum.DELETE_FUNCTIONALITY);
		log.info("end method /functionality/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FunctionalityDto> getByCriteria(@RequestBody Request<FunctionalityDto> request) {
    	log.info("start method /functionality/getByCriteria");
        Response<FunctionalityDto> response = controllerFactory.getByCriteria(functionalityBusiness, request, FunctionalityEnum.VIEW_FUNCTIONALITY);
		log.info("end method /functionality/getByCriteria");
        return response;
    }
}
