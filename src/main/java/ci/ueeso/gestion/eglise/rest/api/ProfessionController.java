

/*
 * Java controller for entity table profession 
 * Created on 2019-11-30 ( Time 18:43:49 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "profession"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/profession")
public class ProfessionController {

	@Autowired
    private ControllerFactory<ProfessionDto> controllerFactory;
	@Autowired
	private ProfessionBusiness professionBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<ProfessionDto> create(@RequestBody Request<ProfessionDto> request) {
    	log.info("start method /profession/create");
        Response<ProfessionDto> response = controllerFactory.create(professionBusiness, request, FunctionalityEnum.CREATE_PROFESSION);
		log.info("end method /profession/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<ProfessionDto> update(@RequestBody Request<ProfessionDto> request) {
    	log.info("start method /profession/update");
        Response<ProfessionDto> response = controllerFactory.update(professionBusiness, request, FunctionalityEnum.UPDATE_PROFESSION);
		log.info("end method /profession/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<ProfessionDto> delete(@RequestBody Request<ProfessionDto> request) {
    	log.info("start method /profession/delete");
        Response<ProfessionDto> response = controllerFactory.delete(professionBusiness, request, FunctionalityEnum.DELETE_PROFESSION);
		log.info("end method /profession/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<ProfessionDto> forceDelete(@RequestBody Request<ProfessionDto> request) {
    	log.info("start method /profession/forceDelete");
        Response<ProfessionDto> response = controllerFactory.forceDelete(professionBusiness, request, FunctionalityEnum.DELETE_PROFESSION);
		log.info("end method /profession/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<ProfessionDto> getByCriteria(@RequestBody Request<ProfessionDto> request) {
    	log.info("start method /profession/getByCriteria");
        Response<ProfessionDto> response = controllerFactory.getByCriteria(professionBusiness, request, FunctionalityEnum.VIEW_PROFESSION);
		log.info("end method /profession/getByCriteria");
        return response;
    }
}
