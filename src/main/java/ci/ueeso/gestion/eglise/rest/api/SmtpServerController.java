

/*
 * Java controller for entity table smtp_server 
 * Created on 2020-01-24 ( Time 19:04:40 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "smtp_server"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/smtpServer")
public class SmtpServerController {

	@Autowired
    private ControllerFactory<SmtpServerDto> controllerFactory;
	@Autowired
	private SmtpServerBusiness smtpServerBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SmtpServerDto> create(@RequestBody Request<SmtpServerDto> request) {
    	log.info("start method /smtpServer/create");
        Response<SmtpServerDto> response = controllerFactory.create(smtpServerBusiness, request, FunctionalityEnum.CREATE_SMTP_SERVER);
		log.info("end method /smtpServer/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SmtpServerDto> update(@RequestBody Request<SmtpServerDto> request) {
    	log.info("start method /smtpServer/update");
        Response<SmtpServerDto> response = controllerFactory.update(smtpServerBusiness, request, FunctionalityEnum.UPDATE_SMTP_SERVER);
		log.info("end method /smtpServer/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SmtpServerDto> delete(@RequestBody Request<SmtpServerDto> request) {
    	log.info("start method /smtpServer/delete");
        Response<SmtpServerDto> response = controllerFactory.delete(smtpServerBusiness, request, FunctionalityEnum.DELETE_SMTP_SERVER);
		log.info("end method /smtpServer/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SmtpServerDto> forceDelete(@RequestBody Request<SmtpServerDto> request) {
    	log.info("start method /smtpServer/forceDelete");
        Response<SmtpServerDto> response = controllerFactory.forceDelete(smtpServerBusiness, request, FunctionalityEnum.DELETE_SMTP_SERVER);
		log.info("end method /smtpServer/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SmtpServerDto> getByCriteria(@RequestBody Request<SmtpServerDto> request) {
    	log.info("start method /smtpServer/getByCriteria");
        Response<SmtpServerDto> response = controllerFactory.getByCriteria(smtpServerBusiness, request, FunctionalityEnum.VIEW_SMTP_SERVER);
		log.info("end method /smtpServer/getByCriteria");
        return response;
    }
}
