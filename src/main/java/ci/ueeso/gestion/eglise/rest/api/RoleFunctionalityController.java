

/*
 * Java controller for entity table role_functionality 
 * Created on 2019-11-30 ( Time 18:43:51 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "role_functionality"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/roleFunctionality")
public class RoleFunctionalityController {

	@Autowired
    private ControllerFactory<RoleFunctionalityDto> controllerFactory;
	@Autowired
	private RoleFunctionalityBusiness roleFunctionalityBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RoleFunctionalityDto> create(@RequestBody Request<RoleFunctionalityDto> request) {
    	log.info("start method /roleFunctionality/create");
        Response<RoleFunctionalityDto> response = controllerFactory.create(roleFunctionalityBusiness, request, FunctionalityEnum.CREATE_ROLE_FUNCTIONALITY);
		log.info("end method /roleFunctionality/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RoleFunctionalityDto> update(@RequestBody Request<RoleFunctionalityDto> request) {
    	log.info("start method /roleFunctionality/update");
        Response<RoleFunctionalityDto> response = controllerFactory.update(roleFunctionalityBusiness, request, FunctionalityEnum.UPDATE_ROLE_FUNCTIONALITY);
		log.info("end method /roleFunctionality/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RoleFunctionalityDto> delete(@RequestBody Request<RoleFunctionalityDto> request) {
    	log.info("start method /roleFunctionality/delete");
        Response<RoleFunctionalityDto> response = controllerFactory.delete(roleFunctionalityBusiness, request, FunctionalityEnum.DELETE_ROLE_FUNCTIONALITY);
		log.info("end method /roleFunctionality/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RoleFunctionalityDto> forceDelete(@RequestBody Request<RoleFunctionalityDto> request) {
    	log.info("start method /roleFunctionality/forceDelete");
        Response<RoleFunctionalityDto> response = controllerFactory.forceDelete(roleFunctionalityBusiness, request, FunctionalityEnum.DELETE_ROLE_FUNCTIONALITY);
		log.info("end method /roleFunctionality/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<RoleFunctionalityDto> getByCriteria(@RequestBody Request<RoleFunctionalityDto> request) {
    	log.info("start method /roleFunctionality/getByCriteria");
        Response<RoleFunctionalityDto> response = controllerFactory.getByCriteria(roleFunctionalityBusiness, request, FunctionalityEnum.VIEW_ROLE_FUNCTIONALITY);
		log.info("end method /roleFunctionality/getByCriteria");
        return response;
    }
}
