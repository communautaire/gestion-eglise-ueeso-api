

/*
 * Java controller for entity table periode 
 * Created on 2019-11-30 ( Time 18:43:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "periode"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/periode")
public class PeriodeController {

	@Autowired
    private ControllerFactory<PeriodeDto> controllerFactory;
	@Autowired
	private PeriodeBusiness periodeBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PeriodeDto> create(@RequestBody Request<PeriodeDto> request) {
    	log.info("start method /periode/create");
        Response<PeriodeDto> response = controllerFactory.create(periodeBusiness, request, FunctionalityEnum.CREATE_PERIODE);
		log.info("end method /periode/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PeriodeDto> update(@RequestBody Request<PeriodeDto> request) {
    	log.info("start method /periode/update");
        Response<PeriodeDto> response = controllerFactory.update(periodeBusiness, request, FunctionalityEnum.UPDATE_PERIODE);
		log.info("end method /periode/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PeriodeDto> delete(@RequestBody Request<PeriodeDto> request) {
    	log.info("start method /periode/delete");
        Response<PeriodeDto> response = controllerFactory.delete(periodeBusiness, request, FunctionalityEnum.DELETE_PERIODE);
		log.info("end method /periode/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PeriodeDto> forceDelete(@RequestBody Request<PeriodeDto> request) {
    	log.info("start method /periode/forceDelete");
        Response<PeriodeDto> response = controllerFactory.forceDelete(periodeBusiness, request, FunctionalityEnum.DELETE_PERIODE);
		log.info("end method /periode/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PeriodeDto> getByCriteria(@RequestBody Request<PeriodeDto> request) {
    	log.info("start method /periode/getByCriteria");
        Response<PeriodeDto> response = controllerFactory.getByCriteria(periodeBusiness, request, FunctionalityEnum.VIEW_PERIODE);
		log.info("end method /periode/getByCriteria");
        return response;
    }
}
