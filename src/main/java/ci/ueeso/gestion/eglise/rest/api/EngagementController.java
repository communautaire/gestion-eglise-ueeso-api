

/*
 * Java controller for entity table engagement 
 * Created on 2019-11-30 ( Time 18:43:44 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "engagement"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/engagement")
public class EngagementController {

	@Autowired
    private ControllerFactory<EngagementDto> controllerFactory;
	@Autowired
	private EngagementBusiness engagementBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<EngagementDto> create(@RequestBody Request<EngagementDto> request) {
    	log.info("start method /engagement/create");
        Response<EngagementDto> response = controllerFactory.create(engagementBusiness, request, FunctionalityEnum.CREATE_ENGAGEMENT);
		log.info("end method /engagement/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<EngagementDto> update(@RequestBody Request<EngagementDto> request) {
    	log.info("start method /engagement/update");
        Response<EngagementDto> response = controllerFactory.update(engagementBusiness, request, FunctionalityEnum.UPDATE_ENGAGEMENT);
		log.info("end method /engagement/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<EngagementDto> delete(@RequestBody Request<EngagementDto> request) {
    	log.info("start method /engagement/delete");
        Response<EngagementDto> response = controllerFactory.delete(engagementBusiness, request, FunctionalityEnum.DELETE_ENGAGEMENT);
		log.info("end method /engagement/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<EngagementDto> forceDelete(@RequestBody Request<EngagementDto> request) {
    	log.info("start method /engagement/forceDelete");
        Response<EngagementDto> response = controllerFactory.forceDelete(engagementBusiness, request, FunctionalityEnum.DELETE_ENGAGEMENT);
		log.info("end method /engagement/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<EngagementDto> getByCriteria(@RequestBody Request<EngagementDto> request) {
    	log.info("start method /engagement/getByCriteria");
        Response<EngagementDto> response = controllerFactory.getByCriteria(engagementBusiness, request, FunctionalityEnum.VIEW_ENGAGEMENT);
		log.info("end method /engagement/getByCriteria");
        return response;
    }
}
