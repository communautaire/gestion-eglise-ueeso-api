

/*
 * Java controller for entity table commune 
 * Created on 2020-01-18 ( Time 17:29:06 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "commune"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/commune")
public class CommuneController {

	@Autowired
    private ControllerFactory<CommuneDto> controllerFactory;
	@Autowired
	private CommuneBusiness communeBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<CommuneDto> create(@RequestBody Request<CommuneDto> request) {
    	log.info("start method /commune/create");
        Response<CommuneDto> response = controllerFactory.create(communeBusiness, request, FunctionalityEnum.CREATE_COMMUNE);
		log.info("end method /commune/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<CommuneDto> update(@RequestBody Request<CommuneDto> request) {
    	log.info("start method /commune/update");
        Response<CommuneDto> response = controllerFactory.update(communeBusiness, request, FunctionalityEnum.UPDATE_COMMUNE);
		log.info("end method /commune/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<CommuneDto> delete(@RequestBody Request<CommuneDto> request) {
    	log.info("start method /commune/delete");
        Response<CommuneDto> response = controllerFactory.delete(communeBusiness, request, FunctionalityEnum.DELETE_COMMUNE);
		log.info("end method /commune/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<CommuneDto> forceDelete(@RequestBody Request<CommuneDto> request) {
    	log.info("start method /commune/forceDelete");
        Response<CommuneDto> response = controllerFactory.forceDelete(communeBusiness, request, FunctionalityEnum.DELETE_COMMUNE);
		log.info("end method /commune/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<CommuneDto> getByCriteria(@RequestBody Request<CommuneDto> request) {
    	log.info("start method /commune/getByCriteria");
        Response<CommuneDto> response = controllerFactory.getByCriteria(communeBusiness, request, FunctionalityEnum.VIEW_COMMUNE);
		log.info("end method /commune/getByCriteria");
        return response;
    }
}
