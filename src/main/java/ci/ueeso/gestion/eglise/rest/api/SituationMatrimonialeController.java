

/*
 * Java controller for entity table situation_matrimoniale 
 * Created on 2019-11-30 ( Time 18:43:51 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "situation_matrimoniale"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/situationMatrimoniale")
public class SituationMatrimonialeController {

	@Autowired
    private ControllerFactory<SituationMatrimonialeDto> controllerFactory;
	@Autowired
	private SituationMatrimonialeBusiness situationMatrimonialeBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationMatrimonialeDto> create(@RequestBody Request<SituationMatrimonialeDto> request) {
    	log.info("start method /situationMatrimoniale/create");
        Response<SituationMatrimonialeDto> response = controllerFactory.create(situationMatrimonialeBusiness, request, FunctionalityEnum.CREATE_SITUATION_MATRIMONIALE);
		log.info("end method /situationMatrimoniale/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationMatrimonialeDto> update(@RequestBody Request<SituationMatrimonialeDto> request) {
    	log.info("start method /situationMatrimoniale/update");
        Response<SituationMatrimonialeDto> response = controllerFactory.update(situationMatrimonialeBusiness, request, FunctionalityEnum.UPDATE_SITUATION_MATRIMONIALE);
		log.info("end method /situationMatrimoniale/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationMatrimonialeDto> delete(@RequestBody Request<SituationMatrimonialeDto> request) {
    	log.info("start method /situationMatrimoniale/delete");
        Response<SituationMatrimonialeDto> response = controllerFactory.delete(situationMatrimonialeBusiness, request, FunctionalityEnum.DELETE_SITUATION_MATRIMONIALE);
		log.info("end method /situationMatrimoniale/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationMatrimonialeDto> forceDelete(@RequestBody Request<SituationMatrimonialeDto> request) {
    	log.info("start method /situationMatrimoniale/forceDelete");
        Response<SituationMatrimonialeDto> response = controllerFactory.forceDelete(situationMatrimonialeBusiness, request, FunctionalityEnum.DELETE_SITUATION_MATRIMONIALE);
		log.info("end method /situationMatrimoniale/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationMatrimonialeDto> getByCriteria(@RequestBody Request<SituationMatrimonialeDto> request) {
    	log.info("start method /situationMatrimoniale/getByCriteria");
        Response<SituationMatrimonialeDto> response = controllerFactory.getByCriteria(situationMatrimonialeBusiness, request, FunctionalityEnum.VIEW_SITUATION_MATRIMONIALE);
		log.info("end method /situationMatrimoniale/getByCriteria");
        return response;
    }
}
