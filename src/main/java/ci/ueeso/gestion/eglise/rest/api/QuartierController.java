

/*
 * Java controller for entity table quartier 
 * Created on 2020-03-15 ( Time 00:33:37 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "quartier"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/quartier")
public class QuartierController {

	@Autowired
    private ControllerFactory<QuartierDto> controllerFactory;
	@Autowired
	private QuartierBusiness quartierBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<QuartierDto> create(@RequestBody Request<QuartierDto> request) {
    	log.info("start method /quartier/create");
        Response<QuartierDto> response = controllerFactory.create(quartierBusiness, request, FunctionalityEnum.CREATE_QUARTIER);
		log.info("end method /quartier/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<QuartierDto> update(@RequestBody Request<QuartierDto> request) {
    	log.info("start method /quartier/update");
        Response<QuartierDto> response = controllerFactory.update(quartierBusiness, request, FunctionalityEnum.UPDATE_QUARTIER);
		log.info("end method /quartier/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<QuartierDto> delete(@RequestBody Request<QuartierDto> request) {
    	log.info("start method /quartier/delete");
        Response<QuartierDto> response = controllerFactory.delete(quartierBusiness, request, FunctionalityEnum.DELETE_QUARTIER);
		log.info("end method /quartier/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<QuartierDto> forceDelete(@RequestBody Request<QuartierDto> request) {
    	log.info("start method /quartier/forceDelete");
        Response<QuartierDto> response = controllerFactory.forceDelete(quartierBusiness, request, FunctionalityEnum.DELETE_QUARTIER);
		log.info("end method /quartier/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<QuartierDto> getByCriteria(@RequestBody Request<QuartierDto> request) {
    	log.info("start method /quartier/getByCriteria");
        Response<QuartierDto> response = controllerFactory.getByCriteria(quartierBusiness, request, FunctionalityEnum.VIEW_QUARTIER);
		log.info("end method /quartier/getByCriteria");
        return response;
    }
}
