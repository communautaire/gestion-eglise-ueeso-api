

/*
 * Java controller for entity table type_user 
 * Created on 2019-11-30 ( Time 18:43:53 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "type_user"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/typeUser")
public class TypeUserController {

	@Autowired
    private ControllerFactory<TypeUserDto> controllerFactory;
	@Autowired
	private TypeUserBusiness typeUserBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TypeUserDto> create(@RequestBody Request<TypeUserDto> request) {
    	log.info("start method /typeUser/create");
        Response<TypeUserDto> response = controllerFactory.create(typeUserBusiness, request, FunctionalityEnum.CREATE_TYPE_USER);
		log.info("end method /typeUser/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TypeUserDto> update(@RequestBody Request<TypeUserDto> request) {
    	log.info("start method /typeUser/update");
        Response<TypeUserDto> response = controllerFactory.update(typeUserBusiness, request, FunctionalityEnum.UPDATE_TYPE_USER);
		log.info("end method /typeUser/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TypeUserDto> delete(@RequestBody Request<TypeUserDto> request) {
    	log.info("start method /typeUser/delete");
        Response<TypeUserDto> response = controllerFactory.delete(typeUserBusiness, request, FunctionalityEnum.DELETE_TYPE_USER);
		log.info("end method /typeUser/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TypeUserDto> forceDelete(@RequestBody Request<TypeUserDto> request) {
    	log.info("start method /typeUser/forceDelete");
        Response<TypeUserDto> response = controllerFactory.forceDelete(typeUserBusiness, request, FunctionalityEnum.DELETE_TYPE_USER);
		log.info("end method /typeUser/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TypeUserDto> getByCriteria(@RequestBody Request<TypeUserDto> request) {
    	log.info("start method /typeUser/getByCriteria");
        Response<TypeUserDto> response = controllerFactory.getByCriteria(typeUserBusiness, request, FunctionalityEnum.VIEW_TYPE_USER);
		log.info("end method /typeUser/getByCriteria");
        return response;
    }
}
