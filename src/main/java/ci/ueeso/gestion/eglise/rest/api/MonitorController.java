
/*
 * Java transformer for entity table Utilisateurs 
 * Created on 27 juin 2017 ( Time 13:29:58 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.ApplicationHealth;
import ci.ueeso.gestion.eglise.helper.ParamsUtils;

/**
 * Controller for table "Utilisateurs"
 * 
 * @author SFL Back-End developper
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/monitor")
public class MonitorController {

	@Autowired
	private ParamsUtils paramsUtils;

	private Logger slf4jLogger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/application", method = RequestMethod.GET)
	public String application() {
		slf4jLogger.info("start method /monitor/application");
		String response = "KO";
		try {

			if (ApplicationHealth.applicationStatus(paramsUtils)) {
				response = "OK";
			}
		} catch (Exception e) {
			slf4jLogger.info(e.getMessage());
		}
		slf4jLogger.info("end method  /monitor/application");
		return response;
	}

	@RequestMapping(value = "/db", method = RequestMethod.GET)
	public String db() {
		slf4jLogger.info("start method /monitor/db");
		String response = "KO";
		try {

			if (ApplicationHealth.dbStatus(paramsUtils)) {
				response = "OK";
			}
		} catch (Exception e) {
			slf4jLogger.info(e.getMessage());
		}
		slf4jLogger.info("end method  /monitor/db");
		return response;
	}

	@RequestMapping(value = "/elasticsearch", method = RequestMethod.GET)
	public String elasticsearch() {
		slf4jLogger.info("start method /monitor/elasticsearch");
		String response = "KO";
		try {

			if (ApplicationHealth.elasticSearchStatus(paramsUtils)) {
				response = "OK";
			}
		} catch (Exception e) {
			slf4jLogger.info(e.getMessage());
		}
		slf4jLogger.info("end method  /monitor/elasticsearch");
		return response;
	}

	@RequestMapping(value = "/diskSpace", method = RequestMethod.GET)
	public String diskSpace() {
		slf4jLogger.info("start method /monitor/diskSpace");
		String response = "KO";
		try {
			if (ApplicationHealth.diskSpaceStatus(paramsUtils)) {
				response = "OK";
			}
		} catch (Exception e) {
			slf4jLogger.info(e.getMessage());
		}
		slf4jLogger.info("end method  /monitor/diskSpace");
		return response;
	}

}
