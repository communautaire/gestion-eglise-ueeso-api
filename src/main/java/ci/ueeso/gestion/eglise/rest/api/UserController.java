

/*
 * Java controller for entity table user 
 * Created on 2019-11-30 ( Time 18:43:53 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.business.UserBusiness;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.StatusCode;
import ci.ueeso.gestion.eglise.helper.StatusMessage;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.UserDto;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;
import lombok.extern.java.Log;

/**
Controller for table "user"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/user")
public class UserController {

	@Autowired
	private ControllerFactory<UserDto> controllerFactory;
	@Autowired
	private UserBusiness userBusiness;
	@Autowired
	private HttpServletRequest requestBasic;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@Autowired
	private Environment environment;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<UserDto> create(@RequestBody Request<UserDto> request) {
		log.info("start method /user/create");
		Response<UserDto> response = controllerFactory.create(userBusiness, request, FunctionalityEnum.CREATE_USER);
		log.info("end method /user/create");
		return response;
	}

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<UserDto> update(@RequestBody Request<UserDto> request) {
		log.info("start method /user/update");
		Response<UserDto> response = controllerFactory.update(userBusiness, request, FunctionalityEnum.UPDATE_USER);
		log.info("end method /user/update");
		return response;
	}

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<UserDto> delete(@RequestBody Request<UserDto> request) {
		log.info("start method /user/delete");
		Response<UserDto> response = controllerFactory.delete(userBusiness, request, FunctionalityEnum.DELETE_USER);
		log.info("end method /user/delete");
		return response;
	}

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<UserDto> forceDelete(@RequestBody Request<UserDto> request) {
		log.info("start method /user/forceDelete");
		Response<UserDto> response = controllerFactory.forceDelete(userBusiness, request, FunctionalityEnum.DELETE_USER);
		log.info("end method /user/forceDelete");
		return response;
	}

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<UserDto> getByCriteria(@RequestBody Request<UserDto> request) {
		log.info("start method /user/getByCriteria");
		Response<UserDto> response = controllerFactory.getByCriteria(userBusiness, request, FunctionalityEnum.VIEW_USER);
		log.info("end method /user/getByCriteria");
		return response;
	}
	@RequestMapping(value = "/connexion", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
	"application/json" })
	public Response<UserDto> connexion(@RequestBody Request<UserDto> request) {
		log.info("start method /user/connexion");
		Response<UserDto> response = new Response<UserDto>();
		String languageID = (String) requestBasic.getAttribute("CURRENT_LANGUAGE_IDENTIFIER");
		Locale locale = new Locale(languageID, "");
		try {
			response = Validate.validateObject(request, response, functionalError, locale);
			if (!response.isHasError()) {
				response = userBusiness.connexion(request, locale);
			} else {
				// log.info("Erreur| code: {} - message: {}",
				// response.getStatus().getCode(), response.getStatus().getMessage());
				return response;
			}
			if (!response.isHasError()) {
				//    log.info("end method connexion");
				//    log.info("code: {} -  message: {}", StatusCode.SUCCESS, StatusMessage.SUCCESS);
			} else {
				// log.info("Erreur| code: {} - message: {}",
				// response.getStatus().getCode(), response.getStatus().getMessage());
			}
		} catch (CannotCreateTransactionException e) {
			exceptionUtils.CANNOT_CREATE_TRANSACTION_EXCEPTION(response, locale, e);
		} catch (TransactionSystemException e) {
			exceptionUtils.TRANSACTION_SYSTEM_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		}
		// log.info("end method /user/connexion");
		return response;
	}
	@RequestMapping(value = "/deconnexion", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
	"application/json" })
	public Response<UserDto> deconnexion(@RequestBody Request<UserDto> request) {
		log.info("start method /user/connexion");
		Response<UserDto> response = new Response<UserDto>();
		String languageID = (String) requestBasic.getAttribute("CURRENT_LANGUAGE_IDENTIFIER");
		Locale locale = new Locale(languageID, "");
		try {
			response = Validate.validateObject(request, response, functionalError, locale);
			if (!response.isHasError()) {
				response = userBusiness.deconnexion(request, locale);
			} else {
				// log.info("Erreur| code: {} - message: {}",
				// response.getStatus().getCode(), response.getStatus().getMessage());
				return response;
			}
			if (!response.isHasError()) {
				//    log.info("end method connexion");
				//    log.info("code: {} -  message: {}", StatusCode.SUCCESS, StatusMessage.SUCCESS);
			} else {
				// log.info("Erreur| code: {} - message: {}",
				// response.getStatus().getCode(), response.getStatus().getMessage());
			}
		} catch (CannotCreateTransactionException e) {
			exceptionUtils.CANNOT_CREATE_TRANSACTION_EXCEPTION(response, locale, e);
		} catch (TransactionSystemException e) {
			exceptionUtils.TRANSACTION_SYSTEM_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		}
		// log.info("end method /user/connexion");
		return response;
	}
	
	@RequestMapping(value="/validateRegistration",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
	public Response<UserDto> sendTestEmail(@RequestBody Request<UserDto> request) {
		log.info("start method /user/validateRegistration");
		Response<UserDto> response = new Response<UserDto>();

		String languageID = (String)requestBasic.getAttribute("CURRENT_LANGUAGE_IDENTIFIER");
		Locale locale = new Locale(languageID, "");

		try {

			response = Validate.validateList(request, response, functionalError, locale);
			if(response.isHasError()){
				log.info(String.format("Erreur| code: %s -  message: %s", response.getStatus().getCode(), response.getStatus().getMessage()));
				return response;
			}

			response = userBusiness.validateRegistration(request, locale);
			if(response.isHasError()){
				log.info(String.format("Erreur| code: %s -  message: %s", response.getStatus().getCode(), response.getStatus().getMessage()));
				return response;
			}

			response.setStatus(functionalError.SUCCESS("", locale));
			log.info("end method validateRegistration");
			log.info(String.format("code: %s -  message: %s", StatusCode.SUCCESS, StatusMessage.SUCCESS));

		} catch (CannotCreateTransactionException e) {
			exceptionUtils.CANNOT_CREATE_TRANSACTION_EXCEPTION(response, locale, e);
		} catch (TransactionSystemException e) {
			exceptionUtils.TRANSACTION_SYSTEM_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		}
		log.info("end method /user/validateRegistration");
		return response;
	}	
	
	@RequestMapping(value = "/exportUsers", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public ResponseEntity<InputStreamResource> exportUsers(@RequestBody Request<UserDto> request) {
		log.info("start method /user/exportUsers");
		Response<UserDto> response = new Response<UserDto>();
		
		Date begin = new Date();

		String languageID = (String) requestBasic.getAttribute("CURRENT_LANGUAGE_IDENTIFIER");
		Locale locale = new Locale(languageID, "");
		HttpHeaders headers = new HttpHeaders();
		try {

			response = Validate.validateObject(request, response, functionalError, locale);
			if (response.isHasError()) {
				log.info("Erreur| code: "+response.getStatus().getCode()+" -  message: "+response.getStatus().getMessage());
				headers.set("hasError", "true");
				headers.set("statusCode", response.getStatus().getCode());
				headers.set("statusMessage", response.getStatus().getMessage());
				return ResponseEntity.ok().headers(headers).body(null);
			}
			
			response = userBusiness.exportUsers(request, locale);
			if (response.isHasError()) {
				log.info("Erreur| code: "+response.getStatus().getCode()+" -  message: "+response.getStatus().getMessage());
				headers.set("hasError", "true");
				headers.set("statusCode", response.getStatus().getCode());
				headers.set("statusMessage", response.getStatus().getMessage());
				return ResponseEntity.ok().headers(headers).body(null);
			}

			if (response.getFilePath() != null && !response.getFilePath().isEmpty()) {
				File file = new File(response.getFilePath());
				headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel")); // MediaType.EXCELL
				InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
				ResponseEntity<InputStreamResource> result =  ResponseEntity.ok()
						.headers(headers)
						.contentLength(file.length())
						.contentType(MediaType.parseMediaType("application/octet-stream"))
						.body(resource);

				if (Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> env.equals("staging") || env.equals("preprod") || env.equals("prod"))) {
					Utilities.deleteFile(response.getFilePath());
				}

				return result;
			}

			log.info("end method exportUsers");
			log.info("code: "+StatusCode.SUCCESS+" -  message: "+StatusMessage.SUCCESS);

		} catch (CannotCreateTransactionException e) {
			exceptionUtils.CANNOT_CREATE_TRANSACTION_EXCEPTION(response, locale, e);
		} catch (TransactionSystemException e) {
			exceptionUtils.TRANSACTION_SYSTEM_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		}

		log.info("end method /user/exportUsers - Durée: "+Utilities.convertSecondToTime((new Date().getTime() - begin.getTime())/1000));
		log.info("Erreur| code: "+response.getStatus().getCode()+" -  message: "+response.getStatus().getMessage());
		headers.set("hasError", "true");
		headers.set("statusCode", response.getStatus().getCode());
		headers.set("statusMessage", response.getStatus().getMessage());
		return ResponseEntity.ok().headers(headers).body(null);
	}
	
	
}
