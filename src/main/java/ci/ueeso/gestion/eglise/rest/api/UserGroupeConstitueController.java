

/*
 * Java controller for entity table user_groupe_constitue 
 * Created on 2020-01-23 ( Time 10:51:51 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "user_groupe_constitue"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/userGroupeConstitue")
public class UserGroupeConstitueController {

	@Autowired
    private ControllerFactory<UserGroupeConstitueDto> controllerFactory;
	@Autowired
	private UserGroupeConstitueBusiness userGroupeConstitueBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserGroupeConstitueDto> create(@RequestBody Request<UserGroupeConstitueDto> request) {
    	log.info("start method /userGroupeConstitue/create");
        Response<UserGroupeConstitueDto> response = controllerFactory.create(userGroupeConstitueBusiness, request, FunctionalityEnum.CREATE_USER_GROUPE_CONSTITUE);
		log.info("end method /userGroupeConstitue/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserGroupeConstitueDto> update(@RequestBody Request<UserGroupeConstitueDto> request) {
    	log.info("start method /userGroupeConstitue/update");
        Response<UserGroupeConstitueDto> response = controllerFactory.update(userGroupeConstitueBusiness, request, FunctionalityEnum.UPDATE_USER_GROUPE_CONSTITUE);
		log.info("end method /userGroupeConstitue/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserGroupeConstitueDto> delete(@RequestBody Request<UserGroupeConstitueDto> request) {
    	log.info("start method /userGroupeConstitue/delete");
        Response<UserGroupeConstitueDto> response = controllerFactory.delete(userGroupeConstitueBusiness, request, FunctionalityEnum.DELETE_USER_GROUPE_CONSTITUE);
		log.info("end method /userGroupeConstitue/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserGroupeConstitueDto> forceDelete(@RequestBody Request<UserGroupeConstitueDto> request) {
    	log.info("start method /userGroupeConstitue/forceDelete");
        Response<UserGroupeConstitueDto> response = controllerFactory.forceDelete(userGroupeConstitueBusiness, request, FunctionalityEnum.DELETE_USER_GROUPE_CONSTITUE);
		log.info("end method /userGroupeConstitue/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<UserGroupeConstitueDto> getByCriteria(@RequestBody Request<UserGroupeConstitueDto> request) {
    	log.info("start method /userGroupeConstitue/getByCriteria");
        Response<UserGroupeConstitueDto> response = controllerFactory.getByCriteria(userGroupeConstitueBusiness, request, FunctionalityEnum.VIEW_USER_GROUPE_CONSTITUE);
		log.info("end method /userGroupeConstitue/getByCriteria");
        return response;
    }
}
