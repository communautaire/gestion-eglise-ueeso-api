

/*
 * Java controller for entity table mission 
 * Created on 2019-11-30 ( Time 18:43:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "mission"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/mission")
public class MissionController {

	@Autowired
    private ControllerFactory<MissionDto> controllerFactory;
	@Autowired
	private MissionBusiness missionBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MissionDto> create(@RequestBody Request<MissionDto> request) {
    	log.info("start method /mission/create");
        Response<MissionDto> response = controllerFactory.create(missionBusiness, request, FunctionalityEnum.CREATE_MISSION);
		log.info("end method /mission/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MissionDto> update(@RequestBody Request<MissionDto> request) {
    	log.info("start method /mission/update");
        Response<MissionDto> response = controllerFactory.update(missionBusiness, request, FunctionalityEnum.UPDATE_MISSION);
		log.info("end method /mission/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MissionDto> delete(@RequestBody Request<MissionDto> request) {
    	log.info("start method /mission/delete");
        Response<MissionDto> response = controllerFactory.delete(missionBusiness, request, FunctionalityEnum.DELETE_MISSION);
		log.info("end method /mission/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MissionDto> forceDelete(@RequestBody Request<MissionDto> request) {
    	log.info("start method /mission/forceDelete");
        Response<MissionDto> response = controllerFactory.forceDelete(missionBusiness, request, FunctionalityEnum.DELETE_MISSION);
		log.info("end method /mission/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MissionDto> getByCriteria(@RequestBody Request<MissionDto> request) {
    	log.info("start method /mission/getByCriteria");
        Response<MissionDto> response = controllerFactory.getByCriteria(missionBusiness, request, FunctionalityEnum.VIEW_MISSION);
		log.info("end method /mission/getByCriteria");
        return response;
    }
}
