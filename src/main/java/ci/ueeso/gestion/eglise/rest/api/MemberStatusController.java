

/*
 * Java controller for entity table member_status 
 * Created on 2020-03-03 ( Time 18:59:30 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "member_status"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/memberStatus")
public class MemberStatusController {

	@Autowired
    private ControllerFactory<MemberStatusDto> controllerFactory;
	@Autowired
	private MemberStatusBusiness memberStatusBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MemberStatusDto> create(@RequestBody Request<MemberStatusDto> request) {
    	log.info("start method /memberStatus/create");
        Response<MemberStatusDto> response = controllerFactory.create(memberStatusBusiness, request, FunctionalityEnum.CREATE_MEMBER_STATUS);
		log.info("end method /memberStatus/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MemberStatusDto> update(@RequestBody Request<MemberStatusDto> request) {
    	log.info("start method /memberStatus/update");
        Response<MemberStatusDto> response = controllerFactory.update(memberStatusBusiness, request, FunctionalityEnum.UPDATE_MEMBER_STATUS);
		log.info("end method /memberStatus/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MemberStatusDto> delete(@RequestBody Request<MemberStatusDto> request) {
    	log.info("start method /memberStatus/delete");
        Response<MemberStatusDto> response = controllerFactory.delete(memberStatusBusiness, request, FunctionalityEnum.DELETE_MEMBER_STATUS);
		log.info("end method /memberStatus/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MemberStatusDto> forceDelete(@RequestBody Request<MemberStatusDto> request) {
    	log.info("start method /memberStatus/forceDelete");
        Response<MemberStatusDto> response = controllerFactory.forceDelete(memberStatusBusiness, request, FunctionalityEnum.DELETE_MEMBER_STATUS);
		log.info("end method /memberStatus/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<MemberStatusDto> getByCriteria(@RequestBody Request<MemberStatusDto> request) {
    	log.info("start method /memberStatus/getByCriteria");
        Response<MemberStatusDto> response = controllerFactory.getByCriteria(memberStatusBusiness, request, FunctionalityEnum.VIEW_MEMBER_STATUS);
		log.info("end method /memberStatus/getByCriteria");
        return response;
    }
}
