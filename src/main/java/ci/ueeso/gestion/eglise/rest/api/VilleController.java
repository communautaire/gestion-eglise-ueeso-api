

/*
 * Java controller for entity table ville 
 * Created on 2020-01-18 ( Time 17:01:44 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "ville"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/ville")
public class VilleController {

	@Autowired
    private ControllerFactory<VilleDto> controllerFactory;
	@Autowired
	private VilleBusiness villeBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<VilleDto> create(@RequestBody Request<VilleDto> request) {
    	log.info("start method /ville/create");
        Response<VilleDto> response = controllerFactory.create(villeBusiness, request, FunctionalityEnum.CREATE_VILLE);
		log.info("end method /ville/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<VilleDto> update(@RequestBody Request<VilleDto> request) {
    	log.info("start method /ville/update");
        Response<VilleDto> response = controllerFactory.update(villeBusiness, request, FunctionalityEnum.UPDATE_VILLE);
		log.info("end method /ville/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<VilleDto> delete(@RequestBody Request<VilleDto> request) {
    	log.info("start method /ville/delete");
        Response<VilleDto> response = controllerFactory.delete(villeBusiness, request, FunctionalityEnum.DELETE_VILLE);
		log.info("end method /ville/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<VilleDto> forceDelete(@RequestBody Request<VilleDto> request) {
    	log.info("start method /ville/forceDelete");
        Response<VilleDto> response = controllerFactory.forceDelete(villeBusiness, request, FunctionalityEnum.DELETE_VILLE);
		log.info("end method /ville/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<VilleDto> getByCriteria(@RequestBody Request<VilleDto> request) {
    	log.info("start method /ville/getByCriteria");
        Response<VilleDto> response = controllerFactory.getByCriteria(villeBusiness, request, FunctionalityEnum.VIEW_VILLE);
		log.info("end method /ville/getByCriteria");
        return response;
    }
}
