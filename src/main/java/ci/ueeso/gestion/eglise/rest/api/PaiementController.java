

/*
 * Java controller for entity table paiement 
 * Created on 2019-11-30 ( Time 18:43:47 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "paiement"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/paiement")
public class PaiementController {

	@Autowired
    private ControllerFactory<PaiementDto> controllerFactory;
	@Autowired
	private PaiementBusiness paiementBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaiementDto> create(@RequestBody Request<PaiementDto> request) {
    	log.info("start method /paiement/create");
        Response<PaiementDto> response = controllerFactory.create(paiementBusiness, request, FunctionalityEnum.CREATE_PAIEMENT);
		log.info("end method /paiement/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaiementDto> update(@RequestBody Request<PaiementDto> request) {
    	log.info("start method /paiement/update");
        Response<PaiementDto> response = controllerFactory.update(paiementBusiness, request, FunctionalityEnum.UPDATE_PAIEMENT);
		log.info("end method /paiement/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaiementDto> delete(@RequestBody Request<PaiementDto> request) {
    	log.info("start method /paiement/delete");
        Response<PaiementDto> response = controllerFactory.delete(paiementBusiness, request, FunctionalityEnum.DELETE_PAIEMENT);
		log.info("end method /paiement/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaiementDto> forceDelete(@RequestBody Request<PaiementDto> request) {
    	log.info("start method /paiement/forceDelete");
        Response<PaiementDto> response = controllerFactory.forceDelete(paiementBusiness, request, FunctionalityEnum.DELETE_PAIEMENT);
		log.info("end method /paiement/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaiementDto> getByCriteria(@RequestBody Request<PaiementDto> request) {
    	log.info("start method /paiement/getByCriteria");
        Response<PaiementDto> response = controllerFactory.getByCriteria(paiementBusiness, request, FunctionalityEnum.VIEW_PAIEMENT);
		log.info("end method /paiement/getByCriteria");
        return response;
    }
}
