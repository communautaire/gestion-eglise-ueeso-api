

/*
 * Java controller for entity table situation_professionnelle 
 * Created on 2019-11-30 ( Time 18:43:52 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "situation_professionnelle"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/situationProfessionnelle")
public class SituationProfessionnelleController {

	@Autowired
    private ControllerFactory<SituationProfessionnelleDto> controllerFactory;
	@Autowired
	private SituationProfessionnelleBusiness situationProfessionnelleBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationProfessionnelleDto> create(@RequestBody Request<SituationProfessionnelleDto> request) {
    	log.info("start method /situationProfessionnelle/create");
        Response<SituationProfessionnelleDto> response = controllerFactory.create(situationProfessionnelleBusiness, request, FunctionalityEnum.CREATE_SITUATION_PROFESSIONNELLE);
		log.info("end method /situationProfessionnelle/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationProfessionnelleDto> update(@RequestBody Request<SituationProfessionnelleDto> request) {
    	log.info("start method /situationProfessionnelle/update");
        Response<SituationProfessionnelleDto> response = controllerFactory.update(situationProfessionnelleBusiness, request, FunctionalityEnum.UPDATE_SITUATION_PROFESSIONNELLE);
		log.info("end method /situationProfessionnelle/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationProfessionnelleDto> delete(@RequestBody Request<SituationProfessionnelleDto> request) {
    	log.info("start method /situationProfessionnelle/delete");
        Response<SituationProfessionnelleDto> response = controllerFactory.delete(situationProfessionnelleBusiness, request, FunctionalityEnum.DELETE_SITUATION_PROFESSIONNELLE);
		log.info("end method /situationProfessionnelle/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationProfessionnelleDto> forceDelete(@RequestBody Request<SituationProfessionnelleDto> request) {
    	log.info("start method /situationProfessionnelle/forceDelete");
        Response<SituationProfessionnelleDto> response = controllerFactory.forceDelete(situationProfessionnelleBusiness, request, FunctionalityEnum.DELETE_SITUATION_PROFESSIONNELLE);
		log.info("end method /situationProfessionnelle/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<SituationProfessionnelleDto> getByCriteria(@RequestBody Request<SituationProfessionnelleDto> request) {
    	log.info("start method /situationProfessionnelle/getByCriteria");
        Response<SituationProfessionnelleDto> response = controllerFactory.getByCriteria(situationProfessionnelleBusiness, request, FunctionalityEnum.VIEW_SITUATION_PROFESSIONNELLE);
		log.info("end method /situationProfessionnelle/getByCriteria");
        return response;
    }
}
