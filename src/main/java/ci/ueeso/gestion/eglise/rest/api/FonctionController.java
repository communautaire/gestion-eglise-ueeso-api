

/*
 * Java controller for entity table fonction 
 * Created on 2020-03-14 ( Time 19:11:07 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "fonction"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/fonction")
public class FonctionController {

	@Autowired
    private ControllerFactory<FonctionDto> controllerFactory;
	@Autowired
	private FonctionBusiness fonctionBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FonctionDto> create(@RequestBody Request<FonctionDto> request) {
    	log.info("start method /fonction/create");
        Response<FonctionDto> response = controllerFactory.create(fonctionBusiness, request, FunctionalityEnum.CREATE_FONCTION);
		log.info("end method /fonction/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FonctionDto> update(@RequestBody Request<FonctionDto> request) {
    	log.info("start method /fonction/update");
        Response<FonctionDto> response = controllerFactory.update(fonctionBusiness, request, FunctionalityEnum.UPDATE_FONCTION);
		log.info("end method /fonction/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FonctionDto> delete(@RequestBody Request<FonctionDto> request) {
    	log.info("start method /fonction/delete");
        Response<FonctionDto> response = controllerFactory.delete(fonctionBusiness, request, FunctionalityEnum.DELETE_FONCTION);
		log.info("end method /fonction/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FonctionDto> forceDelete(@RequestBody Request<FonctionDto> request) {
    	log.info("start method /fonction/forceDelete");
        Response<FonctionDto> response = controllerFactory.forceDelete(fonctionBusiness, request, FunctionalityEnum.DELETE_FONCTION);
		log.info("end method /fonction/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<FonctionDto> getByCriteria(@RequestBody Request<FonctionDto> request) {
    	log.info("start method /fonction/getByCriteria");
        Response<FonctionDto> response = controllerFactory.getByCriteria(fonctionBusiness, request, FunctionalityEnum.VIEW_FONCTION);
		log.info("end method /fonction/getByCriteria");
        return response;
    }
}
