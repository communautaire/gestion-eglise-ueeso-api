

/*
 * Java controller for entity table pays 
 * Created on 2019-11-30 ( Time 18:43:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "pays"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/pays")
public class PaysController {

	@Autowired
    private ControllerFactory<PaysDto> controllerFactory;
	@Autowired
	private PaysBusiness paysBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaysDto> create(@RequestBody Request<PaysDto> request) {
    	log.info("start method /pays/create");
        Response<PaysDto> response = controllerFactory.create(paysBusiness, request, FunctionalityEnum.CREATE_PAYS);
		log.info("end method /pays/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaysDto> update(@RequestBody Request<PaysDto> request) {
    	log.info("start method /pays/update");
        Response<PaysDto> response = controllerFactory.update(paysBusiness, request, FunctionalityEnum.UPDATE_PAYS);
		log.info("end method /pays/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaysDto> delete(@RequestBody Request<PaysDto> request) {
    	log.info("start method /pays/delete");
        Response<PaysDto> response = controllerFactory.delete(paysBusiness, request, FunctionalityEnum.DELETE_PAYS);
		log.info("end method /pays/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaysDto> forceDelete(@RequestBody Request<PaysDto> request) {
    	log.info("start method /pays/forceDelete");
        Response<PaysDto> response = controllerFactory.forceDelete(paysBusiness, request, FunctionalityEnum.DELETE_PAYS);
		log.info("end method /pays/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<PaysDto> getByCriteria(@RequestBody Request<PaysDto> request) {
    	log.info("start method /pays/getByCriteria");
        Response<PaysDto> response = controllerFactory.getByCriteria(paysBusiness, request, FunctionalityEnum.VIEW_PAYS);
		log.info("end method /pays/getByCriteria");
        return response;
    }
}
