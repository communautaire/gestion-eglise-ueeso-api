

/*
 * Java controller for entity table adresse 
 * Created on 2019-11-30 ( Time 18:43:43 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "adresse"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/adresse")
public class AdresseController {

	@Autowired
    private ControllerFactory<AdresseDto> controllerFactory;
	@Autowired
	private AdresseBusiness adresseBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<AdresseDto> create(@RequestBody Request<AdresseDto> request) {
    	log.info("start method /adresse/create");
        Response<AdresseDto> response = controllerFactory.create(adresseBusiness, request, FunctionalityEnum.CREATE_ADRESSE);
		log.info("end method /adresse/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<AdresseDto> update(@RequestBody Request<AdresseDto> request) {
    	log.info("start method /adresse/update");
        Response<AdresseDto> response = controllerFactory.update(adresseBusiness, request, FunctionalityEnum.UPDATE_ADRESSE);
		log.info("end method /adresse/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<AdresseDto> delete(@RequestBody Request<AdresseDto> request) {
    	log.info("start method /adresse/delete");
        Response<AdresseDto> response = controllerFactory.delete(adresseBusiness, request, FunctionalityEnum.DELETE_ADRESSE);
		log.info("end method /adresse/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<AdresseDto> forceDelete(@RequestBody Request<AdresseDto> request) {
    	log.info("start method /adresse/forceDelete");
        Response<AdresseDto> response = controllerFactory.forceDelete(adresseBusiness, request, FunctionalityEnum.DELETE_ADRESSE);
		log.info("end method /adresse/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<AdresseDto> getByCriteria(@RequestBody Request<AdresseDto> request) {
    	log.info("start method /adresse/getByCriteria");
        Response<AdresseDto> response = controllerFactory.getByCriteria(adresseBusiness, request, FunctionalityEnum.VIEW_ADRESSE);
		log.info("end method /adresse/getByCriteria");
        return response;
    }
}
