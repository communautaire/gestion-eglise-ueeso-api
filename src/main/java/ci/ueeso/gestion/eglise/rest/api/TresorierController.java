

/*
 * Java controller for entity table tresorier 
 * Created on 2019-11-30 ( Time 18:43:52 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "tresorier"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/tresorier")
public class TresorierController {

	@Autowired
    private ControllerFactory<TresorierDto> controllerFactory;
	@Autowired
	private TresorierBusiness tresorierBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TresorierDto> create(@RequestBody Request<TresorierDto> request) {
    	log.info("start method /tresorier/create");
        Response<TresorierDto> response = controllerFactory.create(tresorierBusiness, request, FunctionalityEnum.CREATE_TRESORIER);
		log.info("end method /tresorier/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TresorierDto> update(@RequestBody Request<TresorierDto> request) {
    	log.info("start method /tresorier/update");
        Response<TresorierDto> response = controllerFactory.update(tresorierBusiness, request, FunctionalityEnum.UPDATE_TRESORIER);
		log.info("end method /tresorier/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TresorierDto> delete(@RequestBody Request<TresorierDto> request) {
    	log.info("start method /tresorier/delete");
        Response<TresorierDto> response = controllerFactory.delete(tresorierBusiness, request, FunctionalityEnum.DELETE_TRESORIER);
		log.info("end method /tresorier/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TresorierDto> forceDelete(@RequestBody Request<TresorierDto> request) {
    	log.info("start method /tresorier/forceDelete");
        Response<TresorierDto> response = controllerFactory.forceDelete(tresorierBusiness, request, FunctionalityEnum.DELETE_TRESORIER);
		log.info("end method /tresorier/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<TresorierDto> getByCriteria(@RequestBody Request<TresorierDto> request) {
    	log.info("start method /tresorier/getByCriteria");
        Response<TresorierDto> response = controllerFactory.getByCriteria(tresorierBusiness, request, FunctionalityEnum.VIEW_TRESORIER);
		log.info("end method /tresorier/getByCriteria");
        return response;
    }
}
