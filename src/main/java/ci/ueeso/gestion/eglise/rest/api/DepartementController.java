

/*
 * Java controller for entity table departement 
 * Created on 2020-01-20 ( Time 11:46:11 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2017  Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.rest.api;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.enums.FunctionalityEnum;
import ci.ueeso.gestion.eglise.business.*;
import ci.ueeso.gestion.eglise.rest.fact.ControllerFactory;

/**
Controller for table "departement"
 * 
 * @author SFL Back-End developper
 *
 */
@Log
@CrossOrigin("*")
@RestController
@RequestMapping(value="/departement")
public class DepartementController {

	@Autowired
    private ControllerFactory<DepartementDto> controllerFactory;
	@Autowired
	private DepartementBusiness departementBusiness;

	@RequestMapping(value="/create",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<DepartementDto> create(@RequestBody Request<DepartementDto> request) {
    	log.info("start method /departement/create");
        Response<DepartementDto> response = controllerFactory.create(departementBusiness, request, FunctionalityEnum.CREATE_DEPARTEMENT);
		log.info("end method /departement/create");
        return response;
    }

	@RequestMapping(value="/update",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<DepartementDto> update(@RequestBody Request<DepartementDto> request) {
    	log.info("start method /departement/update");
        Response<DepartementDto> response = controllerFactory.update(departementBusiness, request, FunctionalityEnum.UPDATE_DEPARTEMENT);
		log.info("end method /departement/update");
        return response;
    }

	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<DepartementDto> delete(@RequestBody Request<DepartementDto> request) {
    	log.info("start method /departement/delete");
        Response<DepartementDto> response = controllerFactory.delete(departementBusiness, request, FunctionalityEnum.DELETE_DEPARTEMENT);
		log.info("end method /departement/delete");
        return response;
    }

	@RequestMapping(value="/forceDelete",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<DepartementDto> forceDelete(@RequestBody Request<DepartementDto> request) {
    	log.info("start method /departement/forceDelete");
        Response<DepartementDto> response = controllerFactory.forceDelete(departementBusiness, request, FunctionalityEnum.DELETE_DEPARTEMENT);
		log.info("end method /departement/forceDelete");
        return response;
    }

	@RequestMapping(value="/getByCriteria",method=RequestMethod.POST,consumes = {"application/json"},produces={"application/json"})
    public Response<DepartementDto> getByCriteria(@RequestBody Request<DepartementDto> request) {
    	log.info("start method /departement/getByCriteria");
        Response<DepartementDto> response = controllerFactory.getByCriteria(departementBusiness, request, FunctionalityEnum.VIEW_DEPARTEMENT);
		log.info("end method /departement/getByCriteria");
        return response;
    }
}
