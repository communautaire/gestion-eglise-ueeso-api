package ci.ueeso.gestion.eglise.excel.mass.importation;

import java.util.ArrayList;
import java.util.List;

import ci.ueeso.gestion.eglise.helper.Utilities;
import lombok.Data;

@Data
public class SheetRowReference {

	//protected Boolean fromXlsxFile;
	protected String fromSheetName;
	protected Integer fromRowIndex;
	protected List<String> comments;
	
	
	public SheetRowReference(String fromSheetName, Integer fromRowIndex, List<String> comments) {
		this.fromSheetName = fromSheetName;
		this.fromRowIndex = fromRowIndex;
		this.comments = comments;
	}
	
	public SheetRowReference(String fromSheetName, Integer fromRowIndex) {
		this(fromSheetName, fromRowIndex, null);
	}

	public SheetRowReference() {}
	
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		SheetRowReference other = (SheetRowReference) obj; 

		if (fromSheetName == null) { 
			if (fromRowIndex == null) {
				return other.getFromSheetName() == null && other.getFromRowIndex() == null ; 
			}
			else {
				return other.getFromSheetName() == null && fromRowIndex.equals(other.getFromRowIndex()) ; 
			}
		}
		else {
			if (fromRowIndex == null) {
				return fromSheetName.equals(other.getFromSheetName()) && other.getFromRowIndex() == null ; 
			}
			else {
				return fromSheetName.equals(other.getFromSheetName()) && fromRowIndex.equals(other.getFromRowIndex()) ; 
			}
		}
	} 
	
	public static void addComments(List<SheetRowReference> datasSheetRowReference, List<String> commentsToAdd) {
		if(Utilities.isEmpty(datasSheetRowReference)) {
			return ;
		}

		if(Utilities.isNotEmpty(commentsToAdd)) {
			for (SheetRowReference reference : datasSheetRowReference) {
				List<String> comments = reference.getComments();
				if(Utilities.isEmpty(comments)) {
					comments = new ArrayList<String>();
				}
				comments.addAll(commentsToAdd);
				reference.setComments(comments);
			}
		}

	}

	public static void addComments(List<SheetRowReference> datasPosition, List<SheetRowReference> datasSheetRowReference, List<String> commentsToAdd) {
		if(Utilities.isEmpty(datasSheetRowReference)) {
			return ;
		}
		if(Utilities.isNotEmpty(commentsToAdd)) {
			for (SheetRowReference reference : datasSheetRowReference) {
				if(Utilities.isEmpty(datasPosition) || datasPosition.contains(reference)) {
					List<String> comments = reference.getComments();
					if(Utilities.isEmpty(comments)) {
						comments = new ArrayList<String>();
					}
					comments.addAll(commentsToAdd);
					reference.setComments(comments);
				}
			}
		}
	}

}
