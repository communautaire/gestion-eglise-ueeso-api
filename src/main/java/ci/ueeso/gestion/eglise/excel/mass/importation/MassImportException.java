package ci.ueeso.gestion.eglise.excel.mass.importation;

import java.util.List;


public class MassImportException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	private List<SheetRowReference> datasComments;
	
	
	
	
	public MassImportException(List<SheetRowReference> datasComments) {
		super();
		this.datasComments = datasComments;
	}

	public MassImportException(String message, Throwable cause, List<SheetRowReference> datasComments) {
		super(message, cause);
		this.datasComments = datasComments;
	}

	public MassImportException(String message, List<SheetRowReference> datasComments) {
		super(message);
		this.datasComments = datasComments;
	}

	public MassImportException(Throwable cause, List<SheetRowReference> datasComments) {
		super(cause);
		this.datasComments = datasComments;
	}
	
	
	
	
	
	
	public List<SheetRowReference> getDatasComments() {
		return datasComments;
	}
	public void setDatasComments(List<SheetRowReference> datasComments) {
		this.datasComments = datasComments;
	}
	

}
