package ci.ueeso.gestion.eglise.excel.mass.importation;

import java.util.Objects;

import lombok.Data;

@Data
public class ExportCriteriaLevel {

	private Integer initialCriteriaRowIndex;
	private Integer initialCriteriaColIndex;
	private Integer numberOfCriteriaPerStack;
	private Integer colIncrementStep;
	
	private Integer criteriaRowIndex;
	private Integer criteriaColIndex;
	private int numberOfCriteria;
	

	public ExportCriteriaLevel() {}
	
	public ExportCriteriaLevel(Integer initialCriteriaRowIndex, Integer initialCriteriaColIndex, Integer numberOfCriteriaPerStack, Integer colIncrementStep) {
		this.initialCriteriaRowIndex = initialCriteriaRowIndex;
		this.initialCriteriaColIndex = initialCriteriaColIndex;
		this.numberOfCriteriaPerStack = numberOfCriteriaPerStack;
		this.colIncrementStep = colIncrementStep;
	}
	
	
	public static void incrementLevel(ExportCriteriaLevel currentLevel) {
		Objects.requireNonNull(currentLevel, "currentLevel ne doit pas être null.");
		Objects.requireNonNull(currentLevel.getInitialCriteriaRowIndex(), "currentLevel.initialCriteriaRowIndex ne doit pas être null.");
		Objects.requireNonNull(currentLevel.getInitialCriteriaColIndex(), "currentLevel.initialCriteriaColIndex ne doit pas être null.");
		Objects.requireNonNull(currentLevel.getNumberOfCriteriaPerStack(), "currentLevel.numberOfCriteriaPerStack ne doit pas être null.");
		Objects.requireNonNull(currentLevel.getColIncrementStep(), "currentLevel.colIncrementStep ne doit pas être null.");
		
		
		
		Integer numberOfCriteriaPerStack = currentLevel.getNumberOfCriteriaPerStack();
		if(numberOfCriteriaPerStack <= 0) {
			numberOfCriteriaPerStack = 4;
		}
		
		Integer colIncrementStep = currentLevel.getColIncrementStep();
		if(colIncrementStep <= 0) {
			colIncrementStep = 2;
		}
		
		if(currentLevel.getNumberOfCriteria() == 0) {
			currentLevel.setCriteriaRowIndex(currentLevel.getInitialCriteriaRowIndex());
			currentLevel.setCriteriaColIndex(currentLevel.getInitialCriteriaColIndex());
		}
		else {
			if(currentLevel.getNumberOfCriteria() % numberOfCriteriaPerStack == 0) {
				currentLevel.setCriteriaRowIndex(currentLevel.getInitialCriteriaRowIndex());
				currentLevel.setCriteriaColIndex(currentLevel.getCriteriaColIndex() + colIncrementStep);
			}
			else {
				currentLevel.setCriteriaRowIndex(currentLevel.getCriteriaRowIndex() + 1);
				//currentLevel.setCriteriaColIndex(currentLevel.getCriteriaColIndex());
			}
		}
		
		currentLevel.setNumberOfCriteria(currentLevel.getNumberOfCriteria() + 1);
	}


}
