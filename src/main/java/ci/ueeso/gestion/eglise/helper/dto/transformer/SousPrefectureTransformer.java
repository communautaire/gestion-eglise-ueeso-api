

/*
 * Java transformer for entity table sous_prefecture 
 * Created on 2020-01-18 ( Time 09:57:26 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.dao.entity.Departement;
import ci.ueeso.gestion.eglise.dao.entity.SousPrefecture;
import ci.ueeso.gestion.eglise.helper.contrat.FullTransformerQualifier;
import ci.ueeso.gestion.eglise.helper.dto.SousPrefectureDto;


/**
 * TRANSFORMER for table "sous_prefecture"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface SousPrefectureTransformer {

	SousPrefectureTransformer INSTANCE = Mappers.getMapper(SousPrefectureTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="departement"),
		@Mapping(source="entity.departement.id", target="idDepartement"),
		@Mapping(source="entity.departement.libelle", target="departementLibelle"),
	})
	SousPrefectureDto toDto(SousPrefecture entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<SousPrefectureDto> toDtos(List<SousPrefecture> entities) throws ParseException;

    public default SousPrefectureDto toLiteDto(SousPrefecture entity) {
		if (entity == null) {
			return null;
		}
		SousPrefectureDto dto = new SousPrefectureDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<SousPrefectureDto> toLiteDtos(List<SousPrefecture> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<SousPrefectureDto> dtos = new ArrayList<SousPrefectureDto>();
		for (SousPrefecture entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.chefLieu", target="chefLieu"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deleted", target="deleted"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="departement", target="departement"),
	})
    SousPrefecture toEntity(SousPrefectureDto dto, Departement departement) throws ParseException;

    //List<SousPrefecture> toEntities(List<SousPrefectureDto> dtos) throws ParseException;

}
