
/*
 * Java dto for entity table role_functionality 
 * Created on 2020-01-18 ( Time 09:57:25 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ci.ueeso.gestion.eglise.helper.contrat.SearchParam;
import ci.ueeso.gestion.eglise.helper.dto.customize._RoleFunctionalityDto;
import lombok.Data;
import lombok.ToString;

/**
 * DTO for table "role_functionality"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class RoleFunctionalityDto extends _RoleFunctionalityDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    idRole               ;
    private Integer    idFonctionality      ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    role;
	private String roleCode;
	private String roleLibelle;
	//private Integer    functionality;
	private String functionalityCode;
	private String functionalityLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  idRoleParam           ;                     
	private SearchParam<Integer>  idFonctionalityParam  ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  roleParam             ;                     
	private SearchParam<String>   roleCodeParam         ;                     
	private SearchParam<String>   roleLibelleParam      ;                     
	private SearchParam<Integer>  functionalityParam    ;                     
	private SearchParam<String>   functionalityCodeParam;                     
	private SearchParam<String>   functionalityLibelleParam;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public RoleFunctionalityDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
