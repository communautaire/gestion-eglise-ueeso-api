

/*
 * Java transformer for entity table smtp_server 
 * Created on 2020-01-24 ( Time 19:04:40 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "smtp_server"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface SmtpServerTransformer {

	SmtpServerTransformer INSTANCE = Mappers.getMapper(SmtpServerTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	SmtpServerDto toDto(SmtpServer entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<SmtpServerDto> toDtos(List<SmtpServer> entities) throws ParseException;

    public default SmtpServerDto toLiteDto(SmtpServer entity) {
		if (entity == null) {
			return null;
		}
		SmtpServerDto dto = new SmtpServerDto();
		dto.setId( entity.getId() );
		dto.setLogin( entity.getLogin() );
		return dto;
    }

	public default List<SmtpServerDto> toLiteDtos(List<SmtpServer> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<SmtpServerDto> dtos = new ArrayList<SmtpServerDto>();
		for (SmtpServer entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.host", target="host"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.port", target="port"),
		@Mapping(source="dto.smtpUser", target="smtpUser"),
		@Mapping(source="dto.auth", target="auth"),
		@Mapping(source="dto.enableSsl", target="enableSsl"),
		@Mapping(source="dto.sslProtocols", target="sslProtocols"),
		@Mapping(source="dto.enableStarttls", target="enableStarttls"),
		@Mapping(source="dto.isStarttlsRequired", target="isStarttlsRequired"),
		@Mapping(source="dto.hostsToTrust", target="hostsToTrust"),
		@Mapping(source="dto.isSmtpsProtocol", target="isSmtpsProtocol"),
		@Mapping(source="dto.isActive", target="isActive"),
		@Mapping(source="dto.senderUsername", target="senderUsername"),
		@Mapping(source="dto.senderEmail", target="senderEmail"),
		@Mapping(source="dto.testEmail", target="testEmail"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    SmtpServer toEntity(SmtpServerDto dto) throws ParseException;

    //List<SmtpServer> toEntities(List<SmtpServerDto> dtos) throws ParseException;

}
