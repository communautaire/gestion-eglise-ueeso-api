

/*
 * Java transformer for entity table departement 
 * Created on 2020-01-18 ( Time 09:57:21 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.dao.entity.Departement;
import ci.ueeso.gestion.eglise.dao.entity.Region;
import ci.ueeso.gestion.eglise.helper.contrat.FullTransformerQualifier;
import ci.ueeso.gestion.eglise.helper.dto.DepartementDto;


/**
 * TRANSFORMER for table "departement"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface DepartementTransformer {

	DepartementTransformer INSTANCE = Mappers.getMapper(DepartementTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="region"),
		@Mapping(source="entity.region.id", target="idRegion"),
		@Mapping(source="entity.region.libelle", target="regionLibelle"),
	})
	DepartementDto toDto(Departement entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<DepartementDto> toDtos(List<Departement> entities) throws ParseException;

    public default DepartementDto toLiteDto(Departement entity) {
		if (entity == null) {
			return null;
		}
		DepartementDto dto = new DepartementDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<DepartementDto> toLiteDtos(List<Departement> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<DepartementDto> dtos = new ArrayList<DepartementDto>();
		for (Departement entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.chefLieu", target="chefLieu"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deleted", target="deleted"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="region", target="region"),
	})
    Departement toEntity(DepartementDto dto, Region region) throws ParseException;

    //List<Departement> toEntities(List<DepartementDto> dtos) throws ParseException;

}
