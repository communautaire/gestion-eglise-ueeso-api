

/*
 * Java transformer for entity table periode 
 * Created on 2019-12-02 ( Time 00:15:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "periode"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface PeriodeTransformer {

	PeriodeTransformer INSTANCE = Mappers.getMapper(PeriodeTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.datePaiement", dateFormat="dd/MM/yyyy",target="datePaiement"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	PeriodeDto toDto(Periode entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<PeriodeDto> toDtos(List<Periode> entities) throws ParseException;

    public default PeriodeDto toLiteDto(Periode entity) {
		if (entity == null) {
			return null;
		}
		PeriodeDto dto = new PeriodeDto();
		dto.setId( entity.getId() );
		return dto;
    }

	public default List<PeriodeDto> toLiteDtos(List<Periode> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<PeriodeDto> dtos = new ArrayList<PeriodeDto>();
		for (Periode entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.datePaiement", dateFormat="dd/MM/yyyy",target="datePaiement"),
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dayofweek", target="dayofweek"),
		@Mapping(source="dto.dayofmonth", target="dayofmonth"),
		@Mapping(source="dto.dayofyear", target="dayofyear"),
		@Mapping(source="dto.weekofmonth", target="weekofmonth"),
		@Mapping(source="dto.weekofyear", target="weekofyear"),
		@Mapping(source="dto.month", target="month"),
		@Mapping(source="dto.trimester", target="trimester"),
		@Mapping(source="dto.semester", target="semester"),
		@Mapping(source="dto.year", target="year"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    Periode toEntity(PeriodeDto dto) throws ParseException;

    //List<Periode> toEntities(List<PeriodeDto> dtos) throws ParseException;

}
