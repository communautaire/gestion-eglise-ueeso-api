
/*
 * Java dto for entity table periode 
 * Created on 2020-01-18 ( Time 09:57:24 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ci.ueeso.gestion.eglise.helper.contrat.SearchParam;
import ci.ueeso.gestion.eglise.helper.dto.customize._PeriodeDto;
import lombok.Data;
import lombok.ToString;

/**
 * DTO for table "periode"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class PeriodeDto extends _PeriodeDto implements Cloneable{

    private Integer    id                   ; // Primary Key

	private String     datePaiement         ;
    private Integer    dayofweek            ;
    private Integer    dayofmonth           ;
    private Integer    dayofyear            ;
    private Integer    weekofmonth          ;
    private Integer    weekofyear           ;
    private Integer    month                ;
    private Integer    trimester            ;
    private Integer    semester             ;
    private Integer    year                 ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   datePaiementParam     ;                     
	private SearchParam<Integer>  dayofweekParam        ;                     
	private SearchParam<Integer>  dayofmonthParam       ;                     
	private SearchParam<Integer>  dayofyearParam        ;                     
	private SearchParam<Integer>  weekofmonthParam      ;                     
	private SearchParam<Integer>  weekofyearParam       ;                     
	private SearchParam<Integer>  monthParam            ;                     
	private SearchParam<Integer>  trimesterParam        ;                     
	private SearchParam<Integer>  semesterParam         ;                     
	private SearchParam<Integer>  yearParam             ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public PeriodeDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
