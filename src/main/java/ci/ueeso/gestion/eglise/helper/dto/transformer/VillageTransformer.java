

/*
 * Java transformer for entity table village 
 * Created on 2020-01-18 ( Time 09:57:28 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.dao.entity.Village;
import ci.ueeso.gestion.eglise.helper.contrat.FullTransformerQualifier;
import ci.ueeso.gestion.eglise.helper.dto.VillageDto;


/**
 * TRANSFORMER for table "village"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface VillageTransformer {

	VillageTransformer INSTANCE = Mappers.getMapper(VillageTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	VillageDto toDto(Village entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<VillageDto> toDtos(List<Village> entities) throws ParseException;

    public default VillageDto toLiteDto(Village entity) {
		if (entity == null) {
			return null;
		}
		VillageDto dto = new VillageDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<VillageDto> toLiteDtos(List<Village> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<VillageDto> dtos = new ArrayList<VillageDto>();
		for (Village entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.chefLieu", target="chefLieu"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deleted", target="deleted"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    Village toEntity(VillageDto dto) throws ParseException;

    //List<Village> toEntities(List<VillageDto> dtos) throws ParseException;

}
