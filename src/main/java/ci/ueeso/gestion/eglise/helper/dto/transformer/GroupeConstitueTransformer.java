

/*
 * Java transformer for entity table groupe_constitue 
 * Created on 2019-11-30 ( Time 18:43:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "groupe_constitue"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface GroupeConstitueTransformer {

	GroupeConstitueTransformer INSTANCE = Mappers.getMapper(GroupeConstitueTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	GroupeConstitueDto toDto(GroupeConstitue entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<GroupeConstitueDto> toDtos(List<GroupeConstitue> entities) throws ParseException;

    public default GroupeConstitueDto toLiteDto(GroupeConstitue entity) {
		if (entity == null) {
			return null;
		}
		GroupeConstitueDto dto = new GroupeConstitueDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<GroupeConstitueDto> toLiteDtos(List<GroupeConstitue> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<GroupeConstitueDto> dtos = new ArrayList<GroupeConstitueDto>();
		for (GroupeConstitue entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    GroupeConstitue toEntity(GroupeConstitueDto dto) throws ParseException;

    //List<GroupeConstitue> toEntities(List<GroupeConstitueDto> dtos) throws ParseException;

}
