package ci.ueeso.gestion.eglise.helper;

import java.util.HashMap;
import java.util.Map;

public class HtmlSpecialChars {

	private final Map<String, String> mappings;
	
	
	public HtmlSpecialChars() {
		mappings = new HashMap<String, String>();
		mappings.put("\"", "&quot;");
		//mappings.put("&", "&amp;");
		mappings.put("€", "&euro;");
		//mappings.put("<", "&lt;");
		//mappings.put(">", "&gt;");
		mappings.put("œ", "&oelig;");
		//mappings.put("Y", "&Yuml;");
		//mappings.put(" ", "&nbsp;");
		mappings.put("¡", "&iexcl;");
		mappings.put("¢", "&cent;");
		mappings.put("£", "&pound;");
		mappings.put("¤", "&curren;");
		mappings.put("¥", "&yen;");
		mappings.put("¦", "&brvbar;");
		mappings.put("§", "&sect;");
		mappings.put("¨", "&uml;");
		mappings.put("©", "&copy;");
		mappings.put("ª", "&ordf;");
		mappings.put("«", "&laquo;");
		mappings.put("¬", "&not;");
		mappings.put("", "&shy;");
		mappings.put("®", "&reg;");
		mappings.put("¯", "&masr;");
		mappings.put("°", "&deg;");
		mappings.put("±", "&plusmn;");
		mappings.put("²", "&sup2;");
		mappings.put("³", "&sup3;");
		//mappings.put("'", "&acute;");
		mappings.put("µ", "&micro;");
		mappings.put("¶", "&para;");
		mappings.put("·", "&middot;");
		mappings.put("¸", "&cedil;");
		mappings.put("¹", "&sup1;");
		mappings.put("º", "&ordm;");
		mappings.put("»", "&raquo;");
		mappings.put("¼", "&frac14;");
		mappings.put("½", "&frac12;");
		mappings.put("¾", "&frac34;");
		mappings.put("¿", "&iquest;");
		mappings.put("À", "&Agrave;");
		mappings.put("Á", "&Aacute;");
		mappings.put("Â", "&Acirc;");
		mappings.put("Ã", "&Atilde;");
		mappings.put("Ä", "&Auml;");
		mappings.put("Å", "&Aring;");
		mappings.put("Æ", "&Aelig;");
		mappings.put("Ç", "&Ccedil;");
		mappings.put("È", "&Egrave;");
		mappings.put("É", "&Eacute;");
		mappings.put("Ê", "&Ecirc;");
		mappings.put("Ë", "&Euml;");
		mappings.put("Ì", "&Igrave;");
		mappings.put("Í", "&Iacute;");
		mappings.put("Î", "&Icirc;");
		mappings.put("Ï", "&Iuml;");
		mappings.put("Ð", "&eth;");
		mappings.put("Ñ", "&Ntilde;");
		mappings.put("Ò", "&Ograve;");
		mappings.put("Ó", "&Oacute;");
		mappings.put("Ô", "&Ocirc;");
		mappings.put("Õ", "&Otilde;");
		mappings.put("Ö", "&Ouml;");
		mappings.put("×", "&times;");
		mappings.put("Ø", "&Oslash;");
		mappings.put("Ù", "&Ugrave;");
		mappings.put("Ú", "&Uacute;");
		mappings.put("Û", "&Ucirc;");
		mappings.put("Ü", "&Uuml;");
		mappings.put("Ý", "&Yacute;");
		mappings.put("Þ", "&thorn;");
		mappings.put("ß", "&szlig;");
		mappings.put("à", "&agrave;");
		mappings.put("á", "&aacute;");
		mappings.put("â", "&acirc;");
		mappings.put("ã", "&atilde;");
		mappings.put("ä", "&auml;");
		mappings.put("å", "&aring;");
		mappings.put("æ", "&aelig;");
		mappings.put("ç", "&ccedil;");
		mappings.put("è", "&egrave;");
		mappings.put("é", "&eacute;");
		mappings.put("ê", "&ecirc;");
		mappings.put("ë", "&euml;");
		mappings.put("ì", "&igrave;");
		mappings.put("í", "&iacute;");
		mappings.put("î", "&icirc;");
		mappings.put("ï", "&iuml;");
		mappings.put("ð", "&eth;");
		mappings.put("ñ", "&ntilde;");
		mappings.put("ò", "&ograve;");
		mappings.put("ó", "&oacute;");
		mappings.put("ô", "&ocirc;");
		mappings.put("õ", "&otilde;");
		mappings.put("ö", "&ouml;");
		mappings.put("÷", "&divide;");
		mappings.put("ø", "&oslash;");
		mappings.put("ù", "&ugrave;");
		mappings.put("ú", "&uacute;");
		mappings.put("û", "&ucirc;");
		mappings.put("ü", "&uuml;");
		mappings.put("ý", "&yacute;");
		mappings.put("þ", "&thorn;");
		mappings.put("ÿ", "&yuml;");
	}

	public String replaceHtmlSpecialChars(String source) {
		if(Utilities.isBlank(source)) {
			return "";
		}
		String result = "";
		for (int i = 0; i < source.length(); i++) {
			//System.out.println("source.charAt(i) = "+source.charAt(i));
			if(mappings.containsKey(source.charAt(i)+"")) {
				String htmlChar = mappings.get(source.charAt(i)+"");
				//source = source.replaceFirst(source.charAt(i)+"", htmlChar);
				result += htmlChar;
			}
			else {
				result += source.charAt(i)+"";
			}
		}
		System.out.println("source = "+source+" ==> result = "+result);
		return result;
	}


	public Map<String, String> getMappings() {
		return mappings;
	}
//	public void setMappings(Map<String, String> mappings) {
//		this.mappings = mappings;
//	}
}
