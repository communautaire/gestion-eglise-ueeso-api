/*
 * Created on 2019-11-30 ( Time 19:16:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Backend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.contrat;


import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import org.mapstruct.Qualifier;

/**
 * Request Base
 * 
 * @author Smile Backend generator
 *
 */
@Qualifier
@Target(ElementType.METHOD)
public @interface FullTransformerQualifier {

}
