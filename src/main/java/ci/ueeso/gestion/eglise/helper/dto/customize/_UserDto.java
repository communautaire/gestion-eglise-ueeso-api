
/*
 * Java dto for entity table user 
 * Created on 2019-11-30 ( Time 18:43:53 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.ueeso.gestion.eglise.helper.dto.AdresseDto;
import ci.ueeso.gestion.eglise.helper.dto.UserGroupeConstitueDto;
import ci.ueeso.gestion.eglise.helper.dto.UserMissionDto;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "user"
 * 
 * @author Smile Back-End generator
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _UserDto {

	
	//image du terminal
	protected String     fileName             ;
    protected String     fileBase64           ;
    protected String     extension            ;
    
    protected Integer residenceCountryId; // Primary Key
    protected String residenceCountryName;
    protected String residenceDistrict;
    protected String residenceDistrictChefLieu;
    protected String residenceVille;
    protected String residenceCommune;
    protected String residenceAdresse;
    protected String residenceQuartier;
    protected String fonction;
    
	protected List<AdresseDto> datasResidenceAdresse;
	protected List<UserGroupeConstitueDto> datasUserGroupeConstitue;
	protected List<UserMissionDto> datasUserMission;
	
	private List<String> fonctionnalities;
	
//	*residence_country
//	*residence_district
//	*residence_ville
//	residence_commune
//	*residence_adresse
}
