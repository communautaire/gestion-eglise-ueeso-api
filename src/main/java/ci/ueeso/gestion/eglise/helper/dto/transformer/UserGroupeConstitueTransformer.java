

/*
 * Java transformer for entity table user_groupe_constitue 
 * Created on 2020-01-23 ( Time 10:51:51 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "user_groupe_constitue"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface UserGroupeConstitueTransformer {

	UserGroupeConstitueTransformer INSTANCE = Mappers.getMapper(UserGroupeConstitueTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="user"),
		@Mapping(source="entity.user.id", target="idUser"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.email", target="userEmail"),
		@Mapping(source="entity.user.telephone", target="userTelephone"),
		@Mapping(source="entity.user.login", target="userLogin"),
		//@Mapping(ignore=true, target="fonction"),
		@Mapping(source="entity.fonction.id", target="idFonction"),
		@Mapping(source="entity.fonction.libelle", target="fonctionLibelle"),
		//@Mapping(ignore=true, target="groupeConstitue"),
		@Mapping(source="entity.groupeConstitue.id", target="idGroupeConstitue"),
		@Mapping(source="entity.groupeConstitue.code", target="groupeConstitueCode"),
		@Mapping(source="entity.groupeConstitue.libelle", target="groupeConstitueLibelle"),
	})
	UserGroupeConstitueDto toDto(UserGroupeConstitue entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<UserGroupeConstitueDto> toDtos(List<UserGroupeConstitue> entities) throws ParseException;

    public default UserGroupeConstitueDto toLiteDto(UserGroupeConstitue entity) {
		if (entity == null) {
			return null;
		}
		UserGroupeConstitueDto dto = new UserGroupeConstitueDto();
		dto.setId( entity.getId() );
		return dto;
    }

	public default List<UserGroupeConstitueDto> toLiteDtos(List<UserGroupeConstitue> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<UserGroupeConstitueDto> dtos = new ArrayList<UserGroupeConstitueDto>();
		for (UserGroupeConstitue entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="user", target="user"),
		@Mapping(source="fonction", target="fonction"),
		@Mapping(source="groupeConstitue", target="groupeConstitue"),
	})
    UserGroupeConstitue toEntity(UserGroupeConstitueDto dto, User user, Fonction fonction, GroupeConstitue groupeConstitue) throws ParseException;

    //List<UserGroupeConstitue> toEntities(List<UserGroupeConstitueDto> dtos) throws ParseException;

}
