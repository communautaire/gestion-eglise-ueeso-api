
/*
 * Java dto for entity table ville 
 * Created on 2020-01-18 ( Time 17:01:44 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.customize;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.ueeso.gestion.eglise.helper.contrat.*;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "ville"
 * 
 * @author Smile Back-End generator
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _VilleDto {

}
