
/*
 * Java dto for entity table tresorier 
 * Created on 2020-01-18 ( Time 09:57:26 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ci.ueeso.gestion.eglise.helper.contrat.SearchParam;
import ci.ueeso.gestion.eglise.helper.dto.customize._TresorierDto;
import lombok.Data;
import lombok.ToString;

/**
 * DTO for table "tresorier"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class TresorierDto extends _TresorierDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    idUser               ;
    private String     mobileAccount1       ;
    private String     mobileAccount2       ;
    private String     mobileAccount3       ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    user;
	private String userNom;
	private String userPrenom;
	private String userEmail;
	private String userTelephone;
	private String userLogin;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  idUserParam           ;                     
	private SearchParam<String>   mobileAccount1Param   ;                     
	private SearchParam<String>   mobileAccount2Param   ;                     
	private SearchParam<String>   mobileAccount3Param   ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  userParam             ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomParam       ;                     
	private SearchParam<String>   userEmailParam        ;                     
	private SearchParam<String>   userTelephoneParam    ;                     
	private SearchParam<String>   userLoginParam        ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public TresorierDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
