

/*
 * Java transformer for entity table quartier 
 * Created on 2020-03-15 ( Time 00:33:36 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "quartier"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface QuartierTransformer {

	QuartierTransformer INSTANCE = Mappers.getMapper(QuartierTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="village"),
		@Mapping(source="entity.village.id", target="idVillage"),
		@Mapping(source="entity.village.libelle", target="villageLibelle"),
		//@Mapping(ignore=true, target="commune"),
		@Mapping(source="entity.commune.id", target="idCommune"),
		@Mapping(source="entity.commune.code", target="communeCode"),
		@Mapping(source="entity.commune.libelle", target="communeLibelle"),
	})
	QuartierDto toDto(Quartier entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<QuartierDto> toDtos(List<Quartier> entities) throws ParseException;

    public default QuartierDto toLiteDto(Quartier entity) {
		if (entity == null) {
			return null;
		}
		QuartierDto dto = new QuartierDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<QuartierDto> toLiteDtos(List<Quartier> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<QuartierDto> dtos = new ArrayList<QuartierDto>();
		for (Quartier entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.longitude", target="longitude"),
		@Mapping(source="dto.latitude", target="latitude"),
		@Mapping(source="dto.superficie", target="superficie"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="village", target="village"),
		@Mapping(source="commune", target="commune"),
	})
    Quartier toEntity(QuartierDto dto, Village village, Commune commune) throws ParseException;

    //List<Quartier> toEntities(List<QuartierDto> dtos) throws ParseException;

}
