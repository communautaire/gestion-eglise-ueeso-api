
/*
 * Java dto for entity table user 
 * Created on 2020-03-07 ( Time 22:09:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.customize._UserDto;

/**
 * DTO for table "user"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class UserDto extends _UserDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private String     nom                  ;
    private String     prenom               ;
    private String     email                ;
    private String     telephone            ;
    private String     login                ;
    private String     preGeneratedLogin    ;
    private String     password             ;
	private String     dateNaissance        ;
    private String     photo                ;
    private Integer    idSituationProfessionnelle ;
    private Integer    idRole               ;
    private Boolean    isLocalResident      ;
    private Integer    idSituationMatrimoniale ;
    private String     nomConjoint          ;
	private String     dateVieConjugale     ;
    private Boolean    isHomeRespo          ;
    private String     homeRespoName        ;
	private String     dateArriveUeeso      ;
	private String     dateConversion       ;
    private Boolean    baptemeImmersion     ;
	private String     dateBapteme          ;
    private String     egliseBapteme        ;
    private String     egliseOrigine        ;
    private Integer    idTypeUser           ;
	/*
	 * Pays d'orgine
	 */
    private Integer    idPays               ;
    private Integer    idProfession         ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;
    private Boolean    locked               ;
    private Boolean    isConnected          ;
	/*
	 * compte utilisateur validé ou pas par l'administrateur
	 */
    private Boolean    isValidated          ;
	/*
	 * lieu de naissance
	 */
    private String     lieuNaissance        ;
	/*
	 * liste des domaine de competence séparés par des virgules
	 */
    private String     domaineCompetence    ;
    private Integer    idMemberStatus       ;
    private String     civilite             ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    profession;
	private String professionCode;
	private String professionLibelle;
	//private Integer    pays;
	private String paysCode;
	private String paysLibelle;
	//private Integer    memberStatus;
	private String memberStatusCode;
	private String memberStatusLibelle;
	//private Integer    situationMatrimoniale;
	private String situationMatrimonialeCode;
	private String situationMatrimonialeLibelle;
	//private Integer    situationProfessionnelle;
	private String situationProfessionnelleCode;
	private String situationProfessionnelleLibelle;
	//private Integer    typeUser;
	private String typeUserCode;
	private String typeUserLibelle;
	//private Integer    role;
	private String roleCode;
	private String roleLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   prenomParam           ;                     
	private SearchParam<String>   emailParam            ;                     
	private SearchParam<String>   telephoneParam        ;                     
	private SearchParam<String>   loginParam            ;                     
	private SearchParam<String>   preGeneratedLoginParam;                     
	private SearchParam<String>   passwordParam         ;                     
	private SearchParam<String>   dateNaissanceParam    ;                     
	private SearchParam<String>   photoParam            ;                     
	private SearchParam<Integer>  idSituationProfessionnelleParam;                     
	private SearchParam<Integer>  idRoleParam           ;                     
	private SearchParam<Boolean>  isLocalResidentParam  ;                     
	private SearchParam<Integer>  idSituationMatrimonialeParam;                     
	private SearchParam<String>   nomConjointParam      ;                     
	private SearchParam<String>   dateVieConjugaleParam ;                     
	private SearchParam<Boolean>  isHomeRespoParam      ;                     
	private SearchParam<String>   homeRespoNameParam    ;                     
	private SearchParam<String>   dateArriveUeesoParam  ;                     
	private SearchParam<String>   dateConversionParam   ;                     
	private SearchParam<Boolean>  baptemeImmersionParam ;                     
	private SearchParam<String>   dateBaptemeParam      ;                     
	private SearchParam<String>   egliseBaptemeParam    ;                     
	private SearchParam<String>   egliseOrigineParam    ;                     
	private SearchParam<Integer>  idTypeUserParam       ;                     
	private SearchParam<Integer>  idPaysParam           ;                     
	private SearchParam<Integer>  idProfessionParam     ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Boolean>  lockedParam           ;                     
	private SearchParam<Boolean>  isConnectedParam      ;                     
	private SearchParam<Boolean>  isValidatedParam      ;                     
	private SearchParam<String>   lieuNaissanceParam    ;                     
	private SearchParam<String>   domaineCompetenceParam;                     
	private SearchParam<Integer>  idMemberStatusParam   ;                     
	private SearchParam<String>   civiliteParam         ;                     
	private SearchParam<Integer>  professionParam       ;                     
	private SearchParam<String>   professionCodeParam   ;                     
	private SearchParam<String>   professionLibelleParam;                     
	private SearchParam<Integer>  paysParam             ;                     
	private SearchParam<String>   paysCodeParam         ;                     
	private SearchParam<String>   paysLibelleParam      ;                     
	private SearchParam<Integer>  memberStatusParam     ;                     
	private SearchParam<String>   memberStatusCodeParam ;                     
	private SearchParam<String>   memberStatusLibelleParam;                     
	private SearchParam<Integer>  situationMatrimonialeParam;                     
	private SearchParam<String>   situationMatrimonialeCodeParam;                     
	private SearchParam<String>   situationMatrimonialeLibelleParam;                     
	private SearchParam<Integer>  situationProfessionnelleParam;                     
	private SearchParam<String>   situationProfessionnelleCodeParam;                     
	private SearchParam<String>   situationProfessionnelleLibelleParam;                     
	private SearchParam<Integer>  typeUserParam         ;                     
	private SearchParam<String>   typeUserCodeParam     ;                     
	private SearchParam<String>   typeUserLibelleParam  ;                     
	private SearchParam<Integer>  roleParam             ;                     
	private SearchParam<String>   roleCodeParam         ;                     
	private SearchParam<String>   roleLibelleParam      ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public UserDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
