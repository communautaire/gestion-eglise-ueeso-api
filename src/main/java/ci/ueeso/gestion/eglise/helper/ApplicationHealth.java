package ci.ueeso.gestion.eglise.helper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

public class ApplicationHealth {

	public static boolean applicationStatus(ParamsUtils paramsUtils) throws IOException {
		boolean status = false;
		okhttp3.Response res = null;
		res = CallAPI(paramsUtils.getMonitorAppUrl());
		String message = "";
		if (res.code() == 200) {
			message = res.body().string();
			if (message != null) {
				JSONObject jsonObject = new JSONObject(message);
				if (jsonObject.get("status").toString().equals("UP")) {
					status = true;
				}
			}
		}
		return status;
	}

	public static boolean elasticSearchStatus(ParamsUtils paramsUtils) throws IOException {
		boolean status = false;
		okhttp3.Response res = null;
		res = CallAPI(paramsUtils.getMonitorAppUrl());
		String message = "";
		if (res.code() == 200) {
			message = res.body().string();
			if (message != null) {
				JSONObject jsonObject = new JSONObject(message);
				if (jsonObject.getJSONObject("elasticSearchHealth").get("status").toString().equals("UP")) {
					status = true;
				}
			}
		}
		return status;
	}

	public static boolean dbStatus(ParamsUtils paramsUtils) throws IOException {
		boolean status = false;
		okhttp3.Response res = null;
		res = CallAPI(paramsUtils.getMonitorAppUrl());
		String message = "";
		if (res.code() == 200) {
			message = res.body().string();
			if (message != null) {
				JSONObject jsonObject = new JSONObject(message);
				if (jsonObject.getJSONObject("db").get("status").toString().equals("UP")) {
					status = true;
				}
			}
		}
		return status;
	}

	public static boolean diskSpaceStatus(ParamsUtils paramsUtils) throws IOException {
		boolean status = false;
		okhttp3.Response res = null;
		res = CallAPI(paramsUtils.getMonitorAppUrl());
		String message = "";
		if (res.code() == 200) {
			message = res.body().string();
			if (message != null) {
				JSONObject jsonObject = new JSONObject(message);
				if (jsonObject.getJSONObject("diskSpace").get("status").toString().equals("UP")) {
					status = true;
				}
			}
		}
		return status;
	}

	public static okhttp3.Response CallAPI(String url) throws IOException {
		// avoid creating several instances, should be singleon
		// OkHttpClient client = new OkHttpClient();
		okhttp3.OkHttpClient client = new okhttp3.OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES)
				.writeTimeout(1, TimeUnit.MINUTES).build();
		okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();

		okhttp3.Response response = client.newCall(request).execute();

		return response;
	}

}
