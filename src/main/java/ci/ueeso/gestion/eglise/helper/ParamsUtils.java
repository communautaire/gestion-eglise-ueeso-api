
/*
 * Created on 2019-11-30 ( Time 19:16:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Smile Backend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Math Utils
 * 
 * @author Smile Backend generator
 *
 */

@Component
@Getter
@NoArgsConstructor
@ToString
public class ParamsUtils {    	
	
	@Value("${jwt.header}")
	private String	tokenHeader;

	@Value("${jwt.secret}")
	private String	secret;

	@Value("${jwt.bearer}")
	private String	bearer;

	@Value("${smtp.mail.host}")
	private String	smtpHost;

	@Value("${smtp.mail.port}")
	private Integer	smtpPort;

	@Value("${smtp.mail.username}")
	private String	smtpUsername;

	@Value("${smtp.mail.password}")
	private String	smtpPassword;





	//Urls

	@Value("${monitor.app.url}")
	private String	monitorAppUrl;

	@Value("${url.root}")
	private String	rootFilesUrl;

	@Value("${path.root}")
	private String	rootFilesPath;

	@Value("${url.root.office.front}")
	private String frontOfficeRootUrl;

	@Value("${url.root.office.back}")
	private String backOfficeRootUrl;

//	@Value("${files.directory.report}")
//	private String  filesDirectoryReport;





	//Emails templates

	@Value("${template.email.test}")
	private String testEmailTemplate;

	@Value("${template.email.welcome}")
	private String welcomeEmailTemplate;

	@Value("${template.email.reset.forgotten.user.password}")
	private String resetForgottenUserPasswordEmailTemplate;

	@Value("${template.email.reset.password.succes}")
	private String resetPasswordSuccesEmailTemplate;

	@Value("${template.email.notification.modification.compte}")
	private String notificationModificationCompteEmailTemplate;





	//Files location


	@Value("${textfile.directory}")
	private String			textfileDirectory;

	@Value("${image.directory}")
	private String			imageDirectory;

	@Value("${video.directory}")
	private String			videoDirectory;

	@Value("${otherfile.directory}")
	private String			otherfileDirectory;

	



	//Others value
	
	
	@Value("${office.excel.max.line.number}")
	private Integer			excelMaxLineNumber;
	
	

	@Value("${files.directory}")
	private String	filesDirectory;

	
	public String getFilesDirectory() {
		String baseDir = System.getenv("CATALINA_HOME");
		System.out.println(baseDir);
		
		if(baseDir!=null) {
			if (!baseDir.endsWith("/"))
				baseDir += "/";
		}else {
			baseDir = "/";
		}
		return baseDir + filesDirectory;
	}
	
	
	
	
	
	
	
	

}
