

/*
 * Java transformer for entity table situation_matrimoniale 
 * Created on 2020-01-24 ( Time 22:03:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "situation_matrimoniale"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface SituationMatrimonialeTransformer {

	SituationMatrimonialeTransformer INSTANCE = Mappers.getMapper(SituationMatrimonialeTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
	})
	SituationMatrimonialeDto toDto(SituationMatrimoniale entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<SituationMatrimonialeDto> toDtos(List<SituationMatrimoniale> entities) throws ParseException;

    public default SituationMatrimonialeDto toLiteDto(SituationMatrimoniale entity) {
		if (entity == null) {
			return null;
		}
		SituationMatrimonialeDto dto = new SituationMatrimonialeDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<SituationMatrimonialeDto> toLiteDtos(List<SituationMatrimoniale> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<SituationMatrimonialeDto> dtos = new ArrayList<SituationMatrimonialeDto>();
		for (SituationMatrimoniale entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.hasWeddedLife", target="hasWeddedLife"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    SituationMatrimoniale toEntity(SituationMatrimonialeDto dto) throws ParseException;

    //List<SituationMatrimoniale> toEntities(List<SituationMatrimonialeDto> dtos) throws ParseException;

}
