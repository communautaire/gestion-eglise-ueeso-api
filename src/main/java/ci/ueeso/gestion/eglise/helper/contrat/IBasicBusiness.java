
/*
 * Created on 2019-11-30 ( Time 19:16:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.contrat;

import java.text.ParseException;
import java.util.Locale;

/**
 * IBasic Business
 * 
 * @author Smile Bakend generator
 *
 */
public interface IBasicBusiness<T, K> {

	/**
	 * create Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * @throws ParseException 
	 * 
	 */
	K create(T request, Locale locale) throws ParseException, Exception;

	/**
	 * update Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	K update(T request, Locale locale) throws ParseException, Exception;

	/**
	 * delete Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	K delete(T request, Locale locale) throws Exception;

	/**
	 * force deletetion Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	K forceDelete(T request, Locale locale) throws ParseException, Exception;

	/**
	 * get a List of Object by using T as criteria object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	K getByCriteria(T request, Locale locale) throws Exception;
}
