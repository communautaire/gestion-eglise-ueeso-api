

/*
 * Java transformer for entity table user 
 * Created on 2020-03-07 ( Time 22:09:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "user"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface UserTransformer {

	UserTransformer INSTANCE = Mappers.getMapper(UserTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.dateNaissance", dateFormat="dd/MM/yyyy",target="dateNaissance"),
		@Mapping(source="entity.dateVieConjugale", dateFormat="dd/MM/yyyy",target="dateVieConjugale"),
		@Mapping(source="entity.dateArriveUeeso", dateFormat="dd/MM/yyyy",target="dateArriveUeeso"),
		@Mapping(source="entity.dateConversion", dateFormat="dd/MM/yyyy",target="dateConversion"),
		@Mapping(source="entity.dateBapteme", dateFormat="dd/MM/yyyy",target="dateBapteme"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="profession"),
		@Mapping(source="entity.profession.id", target="idProfession"),
		@Mapping(source="entity.profession.code", target="professionCode"),
		@Mapping(source="entity.profession.libelle", target="professionLibelle"),
		//@Mapping(ignore=true, target="pays"),
		@Mapping(source="entity.pays.id", target="idPays"),
		@Mapping(source="entity.pays.code", target="paysCode"),
		@Mapping(source="entity.pays.libelle", target="paysLibelle"),
		//@Mapping(ignore=true, target="memberStatus"),
		@Mapping(source="entity.memberStatus.id", target="idMemberStatus"),
		@Mapping(source="entity.memberStatus.code", target="memberStatusCode"),
		@Mapping(source="entity.memberStatus.libelle", target="memberStatusLibelle"),
		//@Mapping(ignore=true, target="situationMatrimoniale"),
		@Mapping(source="entity.situationMatrimoniale.id", target="idSituationMatrimoniale"),
		@Mapping(source="entity.situationMatrimoniale.code", target="situationMatrimonialeCode"),
		@Mapping(source="entity.situationMatrimoniale.libelle", target="situationMatrimonialeLibelle"),
		//@Mapping(ignore=true, target="situationProfessionnelle"),
		@Mapping(source="entity.situationProfessionnelle.id", target="idSituationProfessionnelle"),
		@Mapping(source="entity.situationProfessionnelle.code", target="situationProfessionnelleCode"),
		@Mapping(source="entity.situationProfessionnelle.libelle", target="situationProfessionnelleLibelle"),
		//@Mapping(ignore=true, target="typeUser"),
		@Mapping(source="entity.typeUser.id", target="idTypeUser"),
		@Mapping(source="entity.typeUser.code", target="typeUserCode"),
		@Mapping(source="entity.typeUser.libelle", target="typeUserLibelle"),
		//@Mapping(ignore=true, target="role"),
		@Mapping(source="entity.role.id", target="idRole"),
		@Mapping(source="entity.role.code", target="roleCode"),
		@Mapping(source="entity.role.libelle", target="roleLibelle"),
	})
	UserDto toDto(User entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<UserDto> toDtos(List<User> entities) throws ParseException;

    public default UserDto toLiteDto(User entity) {
		if (entity == null) {
			return null;
		}
		UserDto dto = new UserDto();
		dto.setId( entity.getId() );
		dto.setNom( entity.getNom() );
		dto.setPrenom( entity.getPrenom() );
		dto.setEmail( entity.getEmail() );
		dto.setTelephone( entity.getTelephone() );
		dto.setLogin( entity.getLogin() );
		return dto;
    }

	public default List<UserDto> toLiteDtos(List<User> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<UserDto> dtos = new ArrayList<UserDto>();
		for (User entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenom", target="prenom"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.telephone", target="telephone"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.preGeneratedLogin", target="preGeneratedLogin"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.dateNaissance", dateFormat="dd/MM/yyyy",target="dateNaissance"),
		@Mapping(source="dto.photo", target="photo"),
		@Mapping(source="dto.isLocalResident", target="isLocalResident"),
		@Mapping(source="dto.nomConjoint", target="nomConjoint"),
		@Mapping(source="dto.dateVieConjugale", dateFormat="dd/MM/yyyy",target="dateVieConjugale"),
		@Mapping(source="dto.isHomeRespo", target="isHomeRespo"),
		@Mapping(source="dto.homeRespoName", target="homeRespoName"),
		@Mapping(source="dto.dateArriveUeeso", dateFormat="dd/MM/yyyy",target="dateArriveUeeso"),
		@Mapping(source="dto.dateConversion", dateFormat="dd/MM/yyyy",target="dateConversion"),
		@Mapping(source="dto.baptemeImmersion", target="baptemeImmersion"),
		@Mapping(source="dto.dateBapteme", dateFormat="dd/MM/yyyy",target="dateBapteme"),
		@Mapping(source="dto.egliseBapteme", target="egliseBapteme"),
		@Mapping(source="dto.egliseOrigine", target="egliseOrigine"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="dto.locked", target="locked"),
		@Mapping(source="dto.isConnected", target="isConnected"),
		@Mapping(source="dto.isValidated", target="isValidated"),
		@Mapping(source="dto.lieuNaissance", target="lieuNaissance"),
		@Mapping(source="dto.domaineCompetence", target="domaineCompetence"),
		@Mapping(source="dto.civilite", target="civilite"),
		@Mapping(source="profession", target="profession"),
		@Mapping(source="pays", target="pays"),
		@Mapping(source="memberStatus", target="memberStatus"),
		@Mapping(source="situationMatrimoniale", target="situationMatrimoniale"),
		@Mapping(source="situationProfessionnelle", target="situationProfessionnelle"),
		@Mapping(source="typeUser", target="typeUser"),
		@Mapping(source="role", target="role"),
	})
    User toEntity(UserDto dto, Profession profession, Pays pays, MemberStatus memberStatus, SituationMatrimoniale situationMatrimoniale, SituationProfessionnelle situationProfessionnelle, TypeUser typeUser, Role role) throws ParseException;

    //List<User> toEntities(List<UserDto> dtos) throws ParseException;

}
