

/*
 * Java transformer for entity table commune 
 * Created on 2020-01-18 ( Time 18:17:47 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "commune"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface CommuneTransformer {

	CommuneTransformer INSTANCE = Mappers.getMapper(CommuneTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="pays"),
		@Mapping(source="entity.pays.id", target="idPays"),
		@Mapping(source="entity.pays.code", target="paysCode"),
		@Mapping(source="entity.pays.libelle", target="paysLibelle"),
		//@Mapping(ignore=true, target="ville"),
		@Mapping(source="entity.ville.id", target="idVille"),
		@Mapping(source="entity.ville.libelle", target="villeLibelle"),
	})
	CommuneDto toDto(Commune entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<CommuneDto> toDtos(List<Commune> entities) throws ParseException;

    public default CommuneDto toLiteDto(Commune entity) {
		if (entity == null) {
			return null;
		}
		CommuneDto dto = new CommuneDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	public default List<CommuneDto> toLiteDtos(List<Commune> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<CommuneDto> dtos = new ArrayList<CommuneDto>();
		for (Commune entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.longitude", target="longitude"),
		@Mapping(source="dto.latitude", target="latitude"),
		@Mapping(source="dto.superficie", target="superficie"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="pays", target="pays"),
		@Mapping(source="ville", target="ville"),
	})
    Commune toEntity(CommuneDto dto, Pays pays, Ville ville) throws ParseException;

    //List<Commune> toEntities(List<CommuneDto> dtos) throws ParseException;

}
