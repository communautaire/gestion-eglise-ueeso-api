
/*
 * Java dto for entity table paiement 
 * Created on 2020-01-18 ( Time 09:57:23 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ci.ueeso.gestion.eglise.helper.contrat.SearchParam;
import ci.ueeso.gestion.eglise.helper.dto.customize._PaiementDto;
import lombok.Data;
import lombok.ToString;

/**
 * DTO for table "paiement"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class PaiementDto extends _PaiementDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    montant              ;
    private Integer    idEngagement         ;
    private Integer    idDatePaiement       ;
    private Integer    idTresorier          ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    engagement;
	//private Integer    periode;
	//private Integer    tresorier;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  montantParam          ;                     
	private SearchParam<Integer>  idEngagementParam     ;                     
	private SearchParam<Integer>  idDatePaiementParam   ;                     
	private SearchParam<Integer>  idTresorierParam      ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  engagementParam       ;                     
	private SearchParam<Integer>  periodeParam          ;                     
	private SearchParam<Integer>  tresorierParam        ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public PaiementDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
