
/*
 * Java dto for entity table smtp_server 
 * Created on 2020-01-24 ( Time 19:04:40 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.customize;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "smtp_server"
 * 
 * @author Smile Back-End generator
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _SmtpServerDto {

	protected List<String> datasHostsToTrust;
	protected List<String> datasSslProtocols;
	
}
