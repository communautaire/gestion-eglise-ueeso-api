
/*
 * Java dto for entity table user_mission 
 * Created on 2019-11-30 ( Time 18:43:55 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.ueeso.gestion.eglise.helper.dto.MissionDto;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "user_mission"
 * 
 * @author Smile Back-End generator
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _UserMissionDto {

	protected String missionDescription;
	protected String missionAdresse;
	
	protected MissionDto dataMission;
}
