

/*
 * Java transformer for entity table paiement 
 * Created on 2019-12-02 ( Time 00:15:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "paiement"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface PaiementTransformer {

	PaiementTransformer INSTANCE = Mappers.getMapper(PaiementTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="engagement"),
		@Mapping(source="entity.engagement.id", target="idEngagement"),
		//@Mapping(ignore=true, target="periode"),
		@Mapping(source="entity.periode.id", target="idDatePaiement"),
		//@Mapping(ignore=true, target="tresorier"),
		@Mapping(source="entity.tresorier.id", target="idTresorier"),
	})
	PaiementDto toDto(Paiement entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<PaiementDto> toDtos(List<Paiement> entities) throws ParseException;

    public default PaiementDto toLiteDto(Paiement entity) {
		if (entity == null) {
			return null;
		}
		PaiementDto dto = new PaiementDto();
		dto.setId( entity.getId() );
		return dto;
    }

	public default List<PaiementDto> toLiteDtos(List<Paiement> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<PaiementDto> dtos = new ArrayList<PaiementDto>();
		for (Paiement entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.montant", target="montant"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="engagement", target="engagement"),
		@Mapping(source="periode", target="periode"),
		@Mapping(source="tresorier", target="tresorier"),
	})
    Paiement toEntity(PaiementDto dto, Engagement engagement, Periode periode, Tresorier tresorier) throws ParseException;

    //List<Paiement> toEntities(List<PaiementDto> dtos) throws ParseException;

}
