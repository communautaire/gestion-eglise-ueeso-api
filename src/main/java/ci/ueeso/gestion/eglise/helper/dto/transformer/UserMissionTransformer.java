

/*
 * Java transformer for entity table user_mission 
 * Created on 2020-03-14 ( Time 21:14:44 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "user_mission"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface UserMissionTransformer {

	UserMissionTransformer INSTANCE = Mappers.getMapper(UserMissionTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="mission"),
		@Mapping(source="entity.mission.id", target="idMission"),
		@Mapping(source="entity.mission.code", target="missionCode"),
		@Mapping(source="entity.mission.libelle", target="missionLibelle"),
		//@Mapping(ignore=true, target="fonction"),
		@Mapping(source="entity.fonction.id", target="idFonction"),
		@Mapping(source="entity.fonction.libelle", target="fonctionLibelle"),
		@Mapping(source="entity.fonction.code", target="fonctionCode"),
		//@Mapping(ignore=true, target="user"),
		@Mapping(source="entity.user.id", target="idUser"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.email", target="userEmail"),
		@Mapping(source="entity.user.telephone", target="userTelephone"),
		@Mapping(source="entity.user.login", target="userLogin"),
	})
	UserMissionDto toDto(UserMission entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<UserMissionDto> toDtos(List<UserMission> entities) throws ParseException;

    public default UserMissionDto toLiteDto(UserMission entity) {
		if (entity == null) {
			return null;
		}
		UserMissionDto dto = new UserMissionDto();
		dto.setId( entity.getId() );
		return dto;
    }

	public default List<UserMissionDto> toLiteDtos(List<UserMission> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<UserMissionDto> dtos = new ArrayList<UserMissionDto>();
		for (UserMission entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="mission", target="mission"),
		@Mapping(source="fonction", target="fonction"),
		@Mapping(source="user", target="user"),
	})
    UserMission toEntity(UserMissionDto dto, Mission mission, Fonction fonction, User user) throws ParseException;

    //List<UserMission> toEntities(List<UserMissionDto> dtos) throws ParseException;

}
