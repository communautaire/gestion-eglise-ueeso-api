
/*
 * Java dto for entity table smtp_server 
 * Created on 2020-01-24 ( Time 19:04:40 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.customize._SmtpServerDto;

/**
 * DTO for table "smtp_server"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class SmtpServerDto extends _SmtpServerDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private String     description          ;
    private String     host                 ;
    private String     login                ;
    private String     password             ;
    private Integer    port                 ;
    private String     smtpUser             ;
	/*
	 * authentifier l'utilisateur
	 */
    private Boolean    auth                 ;
	/*
	 * utilisez SSL pour vous connecter et utiliser le port SSL par défaut. La valeur par défaut est false pour le protocole "smtp" et true pour le protocole "smtps"
	 */
    private Boolean    enableSsl            ;
	/*
	 * Spécifie les protocoles SSL qui seront activés pour les connexions SSL (Ex: "SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2")
	 */
    private String     sslProtocols         ;
	/*
	 * permet l'utilisation de la commande STARTTLS (si elle est prise en charge par le serveur) pour basculer la connexion vers une connexion protégée par TLS avant d'émettre des commandes de connexion. Notez qu'un magasin de confiance approprié doit être configuré pour que le client fasse confiance au certificat du serveur
	 */
    private Boolean    enableStarttls       ;
	/*
	 * Utiliser forcément la commande STARTTLS. Si le serveur ne supporte pas cette commande ou si elle echoue, la connection echouera
	 */
    private Boolean    isStarttlsRequired   ;
	/*
	 * Ensemble de host qui faire confiance
	 */
    private String     hostsToTrust         ;
	/*
	 * Utiliser le protocole SMTPS
	 */
    private Boolean    isSmtpsProtocol      ;
	/*
	 * Serveur a utiliser pour les envoi de mail
	 */
    private Boolean    isActive             ;
    private String     senderUsername       ;
    private String     senderEmail          ;
	/*
	 * Adresse email sur lequel un email de test sera envoye apres chaque modifcation pour s'assurer que les parametres du serveur conviennent
	 */
    private String     testEmail            ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   descriptionParam      ;                     
	private SearchParam<String>   hostParam             ;                     
	private SearchParam<String>   loginParam            ;                     
	private SearchParam<String>   passwordParam         ;                     
	private SearchParam<Integer>  portParam             ;                     
	private SearchParam<String>   smtpUserParam         ;                     
	private SearchParam<Boolean>  authParam             ;                     
	private SearchParam<Boolean>  enableSslParam        ;                     
	private SearchParam<String>   sslProtocolsParam     ;                     
	private SearchParam<Boolean>  enableStarttlsParam   ;                     
	private SearchParam<Boolean>  isStarttlsRequiredParam;                     
	private SearchParam<String>   hostsToTrustParam     ;                     
	private SearchParam<Boolean>  isSmtpsProtocolParam  ;                     
	private SearchParam<Boolean>  isActiveParam         ;                     
	private SearchParam<String>   senderUsernameParam   ;                     
	private SearchParam<String>   senderEmailParam      ;                     
	private SearchParam<String>   testEmailParam        ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public SmtpServerDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
