package ci.ueeso.gestion.eglise.helper.dto.customize;

public class _InfoUpdateDto {

	private String propertyName;
	private String oldValue;
	private String newValue;
	
	
	
	

	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	
	
	
}
