
/*
 * Java dto for entity table role 
 * Created on 2019-11-30 ( Time 18:43:50 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.customize;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.ueeso.gestion.eglise.helper.contrat.*;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "role"
 * 
 * @author Smile Back-End generator
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _RoleDto {

}
