
/*
 * Java dto for entity table user_mission 
 * Created on 2020-03-14 ( Time 21:14:43 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.customize._UserMissionDto;

/**
 * DTO for table "user_mission"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class UserMissionDto extends _UserMissionDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    idFonction           ;
    private Integer    idUser               ;
    private Integer    idMission            ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    mission;
	private String missionCode;
	private String missionLibelle;
	//private Integer    fonction;
	private String fonctionLibelle;
	private String fonctionCode;
	//private Integer    user;
	private String userNom;
	private String userPrenom;
	private String userEmail;
	private String userTelephone;
	private String userLogin;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  idFonctionParam       ;                     
	private SearchParam<Integer>  idUserParam           ;                     
	private SearchParam<Integer>  idMissionParam        ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  missionParam          ;                     
	private SearchParam<String>   missionCodeParam      ;                     
	private SearchParam<String>   missionLibelleParam   ;                     
	private SearchParam<Integer>  fonctionParam         ;                     
	private SearchParam<String>   fonctionLibelleParam  ;                     
	private SearchParam<String>   fonctionCodeParam     ;                     
	private SearchParam<Integer>  userParam             ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomParam       ;                     
	private SearchParam<String>   userEmailParam        ;                     
	private SearchParam<String>   userTelephoneParam    ;                     
	private SearchParam<String>   userLoginParam        ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public UserMissionDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
