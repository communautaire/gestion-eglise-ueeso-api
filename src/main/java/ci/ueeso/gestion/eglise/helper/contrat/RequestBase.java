/*
 * Created on 2019-11-30 ( Time 19:16:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.contrat;

import java.util.List;

import lombok.*;

import ci.ueeso.gestion.eglise.helper.dto.customize._AggregationDto;

/**
 * Request Base
 * 
 * @author Smile Bakend generator
 *
 */
@Data
@ToString
@NoArgsConstructor
public class RequestBase {
	protected String		sessionUser;
	protected Integer		size;
	protected Integer		index;
	protected String		lang;
	protected String		businessLineCode;
	protected String		caseEngine;
	protected Boolean		isAnd;
	protected Integer		user;
	protected Boolean 		isSimpleLoading;
	protected Integer		smtpServerId;

	protected List<_AggregationDto> datasAggregation;
    protected String[] datasElasticSearchIndices;
}