

/*
 * Java transformer for entity table engagement 
 * Created on 2019-11-30 ( Time 18:43:44 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "engagement"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface EngagementTransformer {

	EngagementTransformer INSTANCE = Mappers.getMapper(EngagementTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.dateEngagement", dateFormat="dd/MM/yyyy",target="dateEngagement"),
		@Mapping(source="entity.settleAt", dateFormat="dd/MM/yyyy",target="settleAt"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="user"),
		@Mapping(source="entity.user.id", target="idUser"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.email", target="userEmail"),
		@Mapping(source="entity.user.telephone", target="userTelephone"),
		//@Mapping(ignore=true, target="projet"),
		@Mapping(source="entity.projet.id", target="idProjet"),
		@Mapping(source="entity.projet.code", target="projetCode"),
		@Mapping(source="entity.projet.libelle", target="projetLibelle"),
	})
	EngagementDto toDto(Engagement entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<EngagementDto> toDtos(List<Engagement> entities) throws ParseException;

    public default EngagementDto toLiteDto(Engagement entity) {
		if (entity == null) {
			return null;
		}
		EngagementDto dto = new EngagementDto();
		dto.setId( entity.getId() );
		return dto;
    }

	public default List<EngagementDto> toLiteDtos(List<Engagement> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<EngagementDto> dtos = new ArrayList<EngagementDto>();
		for (Engagement entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dateEngagement", dateFormat="dd/MM/yyyy",target="dateEngagement"),
		@Mapping(source="dto.montantEngagement", target="montantEngagement"),
		@Mapping(source="dto.montantGlobalSolde", target="montantGlobalSolde"),
		@Mapping(source="dto.isSettle", target="isSettle"),
		@Mapping(source="dto.settleAt", dateFormat="dd/MM/yyyy",target="settleAt"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="user", target="user"),
		@Mapping(source="projet", target="projet"),
	})
    Engagement toEntity(EngagementDto dto, User user, Projet projet) throws ParseException;

    //List<Engagement> toEntities(List<EngagementDto> dtos) throws ParseException;

}
