

/*
 * Java transformer for entity table adresse 
 * Created on 2021-10-30 ( Time 19:02:37 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.dao.entity.*;


/**
 * TRANSFORMER for table "adresse"
 * 
 * @author Smile Backend generator
 *
 */
@Mapper
public interface AdresseTransformer {

	AdresseTransformer INSTANCE = Mappers.getMapper(AdresseTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		//@Mapping(ignore=true, target="commune"),
		@Mapping(source="entity.commune.id", target="idCommune"),
		@Mapping(source="entity.commune.code", target="communeCode"),
		@Mapping(source="entity.commune.libelle", target="communeLibelle"),
		//@Mapping(ignore=true, target="user"),
		@Mapping(source="entity.user.id", target="idUser"),
		@Mapping(source="entity.user.nom", target="userNom"),
		@Mapping(source="entity.user.prenom", target="userPrenom"),
		@Mapping(source="entity.user.email", target="userEmail"),
		@Mapping(source="entity.user.telephone", target="userTelephone"),
		@Mapping(source="entity.user.login", target="userLogin"),
		//@Mapping(ignore=true, target="pays"),
		@Mapping(source="entity.pays.id", target="idPays"),
		@Mapping(source="entity.pays.code", target="paysCode"),
		@Mapping(source="entity.pays.libelle", target="paysLibelle"),
		//@Mapping(ignore=true, target="ville"),
		@Mapping(source="entity.ville.id", target="idVille"),
		@Mapping(source="entity.ville.libelle", target="villeLibelle"),
		//@Mapping(ignore=true, target="quartier"),
		@Mapping(source="entity.quartier.id", target="idQuartier"),
		@Mapping(source="entity.quartier.code", target="quartierCode"),
		@Mapping(source="entity.quartier.libelle", target="quartierLibelle"),
	})
	AdresseDto toDto(Adresse entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<AdresseDto> toDtos(List<Adresse> entities) throws ParseException;

    public default AdresseDto toLiteDto(Adresse entity) {
		if (entity == null) {
			return null;
		}
		AdresseDto dto = new AdresseDto();
		dto.setId( entity.getId() );
		return dto;
    }

	public default List<AdresseDto> toLiteDtos(List<Adresse> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<AdresseDto> dtos = new ArrayList<AdresseDto>();
		for (Adresse entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.adressePostale", target="adressePostale"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.deletedAt", dateFormat="dd/MM/yyyy",target="deletedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.deletedBy", target="deletedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="commune", target="commune"),
		@Mapping(source="user", target="user"),
		@Mapping(source="pays", target="pays"),
		@Mapping(source="ville", target="ville"),
		@Mapping(source="quartier", target="quartier"),
	})
    Adresse toEntity(AdresseDto dto, Commune commune, User user, Pays pays, Ville ville, Quartier quartier) throws ParseException;

    //List<Adresse> toEntities(List<AdresseDto> dtos) throws ParseException;

}
