
/*
 * Java dto for entity table sous_prefecture 
 * Created on 2020-01-18 ( Time 09:57:26 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ci.ueeso.gestion.eglise.helper.contrat.SearchParam;
import ci.ueeso.gestion.eglise.helper.dto.customize._SousPrefectureDto;
import lombok.Data;
import lombok.ToString;

/**
 * DTO for table "sous_prefecture"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class SousPrefectureDto extends _SousPrefectureDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private String     libelle              ;
    private Integer    idDepartement        ;
    private String     chefLieu             ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deleted              ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    departement;
	private String departementLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   libelleParam          ;                     
	private SearchParam<Integer>  idDepartementParam    ;                     
	private SearchParam<String>   chefLieuParam         ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedParam          ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  departementParam      ;                     
	private SearchParam<String>   departementLibelleParam;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public SousPrefectureDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
