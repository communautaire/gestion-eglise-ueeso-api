
/*
 * Java dto for entity table adresse 
 * Created on 2021-10-30 ( Time 19:02:37 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.dto.customize._AdresseDto;

/**
 * DTO for table "adresse"
 *
 * @author Smile Back-End generator
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class AdresseDto extends _AdresseDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    idUser               ;
	/*
	 * Pays de residence
	 */
    private Integer    idPays               ;
    private Integer    idVille              ;
    private Integer    idCommune            ;
    private Integer    idQuartier           ;
    private String     adressePostale       ;
	private String     createdAt            ;
	private String     updatedAt            ;
	private String     deletedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Integer    deletedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	//private Integer    commune;
	private String communeCode;
	private String communeLibelle;
	//private Integer    user;
	private String userNom;
	private String userPrenom;
	private String userEmail;
	private String userTelephone;
	private String userLogin;
	//private Integer    pays;
	private String paysCode;
	private String paysLibelle;
	//private Integer    ville;
	private String villeLibelle;
	//private Integer    quartier;
	private String quartierCode;
	private String quartierLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  idUserParam           ;                     
	private SearchParam<Integer>  idPaysParam           ;                     
	private SearchParam<Integer>  idVilleParam          ;                     
	private SearchParam<Integer>  idCommuneParam        ;                     
	private SearchParam<Integer>  idQuartierParam       ;                     
	private SearchParam<String>   adressePostaleParam   ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<String>   deletedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Integer>  deletedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  communeParam          ;                     
	private SearchParam<String>   communeCodeParam      ;                     
	private SearchParam<String>   communeLibelleParam   ;                     
	private SearchParam<Integer>  userParam             ;                     
	private SearchParam<String>   userNomParam          ;                     
	private SearchParam<String>   userPrenomParam       ;                     
	private SearchParam<String>   userEmailParam        ;                     
	private SearchParam<String>   userTelephoneParam    ;                     
	private SearchParam<String>   userLoginParam        ;                     
	private SearchParam<Integer>  paysParam             ;                     
	private SearchParam<String>   paysCodeParam         ;                     
	private SearchParam<String>   paysLibelleParam      ;                     
	private SearchParam<Integer>  villeParam            ;                     
	private SearchParam<String>   villeLibelleParam     ;                     
	private SearchParam<Integer>  quartierParam         ;                     
	private SearchParam<String>   quartierCodeParam     ;                     
	private SearchParam<String>   quartierLibelleParam  ;                     

	// order param
	private String orderField;
	private String orderDirection;



    /**
     * Default constructor
     */
    public AdresseDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
