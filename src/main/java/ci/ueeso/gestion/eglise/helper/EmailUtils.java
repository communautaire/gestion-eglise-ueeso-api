
/*
 * Created on 27 oct. 2018 ( Time 23:09:09 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.sun.mail.smtp.SMTPAddressFailedException;

import ci.ueeso.gestion.eglise.dao.entity.SmtpServer;
import ci.ueeso.gestion.eglise.dao.repository.SmtpServerRepository;
import ci.ueeso.gestion.eglise.helper.contrat.Response;

@Service
@Component
@Configuration
@EnableAsync
public class EmailUtils {

	@Autowired
	private ExceptionUtils			exceptionUtils;


	//	private Response response;
	@Autowired
	private TemplateEngine templateEngine;
	@Autowired
	private Environment environment;
	@Autowired
	private ParamsUtils paramsUtils;

	@Autowired
	private SmtpServerRepository smtpServerRepository;


	private static final Logger slf4jLogger = LoggerFactory.getLogger(EmailUtils.class);
	public static List<String> URI_AS_CHECK = Arrays.asList("user/login", "user/forgotPassword", "user/forgotPasswordValidation");
	private final static String MAILER_VERSION = "Java";
	public final static String EMAIL_ADDR="email";
	public final static String USER_NAME="user";

	@Async
	public void sendEmailWithAttachmentsAsync(Map<String, String> from, List<Map<String, String>> toRecipients, List<Map<String, String>> inCc, String subject, String body, List<String> attachmentsFilesAbsolutePaths, Context context, String templateName, Locale locale, Integer ...smtpServerId) throws ConnectException, SendFailedException, SMTPAddressFailedException, MessagingException, UnsupportedEncodingException, Exception
	{
		sendEmailWithAttachments(from, toRecipients, inCc, subject, body, attachmentsFilesAbsolutePaths, context, templateName, locale, smtpServerId);
	}
	
	public void sendEmailWithAttachments(Map<String, String> from, List<Map<String, String>> toRecipients, List<Map<String, String>> inCc, String subject, String body, List<String> attachmentsFilesAbsolutePaths, Context context, String templateName, Locale locale, Integer ...smtpServerId) throws ConnectException, SendFailedException, SMTPAddressFailedException, MessagingException, UnsupportedEncodingException, Exception
	{
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

		SmtpServer smtpServer = null;
		if(smtpServerId != null && smtpServerId.length > 0 && Utilities.isValidID(((Integer)smtpServerId[0]))) {
			smtpServer = smtpServerRepository.findOne((Integer)smtpServerId[0], false);
		}
		else {
			smtpServer = smtpServerRepository.getServer();
		}

		if(smtpServer == null) {
			throw new NullArgumentException("Aucun serveur actif ou prÃ©cisÃ© explicitement !");
		}
		
		System.out.println("smtpServer = "+smtpServer);

		//Boolean auth = false;
		javaMailSender.setHost(smtpServer.getHost());
		javaMailSender.setPort(smtpServer.getPort());
		if(smtpServer.getAuth() != null && smtpServer.getAuth() && Utilities.notBlank(smtpServer.getLogin()) && Utilities.notBlank(smtpServer.getPassword())){
			javaMailSender.setUsername(smtpServer.getLogin());
			javaMailSender.setPassword(smtpServer.getPassword());
			//auth = true;
		}
		//javaMailSender.setJavaMailProperties(getMailProperties(smtpServer.getHost(), auth));
		javaMailSender.setJavaMailProperties(getMailProperties(smtpServer));


		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		//sender
		if(from != null && !from.isEmpty()) {
			helper.setFrom(new InternetAddress(from.get(EMAIL_ADDR), from.get(USER_NAME)));
		}
		else {
			if(Utilities.notBlank(smtpServer.getSenderUsername()) && Utilities.notBlank(smtpServer.getSenderEmail())) {
				helper.setFrom(new InternetAddress(smtpServer.getSenderEmail(), smtpServer.getSenderUsername()));
			}
			else {
				helper.setFrom(new InternetAddress("smartt.backend@gmail.com", "SMART-T"));
			}
		}


		//recipients
		List <InternetAddress> to = new ArrayList<InternetAddress>();
		for (Map<String, String> recipient : toRecipients)
			to.add(new InternetAddress(recipient.get(EMAIL_ADDR), recipient.get(USER_NAME)));
		helper.setTo(to.toArray(new InternetAddress[0]));
	

		//persons in Copy
		if(inCc != null && !inCc.isEmpty()){
			List <InternetAddress> cc = new ArrayList<InternetAddress>();
			for (Map<String, String> ccRecipient : inCc)
				cc.add(new InternetAddress(ccRecipient.get(EMAIL_ADDR), ccRecipient.get(USER_NAME)));
			helper.setCc(cc.toArray(new InternetAddress[0]));
		}

		//Subject and body
		helper.setSubject(subject);
		if(context != null && Utilities.notBlank(templateName)){
			body = templateEngine.process(templateName+"_"+locale, context);
		}
		helper.setText(body, true);
		
		//helper.setText(body);

		//Attachments
		if(attachmentsFilesAbsolutePaths != null && ! attachmentsFilesAbsolutePaths.isEmpty())
			for (String attachmentPath : attachmentsFilesAbsolutePaths)
			{
				File pieceJointe = new File (attachmentPath);
				FileSystemResource file = new FileSystemResource(attachmentPath);
				if( pieceJointe.exists() && pieceJointe.isFile()){
					helper.addAttachment(file.getFilename(), file);
				}
			}
		javaMailSender.send(message);
	}

	
	
	public void sendEmailWithAttachmentsWithEmbledImage(Map<String, String> from, List<Map<String, String>> toRecipients, List<Map<String, String>> inCc, String subject, String body, List<String> attachmentsFilesAbsolutePaths, Context context, String templateName, Locale locale,boolean addInline,List<Map<String, Object>> filesImageAbsolutePaths, Integer ...smtpServerId) throws ConnectException, SendFailedException, SMTPAddressFailedException, MessagingException, UnsupportedEncodingException, Exception
	{
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

		SmtpServer smtpServer = null;
		if(smtpServerId != null && smtpServerId.length > 0 && Utilities.isValidID(((Integer)smtpServerId[0]))) {
			smtpServer = smtpServerRepository.findOne((Integer)smtpServerId[0], false);
		}
		else {
			smtpServer = smtpServerRepository.getServer();
		}

		if(smtpServer == null) {
			throw new NullArgumentException("Aucun serveur actif ou prÃ©cisÃ© explicitement !");
		}

		//Boolean auth = false;
		javaMailSender.setHost(smtpServer.getHost());
		javaMailSender.setPort(smtpServer.getPort());
		if(smtpServer.getAuth() != null && smtpServer.getAuth() && Utilities.notBlank(smtpServer.getLogin()) && Utilities.notBlank(smtpServer.getPassword())){
			javaMailSender.setUsername(smtpServer.getLogin());
			javaMailSender.setPassword(smtpServer.getPassword());
			//auth = true;
		}
		//javaMailSender.setJavaMailProperties(getMailProperties(smtpServer.getHost(), auth));
		javaMailSender.setJavaMailProperties(getMailProperties(smtpServer));


		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		//sender
		if(from != null && !from.isEmpty()) {
			helper.setFrom(new InternetAddress(from.get(EMAIL_ADDR), from.get(USER_NAME)));
		}
		else {
			if(Utilities.notBlank(smtpServer.getSenderUsername()) && Utilities.notBlank(smtpServer.getSenderEmail())) {
				helper.setFrom(new InternetAddress(smtpServer.getSenderEmail(), smtpServer.getSenderUsername()));
			}
			else {
				helper.setFrom(new InternetAddress("smartt.backend@gmail.com", "SMART-T"));
			}
		}


		//recipients
		List <InternetAddress> to = new ArrayList<InternetAddress>();
		for (Map<String, String> recipient : toRecipients)
			to.add(new InternetAddress(recipient.get(EMAIL_ADDR), recipient.get(USER_NAME)));
		helper.setTo(to.toArray(new InternetAddress[0]));
	

		//persons in Copy
		if(inCc != null && !inCc.isEmpty()){
			List <InternetAddress> cc = new ArrayList<InternetAddress>();
			for (Map<String, String> ccRecipient : inCc)
				cc.add(new InternetAddress(ccRecipient.get(EMAIL_ADDR), ccRecipient.get(USER_NAME)));
			helper.setCc(cc.toArray(new InternetAddress[0]));
		}

		//Subject and body
		helper.setSubject(subject);
		if(context != null && Utilities.notBlank(templateName)){
			body = templateEngine.process(templateName+"_"+locale, context);
		}else{
			if(addInline){	
				for (Map<String, Object> attachmentPath : filesImageAbsolutePaths)
				{
					FileSystemResource res = new FileSystemResource(new File(attachmentPath.get("chemin").toString()));
					helper.addInline(attachmentPath.get("identifiant").toString(), res);
					
				}
			}
		}
		helper.setText(body, true);
		
		//helper.setText(body);

		//Attachments
		if(attachmentsFilesAbsolutePaths != null && ! attachmentsFilesAbsolutePaths.isEmpty())
			for (String attachmentPath : attachmentsFilesAbsolutePaths)
			{
				File pieceJointe = new File (attachmentPath);
				FileSystemResource file = new FileSystemResource(attachmentPath);
				if( pieceJointe.exists() && pieceJointe.isFile()){
					helper.addAttachment(file.getFilename(), file);
				}
			}
		javaMailSender.send(message);
	}



	@Async
	public <T> Response <T> sendEmail(Map<String, String> from, List<Map<String, String>> toRecipients, String subject,
			String body, List<String> attachmentsFilesAbsolutePaths, Context context, String templateName, Locale locale) {
		Response<T> response = new Response<T>();
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

		//		/*
		//		 * A retirer
		//		 */
		//		if(true)
		//		return response;
		//
		//		/*
		//		 * retirer
		//		 */
		SmtpServer smtpServer = smtpServerRepository.findSmtpServerByIsActive(true, false);
		//String smtpServer = paramsUtils.getSmtpHost();
		if (smtpServer != null) {
			Boolean auth = false;
			javaMailSender.setHost(smtpServer.getHost());
			javaMailSender.setPort(smtpServer.getPort());
			javaMailSender.setUsername(smtpServer.getLogin());
			javaMailSender.setPassword(smtpServer.getPassword());
			auth = true;

			javaMailSender.setJavaMailProperties(getMailProperties(smtpServer.getHost(), auth));

			MimeMessage message = javaMailSender.createMimeMessage();
			try {
				MimeMessageHelper helper = new MimeMessageHelper(message, Boolean.TRUE,"UTF-8");
				//				MimeMessageHelper helper = new MimeMessageHelper(message, Boolean.TRUE);

				// sender
				helper.setFrom(new InternetAddress(from.get("email"), from.get("user")));
				// recipients
				List<InternetAddress> to = new ArrayList<InternetAddress>();
				for (Map<String, String> recipient : toRecipients){
					to.add(new InternetAddress(recipient.get("email"), recipient.get("user")));
				}
				helper.setTo(to.toArray(new InternetAddress[0]));

				// Subject and body
				helper.setSubject(subject);
				if(context != null && templateName != null ){
					body = templateEngine.process(templateName, context);
				}
				helper.setText(body, true);

				// Attachments
				if (attachmentsFilesAbsolutePaths != null && !attachmentsFilesAbsolutePaths.isEmpty()){
					for (String attachmentPath : attachmentsFilesAbsolutePaths) {
						File pieceJointe = new File(attachmentPath);
						FileSystemResource file = new FileSystemResource(attachmentPath);
						if (pieceJointe.exists() && pieceJointe.isFile()) {
							helper.addAttachment(file.getFilename(), file);
						}
					}
				}

				javaMailSender.send(message);
				response.setHasError(Boolean.FALSE);
				/// gerer les cas d'exeption de non envoi de mail
			} catch (MessagingException e) {
				e.printStackTrace();
				exceptionUtils.EXCEPTION(response, locale, e);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				exceptionUtils.EXCEPTION(response, locale, e);
			}
		}
		return response;
	}


	private Properties getMailProperties(String host, Boolean auth) {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.auth", auth.toString());
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.starttls.required", "true");
		//properties.setProperty("mail.debug", "true");
		if(host.equals("smtp.orange.ci")) {
			properties.setProperty("mail.smtp.ssl.trust", "smtp.orange.ci");
		}
		if (host.equals("smtp.gmail.com")) {
			properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
		}
		return properties;
	}

	/*
	private Properties getMailProperties(String host, Boolean auth) {
		Properties properties = new Properties();
		if (environment.getActiveProfiles() != null && environment.getActiveProfiles().length > 0) {
			String activeProfiles = "";
			for (int i = 0; i < environment.getActiveProfiles().length; i++) {
				activeProfiles += environment.getActiveProfiles()[i];
			}
			Utilities.logger("active profile: " + activeProfiles);

			properties.setProperty("mail.transport.protocol", "smtp");
			properties.setProperty("mail.smtp.auth", auth.toString());
			properties.setProperty("mail.smtp.starttls.enable", "true");
			properties.setProperty("mail.smtp.starttls.required", "true");
			properties.setProperty("mail.debug", "true");
			if (host.equals("smtp.gmail.com")) {
				properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
			}
      if(host.equals("smtp.orange.ci")) {
  			properties.setProperty("mail.smtp.ssl.trust", "smtp.orange.ci");
  		}
			// Check if Active profiles contains profile to ignore
			if (Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> env.equals("dev"))) {
				//DEV
				Utilities.logger("Profil DEV pour mail");
				properties.setProperty("mail.transport.protocol", "smtp");
				properties.setProperty("mail.smtp.auth", auth.toString());
				properties.setProperty("mail.smtp.starttls.enable", "true");
				properties.setProperty("mail.smtp.starttls.required", "true");
				properties.setProperty("mail.debug", "true");
				if (host.equals("smtp.gmail.com")) {
					properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
				}
			}
			if (Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> env.equals("prod"))) {
				//PROD
				 properties.setProperty("mail.smtp.auth", auth.toString());
				 properties.setProperty("mail.transport.protocol", "smtp");
				 properties.put("mail.smtp.starttls.enable", "false");
				 properties.setProperty("mail.debug", "true");
			}
		}
		return properties;
	}

	 */


	private Properties getMailProperties(SmtpServer smtpServer) {
		Properties properties = new Properties();
		String protocol = "smtp";
		if(smtpServer != null)
		{
			if(smtpServer.getIsSmtpsProtocol() != null && smtpServer.getIsSmtpsProtocol()) {
				protocol = "smtps";
			}
			properties.setProperty("mail.transport.protocol", protocol);
			if(smtpServer.getAuth() != null && smtpServer.getAuth()) {
				properties.setProperty("mail."+protocol+".auth", smtpServer.getAuth().toString());
			}
			if(smtpServer.getEnableStarttls() != null && smtpServer.getEnableStarttls()) {
				properties.setProperty("mail."+protocol+".starttls.enable", smtpServer.getEnableStarttls().toString());
			}
			if(smtpServer.getIsStarttlsRequired() != null && smtpServer.getIsStarttlsRequired()) {
				properties.setProperty("mail."+protocol+".starttls.required", smtpServer.getIsStarttlsRequired().toString());
			}
			if(smtpServer.getHostsToTrust() != null && !smtpServer.getHostsToTrust().isEmpty()) {
				properties.setProperty("mail."+protocol+".ssl.trust", smtpServer.getHostsToTrust());
			}
			if(smtpServer.getSmtpUser() != null && !smtpServer.getSmtpUser().isEmpty()) {
				properties.setProperty("mail."+protocol+".user", smtpServer.getSmtpUser());
			}
			if(smtpServer.getEnableSsl() != null && smtpServer.getEnableSsl()) {
				properties.setProperty("mail."+protocol+".ssl.enable", smtpServer.getEnableSsl().toString());
			}
			if(smtpServer.getSslProtocols() != null && !smtpServer.getSslProtocols().isEmpty()) {
				properties.setProperty("mail."+protocol+".ssl.protocols", smtpServer.getSslProtocols());
			}
			//properties.setProperty("mail.debug", "true");
		}
		return properties;
	}

}
