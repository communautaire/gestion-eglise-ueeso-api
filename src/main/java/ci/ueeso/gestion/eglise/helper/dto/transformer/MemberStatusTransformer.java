

/*
 * Java transformer for entity table member_status 
 * Created on 2020-01-07 ( Time 18:54:36 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.ueeso.gestion.eglise.dao.entity.MemberStatus;
import ci.ueeso.gestion.eglise.helper.dto.MemberStatusDto;


/**
TRANSFORMER for table "member_status"
 * 
 * @author SFL Back-End developper
 *
 */
@Mapper
public interface MemberStatusTransformer {

	MemberStatusTransformer INSTANCE = Mappers.getMapper(MemberStatusTransformer.class);

	@Mappings({
	})
	MemberStatusDto toDto(MemberStatus entity) throws ParseException;

    List<MemberStatusDto> toDtos(List<MemberStatus> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.comment", target="comment"),
	})
    MemberStatus toEntity(MemberStatusDto dto) throws ParseException;

    //List<MemberStatus> toEntities(List<MemberStatusDto> dtos) throws ParseException;

}
