package ci.ueeso.gestion.eglise.helper.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum TypeUserEnum {

	FIDELE("FDLE"),
	DONNATEUR("DNTR");
	
	private final String code;
	
	
	public String getCode() {
		return code;
	}
	
	

	private TypeUserEnum(String code) {
		this.code = code;
	}



	public static boolean isValid(String userType) {
		return Arrays.asList(values()).stream().anyMatch(r -> r.getCode().equals(userType));
	}
	
	public static TypeUserEnum get(String ruleType) {
		return Arrays.asList(values()).stream().filter(r -> r.getCode().equals(ruleType)).findFirst().orElse(null);
	}
	
	public static List<String> getValues() {
		return Arrays.asList(values()).stream().map(r -> r.getCode()).collect(Collectors.toList());
	}
}
