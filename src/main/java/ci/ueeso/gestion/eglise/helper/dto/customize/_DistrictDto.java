
/*
 * Java dto for entity table district 
 * Created on 2020-01-18 ( Time 09:57:21 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.helper.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "district"
 * 
 * @author Smile Back-End generator
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _DistrictDto {

}
