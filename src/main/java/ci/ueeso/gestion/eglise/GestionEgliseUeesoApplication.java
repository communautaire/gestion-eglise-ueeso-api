package ci.ueeso.gestion.eglise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionEgliseUeesoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionEgliseUeesoApplication.class, args);
	}

}
