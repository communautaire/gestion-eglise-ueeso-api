                                                    													

/*
 * Java business for entity table departement 
 * Created on 2020-01-18 ( Time 09:57:21 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Departement;
import ci.ueeso.gestion.eglise.dao.entity.Region;
import ci.ueeso.gestion.eglise.dao.entity.SousPrefecture;
import ci.ueeso.gestion.eglise.dao.repository.DepartementRepository;
import ci.ueeso.gestion.eglise.dao.repository.RegionRepository;
import ci.ueeso.gestion.eglise.dao.repository.SousPrefectureRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.DepartementDto;
import ci.ueeso.gestion.eglise.helper.dto.SousPrefectureDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.DepartementTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.SousPrefectureTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "departement"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class DepartementBusiness implements IBasicBusiness<Request<DepartementDto>, Response<DepartementDto>> {

	private Response<DepartementDto> response;
	@Autowired
	private DepartementRepository departementRepository;
	@Autowired
	private RegionRepository regionRepository;
	@Autowired
	private SousPrefectureRepository sousPrefectureRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

                
        @Autowired
    private SousPrefectureBusiness sousPrefectureBusiness;
  
    
          @Autowired
    private UserBusiness userBusiness;
    
	public DepartementBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
	
	/**
	 * create Departement by using DepartementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<DepartementDto> create(Request<DepartementDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Departement-----");

		Response<DepartementDto> response = new Response<DepartementDto>();
		List<Departement>        items    = new ArrayList<Departement>();
			
		for (DepartementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("idRegion", dto.getIdRegion());
			fieldsToVerify.put("chefLieu", dto.getChefLieu());
			//fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if departement to insert do not exist
			Departement existingEntity = null;
			/*if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("departement id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}*/

			// verif unique libelle in db
			existingEntity = departementRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("departement libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			// Verify if region exist
			Region existingRegion = null;
			if (Utilities.isValidID(dto.getIdRegion())){
				existingRegion = regionRepository.findOne(dto.getIdRegion(), false);
				if (existingRegion == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("region idRegion -> " + dto.getIdRegion(), locale));
					response.setHasError(true);
					return response;
				}
			}
			Departement entityToSave = DepartementTransformer.INSTANCE.toEntity(dto, existingRegion);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Departement> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = departementRepository.saveAll((Iterable<Departement>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("departement", locale));
				response.setHasError(true);
				return response;
			}
			List<DepartementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? DepartementTransformer.INSTANCE.toLiteDtos(itemsSaved) : DepartementTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Departement-----");
		return response;
	}

	/**
	 * update Departement by using DepartementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<DepartementDto> update(Request<DepartementDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Departement-----");

		Response<DepartementDto> response = new Response<DepartementDto>();
		List<Departement>        items    = new ArrayList<Departement>();
			
		for (DepartementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la departement existe
			Departement entityToSave = null;
			entityToSave = departementRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("departement id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if region exist
			if (Utilities.isValidID(dto.getIdRegion()) && !entityToSave.getRegion().getId().equals(dto.getIdRegion())){
				Region existingRegion = regionRepository.findOne(dto.getIdRegion(), false);
				if (existingRegion == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("region idRegion -> " + dto.getIdRegion(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setRegion(existingRegion);
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
			                 Departement existingEntity = departementRepository.findByLibelle(dto.getLibelle(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("departement -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les departements", locale));
                  response.setHasError(true);
                  return response;
                }
       			entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.isNotBlank(dto.getChefLieu()) && !dto.getChefLieu().equals(entityToSave.getChefLieu())) {
			 				entityToSave.setChefLieu(dto.getChefLieu());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Departement> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = departementRepository.saveAll((Iterable<Departement>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("departement", locale));
				response.setHasError(true);
				return response;
			}
			List<DepartementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? DepartementTransformer.INSTANCE.toLiteDtos(itemsSaved) : DepartementTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Departement-----");
		return response;
	}

	/**
	 * delete Departement by using DepartementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<DepartementDto> delete(Request<DepartementDto> request, Locale locale)  {
		log.info("----begin delete Departement-----");

		Response<DepartementDto> response = new Response<DepartementDto>();
		List<Departement>        items    = new ArrayList<Departement>();
			
		for (DepartementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la departement existe
			Departement existingEntity = null;
			existingEntity = departementRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("departement -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// sousPrefecture
			List<SousPrefecture> listOfSousPrefecture = sousPrefectureRepository.findByIdDepartement(existingEntity.getId(), false);
			if (listOfSousPrefecture != null && !listOfSousPrefecture.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSousPrefecture.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			departementRepository.saveAll((Iterable<Departement>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Departement-----");
		return response;
	}

	/**
	 * forceDelete Departement by using DepartementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<DepartementDto> forceDelete(Request<DepartementDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Departement-----");

		Response<DepartementDto> response = new Response<DepartementDto>();
		List<Departement>        items    = new ArrayList<Departement>();
			
		for (DepartementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la departement existe
			Departement existingEntity = null;
			existingEntity = departementRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("departement -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

		// sousPrefecture
        List<SousPrefecture> listOfSousPrefecture = sousPrefectureRepository.findByIdDepartement(existingEntity.getId(), false);
        if (listOfSousPrefecture != null && !listOfSousPrefecture.isEmpty()){
          Request<SousPrefectureDto> deleteRequest = new Request<>();
          deleteRequest.setDatas(SousPrefectureTransformer.INSTANCE.toDtos(listOfSousPrefecture));
          deleteRequest.setUser(request.getUser());
          Response<SousPrefectureDto> deleteResponse = sousPrefectureBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			departementRepository.saveAll((Iterable<Departement>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Departement-----");
		return response;
	}

	/**
	 * get Departement by using DepartementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<DepartementDto> getByCriteria(Request<DepartementDto> request, Locale locale)  throws Exception {
		log.info("----begin get Departement-----");

		Response<DepartementDto> response = new Response<DepartementDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Departement> items = departementRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("departement", locale));
			response.setHasError(false);
			return response;
		}

		List<DepartementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? DepartementTransformer.INSTANCE.toLiteDtos(items) : DepartementTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setCount(departementRepository.count(request, em, locale));
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Departement-----");
		return response;
	}

	/**
	 * get full DepartementDto by using Departement as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private DepartementDto getFullInfos(DepartementDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
