                                                                    															

/*
 * Java business for entity table projet 
 * Created on 2019-11-30 ( Time 18:43:49 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Projet;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "projet"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class ProjetBusiness implements IBasicBusiness<Request<ProjetDto>, Response<ProjetDto>> {

	private Response<ProjetDto> response;
	@Autowired
	private ProjetRepository projetRepository;
	@Autowired
	private EngagementRepository engagementRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

                
        @Autowired
    private EngagementBusiness engagementBusiness;
  
    
          @Autowired
    private UserBusiness userBusiness;
    
	public ProjetBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
	
	/**
	 * create Projet by using ProjetDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProjetDto> create(Request<ProjetDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Projet-----");

		Response<ProjetDto> response = new Response<ProjetDto>();
		List<Projet>        items    = new ArrayList<Projet>();
			
		for (ProjetDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("description", dto.getDescription());
			fieldsToVerify.put("startDate", dto.getStartDate());
			fieldsToVerify.put("endDate", dto.getEndDate());
			fieldsToVerify.put("cout", dto.getCout());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if projet to insert do not exist
			Projet existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("projet id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// verif unique code in db
			existingEntity = projetRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("projet code -> " + dto.getCode(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" code ", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = projetRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("projet libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

				Projet entityToSave = ProjetTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Projet> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = projetRepository.saveAll((Iterable<Projet>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("projet", locale));
				response.setHasError(true);
				return response;
			}
			List<ProjetDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ProjetTransformer.INSTANCE.toLiteDtos(itemsSaved) : ProjetTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Projet-----");
		return response;
	}

	/**
	 * update Projet by using ProjetDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProjetDto> update(Request<ProjetDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Projet-----");

		Response<ProjetDto> response = new Response<ProjetDto>();
		List<Projet>        items    = new ArrayList<Projet>();
			
		for (ProjetDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la projet existe
			Projet entityToSave = null;
			entityToSave = projetRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("projet id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
			                 Projet existingEntity = projetRepository.findByCode(dto.getCode(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("projet -> " + dto.getCode(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les projets", locale));
                  response.setHasError(true);
                  return response;
                }
       						entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
			                 Projet existingEntity = projetRepository.findByLibelle(dto.getLibelle(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("projet -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les projets", locale));
                  response.setHasError(true);
                  return response;
                }
       						entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.isNotBlank(dto.getDescription()) && !dto.getDescription().equals(entityToSave.getDescription())) {
			 				entityToSave.setDescription(dto.getDescription());
			}
			if (dto.getStartDate() != null) {
				entityToSave.setStartDate(dto.getStartDate());
			}
			if (dto.getEndDate() != null) {
				entityToSave.setEndDate(dto.getEndDate());
			}
			if (dto.getCout() != null && dto.getCout() > 0) {
				entityToSave.setCout(dto.getCout());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Projet> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = projetRepository.saveAll((Iterable<Projet>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("projet", locale));
				response.setHasError(true);
				return response;
			}
			List<ProjetDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ProjetTransformer.INSTANCE.toLiteDtos(itemsSaved) : ProjetTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Projet-----");
		return response;
	}

	/**
	 * delete Projet by using ProjetDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProjetDto> delete(Request<ProjetDto> request, Locale locale)  {
		log.info("----begin delete Projet-----");

		Response<ProjetDto> response = new Response<ProjetDto>();
		List<Projet>        items    = new ArrayList<Projet>();
			
		for (ProjetDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la projet existe
			Projet existingEntity = null;
			existingEntity = projetRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("projet -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// engagement
			List<Engagement> listOfEngagement = engagementRepository.findByIdProjet(existingEntity.getId(), false);
			if (listOfEngagement != null && !listOfEngagement.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEngagement.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			projetRepository.saveAll((Iterable<Projet>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Projet-----");
		return response;
	}

	/**
	 * forceDelete Projet by using ProjetDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProjetDto> forceDelete(Request<ProjetDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Projet-----");

		Response<ProjetDto> response = new Response<ProjetDto>();
		List<Projet>        items    = new ArrayList<Projet>();
			
		for (ProjetDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la projet existe
			Projet existingEntity = null;
			existingEntity = projetRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("projet -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

		// engagement
        List<Engagement> listOfEngagement = engagementRepository.findByIdProjet(existingEntity.getId(), false);
        if (listOfEngagement != null && !listOfEngagement.isEmpty()){
          Request<EngagementDto> deleteRequest = new Request<EngagementDto>();
          deleteRequest.setDatas(EngagementTransformer.INSTANCE.toDtos(listOfEngagement));
          deleteRequest.setUser(request.getUser());
          Response<EngagementDto> deleteResponse = engagementBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			projetRepository.saveAll((Iterable<Projet>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Projet-----");
		return response;
	}

	/**
	 * get Projet by using ProjetDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProjetDto> getByCriteria(Request<ProjetDto> request, Locale locale)  throws Exception {
		log.info("----begin get Projet-----");

		Response<ProjetDto> response = new Response<ProjetDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Projet> items 			 = projetRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("projet", locale));
			response.setHasError(false);
			return response;
		}

		List<ProjetDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ProjetTransformer.INSTANCE.toLiteDtos(items) : ProjetTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setCount(projetRepository.count(request, em, locale));
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Projet-----");
		return response;
	}

	/**
	 * get full ProjetDto by using Projet as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private ProjetDto getFullInfos(ProjetDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
