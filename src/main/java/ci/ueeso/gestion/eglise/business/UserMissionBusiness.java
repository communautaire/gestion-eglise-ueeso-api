/*
 * Java business for entity table user_mission 
 * Created on 2020-03-14 ( Time 21:14:44 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Fonction;
import ci.ueeso.gestion.eglise.dao.entity.Mission;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.entity.UserMission;
import ci.ueeso.gestion.eglise.dao.repository.FonctionRepository;
import ci.ueeso.gestion.eglise.dao.repository.MissionRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserMissionRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.MissionDto;
import ci.ueeso.gestion.eglise.helper.dto.UserMissionDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserMissionTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "user_mission"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class UserMissionBusiness implements IBasicBusiness<Request<UserMissionDto>, Response<UserMissionDto>> {

	private Response<UserMissionDto> response;
	@Autowired
	private UserMissionRepository userMissionRepository;
	@Autowired
	private MissionRepository missionRepository;
	@Autowired
	private FonctionRepository fonctionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;
	@Autowired
	private MissionBusiness missionBusiness;

	public UserMissionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create UserMission by using UserMissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserMissionDto> create(Request<UserMissionDto> request, Locale locale)  throws ParseException {
		log.info("----begin create UserMission-----");

		Response<UserMissionDto> response = new Response<UserMissionDto>();
		List<UserMission>        items    = new ArrayList<UserMission>();

		for (UserMissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("idFonction/fonctionLibelle", Arrays.asList(dto.getIdFonction(), dto.getFonctionLibelle()));
			fieldsToVerify.put("idUser", dto.getIdUser());
			fieldsToVerify.put("idMission/dataMission", Arrays.asList(dto.getIdMission(), dto.getDataMission()));
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			
			// Verify if user exist
			User existingUser = userRepository.findOne(dto.getIdUser(), false);
			if (existingUser == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
				response.setHasError(true);
				return response;
			}
			
			// Verify if mission exist
			Mission existingMission = null;
			if (Utilities.isValidID(dto.getIdMission())){
				existingMission = missionRepository.findOne(dto.getIdMission(), false);
				if (existingMission == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("mission idMission -> " + dto.getIdMission(), locale));
					response.setHasError(true);
					return response;
				}
			}
			else {
				Request<MissionDto> subRequest = new Request<MissionDto>();
				subRequest.setUser(request.getUser());
				subRequest.setDatas(Arrays.asList(dto.getDataMission()));
				
				Response<MissionDto> subResponse = missionBusiness.create(subRequest, locale);
				if(subResponse.isHasError()) {
					response.setStatus(subResponse.getStatus());
					response.setHasError(true);
					return response;
				}
				
				existingMission = missionRepository.findOne(subResponse.getItems().get(0).getId(), false);
			}
			
			// Verify if userMission to insert do not exist
			UserMission existingEntity = userMissionRepository.findByIdUserAndIdMission(dto.getIdUser(), dto.getIdMission(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("L'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' est deja enregistré dans la mission '"+existingMission.getLibelle()+"' avec la fonction '"+existingEntity.getFonction().getLibelle()+"'", locale));
				response.setHasError(true);
				return response;
			}
			if (items.stream().anyMatch(e->e.getUser().getId().equals(dto.getIdUser()) && e.getMission().getId().equals(dto.getIdMission()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de l'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' dans la mission '"+existingMission.getLibelle()+"'.", locale));
				response.setHasError(true);
				return response;
			}
			
			// Verify if fonction exist
			Fonction existingFonction = null;
			if (Utilities.isValidID(dto.getIdFonction())){
				existingFonction = fonctionRepository.findOne(dto.getIdFonction(), false);
				if (existingFonction == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("fonction idFonction -> " + dto.getIdFonction(), locale));
					response.setHasError(true);
					return response;
				}
			}
			else {
				existingFonction = fonctionRepository.findByLibelle(dto.getFonctionLibelle(), false);
				if (existingFonction == null) {
					String fonctionCode = Utilities.getCodeFromLibelle(dto.getFonctionLibelle());
					existingFonction = fonctionRepository.findByCode(fonctionCode, false);
					if (existingFonction == null) {
						Fonction fonction = new Fonction();
						fonction.setCode(fonctionCode);
						fonction.setLibelle(dto.getFonctionLibelle());
						fonction.setCreatedAt(new Date());
						fonction.setCreatedBy(request.getUser());
						fonction.setIsDeleted(false);
						
						existingFonction = fonctionRepository.save(fonction);
					}
				}
			}
			
			UserMission entityToSave = UserMissionTransformer.INSTANCE.toEntity(dto, existingMission, existingFonction, existingUser);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<UserMission> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = userMissionRepository.saveAll((Iterable<UserMission>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("userMission", locale));
				response.setHasError(true);
				return response;
			}
			List<UserMissionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserMissionTransformer.INSTANCE.toLiteDtos(itemsSaved) : UserMissionTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create UserMission-----");
		return response;
	}

	/**
	 * update UserMission by using UserMissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserMissionDto> update(Request<UserMissionDto> request, Locale locale)  throws ParseException {
		log.info("----begin update UserMission-----");

		Response<UserMissionDto> response = new Response<UserMissionDto>();
		List<UserMission>        items    = new ArrayList<UserMission>();

		for (UserMissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la userMission existe
			UserMission entityToSave = userMissionRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("userMission id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();
			
			// Verify if user exist
			User existingUser = entityToSave.getUser();
			if (Utilities.isValidID(dto.getIdUser()) && !entityToSave.getUser().getId().equals(dto.getIdUser())){
				existingUser = userRepository.findOne(dto.getIdUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
			}
			
			// Verify if mission exist
			Mission existingMission = entityToSave.getMission();
			if ((Utilities.isValidID(dto.getIdMission()) && !entityToSave.getMission().getId().equals(dto.getIdMission())) || dto.getDataMission() != null){
				if (Utilities.isValidID(dto.getIdMission()) && !entityToSave.getMission().getId().equals(dto.getIdMission())){
					existingMission = missionRepository.findOne(dto.getIdMission(), false);
					if (existingMission == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("mission idMission -> " + dto.getIdMission(), locale));
						response.setHasError(true);
						return response;
					}
				}
				else {
					Request<MissionDto> subRequest = new Request<MissionDto>();
					subRequest.setUser(request.getUser());
					subRequest.setDatas(Arrays.asList(dto.getDataMission()));
					
					Response<MissionDto> subResponse = missionBusiness.create(subRequest, locale);
					if(subResponse.isHasError()) {
						response.setStatus(subResponse.getStatus());
						response.setHasError(true);
						return response;
					}
					
					existingMission = missionRepository.findOne(subResponse.getItems().get(0).getId(), false);
				}
			}
			
			Integer existingUserId = existingUser.getId();
			Integer existingMissionId = existingMission.getId();

			// Verify if userGroupeConstitue to insert do not exist
			UserMission existingEntity = userMissionRepository.findByIdUserAndIdMission(existingUser.getId(), existingMission.getId(), false);
			if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
				response.setStatus(functionalError.DATA_EXIST("L'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' est deja enregistré dans la mission '"+existingMission.getLibelle()+"' avec la fonction '"+existingEntity.getFonction().getLibelle()+"'", locale));
				response.setHasError(true);
				return response;
			}
			if (items.stream().anyMatch(e->e.getUser().getId().equals(existingUserId) && e.getMission().getId().equals(existingMissionId) && !e.getId().equals(entityToSaveId))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de l'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' dans la mission '"+existingMission.getLibelle()+"'.", locale));
				response.setHasError(true);
				return response;
			}

			// Verify if fonction exist
			Fonction existingFonction = entityToSave.getFonction();
			if ((Utilities.isValidID(dto.getIdFonction()) && !existingFonction.getId().equals(dto.getIdFonction())) || (Utilities.isNotBlank(dto.getFonctionLibelle()) && !dto.getFonctionLibelle().equals(existingFonction.getLibelle()))){
				if (Utilities.isValidID(dto.getIdFonction()) && !entityToSave.getFonction().getId().equals(dto.getIdFonction())){
					existingFonction = fonctionRepository.findOne(dto.getIdFonction(), false);
					if (existingFonction == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("fonction idFonction -> " + dto.getIdFonction(), locale));
						response.setHasError(true);
						return response;
					}
				}
				else {
					existingFonction = fonctionRepository.findByLibelle(dto.getFonctionLibelle(), false);
					if (existingFonction == null) {
						String fonctionCode = Utilities.getCodeFromLibelle(dto.getFonctionLibelle());
						existingFonction = fonctionRepository.findByCode(fonctionCode, false);
						if (existingFonction == null) {
							Fonction fonction = new Fonction();
							fonction.setCode(fonctionCode);
							fonction.setLibelle(dto.getFonctionLibelle());
							fonction.setCreatedAt(new Date());
							fonction.setCreatedBy(request.getUser());
							fonction.setIsDeleted(false);
							
							existingFonction = fonctionRepository.save(fonction);
						}
					}
				}
			}

			entityToSave.setUser(existingUser);
			entityToSave.setMission(existingMission);
			entityToSave.setFonction(existingFonction);
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<UserMission> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = userMissionRepository.saveAll((Iterable<UserMission>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("userMission", locale));
				response.setHasError(true);
				return response;
			}
			List<UserMissionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserMissionTransformer.INSTANCE.toLiteDtos(itemsSaved) : UserMissionTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update UserMission-----");
		return response;
	}

	/**
	 * delete UserMission by using UserMissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserMissionDto> delete(Request<UserMissionDto> request, Locale locale)  {
		log.info("----begin delete UserMission-----");

		Response<UserMissionDto> response = new Response<UserMissionDto>();
		List<UserMission>        items    = new ArrayList<UserMission>();

		for (UserMissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la userMission existe
			UserMission existingEntity = userMissionRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("userMission -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userMissionRepository.saveAll((Iterable<UserMission>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete UserMission-----");
		return response;
	}

	/**
	 * forceDelete UserMission by using UserMissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserMissionDto> forceDelete(Request<UserMissionDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete UserMission-----");

		Response<UserMissionDto> response = new Response<UserMissionDto>();
		List<UserMission>        items    = new ArrayList<UserMission>();

		for (UserMissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la userMission existe
			UserMission existingEntity = userMissionRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("userMission -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userMissionRepository.saveAll((Iterable<UserMission>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete UserMission-----");
		return response;
	}

	/**
	 * get UserMission by using UserMissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserMissionDto> getByCriteria(Request<UserMissionDto> request, Locale locale)  throws Exception {
		log.info("----begin get UserMission-----");

		Response<UserMissionDto> response = new Response<UserMissionDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("mission.libelle");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<UserMission> items 			 = userMissionRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("userMission", locale));
			response.setHasError(false);
			return response;
		}

		List<UserMissionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserMissionTransformer.INSTANCE.toLiteDtos(items) : UserMissionTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(userMissionRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get UserMission-----");
		return response;
	}

	/**
	 * get full UserMissionDto by using UserMission as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private UserMissionDto getFullInfos(UserMissionDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

	public UserMissionDto getFullInfos(UserMission entity, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		UserMissionDto dto = (Utilities.isTrue(isSimpleLoading)) ? UserMissionTransformer.INSTANCE.toLiteDto(entity) : UserMissionTransformer.INSTANCE.toDto(entity);
		dto.setMissionAdresse(entity.getMission().getAdresse());
		dto.setMissionDescription(entity.getMission().getDescription());

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

}
