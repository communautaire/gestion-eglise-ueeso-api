

/*
 * Java business for entity table profession 
 * Created on 2019-11-30 ( Time 18:43:49 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Profession;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.repository.ProfessionRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.ProfessionDto;
import ci.ueeso.gestion.eglise.helper.dto.UserDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.ProfessionTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "profession"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class ProfessionBusiness implements IBasicBusiness<Request<ProfessionDto>, Response<ProfessionDto>> {

	private Response<ProfessionDto> response;
	@Autowired
	private ProfessionRepository professionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;

	public ProfessionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Profession by using ProfessionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProfessionDto> create(Request<ProfessionDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Profession-----");

		Response<ProfessionDto> response = new Response<ProfessionDto>();
		List<Profession>        items    = new ArrayList<Profession>();

		for (ProfessionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("isMissionRelated", dto.getIsMissionRelated());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if profession to insert do not exist
			Profession existingEntity = professionRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("La profession de code `" + dto.getCode()+"` existe déjà.", locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la profession de code `"+dto.getCode()+"`.", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = professionRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("La profession de libelle `" + dto.getLibelle()+"` existe déjà.", locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la profession de libelle `"+dto.getLibelle()+"`.", locale));
				response.setHasError(true);
				return response;
			}

			Profession entityToSave = ProfessionTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Profession> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = professionRepository.saveAll((Iterable<Profession>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("profession", locale));
				response.setHasError(true);
				return response;
			}
			List<ProfessionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ProfessionTransformer.INSTANCE.toLiteDtos(itemsSaved) : ProfessionTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Profession-----");
		return response;
	}

	/**
	 * update Profession by using ProfessionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProfessionDto> update(Request<ProfessionDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Profession-----");

		Response<ProfessionDto> response = new Response<ProfessionDto>();
		List<Profession>        items    = new ArrayList<Profession>();

		for (ProfessionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la profession existe
			Profession entityToSave = null;
			entityToSave = professionRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("profession id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				Profession existingEntity = professionRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("La profession de code `" + dto.getCode()+"` existe déjà.", locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la profession de code `"+dto.getCode()+"`.", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				Profession existingEntity = professionRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("La profession de libelle `" + dto.getLibelle()+"` existe déjà.", locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la profession de libelle `"+dto.getLibelle()+"`.", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			
			if (dto.getIsMissionRelated() != null) {
				entityToSave.setIsMissionRelated(dto.getIsMissionRelated());
			}
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Profession> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = professionRepository.saveAll((Iterable<Profession>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("profession", locale));
				response.setHasError(true);
				return response;
			}
			List<ProfessionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ProfessionTransformer.INSTANCE.toLiteDtos(itemsSaved) : ProfessionTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Profession-----");
		return response;
	}

	/**
	 * delete Profession by using ProfessionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProfessionDto> delete(Request<ProfessionDto> request, Locale locale)  {
		log.info("----begin delete Profession-----");

		Response<ProfessionDto> response = new Response<ProfessionDto>();
		List<Profession>        items    = new ArrayList<Profession>();

		for (ProfessionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la profession existe
			Profession existingEntity = professionRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("profession -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// user
			List<User> listOfUser = userRepository.findByIdProfession(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			professionRepository.saveAll((Iterable<Profession>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Profession-----");
		return response;
	}

	/**
	 * forceDelete Profession by using ProfessionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProfessionDto> forceDelete(Request<ProfessionDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Profession-----");

		Response<ProfessionDto> response = new Response<ProfessionDto>();
		List<Profession>        items    = new ArrayList<Profession>();

		for (ProfessionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la profession existe
			Profession existingEntity = professionRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("profession -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// user
			List<User> listOfUser = userRepository.findByIdProfession(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				Request<UserDto> deleteRequest = new Request<UserDto>();
				deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
				deleteRequest.setUser(request.getUser());
				Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			professionRepository.saveAll((Iterable<Profession>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Profession-----");
		return response;
	}

	/**
	 * get Profession by using ProfessionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<ProfessionDto> getByCriteria(Request<ProfessionDto> request, Locale locale)  throws Exception {
		log.info("----begin get Profession-----");

		Response<ProfessionDto> response = new Response<ProfessionDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("libelle");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Profession> items 			 = professionRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("profession", locale));
			response.setHasError(false);
			return response;
		}

		List<ProfessionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ProfessionTransformer.INSTANCE.toLiteDtos(items) : ProfessionTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(professionRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Profession-----");
		return response;
	}

	/**
	 * get full ProfessionDto by using Profession as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private ProfessionDto getFullInfos(ProfessionDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
