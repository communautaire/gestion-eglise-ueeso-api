
/*
 * Java business for entity table user 
 * Created on 2019-11-30 ( Time 19:22:12 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.ueeso.gestion.eglise.dao.entity.Adresse;
import ci.ueeso.gestion.eglise.dao.entity.Commune;
import ci.ueeso.gestion.eglise.dao.entity.Engagement;
import ci.ueeso.gestion.eglise.dao.entity.Functionality;
import ci.ueeso.gestion.eglise.dao.entity.MemberStatus;
import ci.ueeso.gestion.eglise.dao.entity.Pays;
import ci.ueeso.gestion.eglise.dao.entity.Profession;
import ci.ueeso.gestion.eglise.dao.entity.Quartier;
import ci.ueeso.gestion.eglise.dao.entity.Role;
import ci.ueeso.gestion.eglise.dao.entity.SituationMatrimoniale;
import ci.ueeso.gestion.eglise.dao.entity.SituationProfessionnelle;
import ci.ueeso.gestion.eglise.dao.entity.Tresorier;
import ci.ueeso.gestion.eglise.dao.entity.TypeUser;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.entity.UserGroupeConstitue;
import ci.ueeso.gestion.eglise.dao.entity.UserMission;
import ci.ueeso.gestion.eglise.dao.entity.Ville;
import ci.ueeso.gestion.eglise.dao.repository.AdresseRepository;
import ci.ueeso.gestion.eglise.dao.repository.CommuneRepository;
import ci.ueeso.gestion.eglise.dao.repository.EngagementRepository;
import ci.ueeso.gestion.eglise.dao.repository.GroupeConstitueRepository;
import ci.ueeso.gestion.eglise.dao.repository.MemberStatusRepository;
import ci.ueeso.gestion.eglise.dao.repository.PaysRepository;
import ci.ueeso.gestion.eglise.dao.repository.ProfessionRepository;
import ci.ueeso.gestion.eglise.dao.repository.QuartierRepository;
import ci.ueeso.gestion.eglise.dao.repository.RoleFunctionalityRepository;
import ci.ueeso.gestion.eglise.dao.repository.RoleRepository;
import ci.ueeso.gestion.eglise.dao.repository.SituationMatrimonialeRepository;
import ci.ueeso.gestion.eglise.dao.repository.SituationProfessionnelleRepository;
import ci.ueeso.gestion.eglise.dao.repository.TresorierRepository;
import ci.ueeso.gestion.eglise.dao.repository.TypeUserRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserGroupeConstitueRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserMissionRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserRepository;
import ci.ueeso.gestion.eglise.dao.repository.VilleRepository;
import ci.ueeso.gestion.eglise.excel.mass.importation.ExportCriteriaLevel;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.ParamsUtils;
import ci.ueeso.gestion.eglise.helper.PoiUtils;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.AdresseDto;
import ci.ueeso.gestion.eglise.helper.dto.CommuneDto;
import ci.ueeso.gestion.eglise.helper.dto.EngagementDto;
import ci.ueeso.gestion.eglise.helper.dto.QuartierDto;
import ci.ueeso.gestion.eglise.helper.dto.TresorierDto;
import ci.ueeso.gestion.eglise.helper.dto.UserDto;
import ci.ueeso.gestion.eglise.helper.dto.UserGroupeConstitueDto;
import ci.ueeso.gestion.eglise.helper.dto.UserMissionDto;
import ci.ueeso.gestion.eglise.helper.dto.VilleDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.AdresseTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.CommuneTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.EngagementTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.TresorierTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserGroupeConstitueTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserMissionTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.VilleTransformer;
import ci.ueeso.gestion.eglise.helper.enums.TypeUserEnum;
import lombok.extern.java.Log;

/**
 * BUSINESS for table "user"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class UserBusiness implements IBasicBusiness<Request<UserDto>, Response<UserDto>> {

	private Response<UserDto> response;
	
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EngagementRepository engagementRepository;
	@Autowired
	private ProfessionRepository professionRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private MemberStatusRepository memberStatusRepository;
	@Autowired
	private TresorierRepository tresorierRepository;
	@Autowired
	private SituationMatrimonialeRepository situationMatrimonialeRepository;
	@Autowired
	private UserGroupeConstitueRepository userGroupeConstitueRepository;
	@Autowired
	private GroupeConstitueRepository groupeConstitueRepository;
	@Autowired
	private SituationProfessionnelleRepository situationProfessionnelleRepository;
	@Autowired
	private TypeUserRepository typeUserRepository;
	@Autowired
	private UserMissionRepository userMissionRepository;
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private RoleFunctionalityRepository roleFunctionalityRepository;
	@Autowired
	private QuartierRepository quartierRepository;
	
	
	@Autowired
	private ParamsUtils paramsUtils;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	
	@Autowired
	private VilleBusiness villeBusiness;
	@Autowired
	private CommuneBusiness communeBusiness;
	@Autowired
	private EngagementBusiness engagementBusiness;
	@Autowired
	private TresorierBusiness tresorierBusiness;
	@Autowired
	private UserGroupeConstitueBusiness userGroupeConstitueBusiness;
	@Autowired
	private UserMissionBusiness userMissionBusiness;
	@Autowired
	private AdresseBusiness adresseBusiness;
	@Autowired
	private MailBusiness mailBusiness;
	@Autowired
	private QuartierBusiness quartierBusiness;
	

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;
	
	
	public UserBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create User by using UserDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws Exception
	 * 
	 */
	@Override
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> create(Request<UserDto> request, Locale locale) {
		log.info("----begin create User-----");

		try {

			response = new Response<UserDto>();
			List<User> items = new ArrayList<User>();
			List<Adresse> itemsAdresse = new ArrayList<>();
			List<UserGroupeConstitue> itemsGroupeConstitues = new ArrayList<>();

			for (UserDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("nom", dto.getNom()); 
				fieldsToVerify.put("prenom", dto.getPrenom()); 
				//fieldsToVerify.put("email", dto.getEmail()); 
				//fieldsToVerify.put("telephone", dto.getTelephone()); 
				fieldsToVerify.put("dateNaissance", dto.getDateNaissance()); 
				//fieldsToVerify.put("login", dto.getLogin());   						//cdnl 2
				//fieldsToVerify.put("password", dto.getPassword());  					//cdnl 2
				//fieldsToVerify.put("photo", dto.getPhoto()); 
				fieldsToVerify.put("idSituationProfessionnelle", dto.getIdSituationProfessionnelle()); 
				//fieldsToVerify.put("idRole", dto.getIdRole()); 						//cdnl 2
				fieldsToVerify.put("isLocalResident", dto.getIsLocalResident()); 
				fieldsToVerify.put("idSituationMatrimoniale", dto.getIdSituationMatrimoniale()); 
				//fieldsToVerify.put("nomConjoint", dto.getNomConjoint()); 				//cdnl 1 idSituationMatrimoniale
				//fieldsToVerify.put("dateVieConjugale", dto.getDateVieConjugale()); 	//cdnl 1 idSituationMatrimoniale
				fieldsToVerify.put("isHomeRespo", dto.getIsHomeRespo()); 
				//fieldsToVerify.put("homeRespoName", dto.getHomeRespoName());  		//cdnl 1 isHomeRespo
				fieldsToVerify.put("baptemeImmersion", dto.getBaptemeImmersion()); 
				//fieldsToVerify.put("dateConversion", dto.getDateConversion()); 		//cdnl 1b baptemeImmersion
				//fieldsToVerify.put("dateBapteme", dto.getDateBapteme()); 				//cdnl 1 baptemeImmersion
				//fieldsToVerify.put("egliseBapteme", dto.getEgliseBapteme()); 			//cdnl 1 baptemeImmersion
				fieldsToVerify.put("egliseOrigine", dto.getEgliseOrigine());
				fieldsToVerify.put("typeUserCode", dto.getTypeUserCode()); //fieldsToVerify.put("idTypeUser", dto.getIdTypeUser());
				fieldsToVerify.put("idPays (Origine)", dto.getIdPays()); 
				fieldsToVerify.put("idProfession", dto.getIdProfession());  			
				//				fieldsToVerify.put("locked", dto.getLocked()); 
				//				fieldsToVerify.put("isConnected", dto.getIsConnected()); 
				//				fieldsToVerify.put("isValidated", dto.getIsValidated());
				fieldsToVerify.put("lieuNaissance", dto.getLieuNaissance());
				fieldsToVerify.put("domaineCompetence", dto.getDomaineCompetence());
				fieldsToVerify.put("civilite", dto.getCivilite());

				fieldsToVerify.put("residenceCountryId", dto.getResidenceCountryId());
				//fieldsToVerify.put("residenceDistrict", dto.getResidenceDistrict());
				fieldsToVerify.put("residenceVille", dto.getResidenceVille());
				fieldsToVerify.put("residenceAdresse", dto.getResidenceAdresse());

				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				if(!TypeUserEnum.isValid(dto.getTypeUserCode())) {
					response.setStatus(functionalError.INVALID_DATA("typeUserCode " + dto.getTypeUserCode(), locale));
					response.setHasError(true);
					return response;
				}

				//Champs uniquement requis pour les Fidèles
				if(dto.getTypeUserCode().equals(TypeUserEnum.FIDELE.getCode())) {
					fieldsToVerify = new HashMap<String, java.lang.Object>();
					//fieldsToVerify.put("datasUserGroupeConstitue", dto.getDatasUserGroupeConstitue());
					fieldsToVerify.put("dateArriveUeeso", dto.getDateArriveUeeso()); 
					fieldsToVerify.put("idMemberStatus", dto.getIdMemberStatus());
					if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
						response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
						response.setHasError(true);
						return response;
					}
				}

				//residence de l'utilisateur
				Adresse adresse = new Adresse();
				Pays existingResidencePays = null;
				// pays de residence
				if (Utilities.isValidID(dto.getResidenceCountryId())) {
					existingResidencePays = paysRepository.findOne(dto.getResidenceCountryId(), false);
					if (existingResidencePays == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("residenceCountryId -> " + dto.getResidenceCountryId(), locale));
						response.setHasError(true);
						return response;
					}
					adresse.setPays(existingResidencePays);
				}

				//ville de residence
				Ville existingVille = null;
				if (Utilities.notBlank(dto.getResidenceVille())) {
					existingVille = villeRepository.findByLibelle(dto.getResidenceVille(), false);
					//creation  de nouvelle ville
					if(existingVille == null) {
						VilleDto dataVille = new VilleDto();
						dataVille.setIdPays(dto.getResidenceCountryId());
						//dataVille.setIdPays(dto.getIdPays());
						dataVille.setLibelle(dto.getResidenceVille());

						Request<VilleDto> requestVille = new Request<VilleDto>();
						requestVille.setDatas(Arrays.asList(dataVille));
						requestVille.setUser(request.getUser());

						Response<VilleDto> responseVille = villeBusiness.create(requestVille, locale);
						if(responseVille.isHasError()) {
							response.setStatus(responseVille.getStatus());
							response.setHasError(true);
							return response;
						}
						if (Utilities.isNotEmpty(responseVille.getItems())) {
							existingVille = VilleTransformer.INSTANCE.toEntity(responseVille.getItems().get(0),existingResidencePays);
							log.info("responseVille "+responseVille);
						}
					}	
					adresse.setVille(existingVille);	
				}

				//commune de residence
				Commune existingCommune = null;
				if (Utilities.notBlank(dto.getResidenceCommune())) {
					if (existingVille != null) {				
						existingCommune = communeRepository.findByLibelleAndVille(dto.getResidenceCommune(),existingVille.getId(), false);					
					}
					if (existingCommune == null) {				
						existingCommune = communeRepository.findByLibelleAndPays(dto.getResidenceCommune(),existingResidencePays.getId(), false);					
					}
					//creation  de nouvelle ville
					if(existingCommune == null) {
						CommuneDto dataCommune = new CommuneDto();
						dataCommune.setLibelle(dto.getResidenceCommune());
						dataCommune.setIdPays(existingResidencePays.getId());
						dataCommune.setIdVille(existingVille.getId());

						Request<CommuneDto> requestCommune = new Request<>();
						requestCommune.setDatas(Arrays.asList(dataCommune));
						requestCommune.setUser(request.getUser());

						Response<CommuneDto> responseCommune = communeBusiness.create(requestCommune, locale);
						if(responseCommune.isHasError()) {
							response.setStatus(responseCommune.getStatus());
							response.setHasError(true);
							return response;
						}
						
						if (Utilities.isNotEmpty(responseCommune.getItems())) {
							existingCommune = CommuneTransformer.INSTANCE.toEntity(responseCommune.getItems().get(0),existingResidencePays,existingVille);
						}
					}
					adresse.setCommune(existingCommune);
					
					if(Utilities.isNotBlank(dto.getResidenceQuartier())) {
						Quartier existingQuartier = quartierRepository.findByLibelleAndIdCommune(dto.getResidenceQuartier(), existingCommune.getId(), false);
						if(existingQuartier == null) {
							QuartierDto data = new QuartierDto();
							data.setLibelle(dto.getResidenceQuartier());
							data.setIdCommune(existingCommune.getId());
							
							Request<QuartierDto> subRequest = new Request<QuartierDto>();
							subRequest.setDatas(Arrays.asList(data));
							subRequest.setUser(request.getUser());

							Response<QuartierDto> subResponse = quartierBusiness.create(subRequest, locale);
							if (subResponse.isHasError()) {
								response.setStatus(subResponse.getStatus());
								response.setHasError(true);
								return response;
							}
							
							existingQuartier = quartierRepository.findOne(subResponse.getItems().get(0).getId(), false);
						}
						adresse.setQuartier(existingQuartier);
					}
				}

				//adresse de residence
				if (Utilities.notBlank(dto.getResidenceAdresse())) {
					adresse.setAdressePostale(dto.getResidenceAdresse());
				}

				// Verify if user to insert do not exist
				User existingEntity = null;

				// verif unique email in db
				if(Utilities.isNotBlank(dto.getEmail())) {
					if (!Utilities.isValidEmail(dto.getEmail())) {
						response.setStatus(functionalError.INVALID_DATA("`" + dto.getEmail()+"` n'est pas un email valide.", locale));
						response.setHasError(true);
						return response;
					}
					existingEntity = userRepository.findByEmail(dto.getEmail(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user email -> " + dto.getEmail(), locale));
						response.setHasError(true);
						return response;
					}
					// verif unique email in items to save
					if (items.stream().anyMatch(a -> a.getEmail().equalsIgnoreCase(dto.getEmail()))) {
						response.setStatus(functionalError.DATA_DUPLICATE(" email ", locale));
						response.setHasError(true);
						return response;
					}
				}

				// verif unique telephone in db
				if(Utilities.isNotBlank(dto.getTelephone())) {
					/*
					if (!Utilities.isValidateIvorianPhoneNumber(dto.getTelephone())) {
						response.setStatus(functionalError.INVALID_DATA("`" + dto.getTelephone()+"` n'est pas un numéro de téléphone valide.", locale));
						response.setHasError(true);
						return response;
					}
					dto.setTelephone(Utilities.ivorianPhoneNumberToStandardFormat(dto.getTelephone()));
					 */
					existingEntity = userRepository.findByTelephone(dto.getTelephone(), false);
					if (existingEntity != null) {
						response.setStatus(functionalError.DATA_EXIST("user telephone -> " + dto.getTelephone(), locale));
						response.setHasError(true);
						return response;
					}
					// verif unique telephone in items to save
					if (items.stream().anyMatch(a -> a.getTelephone().equalsIgnoreCase(dto.getTelephone()))) {
						response.setStatus(functionalError.DATA_DUPLICATE("Telephone ", locale));
						response.setHasError(true);
						return response;
					}
				}

				Date dateNaissance = null;
				if(Utilities.isNotBlank(dto.getDateNaissance())) {
					String pattern = Utilities.findDateFormatByParsing(dto.getDateNaissance());
					dateNaissance = new SimpleDateFormat(pattern).parse(dto.getDateNaissance());
				}
				dto.setDateNaissance(null);

				if(!dto.getIsHomeRespo()) {
					if (Utilities.isBlank(dto.getHomeRespoName())) {
						response.setStatus(functionalError.FIELD_EMPTY("Home responsible full name", locale));
						response.setHasError(true);
						return response;
					}
				}
				else {
					dto.setHomeRespoName(null);
				}

				Date dateBapteme = null;
				if(dto.getBaptemeImmersion()) {
					fieldsToVerify = new HashMap<String, java.lang.Object>();
					fieldsToVerify.put("dateConversion", dto.getDateConversion()); 		//cdnl 1b baptemeImmersion
					fieldsToVerify.put("dateBapteme", dto.getDateBapteme()); 				//cdnl 1 baptemeImmersion
					fieldsToVerify.put("egliseBapteme", dto.getEgliseBapteme()); 			//cdnl 1 baptemeImmersion
					if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
						response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
						response.setHasError(true);
						return response;
					}

					String pattern = Utilities.findDateFormatByParsing(dto.getDateBapteme());
					dateBapteme = new SimpleDateFormat(pattern).parse(dto.getDateBapteme());
				}
				else {
					dto.setEgliseBapteme(null);
				}
				dto.setDateBapteme(null);

				Date dateConversion = null;
				if(Utilities.isNotBlank(dto.getDateConversion())) {
					String pattern = Utilities.findDateFormatByParsing(dto.getDateConversion());
					dateConversion = new SimpleDateFormat(pattern).parse(dto.getDateConversion());
				}
				dto.setDateConversion(null);

				// Verify if situationProfessionnelle exist
				SituationProfessionnelle existingSituationProfessionnelle = null;
				if (Utilities.isValidID(dto.getIdSituationProfessionnelle())) {
					existingSituationProfessionnelle = situationProfessionnelleRepository.findOne(dto.getIdSituationProfessionnelle(), false);
					if (existingSituationProfessionnelle == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("situationProfessionnelle idSituationProfessionnelle -> " + dto.getIdSituationProfessionnelle(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if profession exist
				Profession existingProfession = null;
				if (Utilities.isValidID(dto.getIdProfession())) {
					existingProfession = professionRepository.findOne(dto.getIdProfession(), false);
					if (existingProfession == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("profession idProfession -> " + dto.getIdProfession(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if pays exist
				Pays existingPays = null;
				if (Utilities.isValidID(dto.getIdPays())) {
					existingPays = paysRepository.findOne(dto.getIdPays(), false);
					if (existingPays == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("pays d'origine idPays -> " + dto.getIdPays(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if situationMatrimoniale exist
				SituationMatrimoniale existingSituationMatrimoniale = null;
				Date dateVieConjugale = null;
				if (Utilities.isValidID(dto.getIdSituationMatrimoniale())) {
					existingSituationMatrimoniale = situationMatrimonialeRepository.findOne(dto.getIdSituationMatrimoniale(), false);
					if (existingSituationMatrimoniale == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("situationMatrimoniale idSituationMatrimoniale -> " + dto.getIdSituationMatrimoniale(), locale));
						response.setHasError(true);
						return response;
					}
					if(Utilities.isTrue(existingSituationMatrimoniale.getHasWeddedLife())) {
						fieldsToVerify = new HashMap<String, java.lang.Object>();
						fieldsToVerify.put("nomConjoint", dto.getNomConjoint());
						fieldsToVerify.put("dateVieConjugale", dto.getDateVieConjugale());
						if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
							response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
							response.setHasError(true);
							return response;
						}

						String pattern = Utilities.findDateFormatByParsing(dto.getDateVieConjugale());
						dateVieConjugale = new SimpleDateFormat(pattern).parse(dto.getDateVieConjugale());
					}
					else {
						dto.setNomConjoint(null);
					}
				}
				dto.setDateVieConjugale(null);

				// Verify if typeUser exist
				TypeUser existingTypeUser = null;
				if (Utilities.isNotBlank(dto.getTypeUserCode())) {
					existingTypeUser = typeUserRepository.findByCode(dto.getTypeUserCode(), false);
					if (existingTypeUser == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("typeUser idTypeUser -> " + dto.getIdTypeUser(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// Verify if role exist
				Role existingRole = null;
				if (Utilities.isValidID(dto.getIdRole())) {
					existingRole = roleRepository.findOne(dto.getIdRole(), false);
					if (existingRole == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("role idRole -> " + dto.getIdRole(), locale));
						response.setHasError(true);
						return response;
					}
				}

				MemberStatus existingMemberStatus = null;
				Date dateArriveUeeso = null;
				if(dto.getTypeUserCode().equals(TypeUserEnum.FIDELE.getCode())) {
					if (Utilities.isValidID(dto.getIdMemberStatus())) {
						// Verify if memberStatus exist
						existingMemberStatus = memberStatusRepository.findOne(dto.getIdMemberStatus());
						if (existingMemberStatus == null) {
							response.setStatus(functionalError.DATA_NOT_EXIST("memberStatus -> " + dto.getIdMemberStatus(), locale));
							response.setHasError(true);
							return response;
						}

						String pattern = Utilities.findDateFormatByParsing(dto.getDateArriveUeeso());
						dateArriveUeeso = new SimpleDateFormat(pattern).parse(dto.getDateArriveUeeso());
					}
				}
				dto.setDateArriveUeeso(null);

				// ajout de l'image
				if (Utilities.isNotBlank(dto.getFileName()) && Utilities.isNotBlank(dto.getFileBase64()) && Utilities.isNotBlank(dto.getExtension())) {
					if (!Utilities.fileIsImage(Utilities.normalizeFileName(dto.getFileName()) + "." + dto.getExtension())) {
						response.setStatus(functionalError.TYPE_NOT_CORRECT("" + Utilities.fileIsImage(Utilities.normalizeFileName(dto.getFileName()) + "." + dto.getExtension()), locale));
						response.setHasError(Boolean.TRUE);
						return response;
					}

					// create directory
					String filesDirectory = paramsUtils.getImageDirectory();
					Utilities.createDirectory(filesDirectory);

					String photoFullPath = filesDirectory + "/" + Utilities.normalizeFileName(dto.getFileName()) + "_" + Utilities.normalizeCurrentDateIntoString() + "." + dto.getExtension();
					boolean succes = Utilities.saveImage(dto.getFileBase64(), photoFullPath, dto.getExtension());
					if (!succes) {
						response.setHasError(Boolean.TRUE);
						response.setStatus(functionalError.SAVE_FAIL("", locale));
						return response;
					}

					dto.setPhoto(photoFullPath);
				}

				User entityToSave = UserTransformer.INSTANCE.toEntity(dto, existingProfession, existingPays, existingMemberStatus, existingSituationMatrimoniale, existingSituationProfessionnelle, existingTypeUser, existingRole);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);

				entityToSave.setLogin(null);
				entityToSave.setPassword(null);
				entityToSave.setIsValidated(null);
				entityToSave.setLocked(Boolean.FALSE);
				entityToSave.setIsConnected(Boolean.FALSE);

				entityToSave.setDateVieConjugale(dateVieConjugale);
				entityToSave.setDateConversion(dateConversion);
				entityToSave.setDateBapteme(dateBapteme);
				entityToSave.setDateArriveUeeso(dateArriveUeeso);
				entityToSave.setDateNaissance(dateNaissance);

				User entitySaved=userRepository.save(entityToSave);
				adresse.setUser(entitySaved);
				adresse.setCreatedAt(Utilities.getCurrentDate());
				adresse.setIsDeleted(false);
				itemsAdresse.add(adresse);

				//Construction de la requête des groupes constitués	
				if(dto.getTypeUserCode().equals(TypeUserEnum.FIDELE.getCode())) {					
					if(Utilities.isNotEmpty(dto.getDatasUserGroupeConstitue())) {
						dto.getDatasUserGroupeConstitue().forEach(e->{
							e.setIdUser(entitySaved.getId());
						});

						Request<UserGroupeConstitueDto> grpeConstitueRequest= new Request<UserGroupeConstitueDto>();
						grpeConstitueRequest.setDatas(dto.getDatasUserGroupeConstitue());
						grpeConstitueRequest.setUser(request.getUser());

						Response<UserGroupeConstitueDto> grpeConstitueResponse = userGroupeConstitueBusiness.create(grpeConstitueRequest, locale);
						if(grpeConstitueResponse.isHasError()) {
							response.setStatus(grpeConstitueResponse.getStatus());
							response.setHasError(true);
							return response;
						}
					}
				}
				
				if(existingProfession != null) {
					if(Utilities.isTrue(existingProfession.getIsMissionRelated())) {
						if(Utilities.isEmpty(dto.getDatasUserMission())) {
							response.setStatus(functionalError.FIELD_EMPTY("datasUserMission: La profession `" + existingProfession.getLibelle()+"` est lié à la mission. Vous devez donc renseigner vos champs missionnaires.", locale));
							response.setHasError(true);
							return response;
						}
						dto.getDatasUserMission().forEach(e->{
							e.setIdUser(entitySaved.getId());
						});
						
						Request<UserMissionDto> subRequest= new Request<UserMissionDto>();
						subRequest.setDatas(dto.getDatasUserMission());
						subRequest.setUser(request.getUser());

						Response<UserMissionDto> subResponse = userMissionBusiness.create(subRequest, locale);
						if(subResponse.isHasError()) {
							response.setStatus(subResponse.getStatus());
							response.setHasError(true);
							return response;
						}
					}
				}
				
				items.add(entitySaved);
			}

			if (!items.isEmpty()) {
				List<Adresse> itemsAdresseSaved = null;
				List<UserGroupeConstitue> itemsUserGroupeConstitueSaved = null;
				// inserer les donnees des adresse en base de donnees
				itemsAdresseSaved = adresseRepository.saveAll((Iterable<Adresse>) itemsAdresse);
				if (itemsAdresseSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("adresse", locale));
					response.setHasError(true);
					return response;
				}

				// inserer les donnees des user_groupe_constitue en base de donnees
				itemsUserGroupeConstitueSaved= userGroupeConstitueRepository.saveAll((Iterable<UserGroupeConstitue>)itemsGroupeConstitues);
				if (itemsUserGroupeConstitueSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("user_groupe_constitue", locale));
					response.setHasError(true);
					return response;
				}
				List<UserDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserTransformer.INSTANCE.toLiteDtos(items) : UserTransformer.INSTANCE.toDtos(items);

				final int size = items.size();
				List<String> listOfError = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}

				response.setItems(itemsDto);
				response.setHasError(false);
				response.setStatus(functionalError.SUCCESS("", locale));
			}
			log.info("----end create DeletedRecord-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info("Erreur| code: {} -  message: {}" + response.getStatus().getCode()
						+ response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}

		log.info("----end create User-----");
		return response;
	}

	/**
	 * update User by using UserDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> update(Request<UserDto> request, Locale locale) throws ParseException {
		log.info("----begin update User-----");

		Response<UserDto> response = new Response<UserDto>();
		List<User> items = new ArrayList<User>();
		List<UserDto> oldItems = new ArrayList<UserDto>();

		for (UserDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la user existe
			User entityToSave = userRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			UserDto oldEntityToSaveDto = UserTransformer.INSTANCE.toDto(entityToSave);
			Boolean dataChanged = false;

			// Verify if profession exist
			if (Utilities.isValidID(dto.getIdProfession()) && !entityToSave.getProfession().getId().equals(dto.getIdProfession())) {
				Profession existingProfession = professionRepository.findOne(dto.getIdProfession(), false);
				if (existingProfession == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("profession idProfession -> " + dto.getIdProfession(), locale));
					response.setHasError(true);
					return response;
				}
				
				if(Utilities.isTrue(existingProfession.getIsMissionRelated())) {
					if(Utilities.isEmpty(dto.getDatasUserMission())) {
						response.setStatus(functionalError.FIELD_EMPTY("datasUserMission: La profession `" + existingProfession.getLibelle()+"` est lié à la mission. Vous devez donc renseigner vos champs missionnaires.", locale));
						response.setHasError(true);
						return response;
					}
					dto.getDatasUserMission().forEach(e->{
						e.setIdUser(entityToSave.getId());
					});

					Request<UserMissionDto> subRequest= new Request<UserMissionDto>();
					subRequest.setUser(request.getUser());
					subRequest.setDatas(dto.getDatasUserMission());

					Response<UserMissionDto> subResponse = userMissionBusiness.create(subRequest, locale);
					if(subResponse.isHasError()) {
						response.setStatus(subResponse.getStatus());
						response.setHasError(true);
						return response;
					}
				}
				else {
					List<UserMission> datasUserMission = userMissionRepository.findByIdUser(entityToSave.getId(), false);
					if(Utilities.isNotEmpty(datasUserMission)) {
						userMissionRepository.deleteAll(datasUserMission);
					}
				}
				entityToSave.setProfession(existingProfession);
			}
			// Verify if pays exist
			if (Utilities.isValidID(dto.getIdPays()) && !entityToSave.getPays().getId().equals(dto.getIdPays())) {
				Pays existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setPays(existingPays);
			}
			// Verify if situationMatrimoniale exist
			if (Utilities.isValidID(dto.getIdSituationMatrimoniale()) && !entityToSave.getSituationMatrimoniale().getId().equals(dto.getIdSituationMatrimoniale())) {
				SituationMatrimoniale existingSituationMatrimoniale = situationMatrimonialeRepository.findOne(dto.getIdSituationMatrimoniale(), false);
				if (existingSituationMatrimoniale == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("situationMatrimoniale idSituationMatrimoniale -> " + dto.getIdSituationMatrimoniale(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setSituationMatrimoniale(existingSituationMatrimoniale);
			}
			// Verify if situationProfessionnelle exist
			if (Utilities.isValidID(dto.getIdSituationProfessionnelle()) && !entityToSave.getSituationProfessionnelle().getId().equals(dto.getIdSituationProfessionnelle())) {
				SituationProfessionnelle existingSituationProfessionnelle = situationProfessionnelleRepository.findOne(dto.getIdSituationProfessionnelle(), false);
				if (existingSituationProfessionnelle == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("situationProfessionnelle idSituationProfessionnelle -> " + dto.getIdSituationProfessionnelle(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setSituationProfessionnelle(existingSituationProfessionnelle);
			}
			// Verify if typeUser exist
			if (Utilities.isValidID(dto.getIdTypeUser()) && !entityToSave.getTypeUser().getId().equals(dto.getIdTypeUser())) {
				TypeUser existingTypeUser = typeUserRepository.findOne(dto.getIdTypeUser(), false);
				if (existingTypeUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("typeUser idTypeUser -> " + dto.getIdTypeUser(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setTypeUser(existingTypeUser);
			}
			// Verify if role exist
			if (Utilities.isValidID(dto.getIdRole()) && !entityToSave.getRole().getId().equals(dto.getIdRole())) {
				Role existingRole = roleRepository.findOne(dto.getIdRole(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role idRole -> " + dto.getIdRole(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setRole(existingRole);
			}
			if (Utilities.isNotBlank(dto.getNom()) && !dto.getNom().equals(entityToSave.getNom())) {
				entityToSave.setNom(dto.getNom());
			}
			if (Utilities.isNotBlank(dto.getPrenom()) && !dto.getPrenom().equals(entityToSave.getPrenom())) {
				entityToSave.setPrenom(dto.getPrenom());
			}
			if (Utilities.isNotBlank(dto.getEmail()) && !dto.getEmail().equals(entityToSave.getEmail())) {
				User existingEntity = userRepository.findByEmail(dto.getEmail(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getEmail(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a -> a.getEmail().equalsIgnoreCase(dto.getEmail()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du email '" + dto.getEmail() + "' pour les users", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setEmail(dto.getEmail());
			}
			if (Utilities.isNotBlank(dto.getTelephone()) && !dto.getTelephone().equals(entityToSave.getTelephone())) {
				User existingEntity = userRepository.findByTelephone(dto.getTelephone(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getTelephone(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a -> a.getTelephone().equalsIgnoreCase(dto.getTelephone()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du telephone '" + dto.getTelephone() + "' pour les users", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setTelephone(dto.getTelephone());
			}
			if (dto.getDateNaissance() != null) {
				entityToSave.setDateNaissance(dateFormat.parse(dto.getDateNaissance()));
			}
			if (Utilities.isNotBlank(dto.getLogin()) && !dto.getLogin().equals(entityToSave.getLogin())) {
				User existingEntity = userRepository.findByLogin(dto.getLogin(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("user -> " + dto.getLogin(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a -> a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin() + "' pour les users", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLogin(dto.getLogin());
			}
			if (Utilities.isNotBlank(dto.getPassword()) && !dto.getPassword().equals(entityToSave.getPassword())) {
				entityToSave.setPassword(dto.getPassword());
			}
//			if (Utilities.isNotBlank(dto.getPhoto()) && !dto.getPhoto().equals(entityToSave.getPhoto())) {
//				entityToSave.setPhoto(dto.getPhoto());
//			}
			entityToSave.setPhoto(dto.getPhoto());
			if (dto.getIsLocalResident() != null) {
				entityToSave.setIsLocalResident(dto.getIsLocalResident());
			}
			if (Utilities.isNotBlank(dto.getNomConjoint()) && !dto.getNomConjoint().equals(entityToSave.getNomConjoint())) {
				entityToSave.setNomConjoint(dto.getNomConjoint());
			}
			if (dto.getDateVieConjugale() != null) {
				entityToSave.setDateVieConjugale(dateFormat.parse(dto.getDateVieConjugale()));
			}
			if (dto.getIsHomeRespo() != null) {
				entityToSave.setIsHomeRespo(dto.getIsHomeRespo());
			}
			if (Utilities.isNotBlank(dto.getHomeRespoName()) && !dto.getHomeRespoName().equals(entityToSave.getHomeRespoName())) {
				entityToSave.setHomeRespoName(dto.getHomeRespoName());
			}
			if (dto.getDateArriveUeeso() != null) {
				entityToSave.setDateArriveUeeso(dateFormat.parse(dto.getDateArriveUeeso()));
			}
			if (dto.getDateConversion() != null) {
				entityToSave.setDateConversion(dateFormat.parse(dto.getDateConversion()));
			}
			if (dto.getBaptemeImmersion() != null) {
				entityToSave.setBaptemeImmersion(dto.getBaptemeImmersion());
			}
			if (dto.getDateBapteme() != null) {
				entityToSave.setDateBapteme(dateFormat.parse(dto.getDateBapteme()));
			}
			if (Utilities.isNotBlank(dto.getEgliseBapteme()) && !dto.getEgliseBapteme().equals(entityToSave.getEgliseBapteme())) {
				entityToSave.setEgliseBapteme(dto.getEgliseBapteme());
			}
			if (Utilities.isNotBlank(dto.getEgliseOrigine()) && !dto.getEgliseOrigine().equals(entityToSave.getEgliseOrigine())) {
				entityToSave.setEgliseOrigine(dto.getEgliseOrigine());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}

			if (dto.getLocked() != null) {
				entityToSave.setLocked(dto.getLocked());
			}

			// validation de l'utilisateur
			if (dto.getIsValidated() != null) {
				entityToSave.setIsValidated(dto.getIsValidated());
			}
			
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);


			if(Utilities.notBlank(entityToSave.getEmail())) {
				//mailBusiness.notifyAccountCreation(entityToSave, pwd, locale);
			}  

			if(dataChanged) {
				oldItems.add(oldEntityToSaveDto);
			}

		}

		if (!items.isEmpty()) {
			List<User> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = userRepository.saveAll((Iterable<User>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("user", locale));
				response.setHasError(true);
				return response;
			}
			List<UserDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading()))
					? UserTransformer.INSTANCE.toLiteDtos(itemsSaved)
							: UserTransformer.INSTANCE.toDtos(itemsSaved);

					final int size = itemsSaved.size();
					List<String> listOfError = Collections.synchronizedList(new ArrayList<String>());
					itemsDto.parallelStream().forEach(dto -> {
						try {
							dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
						} catch (Exception e) {
							listOfError.add(e.getMessage());
							e.printStackTrace();
						}
					});
					if (Utilities.isNotEmpty(listOfError)) {
						Object[] objArray = listOfError.stream().distinct().toArray();
						throw new RuntimeException(StringUtils.join(objArray, ", "));
					}

					//Notification changement d'infos d'un compte
					List<User> newItems = itemsSaved.stream().filter(a->oldItems.stream().anyMatch(old->old.getId().equals(a.getId()) && Utilities.isTrue(a.getIsValidated()))).collect(Collectors.toList());
					if(Utilities.isNotEmpty(newItems)) {
						mailBusiness.notifyUserInfosChangement(oldItems, newItems, locale);
					}

					response.setItems(itemsDto);
					response.setHasError(false);
					response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update User-----");
		return response;
	}

	/**
	 * delete User by using UserDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> delete(Request<UserDto> request, Locale locale) {
		log.info("----begin delete User-----");

		Response<UserDto> response = new Response<UserDto>();
		List<User> items = new ArrayList<User>();

		for (UserDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la user existe
			User existingEntity = null;
			existingEntity = userRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// engagement
			List<Engagement> listOfEngagement = engagementRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfEngagement != null && !listOfEngagement.isEmpty()) {
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfEngagement.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// tresorier
			List<Tresorier> listOfTresorier = tresorierRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfTresorier != null && !listOfTresorier.isEmpty()) {
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfTresorier.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// userGroupeConstitue
			List<UserGroupeConstitue> listOfUserGroupeConstitue = userGroupeConstitueRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfUserGroupeConstitue != null && !listOfUserGroupeConstitue.isEmpty()) {
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUserGroupeConstitue.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// userMission
			List<UserMission> listOfUserMission = userMissionRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfUserMission != null && !listOfUserMission.isEmpty()) {
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUserMission.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()) {
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAdresse.size() + ")", locale));
				response.setHasError(true);
				return response;
			}

			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userRepository.saveAll((Iterable<User>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete User-----");
		return response;
	}

	/**
	 * forceDelete User by using UserDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> forceDelete(Request<UserDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete User-----");

		Response<UserDto> response = new Response<UserDto>();
		List<User> items = new ArrayList<User>();

		for (UserDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la user existe
			User existingEntity = null;
			existingEntity = userRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// engagement
			List<Engagement> listOfEngagement = engagementRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfEngagement != null && !listOfEngagement.isEmpty()) {
				Request<EngagementDto> deleteRequest = new Request<EngagementDto>();
				deleteRequest.setDatas(EngagementTransformer.INSTANCE.toDtos(listOfEngagement));
				deleteRequest.setUser(request.getUser());
				Response<EngagementDto> deleteResponse = engagementBusiness.delete(deleteRequest, locale);
				if (deleteResponse.isHasError()) {
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// tresorier
			List<Tresorier> listOfTresorier = tresorierRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfTresorier != null && !listOfTresorier.isEmpty()) {
				Request<TresorierDto> deleteRequest = new Request<TresorierDto>();
				deleteRequest.setDatas(TresorierTransformer.INSTANCE.toDtos(listOfTresorier));
				deleteRequest.setUser(request.getUser());
				Response<TresorierDto> deleteResponse = tresorierBusiness.delete(deleteRequest, locale);
				if (deleteResponse.isHasError()) {
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// userGroupeConstitue
			List<UserGroupeConstitue> listOfUserGroupeConstitue = userGroupeConstitueRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfUserGroupeConstitue != null && !listOfUserGroupeConstitue.isEmpty()) {
				Request<UserGroupeConstitueDto> deleteRequest = new Request<UserGroupeConstitueDto>();
				deleteRequest.setDatas(UserGroupeConstitueTransformer.INSTANCE.toDtos(listOfUserGroupeConstitue));
				deleteRequest.setUser(request.getUser());
				Response<UserGroupeConstitueDto> deleteResponse = userGroupeConstitueBusiness.delete(deleteRequest, locale);
				if (deleteResponse.isHasError()) {
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// userMission
			List<UserMission> listOfUserMission = userMissionRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfUserMission != null && !listOfUserMission.isEmpty()) {
				Request<UserMissionDto> deleteRequest = new Request<UserMissionDto>();
				deleteRequest.setDatas(UserMissionTransformer.INSTANCE.toDtos(listOfUserMission));
				deleteRequest.setUser(request.getUser());
				Response<UserMissionDto> deleteResponse = userMissionBusiness.delete(deleteRequest, locale);
				if (deleteResponse.isHasError()) {
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdUser(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()) {
				Request<AdresseDto> deleteRequest = new Request<AdresseDto>();
				deleteRequest.setDatas(AdresseTransformer.INSTANCE.toDtos(listOfAdresse));
				deleteRequest.setUser(request.getUser());
				Response<AdresseDto> deleteResponse = adresseBusiness.delete(deleteRequest, locale);
				if (deleteResponse.isHasError()) {
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}

			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userRepository.saveAll((Iterable<User>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete User-----");
		return response;
	}

	/**
	 * get User by using UserDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> getByCriteria(Request<UserDto> request, Locale locale) throws Exception {
		log.info("----begin get User-----");

		Response<UserDto> response = new Response<UserDto>();

		if (Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if (Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<User> items = userRepository.getByCriteria(request, em, locale);

		if (Utilities.isEmpty(items)) {
			response.setStatus(functionalError.DATA_EMPTY("user", locale));
			response.setHasError(false);
			return response;
		}

		List<UserDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserTransformer.INSTANCE.toLiteDtos(items) : UserTransformer.INSTANCE.toDtos(items);

				final int size = items.size();
				List<String> listOfError = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}

				response.setItems(itemsDto);
				response.setCount(userRepository.count(request, em, locale));
				response.setHasError(false);
				response.setStatus(functionalError.SUCCESS("", locale));

				log.info("----end get User-----");
				return response;
	}

	/**
	 * get full UserDto by using User as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private UserDto getFullInfos(UserDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		/*
		//asssociation images
		if(new File(dto.getPhoto()).exists()){
			String imgBase64 = Utilities.convertFileToBase64(dto.getPhoto());
			String imgExt = Utilities.getImageExtension(dto.getPhoto());
			String base64Ext = String.format("data:image/%1$s;base64,", imgExt);
			if (!imgBase64.startsWith(base64Ext)) {
				imgBase64 = base64Ext + imgBase64;
			}
			dto.setFileBase64(imgBase64);
		}
		*/
		
		// assocation de l'adresse
		List<Adresse> adresses= adresseRepository.findByIdUser(dto.getId(), false);
		if (Utilities.isNotEmpty(adresses)) {
			dto.setDatasResidenceAdresse(AdresseTransformer.INSTANCE.toDtos(adresses));	
		}
		
		//Les groupes constitués
		List<UserGroupeConstitue> datasUserGroupeConstitue = userGroupeConstitueRepository.findByIdUser(dto.getId(), false);
		if(Utilities.isNotEmpty(datasUserGroupeConstitue)) {
			List<UserGroupeConstitueDto> datasUserGroupeConstitueDto = new ArrayList<UserGroupeConstitueDto>();
			for (UserGroupeConstitue userGroupeConstitue : datasUserGroupeConstitue) {
				datasUserGroupeConstitueDto.add(userGroupeConstitueBusiness.getFullInfos(userGroupeConstitue, 1, isSimpleLoading, locale));
			}
			dto.setDatasUserGroupeConstitue(datasUserGroupeConstitueDto);
		}
		
		//Les missions
		List<UserMission> datasUserMission = userMissionRepository.findByIdUser(dto.getId(), false);
		if(Utilities.isNotEmpty(datasUserMission)) {
			List<UserMissionDto> datasUserMissionDto = new ArrayList<UserMissionDto>();
			for (UserMission userMission : datasUserMission) {
				datasUserMissionDto.add(userMissionBusiness.getFullInfos(userMission, 1, isSimpleLoading, locale));
			}
			dto.setDatasUserMission(datasUserMissionDto);
		}
		
		dto.setPassword(null);
		return dto;
	}


	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> validateRegistration(Request<UserDto> request, Locale locale) throws Exception {
		log.info("----begin validateRegistration User-----");

		Response<UserDto> response = new Response<UserDto>();
		List<User> items = new ArrayList<User>();

		for (UserDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la user existe
			User existingEntity = userRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			if(existingEntity.getIsValidated() != null) {
				//Alors validation déjà faite (Validé ou rejeté)
				continue;
			}

			//générer des credentials
			String pwd = Utilities.generateAlphanumericCode(8);
			String login = null;
			if(Utilities.isNotBlank(existingEntity.getEmail())) {
				login = existingEntity.getEmail();
			}
			else if(Utilities.isNotBlank(existingEntity.getTelephone())) {
				login = existingEntity.getTelephone();
			}
			else {
				String [] firstNameTokens = existingEntity.getNom().split(" ");
				String [] lastNameTokens = existingEntity.getPrenom().split(" ");
				login = Utilities.normalizeFileName(lastNameTokens[lastNameTokens.length - 1]+"."+firstNameTokens[firstNameTokens.length - 1]);
				List<User> datas = userRepository.findByPreGeneratedLogin(login, false);
				if(Utilities.isNotEmpty(datas)) {
					String preGeneratedLogin = login;
					int num = datas.size() + 1;

					User existingDbLogin = null;
					User existingItemsLogin = null;
					do {
						login = preGeneratedLogin+num+"";
						String final_login = login;
						existingDbLogin = userRepository.findByLogin(login, false);
						existingItemsLogin = items.stream().filter(e->e.getLogin().equals(final_login)).findFirst().orElseGet(null);
						num ++;
					}
					while(existingDbLogin != null || existingItemsLogin != null) ;
					existingEntity.setPreGeneratedLogin(preGeneratedLogin);
				}
			}
			existingEntity.setLogin(login);
			existingEntity.setPassword(Utilities.encrypt(pwd));

			existingEntity.setIsValidated(true);
			if(Utilities.notBlank(existingEntity.getEmail())) {
				mailBusiness.notifyAccountCreation(existingEntity, pwd, locale);
			}  

			existingEntity.setUpdatedAt(Utilities.getCurrentDate());
			existingEntity.setUpdatedBy(request.getUser());
			existingEntity.setIsDeleted(false);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userRepository.saveAll((Iterable<User>) items);
			
			List<UserDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserTransformer.INSTANCE.toLiteDtos(items) : UserTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String> listOfError = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}
			
			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end validateRegistration User-----");
		return response;
	}


	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> connexion(Request<UserDto> request, Locale locale) {
		response = new Response<>();

		try {
			log.info("----begin connexion Utilisateur-----");

			List<UserDto> itemsDto = new ArrayList<>();
			UserDto dto = request.getData();
			if (dto != null) {
				// champs obligatoires
				HashMap<String, Object> fieldsToVerify = new HashMap<>();
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());

				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				User userToConnect = null;
				// test login et password concordants
				userToConnect= userRepository.findByLoginAndPassword(dto.getLogin(), Utilities.encrypt(dto.getPassword()), false);
				if (userToConnect == null) {
					response.setStatus(functionalError.LOGIN_FAIL(locale));
					response.setHasError(true);
					return response;
				} else if (userToConnect.getLocked().equals(Boolean.TRUE)) {
					response.setStatus(functionalError.DATA_NOT_EXIST(
							"votre compte a été verrouillé, contactez l'administrateur.----ACCES REFUSE----", locale));
					response.setHasError(true);
					return response;

				}
				// if (userToConnect.getIsDefaultPassword().equals(Boolean.TRUE))
				// userToConnect.setIsDefaultPassword(Boolean.FALSE);

				if (userToConnect != null)
					itemsDto.add(UserTransformer.INSTANCE.toDto(userToConnect));
				UserDto userDto= getFullInfos(UserTransformer.INSTANCE.toDto(userToConnect), 1, false, locale);

				userToConnect.setIsConnected(true);
				userToConnect.setUpdatedAt(Utilities.getCurrentDate());				
				User user= userRepository.save(userToConnect);

				// save token in redis
				/*String token = Utilities.generateCodeOld();
				compteClientDto.setToken(token);
				compteClientDto.setKey(request.getKey());
				compteClientDto.setPassword(null);
				redisUser.save(token, userDto, true);
				redisUser.save(SunshineUtils.VAR+token, userDto, true);*/
				// afficher les infos du user

				List<String> fonctionnalities = new ArrayList<String>();

				if(userDto.getIdRole()!=null && userDto.getIdRole()>0) {
					List<Functionality> list= roleFunctionalityRepository.findFunctionalityByRoleId(userToConnect.getRole().getId(), false);
					if(Utilities.isNotEmpty(list)) {
						fonctionnalities.addAll(list.stream().map(Functionality::getCode).collect(Collectors.toList()));
					}
				}
				userDto.setFonctionnalities(fonctionnalities);
				response.setItems(Arrays.asList(userDto));
				response.setStatus(functionalError.SUCCESS("Utilisateur connecté", locale));
				response.setHasError(false);
			}
			log.info("----end connexion Utilisateur-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
						response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}

		return response;
	}

	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response<UserDto> deconnexion(Request<UserDto> request, Locale locale) {
		response = new Response<>();

		try {
			log.info("----begin connexion Utilisateur-----");

			//List<UserDto> itemsDto = new ArrayList<>();
			UserDto dto = request.getData();
			if (dto != null) {
				// champs obligatoires
				HashMap<String, Object> fieldsToVerify = new HashMap<>();
				fieldsToVerify.put("id", dto.getId());

				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				User userToDisConnect = null;
				// test login et password concordants
				userToDisConnect = userRepository.findOne(dto.getId(), false);
				if (userToDisConnect == null) {
					response.setStatus(functionalError.DATA_EXIST("Utilisateur 'n°"+dto.getId()+"' inexistant en base de donnée !",locale));
					response.setHasError(true);
					return response;
				}
				userToDisConnect.setIsConnected(false);
				userToDisConnect.setUpdatedAt(Utilities.getCurrentDate());
				User user= userRepository.save(userToDisConnect);

				if (user == null) { // le save ne s'est pas bien passé
					response.setHasError(true);
					response.setStatus(technicalError.INTERN_ERROR("une erreur est survenue lors de la déconnexion de l'utilisateur",locale));
					return response;
				}
				response.setStatus(functionalError.SUCCESS("Utilisateur deconnecté avec succès", locale));
				response.setHasError(false);
			}
			log.info("----end connexion Utilisateur-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
						response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}

		return response;
	}
	
	public Response<UserDto> exportUsers(Request<UserDto> request, Locale locale) {
		log.info("----begin exportUsers-----");
		Response<UserDto> response = new Response<UserDto>();
		try {

			Date date = new Date();
			UserDto dto = request.getData();
			
			request.setIndex(0);
			//request.setSize(EsConfig.max_scrool);
			request.setData(dto);
			
			Response<UserDto> userResponse = getByCriteria(request, locale);
			if(userResponse.isHasError()) {
				response.setStatus(userResponse.getStatus());
				response.setHasError(true);
				return response;
			}
			
			List<UserDto> items = userResponse.getItems();
			if(Utilities.isEmpty(items)){
				response.setStatus(functionalError.DATA_NOT_FOUND("Aucun utilisateur trouvé.", locale));
				response.setHasError(true);
				return response;
			}

			ClassPathResource templateClassPathResource = new ClassPathResource("templates/export/xlsx/users.xlsx");			
			InputStream inputstream = templateClassPathResource.getInputStream();
			XSSFWorkbook workbook = new XSSFWorkbook(inputstream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			//workbook.setSheetName(0, sheet.getSheetName()+" "+dateFormat_dMY_Hms.format(date));
			XSSFSheet firstSheet = sheet;

			XSSFRow row = null;
			XSSFCell cell = null;
			XSSFCellStyle criteriaNameCellStyle = PoiUtils.getCell(PoiUtils.getRow(firstSheet, 0), PoiUtils.getColumnIndex("AL")).getCellStyle();
			XSSFCellStyle criteriaValueCellStyle = PoiUtils.getCell(PoiUtils.getRow(firstSheet, 0), PoiUtils.getColumnIndex("AM")).getCellStyle();
			//XSSFCellStyle dataCellStyle = PoiUtils.getCell(PoiUtils.getRow(firstSheet, 0), PoiUtils.getColumnIndex("AN")).getCellStyle();

			UserDto filter = request.getData() != null ? request.getData() : new UserDto();

			//Renseigner les criteres d'exportaion choisis
			
			ExportCriteriaLevel currentLevel = new ExportCriteriaLevel(7, PoiUtils.getColumnIndex("D"), 6, 4);

			ExportCriteriaLevel.incrementLevel(currentLevel);
			row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
			cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
			cell.setCellValue("date d'extraction:");
			cell.setCellStyle(criteriaNameCellStyle);
			
			cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
			cell.setCellValue(dateFormat.format(date));
			cell.setCellStyle(criteriaValueCellStyle);
			
			if(Utilities.isNotBlank(filter.getCivilite()) || Utilities.searchParamIsNotEmpty(filter.getCiviliteParam(), filter.getCivilite())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Civilite:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getCivilite(), filter.getCiviliteParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getNom()) || Utilities.searchParamIsNotEmpty(filter.getNomParam(), filter.getNom())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Nom:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getNom(), filter.getNomParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getPrenom()) || Utilities.searchParamIsNotEmpty(filter.getPrenomParam(), filter.getPrenom())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Prenom:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getPrenom(), filter.getPrenomParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getDateNaissance()) || Utilities.searchParamIsNotEmpty(filter.getDateNaissanceParam(), filter.getDateNaissance())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Date de naissance:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getDateNaissance(), filter.getDateNaissanceParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getLieuNaissance()) || Utilities.searchParamIsNotEmpty(filter.getLieuNaissanceParam(), filter.getLieuNaissance())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Lieu de naissance:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getLieuNaissance(), filter.getLieuNaissanceParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getPaysLibelle()) || Utilities.searchParamIsNotEmpty(filter.getPaysLibelleParam(), filter.getPaysLibelle())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Pays d'origine:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getPaysLibelle(), filter.getPaysLibelleParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(filter.getIsHomeRespo() != null || Utilities.searchParamIsNotEmpty(filter.getIsHomeRespoParam(), filter.getIsHomeRespo())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Responsable de maison:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getIsHomeRespo(), filter.getIsHomeRespoParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getSituationMatrimonialeLibelle()) || Utilities.searchParamIsNotEmpty(filter.getSituationMatrimonialeLibelleParam(), filter.getSituationMatrimonialeLibelle())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Situation Matrimoniale:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getSituationMatrimonialeLibelle(), filter.getSituationMatrimonialeLibelleParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getSituationProfessionnelleLibelle()) || Utilities.searchParamIsNotEmpty(filter.getSituationProfessionnelleLibelleParam(), filter.getSituationProfessionnelleLibelle())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Situation Professionnelle:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getSituationProfessionnelleLibelle(), filter.getSituationProfessionnelleLibelleParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getProfessionLibelle()) || Utilities.searchParamIsNotEmpty(filter.getProfessionLibelleParam(), filter.getProfessionLibelle())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Profession:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getProfessionLibelle(), filter.getProfessionLibelleParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getDomaineCompetence()) || Utilities.searchParamIsNotEmpty(filter.getDomaineCompetenceParam(), filter.getDomaineCompetence())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Autres compétences:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getDomaineCompetence(), filter.getDomaineCompetenceParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(filter.getIsLocalResident() != null || Utilities.searchParamIsNotEmpty(filter.getIsLocalResidentParam(), filter.getIsLocalResident())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("De la diapora:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getIsLocalResident(), filter.getIsLocalResidentParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getEmail()) || Utilities.searchParamIsNotEmpty(filter.getEmailParam(), filter.getEmail())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Email:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getEmail(), filter.getEmailParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getTelephone()) || Utilities.searchParamIsNotEmpty(filter.getTelephoneParam(), filter.getTelephone())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Téléphone:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getTelephone(), filter.getTelephoneParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(filter.getBaptemeImmersion() != null || Utilities.searchParamIsNotEmpty(filter.getBaptemeImmersionParam(), filter.getBaptemeImmersion())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Baptisé par immersion:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getBaptemeImmersion(), filter.getBaptemeImmersionParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getDateBapteme()) || Utilities.searchParamIsNotEmpty(filter.getDateBaptemeParam(), filter.getDateBapteme())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Date de baptême:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getDateBapteme(), filter.getDateBaptemeParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getEgliseBapteme()) || Utilities.searchParamIsNotEmpty(filter.getEgliseBaptemeParam(), filter.getEgliseBapteme())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Eglise de baptême:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getEgliseBapteme(), filter.getEgliseBaptemeParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getDateConversion()) || Utilities.searchParamIsNotEmpty(filter.getDateConversionParam(), filter.getDateConversion())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Date de conversion:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getDateConversion(), filter.getDateConversionParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getEgliseOrigine()) || Utilities.searchParamIsNotEmpty(filter.getEgliseOrigineParam(), filter.getEgliseOrigine())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Eglise de conversion:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getEgliseOrigine(), filter.getEgliseOrigineParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getTypeUserLibelle()) || Utilities.searchParamIsNotEmpty(filter.getTypeUserLibelleParam(), filter.getTypeUserLibelle())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Type de membre:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getTypeUserLibelle(), filter.getTypeUserLibelleParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			if(Utilities.isNotBlank(filter.getDateArriveUeeso()) || Utilities.searchParamIsNotEmpty(filter.getDateArriveUeesoParam(), filter.getDateArriveUeeso())) {
				ExportCriteriaLevel.incrementLevel(currentLevel);
				row = PoiUtils.getRow(sheet, currentLevel.getCriteriaRowIndex());
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex());
				cell.setCellValue("Date d'arrivée à l'UESSO:");
				cell.setCellStyle(criteriaNameCellStyle);
				
				cell = PoiUtils.getCell(row, currentLevel.getCriteriaColIndex() + 2);
				cell.setCellValue(Utilities.getSearchCriteriaAsString(filter.getDateArriveUeeso(), filter.getDateArriveUeesoParam()));
				cell.setCellStyle(criteriaValueCellStyle);
			}
			
			// Création du fichier

			int firstRowIndex = 16;
			
			int rowIndex = firstRowIndex;
			int numero = 1;
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sheetName = "Export des fildèles du "+sdf.format(date);
			workbook.setSheetName(0, sheetName); 
			
			List<List<Integer>> folders = new ArrayList<List<Integer>>();
			
			int [] colrangeToMerge_1 = IntStream.rangeClosed(PoiUtils.getColumnIndex("A"), PoiUtils.getColumnIndex("M")).toArray();
			int [] colrangeToMerge_2 = IntStream.rangeClosed(PoiUtils.getColumnIndex("T"), PoiUtils.getColumnIndex("AB")).toArray();
			
			int [] colrangeToNotMerge_1 = IntStream.rangeClosed(PoiUtils.getColumnIndex("N"), PoiUtils.getColumnIndex("S")).toArray();
			//int [] colrangeToNotMerge_2 = IntStream.rangeClosed(PoiUtils.getColumnIndex("AC"), PoiUtils.getColumnIndex("AD")).toArray();

			List<Integer> colrangeToMerge = new ArrayList<Integer>();
			for (int col : colrangeToMerge_1) {
				colrangeToMerge.add(col);
			}
			for (int col : colrangeToMerge_2) {
				colrangeToMerge.add(col);
			}
			
			for (UserDto user : items) {
				List<AdresseDto> datasAdresse = user.getDatasResidenceAdresse();
				List<UserGroupeConstitueDto> datasUserGroupeConstitue = user.getDatasUserGroupeConstitue();
				int maxLine = Math.max(datasAdresse != null ? datasAdresse.size() : 0, datasUserGroupeConstitue != null ? datasUserGroupeConstitue.size() : 0);
				
				int lastUserRowIndex = rowIndex;
				if(maxLine > 1) {
					lastUserRowIndex = rowIndex - 1 + maxLine;
				}
				
				if(lastUserRowIndex != rowIndex) {
					for (Integer colIndex : colrangeToMerge) {
						PoiUtils.setMerge(sheet, rowIndex, lastUserRowIndex, colIndex, colIndex, null, (short)-1);
					}
					folders.add(Arrays.asList(rowIndex, lastUserRowIndex));
				}
				
				row = PoiUtils.getRow(sheet, rowIndex);
				int colIndex = PoiUtils.getColumnIndex("A");

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(numero++);

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getCivilite()); //Mettre un select

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getNom());
				
				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getPrenom());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getDateNaissance());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getLieuNaissance());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getPaysLibelle()); ////Mettre un select

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(Utilities.isTrue(user.getIsHomeRespo()) ? "OUI" : "NON");

				cell = PoiUtils.getCell(row, colIndex++); 
				cell.setCellValue(user.getSituationMatrimonialeLibelle()); //Mettre un select

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getSituationProfessionnelleLibelle()); //Mettre un select

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getProfessionLibelle()); //Mettre un select

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getDomaineCompetence());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(Utilities.isTrue(user.getIsLocalResident()) ? "NON" : "OUI");
				
				if(Utilities.isNotEmpty(datasAdresse)) {
					for (AdresseDto adresse : datasAdresse) {
						int col = colIndex;
						
						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(adresse.getPaysLibelle()); //Mettre un select
						
						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(""); //Mettre un select si données disponible

						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(adresse.getVilleLibelle()); //Mettre un select si données disponible

						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(adresse.getCommuneLibelle()); //Mettre un select si données disponible

						cell = PoiUtils.getCell(row, col++); //Mettre un select si données disponible
						cell.setCellValue(adresse.getQuartierLibelle());

						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(adresse.getAdressePostale());
					}
				}
				colIndex += colrangeToNotMerge_1.length;
				
				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getEmail());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getTelephone());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(Utilities.isTrue(user.getBaptemeImmersion()) ? "OUI" : "NON");

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getDateBapteme());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getEgliseBapteme());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getDateConversion());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getEgliseOrigine());

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getTypeUserLibelle());  //Mettre un select

				cell = PoiUtils.getCell(row, colIndex++);
				cell.setCellValue(user.getDateArriveUeeso());

				if(Utilities.isNotEmpty(datasUserGroupeConstitue)) {
					for (UserGroupeConstitueDto userGroupeConstitueDto : datasUserGroupeConstitue) {
						int col = colIndex;
						
						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(userGroupeConstitueDto.getGroupeConstitueLibelle()); //Mettre un select

						cell = PoiUtils.getCell(row, col++);
						cell.setCellValue(userGroupeConstitueDto.getFonctionLibelle()); //Mettre un select si données disponible
					}
				}
				colIndex += colrangeToNotMerge_1.length;

				rowIndex = lastUserRowIndex + 1;
			}
  			
			//Foldering
			
			for (List<Integer> folder : folders) {
				sheet.groupRow(folder.get(0), folder.get(1));
	            //sheet.setRowGroupCollapsed(firstRow-1, true);
			}
			sheet.setRowSumsBelow(false);
			//Vider les champs de référence
			
			row = PoiUtils.getRow(firstSheet, 0);
			for (int i = PoiUtils.getColumnIndex("AL"); i <= PoiUtils.getColumnIndex("AN"); i++) {
				cell = PoiUtils.getCell(row, i);
				cell.setCellValue((String)null);
				cell.setCellStyle(null);
			}

			DateFormat dateFormatNom = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSS");

			String workDirectory = Utilities.getSuitableFileDirectory("xlsx", paramsUtils);
			Utilities.createDirectory(workDirectory);
			workDirectory = Utilities.addSlash(workDirectory);

			String fileName = "users-"+dateFormatNom.format(date)+".xlsx";
			String filePath = workDirectory + fileName;
			FileOutputStream outFile = new FileOutputStream(new File(filePath));

			workbook.write(outFile);
			outFile.close();
			inputstream.close();

			response.setHasError(Boolean.FALSE);
			response.setFilePath(filePath);

			log.info("response.setFilePath(filePath): " + filePath);

			log.info("----end exportUsers-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info("Erreur| code: "+response.getStatus().getCode()+" -  message: "+response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
}
