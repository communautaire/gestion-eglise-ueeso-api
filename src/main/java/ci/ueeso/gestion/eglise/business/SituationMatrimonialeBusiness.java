

/*
 * Java business for entity table situation_matrimoniale 
 * Created on 2019-11-30 ( Time 18:43:51 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.SituationMatrimoniale;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "situation_matrimoniale"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class SituationMatrimonialeBusiness implements IBasicBusiness<Request<SituationMatrimonialeDto>, Response<SituationMatrimonialeDto>> {

	private Response<SituationMatrimonialeDto> response;
	@Autowired
	private SituationMatrimonialeRepository situationMatrimonialeRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;


	public SituationMatrimonialeBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create SituationMatrimoniale by using SituationMatrimonialeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationMatrimonialeDto> create(Request<SituationMatrimonialeDto> request, Locale locale)  throws ParseException {
		log.info("----begin create SituationMatrimoniale-----");

		Response<SituationMatrimonialeDto> response = new Response<SituationMatrimonialeDto>();
		List<SituationMatrimoniale>        items    = new ArrayList<SituationMatrimoniale>();

		for (SituationMatrimonialeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if situationMatrimoniale to insert do not exist
			SituationMatrimoniale existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("situationMatrimoniale id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// verif unique code in db
			existingEntity = situationMatrimonialeRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("situationMatrimoniale code -> " + dto.getCode(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" code ", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = situationMatrimonialeRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("situationMatrimoniale libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			SituationMatrimoniale entityToSave = SituationMatrimonialeTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<SituationMatrimoniale> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = situationMatrimonialeRepository.saveAll((Iterable<SituationMatrimoniale>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("situationMatrimoniale", locale));
				response.setHasError(true);
				return response;
			}
			List<SituationMatrimonialeDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SituationMatrimonialeTransformer.INSTANCE.toLiteDtos(itemsSaved) : SituationMatrimonialeTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create SituationMatrimoniale-----");
		return response;
	}

	/**
	 * update SituationMatrimoniale by using SituationMatrimonialeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationMatrimonialeDto> update(Request<SituationMatrimonialeDto> request, Locale locale)  throws ParseException {
		log.info("----begin update SituationMatrimoniale-----");

		Response<SituationMatrimonialeDto> response = new Response<SituationMatrimonialeDto>();
		List<SituationMatrimoniale>        items    = new ArrayList<SituationMatrimoniale>();

		for (SituationMatrimonialeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la situationMatrimoniale existe
			SituationMatrimoniale entityToSave = null;
			entityToSave = situationMatrimonialeRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("situationMatrimoniale id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				SituationMatrimoniale existingEntity = situationMatrimonialeRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("situationMatrimoniale -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les situationMatrimoniales", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				SituationMatrimoniale existingEntity = situationMatrimonialeRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("situationMatrimoniale -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les situationMatrimoniales", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<SituationMatrimoniale> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = situationMatrimonialeRepository.saveAll((Iterable<SituationMatrimoniale>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("situationMatrimoniale", locale));
				response.setHasError(true);
				return response;
			}
			List<SituationMatrimonialeDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SituationMatrimonialeTransformer.INSTANCE.toLiteDtos(itemsSaved) : SituationMatrimonialeTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update SituationMatrimoniale-----");
		return response;
	}

	/**
	 * delete SituationMatrimoniale by using SituationMatrimonialeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationMatrimonialeDto> delete(Request<SituationMatrimonialeDto> request, Locale locale)  {
		log.info("----begin delete SituationMatrimoniale-----");

		Response<SituationMatrimonialeDto> response = new Response<SituationMatrimonialeDto>();
		List<SituationMatrimoniale>        items    = new ArrayList<SituationMatrimoniale>();

		for (SituationMatrimonialeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la situationMatrimoniale existe
			SituationMatrimoniale existingEntity = null;
			existingEntity = situationMatrimonialeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("situationMatrimoniale -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// user
			List<User> listOfUser = userRepository.findByIdSituationMatrimoniale(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			situationMatrimonialeRepository.saveAll((Iterable<SituationMatrimoniale>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete SituationMatrimoniale-----");
		return response;
	}

	/**
	 * forceDelete SituationMatrimoniale by using SituationMatrimonialeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationMatrimonialeDto> forceDelete(Request<SituationMatrimonialeDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete SituationMatrimoniale-----");

		Response<SituationMatrimonialeDto> response = new Response<SituationMatrimonialeDto>();
		List<SituationMatrimoniale>        items    = new ArrayList<SituationMatrimoniale>();

		for (SituationMatrimonialeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la situationMatrimoniale existe
			SituationMatrimoniale existingEntity = null;
			existingEntity = situationMatrimonialeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("situationMatrimoniale -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// user
			List<User> listOfUser = userRepository.findByIdSituationMatrimoniale(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				Request<UserDto> deleteRequest = new Request<UserDto>();
				deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
				deleteRequest.setUser(request.getUser());
				Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			situationMatrimonialeRepository.saveAll((Iterable<SituationMatrimoniale>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete SituationMatrimoniale-----");
		return response;
	}

	/**
	 * get SituationMatrimoniale by using SituationMatrimonialeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationMatrimonialeDto> getByCriteria(Request<SituationMatrimonialeDto> request, Locale locale)  throws Exception {
		log.info("----begin get SituationMatrimoniale-----");

		Response<SituationMatrimonialeDto> response = new Response<SituationMatrimonialeDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<SituationMatrimoniale> items 			 = situationMatrimonialeRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("situationMatrimoniale", locale));
			response.setHasError(false);
			return response;
		}

		List<SituationMatrimonialeDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SituationMatrimonialeTransformer.INSTANCE.toLiteDtos(items) : SituationMatrimonialeTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(situationMatrimonialeRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get SituationMatrimoniale-----");
		return response;
	}

	/**
	 * get full SituationMatrimonialeDto by using SituationMatrimoniale as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private SituationMatrimonialeDto getFullInfos(SituationMatrimonialeDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
