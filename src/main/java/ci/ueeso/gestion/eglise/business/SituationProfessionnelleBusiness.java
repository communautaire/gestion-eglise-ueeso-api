

/*
 * Java business for entity table situation_professionnelle 
 * Created on 2019-11-30 ( Time 18:43:52 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.SituationProfessionnelle;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "situation_professionnelle"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class SituationProfessionnelleBusiness implements IBasicBusiness<Request<SituationProfessionnelleDto>, Response<SituationProfessionnelleDto>> {

	private Response<SituationProfessionnelleDto> response;
	@Autowired
	private SituationProfessionnelleRepository situationProfessionnelleRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;


	public SituationProfessionnelleBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create SituationProfessionnelle by using SituationProfessionnelleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationProfessionnelleDto> create(Request<SituationProfessionnelleDto> request, Locale locale)  throws ParseException {
		log.info("----begin create SituationProfessionnelle-----");

		Response<SituationProfessionnelleDto> response = new Response<SituationProfessionnelleDto>();
		List<SituationProfessionnelle>        items    = new ArrayList<SituationProfessionnelle>();

		for (SituationProfessionnelleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if situationProfessionnelle to insert do not exist
			SituationProfessionnelle existingEntity = situationProfessionnelleRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("situationProfessionnelle code -> " + dto.getCode(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" code ", locale));
				response.setHasError(true);
				return response;
			}
			
			existingEntity = situationProfessionnelleRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("situationProfessionnelle libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("libelle", locale));
				response.setHasError(true);
				return response;
			}

			SituationProfessionnelle entityToSave = SituationProfessionnelleTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<SituationProfessionnelle> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = situationProfessionnelleRepository.saveAll((Iterable<SituationProfessionnelle>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("situationProfessionnelle", locale));
				response.setHasError(true);
				return response;
			}
			List<SituationProfessionnelleDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SituationProfessionnelleTransformer.INSTANCE.toLiteDtos(itemsSaved) : SituationProfessionnelleTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}
		log.info("----end create SituationProfessionnelle-----");
		return response;
	}

	/**
	 * update SituationProfessionnelle by using SituationProfessionnelleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationProfessionnelleDto> update(Request<SituationProfessionnelleDto> request, Locale locale)  throws ParseException {
		log.info("----begin update SituationProfessionnelle-----");

		Response<SituationProfessionnelleDto> response = new Response<SituationProfessionnelleDto>();
		List<SituationProfessionnelle>        items    = new ArrayList<SituationProfessionnelle>();

		for (SituationProfessionnelleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la situationProfessionnelle existe
			SituationProfessionnelle entityToSave = null;
			entityToSave = situationProfessionnelleRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("situationProfessionnelle id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				SituationProfessionnelle existingEntity = situationProfessionnelleRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("situationProfessionnelle -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les situationProfessionnelles", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				SituationProfessionnelle existingEntity = situationProfessionnelleRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("situationProfessionnelle -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les situationProfessionnelles", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<SituationProfessionnelle> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = situationProfessionnelleRepository.saveAll((Iterable<SituationProfessionnelle>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("situationProfessionnelle", locale));
				response.setHasError(true);
				return response;
			}
			List<SituationProfessionnelleDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SituationProfessionnelleTransformer.INSTANCE.toLiteDtos(itemsSaved) : SituationProfessionnelleTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update SituationProfessionnelle-----");
		return response;
	}

	/**
	 * delete SituationProfessionnelle by using SituationProfessionnelleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationProfessionnelleDto> delete(Request<SituationProfessionnelleDto> request, Locale locale)  {
		log.info("----begin delete SituationProfessionnelle-----");

		Response<SituationProfessionnelleDto> response = new Response<SituationProfessionnelleDto>();
		List<SituationProfessionnelle>        items    = new ArrayList<SituationProfessionnelle>();

		for (SituationProfessionnelleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la situationProfessionnelle existe
			SituationProfessionnelle existingEntity = null;
			existingEntity = situationProfessionnelleRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("situationProfessionnelle -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// user
			List<User> listOfUser = userRepository.findByIdSituationProfessionnelle(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("Cette situaton professionnelle est celle " + listOfUser.size() + " utilisateur(s) de l'application. Vous ne devriez pas la supprimer.", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			situationProfessionnelleRepository.saveAll((Iterable<SituationProfessionnelle>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete SituationProfessionnelle-----");
		return response;
	}

	/**
	 * forceDelete SituationProfessionnelle by using SituationProfessionnelleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationProfessionnelleDto> forceDelete(Request<SituationProfessionnelleDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete SituationProfessionnelle-----");

		Response<SituationProfessionnelleDto> response = new Response<SituationProfessionnelleDto>();
		List<SituationProfessionnelle>        items    = new ArrayList<SituationProfessionnelle>();

		for (SituationProfessionnelleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la situationProfessionnelle existe
			SituationProfessionnelle existingEntity = null;
			existingEntity = situationProfessionnelleRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("situationProfessionnelle -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// user
			List<User> listOfUser = userRepository.findByIdSituationProfessionnelle(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				Request<UserDto> deleteRequest = new Request<UserDto>();
				deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
				deleteRequest.setUser(request.getUser());
				Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			situationProfessionnelleRepository.saveAll((Iterable<SituationProfessionnelle>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete SituationProfessionnelle-----");
		return response;
	}

	/**
	 * get SituationProfessionnelle by using SituationProfessionnelleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SituationProfessionnelleDto> getByCriteria(Request<SituationProfessionnelleDto> request, Locale locale)  throws Exception {
		log.info("----begin get SituationProfessionnelle-----");

		Response<SituationProfessionnelleDto> response = new Response<SituationProfessionnelleDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<SituationProfessionnelle> items = situationProfessionnelleRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("situationProfessionnelle", locale));
			response.setHasError(false);
			return response;
		}

		List<SituationProfessionnelleDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SituationProfessionnelleTransformer.INSTANCE.toLiteDtos(items) : SituationProfessionnelleTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(situationProfessionnelleRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get SituationProfessionnelle-----");
		return response;
	}

	/**
	 * get full SituationProfessionnelleDto by using SituationProfessionnelle as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private SituationProfessionnelleDto getFullInfos(SituationProfessionnelleDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
