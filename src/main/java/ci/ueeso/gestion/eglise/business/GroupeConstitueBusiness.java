

/*
 * Java business for entity table groupe_constitue 
 * Created on 2019-11-30 ( Time 18:43:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.GroupeConstitue;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "groupe_constitue"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class GroupeConstitueBusiness implements IBasicBusiness<Request<GroupeConstitueDto>, Response<GroupeConstitueDto>> {

	private Response<GroupeConstitueDto> response;
	@Autowired
	private GroupeConstitueRepository groupeConstitueRepository;
	@Autowired
	private UserGroupeConstitueRepository userGroupeConstitueRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserGroupeConstitueBusiness userGroupeConstitueBusiness;


	@Autowired
	private UserBusiness userBusiness;

	public GroupeConstitueBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create GroupeConstitue by using GroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<GroupeConstitueDto> create(Request<GroupeConstitueDto> request, Locale locale)  throws ParseException {
		log.info("----begin create GroupeConstitue-----");

		Response<GroupeConstitueDto> response = new Response<GroupeConstitueDto>();
		List<GroupeConstitue>        items    = new ArrayList<GroupeConstitue>();

		for (GroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("description", dto.getDescription());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if groupeConstitue to insert do not exist
			GroupeConstitue existingEntity = groupeConstitueRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("Le groupe constitué de code `" + dto.getCode()+"` existe déjà.", locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du groupe constitué de code `"+dto.getCode()+"`.", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = groupeConstitueRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("Le groupe constitué de libelle `" + dto.getLibelle()+"` existe déjà.", locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du groupe constitué de libelle `"+dto.getLibelle()+"`.", locale));
				response.setHasError(true);
				return response;
			}

			GroupeConstitue entityToSave = GroupeConstitueTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<GroupeConstitue> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = groupeConstitueRepository.saveAll((Iterable<GroupeConstitue>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("groupeConstitue", locale));
				response.setHasError(true);
				return response;
			}
			List<GroupeConstitueDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? GroupeConstitueTransformer.INSTANCE.toLiteDtos(itemsSaved) : GroupeConstitueTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}
			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create GroupeConstitue-----");
		return response;
	}

	/**
	 * update GroupeConstitue by using GroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<GroupeConstitueDto> update(Request<GroupeConstitueDto> request, Locale locale)  throws ParseException {
		log.info("----begin update GroupeConstitue-----");

		Response<GroupeConstitueDto> response = new Response<GroupeConstitueDto>();
		List<GroupeConstitue>        items    = new ArrayList<GroupeConstitue>();

		for (GroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la groupeConstitue existe
			GroupeConstitue entityToSave = groupeConstitueRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("groupeConstitue id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				GroupeConstitue existingEntity = groupeConstitueRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("Le groupe constitué de code `" + dto.getCode()+"` existe déjà.", locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du groupe constitué de code `"+dto.getCode()+"`.", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				GroupeConstitue existingEntity = groupeConstitueRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("Le groupe constitué de libelle `" + dto.getLibelle()+"` existe déjà.", locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du groupe constitué de libelle `"+dto.getLibelle()+"`.", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.isNotBlank(dto.getDescription()) && !dto.getDescription().equals(entityToSave.getDescription())) {
				entityToSave.setDescription(dto.getDescription());
			}
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<GroupeConstitue> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = groupeConstitueRepository.saveAll((Iterable<GroupeConstitue>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("groupeConstitue", locale));
				response.setHasError(true);
				return response;
			}
			List<GroupeConstitueDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? GroupeConstitueTransformer.INSTANCE.toLiteDtos(itemsSaved) : GroupeConstitueTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update GroupeConstitue-----");
		return response;
	}

	/**
	 * delete GroupeConstitue by using GroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<GroupeConstitueDto> delete(Request<GroupeConstitueDto> request, Locale locale)  {
		log.info("----begin delete GroupeConstitue-----");

		Response<GroupeConstitueDto> response = new Response<GroupeConstitueDto>();
		List<GroupeConstitue>        items    = new ArrayList<GroupeConstitue>();

		for (GroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la groupeConstitue existe
			GroupeConstitue existingEntity = null;
			existingEntity = groupeConstitueRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("groupeConstitue -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// userGroupeConstitue
			List<UserGroupeConstitue> listOfUserGroupeConstitue = userGroupeConstitueRepository.findByIdGroupeConstitue(existingEntity.getId(), false);
			if (listOfUserGroupeConstitue != null && !listOfUserGroupeConstitue.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUserGroupeConstitue.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			groupeConstitueRepository.saveAll((Iterable<GroupeConstitue>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete GroupeConstitue-----");
		return response;
	}

	/**
	 * forceDelete GroupeConstitue by using GroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<GroupeConstitueDto> forceDelete(Request<GroupeConstitueDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete GroupeConstitue-----");

		Response<GroupeConstitueDto> response = new Response<GroupeConstitueDto>();
		List<GroupeConstitue>        items    = new ArrayList<GroupeConstitue>();

		for (GroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la groupeConstitue existe
			GroupeConstitue existingEntity = null;
			existingEntity = groupeConstitueRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("groupeConstitue -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// userGroupeConstitue
			List<UserGroupeConstitue> listOfUserGroupeConstitue = userGroupeConstitueRepository.findByIdGroupeConstitue(existingEntity.getId(), false);
			if (listOfUserGroupeConstitue != null && !listOfUserGroupeConstitue.isEmpty()){
				Request<UserGroupeConstitueDto> deleteRequest = new Request<UserGroupeConstitueDto>();
				deleteRequest.setDatas(UserGroupeConstitueTransformer.INSTANCE.toDtos(listOfUserGroupeConstitue));
				deleteRequest.setUser(request.getUser());
				Response<UserGroupeConstitueDto> deleteResponse = userGroupeConstitueBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			groupeConstitueRepository.saveAll((Iterable<GroupeConstitue>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete GroupeConstitue-----");
		return response;
	}

	/**
	 * get GroupeConstitue by using GroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<GroupeConstitueDto> getByCriteria(Request<GroupeConstitueDto> request, Locale locale)  throws Exception {
		log.info("----begin get GroupeConstitue-----");

		Response<GroupeConstitueDto> response = new Response<GroupeConstitueDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<GroupeConstitue> items 			 = groupeConstitueRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("groupeConstitue", locale));
			response.setHasError(false);
			return response;
		}

		List<GroupeConstitueDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? GroupeConstitueTransformer.INSTANCE.toLiteDtos(items) : GroupeConstitueTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(groupeConstitueRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get GroupeConstitue-----");
		return response;
	}

	/**
	 * get full GroupeConstitueDto by using GroupeConstitue as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private GroupeConstitueDto getFullInfos(GroupeConstitueDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
