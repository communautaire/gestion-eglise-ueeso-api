

/*
 * Java business for entity table functionality 
 * Created on 2019-11-30 ( Time 18:43:45 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Functionality;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "functionality"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class FunctionalityBusiness implements IBasicBusiness<Request<FunctionalityDto>, Response<FunctionalityDto>> {

	private Response<FunctionalityDto> response;
	@Autowired
	private FunctionalityRepository functionalityRepository;
	@Autowired
	private RoleFunctionalityRepository roleFunctionalityRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private RoleFunctionalityBusiness roleFunctionalityBusiness;


	@Autowired
	private UserBusiness userBusiness;

	public FunctionalityBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Functionality by using FunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<FunctionalityDto> create(Request<FunctionalityDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Functionality-----");

		Response<FunctionalityDto> response = new Response<FunctionalityDto>();
		List<Functionality>        items    = new ArrayList<Functionality>();

		for (FunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			/*fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());*/
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if functionality to insert do not exist
			Functionality existingEntity = null;
			/*if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("functionality id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}*/

			// verif unique code in db
			existingEntity = functionalityRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("functionality code -> " + dto.getCode(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" code ", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = functionalityRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("functionality libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			Functionality entityToSave = FunctionalityTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Functionality> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = functionalityRepository.saveAll((Iterable<Functionality>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("functionality", locale));
				response.setHasError(true);
				return response;
			}
			List<FunctionalityDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? FunctionalityTransformer.INSTANCE.toLiteDtos(itemsSaved) : FunctionalityTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Functionality-----");
		return response;
	}

	/**
	 * update Functionality by using FunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<FunctionalityDto> update(Request<FunctionalityDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Functionality-----");

		Response<FunctionalityDto> response = new Response<FunctionalityDto>();
		List<Functionality>        items    = new ArrayList<Functionality>();

		for (FunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la functionality existe
			Functionality entityToSave = null;
			entityToSave = functionalityRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("functionality id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				Functionality existingEntity = functionalityRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("functionality -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les functionalitys", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				Functionality existingEntity = functionalityRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("functionality -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les functionalitys", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Functionality> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = functionalityRepository.saveAll((Iterable<Functionality>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("functionality", locale));
				response.setHasError(true);
				return response;
			}
			List<FunctionalityDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? FunctionalityTransformer.INSTANCE.toLiteDtos(itemsSaved) : FunctionalityTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Functionality-----");
		return response;
	}

	/**
	 * delete Functionality by using FunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<FunctionalityDto> delete(Request<FunctionalityDto> request, Locale locale)  {
		log.info("----begin delete Functionality-----");

		Response<FunctionalityDto> response = new Response<FunctionalityDto>();
		List<Functionality>        items    = new ArrayList<Functionality>();

		for (FunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la functionality existe
			Functionality existingEntity = null;
			existingEntity = functionalityRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("functionality -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// roleFunctionality
			List<RoleFunctionality> listOfRoleFunctionality = roleFunctionalityRepository.findByIdFonctionality(existingEntity.getId(), false);
			if (listOfRoleFunctionality != null && !listOfRoleFunctionality.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfRoleFunctionality.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			functionalityRepository.saveAll((Iterable<Functionality>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Functionality-----");
		return response;
	}

	/**
	 * forceDelete Functionality by using FunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<FunctionalityDto> forceDelete(Request<FunctionalityDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Functionality-----");

		Response<FunctionalityDto> response = new Response<FunctionalityDto>();
		List<Functionality>        items    = new ArrayList<Functionality>();

		for (FunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la functionality existe
			Functionality existingEntity = null;
			existingEntity = functionalityRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("functionality -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// roleFunctionality
			List<RoleFunctionality> listOfRoleFunctionality = roleFunctionalityRepository.findByIdFonctionality(existingEntity.getId(), false);
			if (listOfRoleFunctionality != null && !listOfRoleFunctionality.isEmpty()){
				Request<RoleFunctionalityDto> deleteRequest = new Request<RoleFunctionalityDto>();
				deleteRequest.setDatas(RoleFunctionalityTransformer.INSTANCE.toDtos(listOfRoleFunctionality));
				deleteRequest.setUser(request.getUser());
				Response<RoleFunctionalityDto> deleteResponse = roleFunctionalityBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			functionalityRepository.saveAll((Iterable<Functionality>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Functionality-----");
		return response;
	}

	/**
	 * get Functionality by using FunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<FunctionalityDto> getByCriteria(Request<FunctionalityDto> request, Locale locale)  throws Exception {
		log.info("----begin get Functionality-----");

		Response<FunctionalityDto> response = new Response<FunctionalityDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Functionality> items 			 = functionalityRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("functionality", locale));
			response.setHasError(false);
			return response;
		}

		List<FunctionalityDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? FunctionalityTransformer.INSTANCE.toLiteDtos(items) : FunctionalityTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(functionalityRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Functionality-----");
		return response;
	}

	/**
	 * get full FunctionalityDto by using Functionality as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private FunctionalityDto getFullInfos(FunctionalityDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
