

/*
 * Java business for entity table mission 
 * Created on 2019-11-30 ( Time 18:43:46 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Mission;
import ci.ueeso.gestion.eglise.dao.entity.UserMission;
import ci.ueeso.gestion.eglise.dao.repository.MissionRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserMissionRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.MissionDto;
import ci.ueeso.gestion.eglise.helper.dto.UserMissionDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.MissionTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserMissionTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "mission"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class MissionBusiness implements IBasicBusiness<Request<MissionDto>, Response<MissionDto>> {

	private Response<MissionDto> response;
	@Autowired
	private MissionRepository missionRepository;
	@Autowired
	private UserMissionRepository userMissionRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserMissionBusiness userMissionBusiness;


	@Autowired
	private UserBusiness userBusiness;

	public MissionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Mission by using MissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<MissionDto> create(Request<MissionDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Mission-----");

		Response<MissionDto> response = new Response<MissionDto>();
		List<Mission>        items    = new ArrayList<Mission>();

		for (MissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			//fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("description", dto.getDescription());
			fieldsToVerify.put("adresse", dto.getAdresse());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if mission to insert do not exist
			Mission existingEntity = missionRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("La mission de libelle '" + dto.getLibelle()+"' existe déjà.", locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de la mission libelle '"+dto.getLibelle()+"' .", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique code in db
			boolean codeFromLibelle = false;
			if(Utilities.isBlank(dto.getCode())) {
				dto.setCode(Utilities.getCodeFromLibelle(dto.getLibelle()));
				codeFromLibelle = true;
			}
			existingEntity = missionRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				String message = "";
				if(codeFromLibelle) {
					message = "Une mission similaire existe déjà: '"+existingEntity.getLibelle()+"'.";
				}
				else {
					message = "La mission code '" + dto.getCode()+"' existe déjà.";
				}
				response.setStatus(functionalError.DATA_EXIST(message, locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				String message = "";
				if(codeFromLibelle) {
					message = "Tentative d'enregistrement de missions quasiment similaire: '"+dto.getLibelle()+"'.";
				}
				else {
					message = "Tentative de duplication de la mission code '" + dto.getCode()+"'.";
				}
				response.setStatus(functionalError.DATA_DUPLICATE(message, locale));
				response.setHasError(true);
				return response;
			}
			
			Mission entityToSave = MissionTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Mission> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = missionRepository.saveAll((Iterable<Mission>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("mission", locale));
				response.setHasError(true);
				return response;
			}
			List<MissionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? MissionTransformer.INSTANCE.toLiteDtos(itemsSaved) : MissionTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Mission-----");
		return response;
	}

	/**
	 * update Mission by using MissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<MissionDto> update(Request<MissionDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Mission-----");

		Response<MissionDto> response = new Response<MissionDto>();
		List<Mission>        items    = new ArrayList<Mission>();

		for (MissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la mission existe
			Mission entityToSave = missionRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("mission id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				Mission existingEntity = missionRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("mission -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les missions", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				Mission existingEntity = missionRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("mission -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les missions", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.isNotBlank(dto.getDescription()) && !dto.getDescription().equals(entityToSave.getDescription())) {
				entityToSave.setDescription(dto.getDescription());
			}
			if (Utilities.isNotBlank(dto.getAdresse()) && !dto.getAdresse().equals(entityToSave.getAdresse())) {
				entityToSave.setAdresse(dto.getAdresse());
			}
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Mission> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = missionRepository.saveAll((Iterable<Mission>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("mission", locale));
				response.setHasError(true);
				return response;
			}
			List<MissionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? MissionTransformer.INSTANCE.toLiteDtos(itemsSaved) : MissionTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Mission-----");
		return response;
	}

	/**
	 * delete Mission by using MissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<MissionDto> delete(Request<MissionDto> request, Locale locale)  {
		log.info("----begin delete Mission-----");

		Response<MissionDto> response = new Response<MissionDto>();
		List<Mission>        items    = new ArrayList<Mission>();

		for (MissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la mission existe
			Mission existingEntity = missionRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("mission -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// userMission
			List<UserMission> listOfUserMission = userMissionRepository.findByIdMission(existingEntity.getId(), false);
			if (listOfUserMission != null && !listOfUserMission.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUserMission.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			missionRepository.saveAll((Iterable<Mission>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Mission-----");
		return response;
	}

	/**
	 * forceDelete Mission by using MissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<MissionDto> forceDelete(Request<MissionDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Mission-----");

		Response<MissionDto> response = new Response<MissionDto>();
		List<Mission>        items    = new ArrayList<Mission>();

		for (MissionDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la mission existe
			Mission existingEntity = missionRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("mission -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// userMission
			List<UserMission> listOfUserMission = userMissionRepository.findByIdMission(existingEntity.getId(), false);
			if (listOfUserMission != null && !listOfUserMission.isEmpty()){
				Request<UserMissionDto> deleteRequest = new Request<UserMissionDto>();
				deleteRequest.setDatas(UserMissionTransformer.INSTANCE.toDtos(listOfUserMission));
				deleteRequest.setUser(request.getUser());
				Response<UserMissionDto> deleteResponse = userMissionBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			missionRepository.saveAll((Iterable<Mission>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Mission-----");
		return response;
	}

	/**
	 * get Mission by using MissionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<MissionDto> getByCriteria(Request<MissionDto> request, Locale locale)  throws Exception {
		log.info("----begin get Mission-----");

		Response<MissionDto> response = new Response<MissionDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("libelle");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Mission> items 			 = missionRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("mission", locale));
			response.setHasError(false);
			return response;
		}

		List<MissionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? MissionTransformer.INSTANCE.toLiteDtos(items) : MissionTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(missionRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Mission-----");
		return response;
	}

	/**
	 * get full MissionDto by using Mission as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private MissionDto getFullInfos(MissionDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
