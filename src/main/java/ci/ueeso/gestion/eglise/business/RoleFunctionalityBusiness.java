

/*
 * Java business for entity table role_functionality 
 * Created on 2019-11-30 ( Time 18:43:51 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.RoleFunctionality;
import ci.ueeso.gestion.eglise.dao.entity.Role;
import ci.ueeso.gestion.eglise.dao.entity.Functionality;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "role_functionality"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class RoleFunctionalityBusiness implements IBasicBusiness<Request<RoleFunctionalityDto>, Response<RoleFunctionalityDto>> {

	private Response<RoleFunctionalityDto> response;
	@Autowired
	private RoleFunctionalityRepository roleFunctionalityRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private FunctionalityRepository functionalityRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;

	public RoleFunctionalityBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create RoleFunctionality by using RoleFunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<RoleFunctionalityDto> create(Request<RoleFunctionalityDto> request, Locale locale)  throws ParseException {
		log.info("----begin create RoleFunctionality-----");

		Response<RoleFunctionalityDto> response = new Response<RoleFunctionalityDto>();
		List<RoleFunctionality>        items    = new ArrayList<RoleFunctionality>();

		for (RoleFunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("idRole", dto.getIdRole());
			fieldsToVerify.put("idFonctionality", dto.getIdFonctionality());
			/*fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());*/
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if roleFunctionality to insert do not exist
			RoleFunctionality existingEntity = null;
			/*if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("roleFunctionality id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}*/

			// Verify if role exist
			Role existingRole = null;
			if (Utilities.isValidID(dto.getIdRole())){
				existingRole = roleRepository.findOne(dto.getIdRole(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role idRole -> " + dto.getIdRole(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if functionality exist
			Functionality existingFunctionality = null;
			if (Utilities.isValidID(dto.getIdFonctionality())){
				existingFunctionality = functionalityRepository.findOne(dto.getIdFonctionality(), false);
				if (existingFunctionality == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("functionality idFonctionality -> " + dto.getIdFonctionality(), locale));
					response.setHasError(true);
					return response;
				}
			}
			RoleFunctionality entityToSave = RoleFunctionalityTransformer.INSTANCE.toEntity(dto, existingRole, existingFunctionality);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<RoleFunctionality> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = roleFunctionalityRepository.saveAll((Iterable<RoleFunctionality>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("roleFunctionality", locale));
				response.setHasError(true);
				return response;
			}
			List<RoleFunctionalityDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? RoleFunctionalityTransformer.INSTANCE.toLiteDtos(itemsSaved) : RoleFunctionalityTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}
		log.info("----end create RoleFunctionality-----");
		return response;
	}

	/**
	 * update RoleFunctionality by using RoleFunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<RoleFunctionalityDto> update(Request<RoleFunctionalityDto> request, Locale locale)  throws ParseException {
		log.info("----begin update RoleFunctionality-----");

		Response<RoleFunctionalityDto> response = new Response<RoleFunctionalityDto>();
		List<RoleFunctionality>        items    = new ArrayList<RoleFunctionality>();

		for (RoleFunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verifier si la roleFunctionality existe
			RoleFunctionality entityToSave = null;
			entityToSave = roleFunctionalityRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("roleFunctionality id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if role exist
			if (Utilities.isValidID(dto.getIdRole()) && !entityToSave.getRole().getId().equals(dto.getIdRole())){
				Role existingRole = roleRepository.findOne(dto.getIdRole(), false);
				if (existingRole == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("role idRole -> " + dto.getIdRole(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setRole(existingRole);
			}
			// Verify if functionality exist
			if (Utilities.isValidID(dto.getIdFonctionality()) && !entityToSave.getFunctionality().getId().equals(dto.getIdFonctionality())){
				Functionality existingFunctionality = functionalityRepository.findOne(dto.getIdFonctionality(), false);
				if (existingFunctionality == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("functionality idFonctionality -> " + dto.getIdFonctionality(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setFunctionality(existingFunctionality);
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<RoleFunctionality> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = roleFunctionalityRepository.saveAll((Iterable<RoleFunctionality>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("roleFunctionality", locale));
				response.setHasError(true);
				return response;
			}
			List<RoleFunctionalityDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? RoleFunctionalityTransformer.INSTANCE.toLiteDtos(itemsSaved) : RoleFunctionalityTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update RoleFunctionality-----");
		return response;
	}

	/**
	 * delete RoleFunctionality by using RoleFunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<RoleFunctionalityDto> delete(Request<RoleFunctionalityDto> request, Locale locale)  {
		log.info("----begin delete RoleFunctionality-----");

		Response<RoleFunctionalityDto> response = new Response<RoleFunctionalityDto>();
		List<RoleFunctionality>        items    = new ArrayList<RoleFunctionality>();

		for (RoleFunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la roleFunctionality existe
			RoleFunctionality existingEntity = null;
			existingEntity = roleFunctionalityRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("roleFunctionality -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			roleFunctionalityRepository.saveAll((Iterable<RoleFunctionality>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete RoleFunctionality-----");
		return response;
	}

	/**
	 * forceDelete RoleFunctionality by using RoleFunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<RoleFunctionalityDto> forceDelete(Request<RoleFunctionalityDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete RoleFunctionality-----");

		Response<RoleFunctionalityDto> response = new Response<RoleFunctionalityDto>();
		List<RoleFunctionality>        items    = new ArrayList<RoleFunctionality>();

		for (RoleFunctionalityDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la roleFunctionality existe
			RoleFunctionality existingEntity = null;
			existingEntity = roleFunctionalityRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("roleFunctionality -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			roleFunctionalityRepository.saveAll((Iterable<RoleFunctionality>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete RoleFunctionality-----");
		return response;
	}

	/**
	 * get RoleFunctionality by using RoleFunctionalityDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<RoleFunctionalityDto> getByCriteria(Request<RoleFunctionalityDto> request, Locale locale)  throws Exception {
		log.info("----begin get RoleFunctionality-----");

		Response<RoleFunctionalityDto> response = new Response<RoleFunctionalityDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<RoleFunctionality> items = roleFunctionalityRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("roleFunctionality", locale));
			response.setHasError(false);
			return response;
		}

		List<RoleFunctionalityDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? RoleFunctionalityTransformer.INSTANCE.toLiteDtos(items) : RoleFunctionalityTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(roleFunctionalityRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get RoleFunctionality-----");
		return response;
	}

	/**
	 * get full RoleFunctionalityDto by using RoleFunctionality as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private RoleFunctionalityDto getFullInfos(RoleFunctionalityDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
