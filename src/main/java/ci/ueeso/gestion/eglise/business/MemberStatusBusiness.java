


                        


/*
 * Java transformer for entity table member_status
 * Created on 2020-01-07 ( Time 18:54:36 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.ueeso.gestion.eglise.dao.entity.MemberStatus;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.repository.MemberStatusRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.MemberStatusDto;
import ci.ueeso.gestion.eglise.helper.dto.UserDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.MemberStatusTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserTransformer;

/**
BUSINESS for table "member_status"
 *
 * @author SFL Back-End developper
 *
 */
@Component
public class MemberStatusBusiness implements IBasicBusiness<Request<MemberStatusDto>, Response<MemberStatusDto>> {

  private Response<MemberStatusDto> response;
  @Autowired
  private MemberStatusRepository memberStatusRepository;
  @Autowired
  private UserRepository userRepository;
                                    
            
	@Autowired
	private UserBusiness userBusiness;
  
  @Autowired
  private FunctionalError functionalError;
  @Autowired
  private TechnicalError technicalError;
  @Autowired
  private ExceptionUtils exceptionUtils;
  @PersistenceContext
  private EntityManager em;

  private Logger slf4jLogger;
  private SimpleDateFormat dateTimeFormat;
  private SimpleDateFormat dateFormat;


  public MemberStatusBusiness() {
    dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    slf4jLogger = LoggerFactory.getLogger(getClass());
  }


  /**
   * create MemberStatus by using MemberStatusDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<MemberStatusDto> create(Request<MemberStatusDto> request, Locale locale)  {
    slf4jLogger.info("----begin create MemberStatus-----");

    response = new Response<MemberStatusDto>();

    try {
/*
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

    Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DEFAULT.getValue(), locale);
	if (userResponse.isHasError()) {
				response.setHasError(true);
				response.setStatus(userResponse.getStatus());
				return response;
			}
*/
      List<MemberStatus> items = new ArrayList<MemberStatus>();

      for (MemberStatusDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
		        fieldsToVerify.put("code", dto.getCode());
		        fieldsToVerify.put("libelle", dto.getLibelle());
		        fieldsToVerify.put("comment", dto.getComment());
	        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }


        // Verify if memberStatus to insert do not exist
        MemberStatus existingEntity = null;
        //TODO: add/replace by the best method
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("memberStatus -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = memberStatusRepository.findByCode(dto.getCode());
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("memberStatus -> " + dto.getCode(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les memberStatuss", locale));
          response.setHasError(true);
          return response;
        }
        existingEntity = memberStatusRepository.findByLibelle(dto.getLibelle());
        if (existingEntity != null) {
          response.setStatus(functionalError.DATA_EXIST("memberStatus -> " + dto.getLibelle(), locale));
          response.setHasError(true);
          return response;
        }
        if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
          response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les memberStatuss", locale));
          response.setHasError(true);
          return response;
        }

        MemberStatus entityToSave = null;
        entityToSave = MemberStatusTransformer.INSTANCE.toEntity(dto);
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<MemberStatus> itemsSaved = null;
        // inserer les donnees en base de donnees
        itemsSaved = memberStatusRepository.saveAll((Iterable<MemberStatus>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("memberStatus", locale));
          response.setHasError(true);
          return response;
        }
        List<MemberStatusDto> itemsDto = new ArrayList<MemberStatusDto>();
        for (MemberStatus entity : itemsSaved) {
          MemberStatusDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end create MemberStatus-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * update MemberStatus by using MemberStatusDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<MemberStatusDto> update(Request<MemberStatusDto> request, Locale locale)  {
    slf4jLogger.info("----begin update MemberStatus-----");

    response = new Response<MemberStatusDto>();

    try {
/*
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }
    Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DEFAULT.getValue(), locale);
	if (userResponse.isHasError()) {
				response.setHasError(true);
				response.setStatus(userResponse.getStatus());
				return response;
			}
*/
      List<MemberStatus> items = new ArrayList<MemberStatus>();

      for (MemberStatusDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }


        // Verifier si la memberStatus existe
        MemberStatus entityToSave = null;
        entityToSave = memberStatusRepository.findOne(dto.getId());
        if (entityToSave == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("memberStatus -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        Integer entityToSaveId = entityToSave.getId();

        if (Utilities.notBlank(dto.getCode())) {
                        MemberStatus existingEntity = memberStatusRepository.findByCode(dto.getCode());
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("memberStatus -> " + dto.getCode(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les memberStatuss", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setCode(dto.getCode());
        }
        if (Utilities.notBlank(dto.getLibelle())) {
                        MemberStatus existingEntity = memberStatusRepository.findByLibelle(dto.getLibelle());
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("memberStatus -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les memberStatuss", locale));
                  response.setHasError(true);
                  return response;
                }
                  entityToSave.setLibelle(dto.getLibelle());
        }
        if (Utilities.notBlank(dto.getComment())) {
                  entityToSave.setComment(dto.getComment());
        }
        items.add(entityToSave);
      }

      if (!items.isEmpty()) {
        List<MemberStatus> itemsSaved = null;
        // maj les donnees en base
        itemsSaved = memberStatusRepository.saveAll((Iterable<MemberStatus>) items);
        if (itemsSaved == null || itemsSaved.isEmpty()) {
          response.setStatus(functionalError.SAVE_FAIL("memberStatus", locale));
          response.setHasError(true);
          return response;
        }
        List<MemberStatusDto> itemsDto = new ArrayList<MemberStatusDto>();
        for (MemberStatus entity : itemsSaved) {
          MemberStatusDto dto = getFullInfos(entity, itemsSaved.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setHasError(false);
      }

      slf4jLogger.info("----end update MemberStatus-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * delete MemberStatus by using MemberStatusDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  @Override
  public Response<MemberStatusDto> delete(Request<MemberStatusDto> request, Locale locale)  {
    slf4jLogger.info("----begin delete MemberStatus-----");

    response = new Response<MemberStatusDto>();

    try {
/*
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

       Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DEFAULT.getValue(), locale);
	if (userResponse.isHasError()) {
				response.setHasError(true);
				response.setStatus(userResponse.getStatus());
				return response;
			}
*/
      List<MemberStatus> items = new ArrayList<MemberStatus>();

      for (MemberStatusDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
        fieldsToVerify.put("id", dto.getId());
        if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }


        // Verifier si la memberStatus existe
        MemberStatus existingEntity = null;
        existingEntity = memberStatusRepository.findOne(dto.getId());
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("memberStatus -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }

        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

        // user
        List<User> listOfUser = userRepository.findByIdMemberStatus(existingEntity.getId(), false);
        if (listOfUser != null && !listOfUser.isEmpty()){
          response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
          response.setHasError(true);
          return response;
        }


        items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
        memberStatusRepository.deleteAll((Iterable<MemberStatus>) items);

        response.setHasError(false);
      }

      slf4jLogger.info("----end delete MemberStatus-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }


  /**
   * forceDelete MemberStatus by using MemberStatusDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
  //@Override
  public Response<MemberStatusDto> forceDelete(Request<MemberStatusDto> request, Locale locale)  {
    slf4jLogger.info("----begin forceDelete MemberStatus-----");

    response = new Response<MemberStatusDto>();

    try {
/*
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

       Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DEFAULT.getValue(), locale);
	if (userResponse.isHasError()) {
				response.setHasError(true);
				response.setStatus(userResponse.getStatus());
				return response;
			}
*/
      List<MemberStatus> items = new ArrayList<MemberStatus>();

      for (MemberStatusDto dto : request.getDatas()) {
        // Definir les parametres obligatoires
        Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
          fieldsToVerify.put("id", dto.getId());
            if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
          response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
          response.setHasError(true);
          return response;
        }

  
        // Verifier si la memberStatus existe
        MemberStatus existingEntity = null;
          existingEntity = memberStatusRepository.findOne(dto.getId());
        if (existingEntity == null) {
          response.setStatus(functionalError.DATA_NOT_EXIST("memberStatus -> " + dto.getId(), locale));
          response.setHasError(true);
          return response;
        }
  
        // -----------------------------------------------------------------------
        // ----------- CHECK IF DATA IS USED
        // -----------------------------------------------------------------------

                                            // user
        List<User> listOfUser = userRepository.findByIdMemberStatus(existingEntity.getId(), false);
        if (listOfUser != null && !listOfUser.isEmpty()){
          Request<UserDto> deleteRequest = new Request<UserDto>();
          deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
          deleteRequest.setUser(request.getUser());
          Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }
    

                                  items.add(existingEntity);
      }

      if (!items.isEmpty()) {
        // supprimer les donnees en base
          memberStatusRepository.deleteAll((Iterable<MemberStatus>) items);
  
        response.setHasError(false);
      }

      slf4jLogger.info("----end forceDelete MemberStatus-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }



  /**
   * get MemberStatus by using MemberStatusDto as object.
   *
   * @param request
   * @return response
   *
   */
  @SuppressWarnings("unused")
  @Override
  public Response<MemberStatusDto> getByCriteria(Request<MemberStatusDto> request, Locale locale) {
    slf4jLogger.info("----begin get MemberStatus-----");

    response = new Response<MemberStatusDto>();
/*
    Map<String, Object> fieldsToVerifyUser = new HashMap<String, Object>();
    fieldsToVerifyUser.put("user", request.getUser());
    if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
      response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
      response.setHasError(true);
      return response;
    }

       Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DEFAULT.getValue(), locale);
	if (userResponse.isHasError()) {
				response.setHasError(true);
				response.setStatus(userResponse.getStatus());
				return response;
			}
*/
    try {
      List<MemberStatus> items = null;
      items = memberStatusRepository.getByCriteria(request, em, locale);
      if (items != null && !items.isEmpty()) {
        List<MemberStatusDto> itemsDto = new ArrayList<MemberStatusDto>();
        for (MemberStatus entity : items) {
          MemberStatusDto dto = getFullInfos(entity, items.size(), locale);
          if (dto == null) continue;
          itemsDto.add(dto);
        }
        response.setItems(itemsDto);
        response.setCount(memberStatusRepository.count(request, em, locale));
        response.setHasError(false);
      } else {
        response.setStatus(functionalError.DATA_EMPTY("memberStatus", locale));
        response.setHasError(false);
        return response;
      }

      slf4jLogger.info("----end get MemberStatus-----");
    } catch (PermissionDeniedDataAccessException e) {
      exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (DataAccessResourceFailureException e) {
      exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
    } catch (DataAccessException e) {
      exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
    } catch (RuntimeException e) {
      exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
    } catch (Exception e) {
      exceptionUtils.EXCEPTION(response, locale, e);
    } finally {
      if (response.isHasError() && response.getStatus() != null) {
        slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
        throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
      }
    }
    return response;
  }

  /**
   * get full MemberStatusDto by using MemberStatus as object.
   *
   * @param entity, locale
   * @return MemberStatusDto
   *
   */
  private MemberStatusDto getFullInfos(MemberStatus entity, Integer size, Locale locale) throws Exception {
    MemberStatusDto dto = MemberStatusTransformer.INSTANCE.toDto(entity);
    if (dto == null){
      return null;
    }
    if (size > 1) {
      return dto;
    }

    return dto;
  }

  


}
