
/*
 * Created on 27 oct. 2018 ( Time 23:09:09 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 * Copyright 2017 Savoir Faire Linux. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;


import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.SendFailedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.sun.mail.smtp.SMTPAddressFailedException;

import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.helper.EmailUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.HtmlSpecialChars;
import ci.ueeso.gestion.eglise.helper.ParamsUtils;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.UserDto;
import ci.ueeso.gestion.eglise.helper.dto.customize._InfoUpdateDto;


/**
Bunsiness for e-mail notifications
 *
 * @author YAO Lazare Kouakou, Back-End developper
 *
 */

@Service
@Component
@Configuration
@EnableAsync
public class MailBusiness {
	@Autowired
	private static Environment environment;

	@Autowired
	private FunctionalError functionalError;

	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ParamsUtils paramsUtils;

	@Autowired
	private EmailUtils emailUtils;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateSdf ;
	private SimpleDateFormat hourSdf ;
	private String datePattern ;
	private String hourPattern ;
	private Context context;

	HtmlSpecialChars htmlSpecialChars = new HtmlSpecialChars();


	private final static String MAILER_VERSION = "Java";
	public final static String EMAIL_ADDR="email";
	public final static String USER_NAME="user";

	private Logger slf4jLogger = LoggerFactory.getLogger(getClass());

	public MailBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		//slf4jLogger = LoggerFactory.getLogger(getClass());

		datePattern = "dd/MM/yyyy";
		dateSdf = new SimpleDateFormat(datePattern);

		hourPattern = "HH:mm";
		hourSdf = new SimpleDateFormat(hourPattern);
	}



	//MAILS


	public Response<Map<String, String>> sendTestEmail(Request<Map<String, String>> request, Locale locale){
		System.out.println("----begin sendTestEmail-----");

		Response<Map<String, String>> response = new Response<Map<String, String>>();
		//List<Map<String, String>> toRecipients = new ArrayList<Map<String, String>>();
		try 
		{
			if(Utilities.isNotEmpty(request.getDatas())) 
			{
				for (Map<String, String> dto : request.getDatas()) {
					Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
					fieldsToVerify.put("Email", dto.get(EMAIL_ADDR));
					fieldsToVerify.put("Nom", dto.get(USER_NAME));
					if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
						response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
						response.setHasError(true);
						return response;
					}

					String userName = dto.get(USER_NAME);
					if(userName == null) {
						userName = "";
					}
					System.out.println("userName = "+userName);

					String appLink = paramsUtils.getBackOfficeRootUrl();

					//sender
					Map<String, String> from = new HashMap<String, String>();

					//subject
					String subject = "Testing email sending";

					//body
					String body = "Testing email sending: Télégramme---"+userName;

					List<String> attachments = new ArrayList<String>();
					//attachments.add("/Users/lazareyao/Desktop/img1.png");
					//attachments.add("/Users/lazareyao/Desktop/img2.png");


					context = new Context();
					context.setVariable("userName",htmlSpecialChars.replaceHtmlSpecialChars(userName));
					//context.setVariable("userName", userName);
					context.setVariable("appLink",appLink);

					String templateName = paramsUtils.getTestEmailTemplate();
					Integer smtpServerId = request.getSmtpServerId();
					emailUtils.sendEmailWithAttachments(from, Arrays.asList(dto), null, subject, body, attachments, context, templateName, locale, smtpServerId);
				}

				response.setHasError(false);
				response.setStatus(functionalError.SUCCESS("", locale));
			}

			System.out.println("----end sendTestEmail-----");	
		}catch (SMTPAddressFailedException e ) {
			e.printStackTrace();
			response.setStatus(technicalError.INTERN_ERROR(e.getMessage(), locale));
			response.setHasError(true); 
		}catch (SendFailedException | ConnectException e ) {
			e.printStackTrace();
			response.setStatus(technicalError.INTERN_ERROR(e.getMessage(), locale));
			response.setHasError(true); 
		}catch (Exception  e ) {
			e.printStackTrace();
			response.setStatus(technicalError.INTERN_ERROR(e.getMessage(), locale));
			response.setHasError(true); 
		}
		return response;
	}


	@Async
	public void notifyAccountCreation(User user, String pwd, Locale locale) throws ConnectException, SMTPAddressFailedException, SendFailedException, UnsupportedEncodingException, MessagingException, Exception {

		// envoi de mail 
		String appLink = paramsUtils.getBackOfficeRootUrl();

		//set mail to user
		//Map<String, String> from=new HashMap<>();

		//recipients and Ccs
		List<Map<String, String>> inCcs = null;
		Map<String, String> recipient = new HashMap<String, String>();
		recipient.put("email", user.getEmail());
		recipient.put("user", user.getPrenom()+" "+user.getNom());

		//subject
		String subject = "Création de votre compte d'accès à la plateforme de l'église UESSO";
		String body = "";
		String civilite = "";
		if(Utilities.isNotBlank(user.getCivilite())) {
			civilite = user.getCivilite();
		}

		context = new Context();
		context.setVariable("userId", user.getId());
		context.setVariable("civilite", htmlSpecialChars.replaceHtmlSpecialChars(civilite));
		context.setVariable("nom", htmlSpecialChars.replaceHtmlSpecialChars(user.getNom()));
		context.setVariable("prenom", htmlSpecialChars.replaceHtmlSpecialChars(user.getPrenom()));
		context.setVariable("login", user.getLogin());
		context.setVariable("password", pwd);
		context.setVariable("appLink",appLink);

		String templateName = paramsUtils.getWelcomeEmailTemplate();

		emailUtils.sendEmailWithAttachments(null, Arrays.asList(recipient), inCcs, subject, body, null, context, templateName, locale, 0);

	}

	@Async
	public void notifyPasswordChange(User user, String newPwd, Locale locale) {
		try {
			String appLink = paramsUtils.getBackOfficeRootUrl();

			//set mail to user
			//Map<String, String> from=new HashMap<>();

			//recipients and Ccs
			List<Map<String, String>> inCcs = null;
			Map<String, String> recipient = new HashMap<String, String>();
			recipient.put("email", user.getEmail());
			recipient.put("user", user.getPrenom()+" "+user.getNom());

			//subject
			String subject = "Changement de mot de passe";
			String body = "";
			context = new Context();
			context.setVariable("userName", user.getLogin());
			context.setVariable("password", newPwd.trim());
			context.setVariable("date", dateFormat.format(Utilities.getCurrentDate()));
			context.setVariable("appLink",appLink);

			String templateName = paramsUtils.getResetPasswordSuccesEmailTemplate();

			emailUtils.sendEmailWithAttachments(null, Arrays.asList(recipient), inCcs, subject, body, null, context, templateName, locale, 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Async
	public void notifyResetPasswordRequest(User user, Locale locale) {
		try {
			String appLink = paramsUtils.getBackOfficeRootUrl();

			//Map<String, String> from=new HashMap<>();

			//recipients and Ccs
			List<Map<String, String>> inCcs = null;
			Map<String, String> recipient = new HashMap<String, String>();
			recipient.put("email", user.getEmail());
			recipient.put("user", user.getPrenom()+" "+user.getNom());

			//subject
			String subject = "Réinitialisation de votre mot de passe";
			String body = "";
			context = new Context();

			context.setVariable("userName", user.getLogin());
			context.setVariable("email", user.getEmail());
			//			context.setVariable("token", user.getToken());
			//			context.setVariable("expirationDate", dateSdf.format(user.getTokenExpireAt()));
			//			context.setVariable("expirationHour", hourSdf.format(user.getTokenExpireAt()));
			context.setVariable("appLink",appLink);

			String templateName = paramsUtils.getResetForgottenUserPasswordEmailTemplate();

			emailUtils.sendEmailWithAttachments(null, Arrays.asList(recipient), inCcs, subject, body, null, context, templateName, locale, 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Async
	public void notifyResetPasswordOpeartion(User user, String newPwd, Locale locale) {
		try {
			String appLink = paramsUtils.getBackOfficeRootUrl();

			//set mail to user
			//Map<String, String> from=new HashMap<>();

			//recipients and Ccs
			List<Map<String, String>> inCcs = null;
			Map<String, String> recipient = new HashMap<String, String>();
			recipient.put("email", user.getEmail());
			recipient.put("user", user.getPrenom()+" "+user.getNom());

			//subject
			String subject = "Réinitialisation de mot de passe";
			String body = "";
			context = new Context();
			context.setVariable("userName", user.getLogin());
			context.setVariable("password", newPwd.trim());
			context.setVariable("date", dateFormat.format(Utilities.getCurrentDate()));
			context.setVariable("appLink",appLink);

			String templateName = paramsUtils.getResetPasswordSuccesEmailTemplate();

			emailUtils.sendEmailWithAttachments(null, Arrays.asList(recipient), inCcs, subject, body, null, context, templateName, locale, 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Async
	public void notifyUserInfosChangement(List<UserDto> oldItems, List<User> newItems, Locale locale) {
		System.out.println("----begin notifyUserInfosChangement-----");

		if(Utilities.isNotEmpty(oldItems) && Utilities.isNotEmpty(newItems)) {
			SimpleDateFormat fullDateSdf = new SimpleDateFormat("EEEEEEEEEEEEE dd/MM/yyyy", locale);
			SimpleDateFormat hourSdf = new SimpleDateFormat("HH:mm", locale);

			for (UserDto oldData : oldItems) {
				User newData = newItems.stream().filter(aud->aud.getId().equals(oldData.getId())).findFirst().orElse(null);
				if(newData != null) {
					List<_InfoUpdateDto> datasChanges = new ArrayList<_InfoUpdateDto>();
					_InfoUpdateDto oneChange = null;

					//Profil
					if(!oldData.getIdRole().equals(newData.getRole().getId())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Profil");
						oneChange.setOldValue(htmlSpecialChars.replaceHtmlSpecialChars(oldData.getRoleLibelle()));
						oneChange.setNewValue(htmlSpecialChars.replaceHtmlSpecialChars(newData.getRole().getLibelle()));
						datasChanges.add(oneChange);
					}

					//UserName
					if((Utilities.isBlank(oldData.getLogin()) && Utilities.isNotBlank(newData.getLogin())) || (Utilities.isNotBlank(oldData.getLogin()) && Utilities.isBlank(newData.getLogin())) || !oldData.getLogin().equals(newData.getLogin())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Nom d'utilisateur");
						oneChange.setOldValue(Utilities.isBlank(oldData.getLogin()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(oldData.getLogin()));
						oneChange.setNewValue(Utilities.isBlank(newData.getLogin()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(newData.getLogin()));
						datasChanges.add(oneChange);
					}

					//FirstName
					if((Utilities.isBlank(oldData.getNom()) && Utilities.isNotBlank(newData.getNom())) || (Utilities.isNotBlank(oldData.getNom()) && Utilities.isBlank(newData.getNom())) || !oldData.getNom().equals(newData.getNom())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Nom");
						oneChange.setOldValue(Utilities.isBlank(oldData.getNom()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(oldData.getNom()));
						oneChange.setNewValue(Utilities.isBlank(newData.getNom()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(newData.getNom()));
						datasChanges.add(oneChange);
					}

					//LastName
					if((Utilities.isBlank(oldData.getPrenom()) && Utilities.isNotBlank(newData.getPrenom())) || (Utilities.isNotBlank(oldData.getPrenom()) && Utilities.isBlank(newData.getPrenom())) || !oldData.getPrenom().equals(newData.getPrenom())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Prenoms");
						oneChange.setOldValue(Utilities.isBlank(oldData.getPrenom()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(oldData.getPrenom()));
						oneChange.setNewValue(Utilities.isBlank(newData.getPrenom()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(newData.getPrenom()));
						datasChanges.add(oneChange);
					}

					//BornOn
					if((oldData.getDateNaissance() == null && newData.getDateNaissance() != null) || (oldData.getDateNaissance() != null && newData.getDateNaissance() == null) || !oldData.getDateNaissance().equals(dateSdf.format(newData.getDateNaissance()))) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Date de naissance");
						oneChange.setOldValue(Utilities.isBlank(oldData.getDateNaissance()) ? "-" : oldData.getDateNaissance());
						oneChange.setNewValue(newData.getDateNaissance() == null ? "-" : dateSdf.format(newData.getDateNaissance()));
						datasChanges.add(oneChange);
					}

					//Email
					if((Utilities.isBlank(oldData.getEmail()) && Utilities.isNotBlank(newData.getEmail())) || (Utilities.isNotBlank(oldData.getEmail()) && Utilities.isBlank(newData.getEmail())) || !oldData.getEmail().equals(newData.getEmail())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Email");
						oneChange.setOldValue(Utilities.isBlank(oldData.getEmail()) ? "-" : oldData.getEmail());
						oneChange.setNewValue(Utilities.isBlank(newData.getEmail()) ? "-" : newData.getEmail());
						datasChanges.add(oneChange);
					}

					//MobilePhone
					if((Utilities.isBlank(oldData.getTelephone()) && Utilities.isNotBlank(newData.getTelephone())) || (Utilities.isNotBlank(oldData.getTelephone()) && Utilities.isBlank(newData.getTelephone())) || !oldData.getTelephone().equals(newData.getTelephone())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName(htmlSpecialChars.replaceHtmlSpecialChars("Téléphone portable"));
						oneChange.setOldValue(Utilities.isBlank(oldData.getTelephone()) ? "-" : oldData.getTelephone());
						oneChange.setNewValue(Utilities.isBlank(newData.getTelephone()) ? "-" : newData.getTelephone());
						datasChanges.add(oneChange);
					}

					//Fonction
					if((Utilities.isBlank(oldData.getProfessionLibelle()) && Utilities.isNotBlank(newData.getProfession().getLibelle()) || (Utilities.isNotBlank(oldData.getProfessionLibelle()) && Utilities.isBlank(newData.getProfession().getLibelle())) || !oldData.getProfessionLibelle().equals(newData.getProfession().getLibelle()))) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName("Fonction");
						oneChange.setOldValue(Utilities.isBlank(oldData.getProfessionLibelle()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(oldData.getProfessionLibelle()));
						oneChange.setNewValue(Utilities.isBlank(newData.getProfession().getLibelle()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(newData.getProfession().getLibelle()));
						datasChanges.add(oneChange);
					}

					//Civilité
					if((Utilities.isBlank(oldData.getCivilite()) && Utilities.isNotBlank(newData.getCivilite())) || (Utilities.isNotBlank(oldData.getCivilite()) && Utilities.isBlank(newData.getCivilite())) || !oldData.getCivilite().equals(newData.getCivilite())) {
						oneChange = new _InfoUpdateDto();
						oneChange.setPropertyName(htmlSpecialChars.replaceHtmlSpecialChars("Civilité"));
						oneChange.setOldValue(Utilities.isBlank(oldData.getCivilite()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(oldData.getCivilite()));
						oneChange.setNewValue(Utilities.isBlank(newData.getCivilite()) ? "-" : htmlSpecialChars.replaceHtmlSpecialChars(newData.getCivilite()));
						datasChanges.add(oneChange);
					}

					try {
						String backOfficeAppLink = paramsUtils.getBackOfficeRootUrl();
						//String frontOfficeAppLink = paramsUtils.getFrontOfficeRootUrl();

						//subject
						String subject = "Modification des informations de votre compte";
						String body = "";

						Map<String, String> recipient = new HashMap<String, String>();
						recipient = new HashMap<String, String>();
						recipient.put("email", oldData.getEmail());
						recipient.put("user", oldData.getPrenom()+" "+oldData.getNom());

						Map<String, String> cc = null;
						if(!oldData.getEmail().equals(newData.getEmail())) {
							cc = new HashMap<String, String>();
							cc.put("email", newData.getEmail());
							cc.put("user", newData.getPrenom()+" "+newData.getNom());
						}

						String civilite = "";
						if(Utilities.notBlank(oldData.getCivilite())) {
							civilite = oldData.getCivilite();
						}

						context = new Context();
						context.setVariable("civilite", htmlSpecialChars.replaceHtmlSpecialChars(civilite));
						context.setVariable("nom", htmlSpecialChars.replaceHtmlSpecialChars(oldData.getNom()));
						context.setVariable("prenom", htmlSpecialChars.replaceHtmlSpecialChars(oldData.getPrenom()));

						context.setVariable("fullDate", fullDateSdf.format(newData.getUpdatedAt()));
						context.setVariable("hour", hourSdf.format(newData.getUpdatedAt()));
						context.setVariable("datasChanges", datasChanges);

						context.setVariable("backOfficeAppLink",backOfficeAppLink);

						String templateName = paramsUtils.getNotificationModificationCompteEmailTemplate();

						emailUtils.sendEmailWithAttachments(null, Arrays.asList(recipient), cc != null ? Arrays.asList(cc) : null, subject, body, null, context, templateName, locale, 0);

						//Thread.sleep(20000);
					} catch (ConnectException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SMTPAddressFailedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SendFailedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			System.out.println("----end notifyUserInfosChangement-----");
		}
	}




}
