

/*
 * Java business for entity table user_groupe_constitue 
 * Created on 2020-01-23 ( Time 10:51:51 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Fonction;
import ci.ueeso.gestion.eglise.dao.entity.GroupeConstitue;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.entity.UserGroupeConstitue;
import ci.ueeso.gestion.eglise.dao.repository.FonctionRepository;
import ci.ueeso.gestion.eglise.dao.repository.GroupeConstitueRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserGroupeConstitueRepository;
import ci.ueeso.gestion.eglise.dao.repository.UserRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.UserGroupeConstitueDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.UserGroupeConstitueTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "user_groupe_constitue"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class UserGroupeConstitueBusiness implements IBasicBusiness<Request<UserGroupeConstitueDto>, Response<UserGroupeConstitueDto>> {

	private Response<UserGroupeConstitueDto> response;
	@Autowired
	private UserGroupeConstitueRepository userGroupeConstitueRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FonctionRepository fonctionRepository;
	@Autowired
	private GroupeConstitueRepository groupeConstitueRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;

	public UserGroupeConstitueBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create UserGroupeConstitue by using UserGroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserGroupeConstitueDto> create(Request<UserGroupeConstitueDto> request, Locale locale)  throws ParseException {
		log.info("----begin create UserGroupeConstitue-----");

		Response<UserGroupeConstitueDto> response = new Response<UserGroupeConstitueDto>();
		List<UserGroupeConstitue>        items    = new ArrayList<UserGroupeConstitue>();

		for (UserGroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("userGroupeConstitue->idFonction/fonctionLibelle", Arrays.asList(dto.getIdFonction(), dto.getFonctionLibelle()));
			fieldsToVerify.put("userGroupeConstitue->idUser", dto.getIdUser());
			fieldsToVerify.put("userGroupeConstitue->idGroupeConstitue", dto.getIdGroupeConstitue());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if user exist
			User existingUser = userRepository.findOne(dto.getIdUser(), false);
			if (existingUser == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if groupeConstitue exist
			GroupeConstitue existingGroupeConstitue = groupeConstitueRepository.findOne(dto.getIdGroupeConstitue(), false);
			if (existingGroupeConstitue == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("groupeConstitue idGroupeConstitue -> " + dto.getIdGroupeConstitue(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if userGroupeConstitue to insert do not exist
			UserGroupeConstitue existingEntity = userGroupeConstitueRepository.findByIdUserAndIdGroupeConstitue(dto.getIdUser(), dto.getIdGroupeConstitue(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("L'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' est deja enregistré dans le groupe '"+existingGroupeConstitue.getLibelle()+"' avec la fonction '"+existingEntity.getFonction().getLibelle()+"'", locale));
				response.setHasError(true);
				return response;
			}
			if (items.stream().anyMatch(e->e.getUser().getId().equals(dto.getIdUser()) && e.getGroupeConstitue().getId().equals(dto.getIdGroupeConstitue()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de l'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' dans le groupe '"+existingGroupeConstitue.getLibelle()+"'.", locale));
				response.setHasError(true);
				return response;
			}
			
			// Verify if fonction exist
			Fonction existingFonction = null;
			if (Utilities.isValidID(dto.getIdFonction())){
				existingFonction = fonctionRepository.findOne(dto.getIdFonction(), false);
				if (existingFonction == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("fonction idFonction -> " + dto.getIdFonction(), locale));
					response.setHasError(true);
					return response;
				}
			}
			else {
				existingFonction = fonctionRepository.findByLibelle(dto.getFonctionLibelle(), false);
				if (existingFonction == null) {
					String fonctionCode = Utilities.getCodeFromLibelle(dto.getFonctionLibelle());
					existingFonction = fonctionRepository.findByCode(fonctionCode, false);
					if (existingFonction == null) {
						Fonction fonction = new Fonction();
						fonction.setCode(fonctionCode);
						fonction.setLibelle(dto.getFonctionLibelle());
						fonction.setCreatedAt(new Date());
						fonction.setCreatedBy(request.getUser());
						fonction.setIsDeleted(false);
						
						existingFonction = fonctionRepository.save(fonction);
					}
				}
			}
			
			UserGroupeConstitue entityToSave = UserGroupeConstitueTransformer.INSTANCE.toEntity(dto, existingUser, existingFonction, existingGroupeConstitue);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<UserGroupeConstitue> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = userGroupeConstitueRepository.saveAll((Iterable<UserGroupeConstitue>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("userGroupeConstitue", locale));
				response.setHasError(true);
				return response;
			}
			List<UserGroupeConstitueDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserGroupeConstitueTransformer.INSTANCE.toLiteDtos(itemsSaved) : UserGroupeConstitueTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create UserGroupeConstitue-----");
		return response;
	}

	/**
	 * update UserGroupeConstitue by using UserGroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserGroupeConstitueDto> update(Request<UserGroupeConstitueDto> request, Locale locale)  throws ParseException {
		log.info("----begin update UserGroupeConstitue-----");

		Response<UserGroupeConstitueDto> response = new Response<UserGroupeConstitueDto>();
		List<UserGroupeConstitue>        items    = new ArrayList<UserGroupeConstitue>();

		for (UserGroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la userGroupeConstitue existe
			UserGroupeConstitue entityToSave = userGroupeConstitueRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("userGroupeConstitue id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if user exist
			User existingUser = entityToSave.getUser();
			if (Utilities.isValidID(dto.getIdUser()) && !entityToSave.getUser().getId().equals(dto.getIdUser())){
				existingUser = userRepository.findOne(dto.getIdUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
			}
			
			// Verify if groupeConstitue exist
			GroupeConstitue existingGroupeConstitue = entityToSave.getGroupeConstitue();
			if (Utilities.isValidID(dto.getIdGroupeConstitue()) && !entityToSave.getGroupeConstitue().getId().equals(dto.getIdGroupeConstitue())){
				existingGroupeConstitue = groupeConstitueRepository.findOne(dto.getIdGroupeConstitue(), false);
				if (existingGroupeConstitue == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("groupeConstitue idGroupeConstitue -> " + dto.getIdGroupeConstitue(), locale));
					response.setHasError(true);
					return response;
				}
			}
			
			Integer existingUserId = existingUser.getId();
			Integer existingGroupeConstitueId = existingGroupeConstitue.getId();

			// Verify if userGroupeConstitue to insert do not exist
			UserGroupeConstitue existingEntity = userGroupeConstitueRepository.findByIdUserAndIdGroupeConstitue(existingUser.getId(), existingGroupeConstitue.getId(), false);
			if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
				response.setStatus(functionalError.DATA_EXIST("L'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' est deja enregistré dans le groupe '"+existingGroupeConstitue.getLibelle()+"' avec la fonction '"+existingEntity.getFonction().getLibelle()+"'", locale));
				response.setHasError(true);
				return response;
			}
			if (items.stream().anyMatch(e->e.getUser().getId().equals(existingUserId) && e.getGroupeConstitue().getId().equals(existingGroupeConstitueId) && !e.getId().equals(entityToSaveId))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication de l'utilisateur '" + existingUser.getPrenom()+" "+existingUser.getNom()+"' dans le groupe '"+existingGroupeConstitue.getLibelle()+"'.", locale));
				response.setHasError(true);
				return response;
			}
			
			// Verify if fonction exist
			Fonction existingFonction = entityToSave.getFonction();
			if ((Utilities.isValidID(dto.getIdFonction()) && !existingFonction.getId().equals(dto.getIdFonction())) || (Utilities.isNotBlank(dto.getFonctionLibelle()) && !dto.getFonctionLibelle().equals(existingFonction.getLibelle()))){
				if (Utilities.isValidID(dto.getIdFonction()) && !entityToSave.getFonction().getId().equals(dto.getIdFonction())){
					existingFonction = fonctionRepository.findOne(dto.getIdFonction(), false);
					if (existingFonction == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("fonction idFonction -> " + dto.getIdFonction(), locale));
						response.setHasError(true);
						return response;
					}
				}
				else {
					existingFonction = fonctionRepository.findByLibelle(dto.getFonctionLibelle(), false);
					if (existingFonction == null) {
						String fonctionCode = Utilities.getCodeFromLibelle(dto.getFonctionLibelle());
						existingFonction = fonctionRepository.findByCode(fonctionCode, false);
						if (existingFonction == null) {
							Fonction fonction = new Fonction();
							fonction.setCode(fonctionCode);
							fonction.setLibelle(dto.getFonctionLibelle());
							fonction.setCreatedAt(new Date());
							fonction.setCreatedBy(request.getUser());
							fonction.setIsDeleted(false);
							
							existingFonction = fonctionRepository.save(fonction);
						}
					}
				}
			}

			entityToSave.setUser(existingUser);
			entityToSave.setGroupeConstitue(existingGroupeConstitue);
			entityToSave.setFonction(existingFonction);
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}
		if (!items.isEmpty()) {
			List<UserGroupeConstitue> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = userGroupeConstitueRepository.saveAll((Iterable<UserGroupeConstitue>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("userGroupeConstitue", locale));
				response.setHasError(true);
				return response;
			}
			List<UserGroupeConstitueDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserGroupeConstitueTransformer.INSTANCE.toLiteDtos(itemsSaved) : UserGroupeConstitueTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}
			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}
		log.info("----end update UserGroupeConstitue-----");
		return response;
	}

	/**
	 * delete UserGroupeConstitue by using UserGroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserGroupeConstitueDto> delete(Request<UserGroupeConstitueDto> request, Locale locale)  {
		log.info("----begin delete UserGroupeConstitue-----");

		Response<UserGroupeConstitueDto> response = new Response<UserGroupeConstitueDto>();
		List<UserGroupeConstitue>        items    = new ArrayList<UserGroupeConstitue>();

		for (UserGroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la userGroupeConstitue existe
			UserGroupeConstitue existingEntity = userGroupeConstitueRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("userGroupeConstitue -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userGroupeConstitueRepository.saveAll((Iterable<UserGroupeConstitue>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete UserGroupeConstitue-----");
		return response;
	}

	/**
	 * forceDelete UserGroupeConstitue by using UserGroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserGroupeConstitueDto> forceDelete(Request<UserGroupeConstitueDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete UserGroupeConstitue-----");

		Response<UserGroupeConstitueDto> response = new Response<UserGroupeConstitueDto>();
		List<UserGroupeConstitue>        items    = new ArrayList<UserGroupeConstitue>();

		for (UserGroupeConstitueDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la userGroupeConstitue existe
			UserGroupeConstitue existingEntity = userGroupeConstitueRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("userGroupeConstitue -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			userGroupeConstitueRepository.saveAll((Iterable<UserGroupeConstitue>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete UserGroupeConstitue-----");
		return response;
	}

	/**
	 * get UserGroupeConstitue by using UserGroupeConstitueDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<UserGroupeConstitueDto> getByCriteria(Request<UserGroupeConstitueDto> request, Locale locale)  throws Exception {
		log.info("----begin get UserGroupeConstitue-----");
		Response<UserGroupeConstitueDto> response = new Response<UserGroupeConstitueDto>();
		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("groupeConstitue.libelle");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}
		List<UserGroupeConstitue> items = userGroupeConstitueRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("userGroupeConstitue", locale));
			response.setHasError(false);
			return response;
		}

		List<UserGroupeConstitueDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? UserGroupeConstitueTransformer.INSTANCE.toLiteDtos(items) : UserGroupeConstitueTransformer.INSTANCE.toDtos(items);
		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}
		response.setItems(itemsDto);
		response.setCount(userGroupeConstitueRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));
		log.info("----end get UserGroupeConstitue-----");
		return response;
	}

	/**
	 * get full UserGroupeConstitueDto by using UserGroupeConstitue as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private UserGroupeConstitueDto getFullInfos(UserGroupeConstitueDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
	
	public UserGroupeConstitueDto getFullInfos(UserGroupeConstitue entity, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here
		UserGroupeConstitueDto dto = (Utilities.isTrue(isSimpleLoading)) ? UserGroupeConstitueTransformer.INSTANCE.toLiteDto(entity) : UserGroupeConstitueTransformer.INSTANCE.toDto(entity);
		dto.setGroupeConstitueDescription(entity.getGroupeConstitue().getDescription());
		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
