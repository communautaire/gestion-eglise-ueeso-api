                                                    													

/*
 * Java business for entity table sous_prefecture 
 * Created on 2020-01-18 ( Time 09:57:26 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Departement;
import ci.ueeso.gestion.eglise.dao.entity.SousPrefecture;
import ci.ueeso.gestion.eglise.dao.repository.DepartementRepository;
import ci.ueeso.gestion.eglise.dao.repository.SousPrefectureRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.SousPrefectureDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.SousPrefectureTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "sous_prefecture"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class SousPrefectureBusiness implements IBasicBusiness<Request<SousPrefectureDto>, Response<SousPrefectureDto>> {

	private Response<SousPrefectureDto> response;
	@Autowired
	private SousPrefectureRepository sousPrefectureRepository;
	@Autowired
	private DepartementRepository departementRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

      
          @Autowired
    private UserBusiness userBusiness;
    
	public SousPrefectureBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
	
	/**
	 * create SousPrefecture by using SousPrefectureDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SousPrefectureDto> create(Request<SousPrefectureDto> request, Locale locale)  throws ParseException {
		log.info("----begin create SousPrefecture-----");

		Response<SousPrefectureDto> response = new Response<SousPrefectureDto>();
		List<SousPrefecture>        items    = new ArrayList<SousPrefecture>();
			
		for (SousPrefectureDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("idDepartement", dto.getIdDepartement());
			fieldsToVerify.put("chefLieu", dto.getChefLieu());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if sousPrefecture to insert do not exist
			SousPrefecture existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("sousPrefecture id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = sousPrefectureRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("sousPrefecture libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			// Verify if departement exist
			Departement existingDepartement = null;
			if (Utilities.isValidID(dto.getIdDepartement())){
				existingDepartement = departementRepository.findOne(dto.getIdDepartement(), false);
				if (existingDepartement == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("departement idDepartement -> " + dto.getIdDepartement(), locale));
					response.setHasError(true);
					return response;
				}
			}
				SousPrefecture entityToSave = SousPrefectureTransformer.INSTANCE.toEntity(dto, existingDepartement);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<SousPrefecture> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = sousPrefectureRepository.saveAll((Iterable<SousPrefecture>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("sousPrefecture", locale));
				response.setHasError(true);
				return response;
			}
			List<SousPrefectureDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SousPrefectureTransformer.INSTANCE.toLiteDtos(itemsSaved) : SousPrefectureTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create SousPrefecture-----");
		return response;
	}

	/**
	 * update SousPrefecture by using SousPrefectureDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SousPrefectureDto> update(Request<SousPrefectureDto> request, Locale locale)  throws ParseException {
		log.info("----begin update SousPrefecture-----");

		Response<SousPrefectureDto> response = new Response<SousPrefectureDto>();
		List<SousPrefecture>        items    = new ArrayList<SousPrefecture>();
			
		for (SousPrefectureDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la sousPrefecture existe
			SousPrefecture entityToSave = null;
			entityToSave = sousPrefectureRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("sousPrefecture id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if departement exist
			if (Utilities.isValidID(dto.getIdDepartement()) && !entityToSave.getDepartement().getId().equals(dto.getIdDepartement())){
				Departement existingDepartement = departementRepository.findOne(dto.getIdDepartement(), false);
				if (existingDepartement == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("departement idDepartement -> " + dto.getIdDepartement(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setDepartement(existingDepartement);
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
			                 SousPrefecture existingEntity = sousPrefectureRepository.findByLibelle(dto.getLibelle(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("sousPrefecture -> " + dto.getLibelle(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les sousPrefectures", locale));
                  response.setHasError(true);
                  return response;
                }
       						entityToSave.setLibelle(dto.getLibelle());
			}
			if (Utilities.isNotBlank(dto.getChefLieu()) && !dto.getChefLieu().equals(entityToSave.getChefLieu())) {
			 				entityToSave.setChefLieu(dto.getChefLieu());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<SousPrefecture> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = sousPrefectureRepository.saveAll((Iterable<SousPrefecture>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("sousPrefecture", locale));
				response.setHasError(true);
				return response;
			}
			List<SousPrefectureDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SousPrefectureTransformer.INSTANCE.toLiteDtos(itemsSaved) : SousPrefectureTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update SousPrefecture-----");
		return response;
	}

	/**
	 * delete SousPrefecture by using SousPrefectureDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SousPrefectureDto> delete(Request<SousPrefectureDto> request, Locale locale)  {
		log.info("----begin delete SousPrefecture-----");

		Response<SousPrefectureDto> response = new Response<SousPrefectureDto>();
		List<SousPrefecture>        items    = new ArrayList<SousPrefecture>();
			
		for (SousPrefectureDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la sousPrefecture existe
			SousPrefecture existingEntity = null;
			existingEntity = sousPrefectureRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("sousPrefecture -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			sousPrefectureRepository.saveAll((Iterable<SousPrefecture>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete SousPrefecture-----");
		return response;
	}

	/**
	 * forceDelete SousPrefecture by using SousPrefectureDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SousPrefectureDto> forceDelete(Request<SousPrefectureDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete SousPrefecture-----");

		Response<SousPrefectureDto> response = new Response<SousPrefectureDto>();
		List<SousPrefecture>        items    = new ArrayList<SousPrefecture>();
			
		for (SousPrefectureDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la sousPrefecture existe
			SousPrefecture existingEntity = null;
			existingEntity = sousPrefectureRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("sousPrefecture -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			sousPrefectureRepository.saveAll((Iterable<SousPrefecture>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete SousPrefecture-----");
		return response;
	}

	/**
	 * get SousPrefecture by using SousPrefectureDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SousPrefectureDto> getByCriteria(Request<SousPrefectureDto> request, Locale locale)  throws Exception {
		log.info("----begin get SousPrefecture-----");

		Response<SousPrefectureDto> response = new Response<SousPrefectureDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<SousPrefecture> items 			 = sousPrefectureRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("sousPrefecture", locale));
			response.setHasError(false);
			return response;
		}

		List<SousPrefectureDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SousPrefectureTransformer.INSTANCE.toLiteDtos(items) : SousPrefectureTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setCount(sousPrefectureRepository.count(request, em, locale));
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get SousPrefecture-----");
		return response;
	}

	/**
	 * get full SousPrefectureDto by using SousPrefecture as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private SousPrefectureDto getFullInfos(SousPrefectureDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
