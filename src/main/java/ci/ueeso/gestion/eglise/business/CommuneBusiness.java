

/*
 * Java business for entity table commune 
 * Created on 2020-01-18 ( Time 18:17:47 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Commune;
import ci.ueeso.gestion.eglise.dao.entity.Pays;
import ci.ueeso.gestion.eglise.dao.entity.Ville;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "commune"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class CommuneBusiness implements IBasicBusiness<Request<CommuneDto>, Response<CommuneDto>> {

	private Response<CommuneDto> response;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private QuartierRepository quartierRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private QuartierBusiness quartierBusiness;


	@Autowired
	private AdresseBusiness adresseBusiness;


	@Autowired
	private UserBusiness userBusiness;

	public CommuneBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Commune by using CommuneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<CommuneDto> create(Request<CommuneDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Commune-----");

		Response<CommuneDto> response = new Response<CommuneDto>();
		List<Commune>        items    = new ArrayList<Commune>();

		for (CommuneDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
//			fieldsToVerify.put("commune->code", dto.getCode());
			fieldsToVerify.put("commune->libelle", dto.getLibelle());
//			fieldsToVerify.put("commune->longitude", dto.getLongitude());
//			fieldsToVerify.put("commune->latitude", dto.getLatitude());
//			fieldsToVerify.put("commune->superficie", dto.getSuperficie());
			fieldsToVerify.put("commune->idVille", dto.getIdVille());
			fieldsToVerify.put("commune->idPays", dto.getIdPays());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if commune to insert do not exist
			Commune existingEntity = null;
			/*if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("commune id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}*/

			// verif unique code in db
			/*existingEntity = communeRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("commune code -> " + dto.getCode(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" code ", locale));
				response.setHasError(true);
				return response;
			}*/
			// verif unique libelle in db
			existingEntity = communeRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("commune libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			// Verify if pays exist
			Pays existingPays = null;
			if (Utilities.isValidID(dto.getIdPays())){
				existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if ville exist
			Ville existingVille = null;
			if (Utilities.isValidID(dto.getIdVille())){
				existingVille = villeRepository.findOne(dto.getIdVille(), false);
				if (existingVille == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville idVille -> " + dto.getIdVille(), locale));
					response.setHasError(true);
					return response;
				}
			}
			Commune entityToSave = CommuneTransformer.INSTANCE.toEntity(dto, existingPays, existingVille);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Commune> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = communeRepository.saveAll((Iterable<Commune>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("commune", locale));
				response.setHasError(true);
				return response;
			}
			List<CommuneDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? CommuneTransformer.INSTANCE.toLiteDtos(itemsSaved) : CommuneTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}
			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}
		log.info("----end create Commune-----");
		return response;
	}

	/**
	 * update Commune by using CommuneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<CommuneDto> update(Request<CommuneDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Commune-----");

		Response<CommuneDto> response = new Response<CommuneDto>();
		List<Commune>        items    = new ArrayList<Commune>();

		for (CommuneDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la commune existe
			Commune entityToSave = null;
			entityToSave = communeRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("commune id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();
			// Verify if pays exist
			if (Utilities.isValidID(dto.getIdPays()) && !entityToSave.getPays().getId().equals(dto.getIdPays())){
				Pays existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setPays(existingPays);
			}
			// Verify if ville exist
			if (Utilities.isValidID(dto.getIdVille()) && !entityToSave.getVille().getId().equals(dto.getIdVille())){
				Ville existingVille = villeRepository.findOne(dto.getIdVille(), false);
				if (existingVille == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville idVille -> " + dto.getIdVille(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setVille(existingVille);
			}
			/*if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
			                 Commune existingEntity = communeRepository.findByCode(dto.getCode(), false);
                if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
                  response.setStatus(functionalError.DATA_EXIST("commune -> " + dto.getCode(), locale));
                  response.setHasError(true);
                  return response;
                }
                if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
                  response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les communes", locale));
                  response.setHasError(true);
                  return response;
                }
       			entityToSave.setCode(dto.getCode());
			}*/
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				Commune existingEntity = communeRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("commune -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les communes", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			if (dto.getLongitude() != null && dto.getLongitude() > 0) {
				entityToSave.setLongitude(dto.getLongitude());
			}
			if (dto.getLatitude() != null && dto.getLatitude() > 0) {
				entityToSave.setLatitude(dto.getLatitude());
			}
			if (dto.getSuperficie() != null && dto.getSuperficie() > 0) {
				entityToSave.setSuperficie(dto.getSuperficie());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Commune> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = communeRepository.saveAll((Iterable<Commune>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("commune", locale));
				response.setHasError(true);
				return response;
			}
			List<CommuneDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? CommuneTransformer.INSTANCE.toLiteDtos(itemsSaved) : CommuneTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Commune-----");
		return response;
	}

	/**
	 * delete Commune by using CommuneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<CommuneDto> delete(Request<CommuneDto> request, Locale locale)  {
		log.info("----begin delete Commune-----");

		Response<CommuneDto> response = new Response<CommuneDto>();
		List<Commune>        items    = new ArrayList<Commune>();

		for (CommuneDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la commune existe
			Commune existingEntity = null;
			existingEntity = communeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("commune -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// quartier
			List<Quartier> listOfQuartier = quartierRepository.findByIdCommune(existingEntity.getId(), false);
			if (listOfQuartier != null && !listOfQuartier.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfQuartier.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdCommune(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAdresse.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			communeRepository.saveAll((Iterable<Commune>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Commune-----");
		return response;
	}

	/**
	 * forceDelete Commune by using CommuneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<CommuneDto> forceDelete(Request<CommuneDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Commune-----");

		Response<CommuneDto> response = new Response<CommuneDto>();
		List<Commune>        items    = new ArrayList<Commune>();

		for (CommuneDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la commune existe
			Commune existingEntity = null;
			existingEntity = communeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("commune -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// quartier
			List<Quartier> listOfQuartier = quartierRepository.findByIdCommune(existingEntity.getId(), false);
			if (listOfQuartier != null && !listOfQuartier.isEmpty()){
				Request<QuartierDto> deleteRequest = new Request<QuartierDto>();
				deleteRequest.setDatas(QuartierTransformer.INSTANCE.toDtos(listOfQuartier));
				deleteRequest.setUser(request.getUser());
				Response<QuartierDto> deleteResponse = quartierBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdCommune(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				Request<AdresseDto> deleteRequest = new Request<AdresseDto>();
				deleteRequest.setDatas(AdresseTransformer.INSTANCE.toDtos(listOfAdresse));
				deleteRequest.setUser(request.getUser());
				Response<AdresseDto> deleteResponse = adresseBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			communeRepository.saveAll((Iterable<Commune>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Commune-----");
		return response;
	}

	/**
	 * get Commune by using CommuneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<CommuneDto> getByCriteria(Request<CommuneDto> request, Locale locale)  throws Exception {
		log.info("----begin get Commune-----");

		Response<CommuneDto> response = new Response<CommuneDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Commune> items 			 = communeRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("commune", locale));
			response.setHasError(false);
			return response;
		}

		List<CommuneDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? CommuneTransformer.INSTANCE.toLiteDtos(items) : CommuneTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(communeRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Commune-----");
		return response;
	}

	/**
	 * get full CommuneDto by using Commune as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private CommuneDto getFullInfos(CommuneDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
