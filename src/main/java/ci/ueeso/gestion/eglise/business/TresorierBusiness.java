                                                            													

/*
 * Java business for entity table tresorier 
 * Created on 2019-11-30 ( Time 18:43:52 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Tresorier;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "tresorier"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class TresorierBusiness implements IBasicBusiness<Request<TresorierDto>, Response<TresorierDto>> {

	private Response<TresorierDto> response;
	@Autowired
	private TresorierRepository tresorierRepository;
	@Autowired
	private PaiementRepository paiementRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

                    
        @Autowired
    private PaiementBusiness paiementBusiness;
  
        
          @Autowired
    private UserBusiness userBusiness;
    
	public TresorierBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
	
	/**
	 * create Tresorier by using TresorierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<TresorierDto> create(Request<TresorierDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Tresorier-----");

		Response<TresorierDto> response = new Response<TresorierDto>();
		List<Tresorier>        items    = new ArrayList<Tresorier>();
			
		for (TresorierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("idUser", dto.getIdUser());
			fieldsToVerify.put("mobileAccount1", dto.getMobileAccount1());
			fieldsToVerify.put("mobileAccount2", dto.getMobileAccount2());
			fieldsToVerify.put("mobileAccount3", dto.getMobileAccount3());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if tresorier to insert do not exist
			Tresorier existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("tresorier id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if user exist
			User existingUser = null;
			if (Utilities.isValidID(dto.getIdUser())){
				existingUser = userRepository.findOne(dto.getIdUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
			}
				Tresorier entityToSave = TresorierTransformer.INSTANCE.toEntity(dto, existingUser);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Tresorier> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = tresorierRepository.saveAll((Iterable<Tresorier>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("tresorier", locale));
				response.setHasError(true);
				return response;
			}
			List<TresorierDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? TresorierTransformer.INSTANCE.toLiteDtos(itemsSaved) : TresorierTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Tresorier-----");
		return response;
	}

	/**
	 * update Tresorier by using TresorierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<TresorierDto> update(Request<TresorierDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Tresorier-----");

		Response<TresorierDto> response = new Response<TresorierDto>();
		List<Tresorier>        items    = new ArrayList<Tresorier>();
			
		for (TresorierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la tresorier existe
			Tresorier entityToSave = null;
			entityToSave = tresorierRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("tresorier id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if user exist
			if (Utilities.isValidID(dto.getIdUser()) && !entityToSave.getUser().getId().equals(dto.getIdUser())){
				User existingUser = userRepository.findOne(dto.getIdUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setUser(existingUser);
			}
			if (Utilities.isNotBlank(dto.getMobileAccount1()) && !dto.getMobileAccount1().equals(entityToSave.getMobileAccount1())) {
			 				entityToSave.setMobileAccount1(dto.getMobileAccount1());
			}
			if (Utilities.isNotBlank(dto.getMobileAccount2()) && !dto.getMobileAccount2().equals(entityToSave.getMobileAccount2())) {
			 				entityToSave.setMobileAccount2(dto.getMobileAccount2());
			}
			if (Utilities.isNotBlank(dto.getMobileAccount3()) && !dto.getMobileAccount3().equals(entityToSave.getMobileAccount3())) {
			 				entityToSave.setMobileAccount3(dto.getMobileAccount3());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Tresorier> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = tresorierRepository.saveAll((Iterable<Tresorier>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("tresorier", locale));
				response.setHasError(true);
				return response;
			}
			List<TresorierDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? TresorierTransformer.INSTANCE.toLiteDtos(itemsSaved) : TresorierTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Tresorier-----");
		return response;
	}

	/**
	 * delete Tresorier by using TresorierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<TresorierDto> delete(Request<TresorierDto> request, Locale locale)  {
		log.info("----begin delete Tresorier-----");

		Response<TresorierDto> response = new Response<TresorierDto>();
		List<Tresorier>        items    = new ArrayList<Tresorier>();
			
		for (TresorierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la tresorier existe
			Tresorier existingEntity = null;
			existingEntity = tresorierRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("tresorier -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// paiement
			List<Paiement> listOfPaiement = paiementRepository.findByIdTresorier(existingEntity.getId(), false);
			if (listOfPaiement != null && !listOfPaiement.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPaiement.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			tresorierRepository.saveAll((Iterable<Tresorier>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Tresorier-----");
		return response;
	}

	/**
	 * forceDelete Tresorier by using TresorierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<TresorierDto> forceDelete(Request<TresorierDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Tresorier-----");

		Response<TresorierDto> response = new Response<TresorierDto>();
		List<Tresorier>        items    = new ArrayList<Tresorier>();
			
		for (TresorierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la tresorier existe
			Tresorier existingEntity = null;
			existingEntity = tresorierRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("tresorier -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

		// paiement
        List<Paiement> listOfPaiement = paiementRepository.findByIdTresorier(existingEntity.getId(), false);
        if (listOfPaiement != null && !listOfPaiement.isEmpty()){
          Request<PaiementDto> deleteRequest = new Request<PaiementDto>();
          deleteRequest.setDatas(PaiementTransformer.INSTANCE.toDtos(listOfPaiement));
          deleteRequest.setUser(request.getUser());
          Response<PaiementDto> deleteResponse = paiementBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			tresorierRepository.saveAll((Iterable<Tresorier>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Tresorier-----");
		return response;
	}

	/**
	 * get Tresorier by using TresorierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<TresorierDto> getByCriteria(Request<TresorierDto> request, Locale locale)  throws Exception {
		log.info("----begin get Tresorier-----");

		Response<TresorierDto> response = new Response<TresorierDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Tresorier> items 			 = tresorierRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("tresorier", locale));
			response.setHasError(false);
			return response;
		}

		List<TresorierDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? TresorierTransformer.INSTANCE.toLiteDtos(items) : TresorierTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setCount(tresorierRepository.count(request, em, locale));
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Tresorier-----");
		return response;
	}

	/**
	 * get full TresorierDto by using Tresorier as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private TresorierDto getFullInfos(TresorierDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
