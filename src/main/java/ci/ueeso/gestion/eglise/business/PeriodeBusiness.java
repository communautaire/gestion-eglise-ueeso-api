                                                                                    																			

/*
 * Java business for entity table periode 
 * Created on 2019-12-02 ( Time 00:15:35 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Periode;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "periode"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class PeriodeBusiness implements IBasicBusiness<Request<PeriodeDto>, Response<PeriodeDto>> {

	private Response<PeriodeDto> response;
	@Autowired
	private PeriodeRepository periodeRepository;
	@Autowired
	private PaiementRepository paiementRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

                    
        @Autowired
    private PaiementBusiness paiementBusiness;
  
    
          @Autowired
    private UserBusiness userBusiness;
    
	public PeriodeBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
	
	/**
	 * create Periode by using PeriodeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PeriodeDto> create(Request<PeriodeDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Periode-----");

		Response<PeriodeDto> response = new Response<PeriodeDto>();
		List<Periode>        items    = new ArrayList<Periode>();
			
		for (PeriodeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("datePaiement", dto.getDatePaiement());
			fieldsToVerify.put("dayofweek", dto.getDayofweek());
			fieldsToVerify.put("dayofmonth", dto.getDayofmonth());
			fieldsToVerify.put("dayofyear", dto.getDayofyear());
			fieldsToVerify.put("weekofmonth", dto.getWeekofmonth());
			fieldsToVerify.put("weekofyear", dto.getWeekofyear());
			fieldsToVerify.put("month", dto.getMonth());
			fieldsToVerify.put("trimester", dto.getTrimester());
			fieldsToVerify.put("semester", dto.getSemester());
			fieldsToVerify.put("year", dto.getYear());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if periode to insert do not exist
			Periode existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("periode id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

				Periode entityToSave = PeriodeTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Periode> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = periodeRepository.saveAll((Iterable<Periode>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("periode", locale));
				response.setHasError(true);
				return response;
			}
			List<PeriodeDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PeriodeTransformer.INSTANCE.toLiteDtos(itemsSaved) : PeriodeTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Periode-----");
		return response;
	}

	/**
	 * update Periode by using PeriodeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PeriodeDto> update(Request<PeriodeDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Periode-----");

		Response<PeriodeDto> response = new Response<PeriodeDto>();
		List<Periode>        items    = new ArrayList<Periode>();
			
		for (PeriodeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la periode existe
			Periode entityToSave = null;
			entityToSave = periodeRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("periode id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.notBlank(dto.getDatePaiement())) {
				entityToSave.setDatePaiement(dateFormat.parse(dto.getDatePaiement()));
			}
			if (dto.getDayofweek() != null && dto.getDayofweek() > 0) {
				entityToSave.setDayofweek(dto.getDayofweek());
			}
			if (dto.getDayofmonth() != null && dto.getDayofmonth() > 0) {
				entityToSave.setDayofmonth(dto.getDayofmonth());
			}
			if (dto.getDayofyear() != null && dto.getDayofyear() > 0) {
				entityToSave.setDayofyear(dto.getDayofyear());
			}
			if (dto.getWeekofmonth() != null && dto.getWeekofmonth() > 0) {
				entityToSave.setWeekofmonth(dto.getWeekofmonth());
			}
			if (dto.getWeekofyear() != null && dto.getWeekofyear() > 0) {
				entityToSave.setWeekofyear(dto.getWeekofyear());
			}
			if (dto.getMonth() != null && dto.getMonth() > 0) {
				entityToSave.setMonth(dto.getMonth());
			}
			if (dto.getTrimester() != null && dto.getTrimester() > 0) {
				entityToSave.setTrimester(dto.getTrimester());
			}
			if (dto.getSemester() != null && dto.getSemester() > 0) {
				entityToSave.setSemester(dto.getSemester());
			}
			if (dto.getYear() != null && dto.getYear() > 0) {
				entityToSave.setYear(dto.getYear());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Periode> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = periodeRepository.saveAll((Iterable<Periode>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("periode", locale));
				response.setHasError(true);
				return response;
			}
			List<PeriodeDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PeriodeTransformer.INSTANCE.toLiteDtos(itemsSaved) : PeriodeTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Periode-----");
		return response;
	}

	/**
	 * delete Periode by using PeriodeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PeriodeDto> delete(Request<PeriodeDto> request, Locale locale)  {
		log.info("----begin delete Periode-----");

		Response<PeriodeDto> response = new Response<PeriodeDto>();
		List<Periode>        items    = new ArrayList<Periode>();
			
		for (PeriodeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la periode existe
			Periode existingEntity = null;
			existingEntity = periodeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("periode -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// paiement
			List<Paiement> listOfPaiement = paiementRepository.findByIdDatePaiement(existingEntity.getId(), false);
			if (listOfPaiement != null && !listOfPaiement.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPaiement.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			periodeRepository.saveAll((Iterable<Periode>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Periode-----");
		return response;
	}

	/**
	 * forceDelete Periode by using PeriodeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PeriodeDto> forceDelete(Request<PeriodeDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Periode-----");

		Response<PeriodeDto> response = new Response<PeriodeDto>();
		List<Periode>        items    = new ArrayList<Periode>();
			
		for (PeriodeDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la periode existe
			Periode existingEntity = null;
			existingEntity = periodeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("periode -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

		// paiement
        List<Paiement> listOfPaiement = paiementRepository.findByIdDatePaiement(existingEntity.getId(), false);
        if (listOfPaiement != null && !listOfPaiement.isEmpty()){
          Request<PaiementDto> deleteRequest = new Request<PaiementDto>();
          deleteRequest.setDatas(PaiementTransformer.INSTANCE.toDtos(listOfPaiement));
          deleteRequest.setUser(request.getUser());
          Response<PaiementDto> deleteResponse = paiementBusiness.delete(deleteRequest, locale);
          if(deleteResponse.isHasError()){
            response.setStatus(deleteResponse.getStatus());
            response.setHasError(true);
            return response;
          }
        }


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			periodeRepository.saveAll((Iterable<Periode>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Periode-----");
		return response;
	}

	/**
	 * get Periode by using PeriodeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PeriodeDto> getByCriteria(Request<PeriodeDto> request, Locale locale)  throws Exception {
		log.info("----begin get Periode-----");

		Response<PeriodeDto> response = new Response<PeriodeDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Periode> items 			 = periodeRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("periode", locale));
			response.setHasError(false);
			return response;
		}

		List<PeriodeDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PeriodeTransformer.INSTANCE.toLiteDtos(items) : PeriodeTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setCount(periodeRepository.count(request, em, locale));
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Periode-----");
		return response;
	}

	/**
	 * get full PeriodeDto by using Periode as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private PeriodeDto getFullInfos(PeriodeDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
