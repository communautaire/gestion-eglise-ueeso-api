

/*
 * Java business for entity table pays 
 * Created on 2019-11-30 ( Time 18:43:48 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Pays;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "pays"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class PaysBusiness implements IBasicBusiness<Request<PaysDto>, Response<PaysDto>> {

	private Response<PaysDto> response;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	@Autowired
	private AdresseBusiness adresseBusiness;
	@Autowired
	private UserBusiness userBusiness;
	
	public PaysBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Pays by using PaysDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaysDto> create(Request<PaysDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Pays-----");

		Response<PaysDto> response = new Response<PaysDto>();
		List<Pays>        items    = new ArrayList<Pays>();

		for (PaysDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if pays to insert do not exist
			Pays existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("pays id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// verif unique code in db
			existingEntity = paysRepository.findByCode(dto.getCode(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("pays code -> " + dto.getCode(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique code in items to save
			if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" code ", locale));
				response.setHasError(true);
				return response;
			}

			// verif unique libelle in db
			existingEntity = paysRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("pays libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			Pays entityToSave = PaysTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Pays> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = paysRepository.saveAll((Iterable<Pays>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("pays", locale));
				response.setHasError(true);
				return response;
			}
			List<PaysDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PaysTransformer.INSTANCE.toLiteDtos(itemsSaved) : PaysTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Pays-----");
		return response;
	}

	/**
	 * update Pays by using PaysDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaysDto> update(Request<PaysDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Pays-----");

		Response<PaysDto> response = new Response<PaysDto>();
		List<Pays>        items    = new ArrayList<Pays>();

		for (PaysDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la pays existe
			Pays entityToSave = null;
			entityToSave = paysRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("pays id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			if (Utilities.isNotBlank(dto.getCode()) && !dto.getCode().equals(entityToSave.getCode())) {
				Pays existingEntity = paysRepository.findByCode(dto.getCode(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getCode().equalsIgnoreCase(dto.getCode()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du code '" + dto.getCode()+"' pour les payss", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCode(dto.getCode());
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				Pays existingEntity = paysRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("pays -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les payss", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Pays> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = paysRepository.saveAll((Iterable<Pays>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("pays", locale));
				response.setHasError(true);
				return response;
			}
			List<PaysDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PaysTransformer.INSTANCE.toLiteDtos(itemsSaved) : PaysTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Pays-----");
		return response;
	}

	/**
	 * delete Pays by using PaysDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaysDto> delete(Request<PaysDto> request, Locale locale)  {
		log.info("----begin delete Pays-----");

		Response<PaysDto> response = new Response<PaysDto>();
		List<Pays>        items    = new ArrayList<Pays>();

		for (PaysDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la pays existe
			Pays existingEntity = null;
			existingEntity = paysRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdPays(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAdresse.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// user
			List<User> listOfUser = userRepository.findByIdPays(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfUser.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			paysRepository.saveAll((Iterable<Pays>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Pays-----");
		return response;
	}

	/**
	 * forceDelete Pays by using PaysDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaysDto> forceDelete(Request<PaysDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Pays-----");

		Response<PaysDto> response = new Response<PaysDto>();
		List<Pays>        items    = new ArrayList<Pays>();

		for (PaysDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la pays existe
			Pays existingEntity = null;
			existingEntity = paysRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("pays -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdPays(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				Request<AdresseDto> deleteRequest = new Request<AdresseDto>();
				deleteRequest.setDatas(AdresseTransformer.INSTANCE.toDtos(listOfAdresse));
				deleteRequest.setUser(request.getUser());
				Response<AdresseDto> deleteResponse = adresseBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// user
			List<User> listOfUser = userRepository.findByIdPays(existingEntity.getId(), false);
			if (listOfUser != null && !listOfUser.isEmpty()){
				Request<UserDto> deleteRequest = new Request<UserDto>();
				deleteRequest.setDatas(UserTransformer.INSTANCE.toDtos(listOfUser));
				deleteRequest.setUser(request.getUser());
				Response<UserDto> deleteResponse = userBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}


			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			paysRepository.saveAll((Iterable<Pays>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Pays-----");
		return response;
	}

	/**
	 * get Pays by using PaysDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaysDto> getByCriteria(Request<PaysDto> request, Locale locale)  throws Exception {
		log.info("----begin get Pays-----");

		Response<PaysDto> response = new Response<PaysDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Pays> items 			 = paysRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("pays", locale));
			response.setHasError(false);
			return response;
		}

		List<PaysDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PaysTransformer.INSTANCE.toLiteDtos(items) : PaysTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(paysRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Pays-----");
		return response;
	}

	/**
	 * get full PaysDto by using Pays as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private PaysDto getFullInfos(PaysDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
