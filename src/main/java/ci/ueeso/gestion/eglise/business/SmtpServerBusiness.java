

/*
 * Java business for entity table smtp_server 
 * Created on 2020-01-24 ( Time 19:04:40 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.SmtpServer;
import ci.ueeso.gestion.eglise.dao.repository.SmtpServerRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.SmtpServerDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.SmtpServerTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "smtp_server"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class SmtpServerBusiness implements IBasicBusiness<Request<SmtpServerDto>, Response<SmtpServerDto>> {

	private Response<SmtpServerDto> response;
	@Autowired
	private SmtpServerRepository smtpServerRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;
	@Autowired
	private MailBusiness mailBusiness;

	public SmtpServerBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create SmtpServer by using SmtpServerDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SmtpServerDto> create(Request<SmtpServerDto> request, Locale locale)  throws ParseException {
		log.info("----begin create SmtpServer-----");

		Response<SmtpServerDto> response = new Response<SmtpServerDto>();
		List<SmtpServer>        items    = new ArrayList<SmtpServer>();

		for (SmtpServerDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			//			fieldsToVerify.put("description", dto.getDescription());
			fieldsToVerify.put("host", dto.getHost());
			//			fieldsToVerify.put("login", dto.getLogin());
			//			fieldsToVerify.put("password", dto.getPassword());
			//			fieldsToVerify.put("port", dto.getPort());
			//			fieldsToVerify.put("smtpUser", dto.getSmtpUser());
			fieldsToVerify.put("auth", dto.getAuth());
			//			fieldsToVerify.put("enableSsl", dto.getEnableSsl());
			//			fieldsToVerify.put("sslProtocols", dto.getSslProtocols());
			//			fieldsToVerify.put("senderUsername", dto.getSenderUsername());
			//			fieldsToVerify.put("enableStarttls", dto.getEnableStarttls());
			//			fieldsToVerify.put("senderEmail", dto.getSenderEmail());
			//			fieldsToVerify.put("isStarttlsRequired", dto.getIsStarttlsRequired());
			//			fieldsToVerify.put("hostsToTrust", dto.getHostsToTrust());
			//			fieldsToVerify.put("isSmtpsProtocol", dto.getIsSmtpsProtocol());
			fieldsToVerify.put("testEmail", dto.getTestEmail());
			fieldsToVerify.put("isActive", dto.getIsActive());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verify if smtpServer to insert do not exist
			SmtpServer existingEntity = smtpServerRepository.findSmtpServerByHost(dto.getHost(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("Un serveur d'envoi de mail de nom d'hôte '" + dto.getHost()+"' existe déjà", locale));
				response.setHasError(true);
				return response;
			}
			if (items.stream().anyMatch(a->a.getHost().equalsIgnoreCase(dto.getHost()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du hostname '" + dto.getHost()+"' pour les smtpServerss", locale));
				response.setHasError(true);
				return response;
			}

			if(Utilities.isTrue(dto.getAuth())) {
				fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
			}
			else {
				dto.setLogin(null);
				dto.setPassword(null);
			}

			/*
			existingEntity = smtpServerRepository.findByLogin(dto.getLogin());
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("smtpServer -> " + dto.getLogin(), locale));
				response.setHasError(true);
				return response;
			}
			if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()))) {
				response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les smtpServers", locale));
				response.setHasError(true);
				return response;
			}
			 */
			if (dto.getPort() != null) {
				if (dto.getPort() <= 0) {
					response.setStatus(functionalError.INVALID_FORMAT(dto.getPort()+" n'est pas un port valide", locale));
					response.setHasError(true);
					return response;
				}
			}
			else {
				dto.setPort(25);
			}

			if (Utilities.notBlank(dto.getSenderEmail())) {
				if (!Utilities.isValidEmailAddress(dto.getSenderEmail())) {
					response.setStatus(functionalError.INVALID_FORMAT(dto.getSenderEmail()+" n'est pas un email valide", locale));
					response.setHasError(true);
					return response;
				}
			}

			if(Utilities.notBlank(dto.getTestEmail())) {
				if (!Utilities.isValidEmailAddress(dto.getTestEmail())) {
					response.setStatus(functionalError.INVALID_FORMAT(dto.getTestEmail()+" n'est pas un email valide", locale));
					response.setHasError(true);
					return response;
				}
			}

			//HostToTrust
			String hostsToTrust = null;
			if(Utilities.isNotEmpty(dto.getDatasHostsToTrust())) {
				for (String htt : dto.getDatasHostsToTrust()) {
					htt = htt.replaceAll(" ", "");
					hostsToTrust += htt+" ";
				}
			}
			else {
				hostsToTrust = dto.getHostsToTrust();
			}
			if(Utilities.notBlank(hostsToTrust)) {
				hostsToTrust = hostsToTrust.trim();
				if(hostsToTrust.contains("*")) {
					hostsToTrust = "*";
				}
			}
			dto.setHostsToTrust(hostsToTrust);

			//SSL protocols To use
			String sslProtocols = null;
			if(Utilities.isNotEmpty(dto.getDatasSslProtocols())) {
				for (String sslp : dto.getDatasSslProtocols()) {
					sslp = sslp.replaceAll(" ", "");
					sslProtocols += sslp+" ";
				}
			}
			else {
				sslProtocols = dto.getSslProtocols();
			}
			if(Utilities.notBlank(sslProtocols)) {
				sslProtocols = sslProtocols.trim();
			}
			dto.setSslProtocols(sslProtocols);

			if(dto.getIsActive() != null && dto.getIsActive()) {
				List<SmtpServer> activeServers =  smtpServerRepository.findByIsActive(true, false);
				if(activeServers != null && !activeServers.isEmpty()) {
					activeServers.forEach(s->{
						s.setIsActive(false);
					});
					activeServers = smtpServerRepository.saveAll(activeServers);
				}
			}

			SmtpServer entityToSave = SmtpServerTransformer.INSTANCE.toEntity(dto);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			SmtpServer entitySaved = smtpServerRepository.save(entityToSave);
			if (entitySaved == null ) {
				response.setStatus(functionalError.SAVE_FAIL("smtpServers", locale));
				response.setHasError(true);
				return response;
			}
			items.add(entitySaved);

			//send test email
			Request<Map<String, String>> dataRequest = new Request<Map<String, String>>();
			Map<String, String> data = new HashMap<String, String>();
			data.put(MailBusiness.EMAIL_ADDR, entitySaved.getTestEmail());
			data.put(MailBusiness.USER_NAME, entitySaved.getSenderUsername() != null ? entitySaved.getSenderUsername() : "TESTEUR");
			dataRequest.setDatas(Arrays.asList(data));
			dataRequest.setSmtpServerId(entitySaved.getId());
			Response<Map<String, String>> dataResponse = mailBusiness.sendTestEmail(dataRequest, locale);
			if(dataResponse.isHasError()) {
				response.setStatus(functionalError.INVALID_FORMAT("L'envoi du mail de test à l'adresse '"+entitySaved.getTestEmail()+"' a échoué avec ce paramétrage du serveur SMTP: "+dataResponse.getStatus().getMessage(), locale));
				response.setHasError(true);
				return response;
			}
		}

		if (!items.isEmpty()) {
			List<SmtpServer> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = smtpServerRepository.saveAll((Iterable<SmtpServer>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("smtpServer", locale));
				response.setHasError(true);
				return response;
			}
			List<SmtpServerDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SmtpServerTransformer.INSTANCE.toLiteDtos(itemsSaved) : SmtpServerTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create SmtpServer-----");
		return response;
	}

	/**
	 * update SmtpServer by using SmtpServerDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SmtpServerDto> update(Request<SmtpServerDto> request, Locale locale)  throws ParseException {
		log.info("----begin update SmtpServer-----");

		Response<SmtpServerDto> response = new Response<SmtpServerDto>();
		List<SmtpServer>        items    = new ArrayList<SmtpServer>();

		for (SmtpServerDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}


			// Verifier si la smtpServer existe
			SmtpServer entityToSave = smtpServerRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("smtpServer -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();
			boolean sendTestEmail = false;
			
			entityToSave.setDescription(dto.getDescription());
			
			if (Utilities.notBlank(dto.getHost())) {
				SmtpServer existingEntity = smtpServerRepository.findSmtpServerByHost(dto.getHost(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("smtpServers -> " + dto.getHost(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getHost().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSave.getId()))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du hostname '" + dto.getHost()+"' pour les smtpServerss", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setHost(dto.getHost());
				sendTestEmail = true;
			}
			
			if (dto.getAuth() != null) {
				entityToSave.setAuth(dto.getAuth());
			}
			if(Utilities.isTrue(entityToSave.getAuth())) {
				fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
			}
			else {
				dto.setLogin(null);
				dto.setPassword(null);
			}
//			if (dto.getLogin() != null && !dto.getLogin().isEmpty()) {
//            		/*
//				SmtpServer existingEntity = smtpServersRepository.findByLogin(dto.getLogin());
//    				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
//    					response.setStatus(functionalError.DATA_EXIST("smtpServers -> " + dto.getLogin()));
//    					response.setHasError(true);
//    					return response;
//    				}
//    				if (items.stream().anyMatch(a->a.getLogin().equalsIgnoreCase(dto.getLogin()) && !a.getId().equals(entityToSave.getId()))) {
//    					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du login '" + dto.getLogin()+"' pour les smtpServerss"));
//    					response.setHasError(true);
//    					return response;
//    				}
//    				*/
//    				entityToSave.setLogin(dto.getLogin());
//			}
			if(Utilities.isNotBlank(entityToSave.getLogin()) && Utilities.isBlank(dto.getLogin()) || 
				Utilities.isNotBlank(dto.getLogin()) && Utilities.isBlank(entityToSave.getLogin()) ||
				Utilities.isNotBlank(dto.getLogin()) && Utilities.isNotBlank(entityToSave.getLogin()) && !entityToSave.getLogin().equals(dto.getLogin())
			) 
			{
				entityToSave.setLogin(dto.getLogin());
				sendTestEmail = true;
			}

			if(Utilities.isNotBlank(entityToSave.getPassword()) && Utilities.isBlank(dto.getPassword()) || 
					Utilities.isNotBlank(dto.getPassword()) && Utilities.isBlank(entityToSave.getPassword()) ||
					Utilities.isNotBlank(dto.getPassword()) && Utilities.isNotBlank(entityToSave.getPassword()) && !entityToSave.getPassword().equals(dto.getPassword())
			) 
			{
				entityToSave.setPassword(dto.getPassword());
				sendTestEmail = true;
			}
			
			if(dto.getPort() != null && !entityToSave.getPort().equals(dto.getPort())){
				if (dto.getPort() <= 0) {
					response.setStatus(functionalError.INVALID_FORMAT(dto.getPort()+" n'est pas un port valide", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setPort(dto.getPort());
				sendTestEmail = true;
			}
			
			if(Utilities.isNotBlank(entityToSave.getSmtpUser()) && Utilities.isBlank(dto.getSmtpUser()) || 
					Utilities.isNotBlank(dto.getSmtpUser()) && Utilities.isBlank(entityToSave.getSmtpUser()) ||
					Utilities.isNotBlank(dto.getSmtpUser()) && Utilities.isNotBlank(entityToSave.getSmtpUser()) && !entityToSave.getSmtpUser().equals(dto.getSmtpUser())
			) 
			{
				entityToSave.setSmtpUser(dto.getSmtpUser());
				sendTestEmail = true;
			}
			
			if (dto.getEnableSsl() != null && !dto.getEnableSsl().equals(entityToSave.getEnableSsl())) {
				entityToSave.setEnableSsl(dto.getEnableSsl());
				sendTestEmail = true;
			}

			entityToSave.setSenderUsername(dto.getSenderUsername());
			
			if (dto.getEnableStarttls() != null && !dto.getEnableStarttls().equals(entityToSave.getEnableStarttls())) {
				entityToSave.setEnableStarttls(dto.getEnableStarttls());
				sendTestEmail = true;
			}

			if (Utilities.isNotBlank(dto.getSenderEmail()) && !dto.getSenderEmail().equals(entityToSave.getSenderEmail())) {
				if (!Utilities.isValidEmailAddress(dto.getSenderEmail())) {
					response.setStatus(functionalError.INVALID_FORMAT(dto.getSenderEmail()+" n'est pas un email valide", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setSenderEmail(dto.getSenderEmail());
				sendTestEmail = true;
			}
			if (dto.getIsStarttlsRequired() != null && !dto.getIsStarttlsRequired().equals(entityToSave.getIsStarttlsRequired())) {
				entityToSave.setIsStarttlsRequired(dto.getIsStarttlsRequired());
				sendTestEmail = true;
			}
			if (dto.getIsSmtpsProtocol() != null && !dto.getIsSmtpsProtocol().equals(entityToSave.getIsSmtpsProtocol())) {
				entityToSave.setIsSmtpsProtocol(dto.getIsSmtpsProtocol());
				sendTestEmail = true;
			}
			if (Utilities.notBlank(dto.getTestEmail()) && !dto.getTestEmail().equals(entityToSave.getTestEmail())) {
				if (!Utilities.isValidEmailAddress(dto.getTestEmail())) {
					response.setStatus(functionalError.INVALID_FORMAT(dto.getTestEmail()+" n'est pas un email valide", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setTestEmail(dto.getTestEmail());
				sendTestEmail = true;
			}

			//HostToTrust
			String hostsToTrust = null;
			if(Utilities.isNotEmpty(dto.getDatasHostsToTrust())) {
				for (String htt : dto.getDatasHostsToTrust()) {
					htt = htt.replaceAll(" ", "");
					hostsToTrust += htt+" ";
				}
			}
			else {
				hostsToTrust = dto.getHostsToTrust();
			}
			if(Utilities.notBlank(hostsToTrust)) {
				hostsToTrust = hostsToTrust.trim();
				if(hostsToTrust.contains("*")) {
					hostsToTrust = "*";
				}
			}
			if(Utilities.isNotBlank(entityToSave.getHostsToTrust()) && Utilities.isBlank(hostsToTrust) || 
					Utilities.isNotBlank(hostsToTrust) && Utilities.isBlank(entityToSave.getHostsToTrust()) ||
					Utilities.isNotBlank(hostsToTrust) && Utilities.isNotBlank(entityToSave.getHostsToTrust()) && !entityToSave.getHostsToTrust().equals(hostsToTrust)
			) 
			{
				entityToSave.setHostsToTrust(hostsToTrust);
				sendTestEmail = true;
			}
			
			//SSL protocols To use
			String sslProtocols = null;
			if(Utilities.isNotEmpty(dto.getDatasSslProtocols())) {
				for (String sslp : dto.getDatasSslProtocols()) {
					sslp = sslp.replaceAll(" ", "");
					sslProtocols += sslp+" ";
				}
			}
			else {
				sslProtocols = dto.getSslProtocols();
			}
			if(Utilities.notBlank(sslProtocols)) {
				sslProtocols = sslProtocols.trim();
			}
			if(Utilities.isNotBlank(entityToSave.getSslProtocols()) && Utilities.isBlank(sslProtocols) || 
					Utilities.isNotBlank(sslProtocols) && Utilities.isBlank(entityToSave.getSslProtocols()) ||
					Utilities.isNotBlank(sslProtocols) && Utilities.isNotBlank(entityToSave.getSslProtocols()) && !entityToSave.getSslProtocols().equals(sslProtocols)
			)
			{
				dto.setSslProtocols(sslProtocols);
				sendTestEmail = true;
			}

			if(dto.getIsActive() != null) {
				if(dto.getIsActive()) {
					List<SmtpServer> activeServers =  smtpServerRepository.findByIsActive(true, false);
					if(activeServers != null && !activeServers.isEmpty()) {
						activeServers.forEach(s->{
							s.setIsActive(false);
						});
						activeServers = smtpServerRepository.saveAll(activeServers);
						sendTestEmail = true;
					}
				}
				else {
					sendTestEmail = false;
				}
				entityToSave.setIsActive(dto.getIsActive());
			}
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			
			//send test email
			if(sendTestEmail) {
				Request<Map<String, String>> dataRequest = new Request<Map<String, String>>();
				Map<String, String> data = new HashMap<String, String>();
				data.put(MailBusiness.EMAIL_ADDR, entityToSave.getTestEmail());
				data.put(MailBusiness.USER_NAME, entityToSave.getSenderUsername() != null ? entityToSave.getSenderUsername() : "TESTEUR");
				dataRequest.setDatas(Arrays.asList(data));
				dataRequest.setSmtpServerId(entityToSave.getId());
				Response<Map<String, String>> dataResponse = mailBusiness.sendTestEmail(dataRequest, locale);
				if(dataResponse.isHasError()) {
					response.setStatus(technicalError.INTERN_ERROR("L'envoi du mail de test à l'adresse '"+entityToSave.getTestEmail()+"' a échoué avec ce paramétrage du serveur SMTP: "+dataResponse.getStatus().getMessage(), locale));
					response.setHasError(true);
					return response;
				}
			}
			
			SmtpServer entitySaved = smtpServerRepository.save(entityToSave);
			if (entitySaved == null ) {
				response.setStatus(functionalError.SAVE_FAIL("smtpServers", locale));
				response.setHasError(true);
				return response;
			}
			items.add(entitySaved);
		}

		if (!items.isEmpty()) {
			List<SmtpServer> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = smtpServerRepository.saveAll((Iterable<SmtpServer>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("smtpServer", locale));
				response.setHasError(true);
				return response;
			}
			List<SmtpServerDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SmtpServerTransformer.INSTANCE.toLiteDtos(itemsSaved) : SmtpServerTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update SmtpServer-----");
		return response;
	}

	/**
	 * delete SmtpServer by using SmtpServerDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SmtpServerDto> delete(Request<SmtpServerDto> request, Locale locale)  {
		log.info("----begin delete SmtpServer-----");

		Response<SmtpServerDto> response = new Response<SmtpServerDto>();
		List<SmtpServer>        items    = new ArrayList<SmtpServer>();

		for (SmtpServerDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la smtpServer existe
			SmtpServer existingEntity = smtpServerRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("smtpServer -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			smtpServerRepository.saveAll((Iterable<SmtpServer>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete SmtpServer-----");
		return response;
	}

	/**
	 * forceDelete SmtpServer by using SmtpServerDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SmtpServerDto> forceDelete(Request<SmtpServerDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete SmtpServer-----");

		Response<SmtpServerDto> response = new Response<SmtpServerDto>();
		List<SmtpServer>        items    = new ArrayList<SmtpServer>();

		for (SmtpServerDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la smtpServer existe
			SmtpServer existingEntity = smtpServerRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("smtpServer -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			smtpServerRepository.saveAll((Iterable<SmtpServer>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete SmtpServer-----");
		return response;
	}

	/**
	 * get SmtpServer by using SmtpServerDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<SmtpServerDto> getByCriteria(Request<SmtpServerDto> request, Locale locale)  throws Exception {
		log.info("----begin get SmtpServer-----");

		Response<SmtpServerDto> response = new Response<SmtpServerDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<SmtpServer> items 			 = smtpServerRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("smtpServer", locale));
			response.setHasError(false);
			return response;
		}

		List<SmtpServerDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SmtpServerTransformer.INSTANCE.toLiteDtos(items) : SmtpServerTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(smtpServerRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get SmtpServer-----");
		return response;
	}

	/**
	 * get full SmtpServerDto by using SmtpServer as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private SmtpServerDto getFullInfos(SmtpServerDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if(dto.getHostsToTrust() != null && !dto.getHostsToTrust().isEmpty()) {
			dto.setDatasHostsToTrust(Arrays.asList(dto.getHostsToTrust().split(" ")));
		}
		if(dto.getSslProtocols() != null && !dto.getSslProtocols().isEmpty()) {
			dto.setDatasSslProtocols(Arrays.asList(dto.getSslProtocols().split(" ")));
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
