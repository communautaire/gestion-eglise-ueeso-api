

/*
 * Java business for entity table ville 
 * Created on 2020-01-18 ( Time 17:01:44 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Ville;
import ci.ueeso.gestion.eglise.dao.entity.Pays;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "ville"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class VilleBusiness implements IBasicBusiness<Request<VilleDto>, Response<VilleDto>> {

	private Response<VilleDto> response;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private AdresseBusiness adresseBusiness;


	@Autowired
	private CommuneBusiness communeBusiness;


	@Autowired
	private UserBusiness userBusiness;

	public VilleBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Ville by using VilleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<VilleDto> create(Request<VilleDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Ville-----");

		Response<VilleDto> response = new Response<VilleDto>();
		List<Ville>        items    = new ArrayList<Ville>();

		for (VilleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("libelle", dto.getLibelle());
			/*fieldsToVerify.put("longitude", dto.getLongitude());
			fieldsToVerify.put("latitude", dto.getLatitude());
			fieldsToVerify.put("superficie", dto.getSuperficie());*/
			fieldsToVerify.put("idPays", dto.getIdPays());
			/*fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());*/
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if ville to insert do not exist
			Ville existingEntity = null;
			/*if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("ville id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}*/

			// verif unique libelle in db
			existingEntity = villeRepository.findByLibelle(dto.getLibelle(), false);
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("ville libelle -> " + dto.getLibelle(), locale));
				response.setHasError(true);
				return response;
			}
			// verif unique libelle in items to save
			if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
				response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
				response.setHasError(true);
				return response;
			}

			// Verify if pays exist
			Pays existingPays = null;
			if (Utilities.isValidID(dto.getIdPays())){
				existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
			}
			Ville entityToSave = VilleTransformer.INSTANCE.toEntity(dto, existingPays);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}
		if (!items.isEmpty()) {
			List<Ville> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = villeRepository.saveAll((Iterable<Ville>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("ville", locale));
				response.setHasError(true);
				return response;
			}
			List<VilleDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? VilleTransformer.INSTANCE.toLiteDtos(itemsSaved) : VilleTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}
		log.info("----end create Ville-----");
		return response;
	}

	/**
	 * update Ville by using VilleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<VilleDto> update(Request<VilleDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Ville-----");

		Response<VilleDto> response = new Response<VilleDto>();
		List<Ville>        items    = new ArrayList<Ville>();

		for (VilleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la ville existe
			Ville entityToSave = null;
			entityToSave = villeRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("ville id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if pays exist
			if (Utilities.isValidID(dto.getIdPays()) && !entityToSave.getPays().getId().equals(dto.getIdPays())){
				Pays existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setPays(existingPays);
			}
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {

				Ville existingEntity = villeRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSave.getId())) {
					response.setStatus(functionalError.DATA_EXIST("ville -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				if (items.stream().anyMatch(a->a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du libelle '" + dto.getLibelle()+"' pour les villes", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLibelle(dto.getLibelle());
			}

			if (dto.getLongitude() != null && dto.getLongitude() > 0) {
				entityToSave.setLongitude(dto.getLongitude());
			}
			if (dto.getLatitude() != null && dto.getLatitude() > 0) {
				entityToSave.setLatitude(dto.getLatitude());
			}
			if (dto.getSuperficie() != null && dto.getSuperficie() > 0) {
				entityToSave.setSuperficie(dto.getSuperficie());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Ville> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = villeRepository.saveAll((Iterable<Ville>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("ville", locale));
				response.setHasError(true);
				return response;
			}
			List<VilleDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? VilleTransformer.INSTANCE.toLiteDtos(itemsSaved) : VilleTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Ville-----");
		return response;
	}

	/**
	 * delete Ville by using VilleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<VilleDto> delete(Request<VilleDto> request, Locale locale)  {
		log.info("----begin delete Ville-----");

		Response<VilleDto> response = new Response<VilleDto>();
		List<Ville>        items    = new ArrayList<Ville>();

		for (VilleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la ville existe
			Ville existingEntity = null;
			existingEntity = villeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdVille(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAdresse.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			// commune
			List<Commune> listOfCommune = communeRepository.findByIdVille(existingEntity.getId(), false);
			if (listOfCommune != null && !listOfCommune.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfCommune.size() + ")", locale));
				response.setHasError(true);
				return response;
			}
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			villeRepository.saveAll((Iterable<Ville>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Ville-----");
		return response;
	}

	/**
	 * forceDelete Ville by using VilleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<VilleDto> forceDelete(Request<VilleDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Ville-----");

		Response<VilleDto> response = new Response<VilleDto>();
		List<Ville>        items    = new ArrayList<Ville>();

		for (VilleDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}
			// Verifier si la ville existe
			Ville existingEntity = null;
			existingEntity = villeRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("ville -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdVille(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				Request<AdresseDto> deleteRequest = new Request<AdresseDto>();
				deleteRequest.setDatas(AdresseTransformer.INSTANCE.toDtos(listOfAdresse));
				deleteRequest.setUser(request.getUser());
				Response<AdresseDto> deleteResponse = adresseBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			// commune
			List<Commune> listOfCommune = communeRepository.findByIdVille(existingEntity.getId(), false);
			if (listOfCommune != null && !listOfCommune.isEmpty()){
				Request<CommuneDto> deleteRequest = new Request<CommuneDto>();
				deleteRequest.setDatas(CommuneTransformer.INSTANCE.toDtos(listOfCommune));
				deleteRequest.setUser(request.getUser());
				Response<CommuneDto> deleteResponse = communeBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			villeRepository.saveAll((Iterable<Ville>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Ville-----");
		return response;
	}

	/**
	 * get Ville by using VilleDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<VilleDto> getByCriteria(Request<VilleDto> request, Locale locale)  throws Exception {
		log.info("----begin get Ville-----");

		Response<VilleDto> response = new Response<VilleDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Ville> items 			 = villeRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("ville", locale));
			response.setHasError(false);
			return response;
		}

		List<VilleDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? VilleTransformer.INSTANCE.toLiteDtos(items) : VilleTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(villeRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Ville-----");
		return response;
	}

	/**
	 * get full VilleDto by using Ville as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private VilleDto getFullInfos(VilleDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
