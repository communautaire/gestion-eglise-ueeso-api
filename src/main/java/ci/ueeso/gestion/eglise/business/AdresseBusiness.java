

/*
 * Java business for entity table adresse 
 * Created on 2019-11-30 ( Time 18:43:43 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Adresse;
import ci.ueeso.gestion.eglise.dao.entity.Commune;
import ci.ueeso.gestion.eglise.dao.entity.User;
import ci.ueeso.gestion.eglise.dao.entity.Pays;
import ci.ueeso.gestion.eglise.dao.entity.Ville;
import ci.ueeso.gestion.eglise.dao.entity.Quartier;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "adresse"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class AdresseBusiness implements IBasicBusiness<Request<AdresseDto>, Response<AdresseDto>> {

	private Response<AdresseDto> response;
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private QuartierRepository quartierRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private UserBusiness userBusiness;

	public AdresseBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Adresse by using AdresseDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<AdresseDto> create(Request<AdresseDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Adresse-----");

		Response<AdresseDto> response = new Response<AdresseDto>();
		List<Adresse>        items    = new ArrayList<Adresse>();

		for (AdresseDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("idUser", dto.getIdUser());
			fieldsToVerify.put("idPays", dto.getIdPays());
			fieldsToVerify.put("idVille", dto.getIdVille());
			fieldsToVerify.put("idCommune", dto.getIdCommune());
			fieldsToVerify.put("idQuartier", dto.getIdQuartier());
			fieldsToVerify.put("adressePostale", dto.getAdressePostale());
			/*fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());*/
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if adresse to insert do not exist
			Adresse existingEntity = null;
			/*if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("adresse id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}*/

			// Verify if commune exist
			Commune existingCommune = null;
			if (Utilities.isValidID(dto.getIdCommune())){
				existingCommune = communeRepository.findOne(dto.getIdCommune(), false);
				if (existingCommune == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("commune idCommune -> " + dto.getIdCommune(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if user exist
			User existingUser = null;
			if (Utilities.isValidID(dto.getIdUser())){
				existingUser = userRepository.findOne(dto.getIdUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if pays exist
			Pays existingPays = null;
			if (Utilities.isValidID(dto.getIdPays())){
				existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if ville exist
			Ville existingVille = null;
			if (Utilities.isValidID(dto.getIdVille())){
				existingVille = villeRepository.findOne(dto.getIdVille(), false);
				if (existingVille == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville idVille -> " + dto.getIdVille(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if quartier exist
			Quartier existingQuartier = null;
			if (Utilities.isValidID(dto.getIdQuartier())){
				existingQuartier = quartierRepository.findOne(dto.getIdQuartier(), false);
				if (existingQuartier == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("quartier idQuartier -> " + dto.getIdQuartier(), locale));
					response.setHasError(true);
					return response;
				}
			}
			Adresse entityToSave = AdresseTransformer.INSTANCE.toEntity(dto, existingCommune, existingUser, existingPays, existingVille, existingQuartier);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Adresse> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = adresseRepository.saveAll((Iterable<Adresse>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("adresse", locale));
				response.setHasError(true);
				return response;
			}
			List<AdresseDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AdresseTransformer.INSTANCE.toLiteDtos(itemsSaved) : AdresseTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Adresse-----");
		return response;
	}

	/**
	 * update Adresse by using AdresseDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<AdresseDto> update(Request<AdresseDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Adresse-----");

		Response<AdresseDto> response = new Response<AdresseDto>();
		List<Adresse>        items    = new ArrayList<Adresse>();

		for (AdresseDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la adresse existe
			Adresse entityToSave = null;
			entityToSave = adresseRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("adresse id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if commune exist
			if (Utilities.isValidID(dto.getIdCommune()) && !entityToSave.getCommune().getId().equals(dto.getIdCommune())){
				Commune existingCommune = communeRepository.findOne(dto.getIdCommune(), false);
				if (existingCommune == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("commune idCommune -> " + dto.getIdCommune(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setCommune(existingCommune);
			}
			// Verify if user exist
			if (Utilities.isValidID(dto.getIdUser()) && !entityToSave.getUser().getId().equals(dto.getIdUser())){
				User existingUser = userRepository.findOne(dto.getIdUser(), false);
				if (existingUser == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("user idUser -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setUser(existingUser);
			}
			// Verify if pays exist
			if (Utilities.isValidID(dto.getIdPays()) && !entityToSave.getPays().getId().equals(dto.getIdPays())){
				Pays existingPays = paysRepository.findOne(dto.getIdPays(), false);
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("pays idPays -> " + dto.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setPays(existingPays);
			}
			// Verify if ville exist
			if (Utilities.isValidID(dto.getIdVille()) && !entityToSave.getVille().getId().equals(dto.getIdVille())){
				Ville existingVille = villeRepository.findOne(dto.getIdVille(), false);
				if (existingVille == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("ville idVille -> " + dto.getIdVille(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setVille(existingVille);
			}
			// Verify if quartier exist
			if (Utilities.isValidID(dto.getIdQuartier()) && !entityToSave.getQuartier().getId().equals(dto.getIdQuartier())){
				Quartier existingQuartier = quartierRepository.findOne(dto.getIdQuartier(), false);
				if (existingQuartier == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("quartier idQuartier -> " + dto.getIdQuartier(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setQuartier(existingQuartier);
			}
			if (Utilities.isNotBlank(dto.getAdressePostale()) && !dto.getAdressePostale().equals(entityToSave.getAdressePostale())) {
				entityToSave.setAdressePostale(dto.getAdressePostale());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Adresse> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = adresseRepository.saveAll((Iterable<Adresse>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("adresse", locale));
				response.setHasError(true);
				return response;
			}
			List<AdresseDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AdresseTransformer.INSTANCE.toLiteDtos(itemsSaved) : AdresseTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Adresse-----");
		return response;
	}

	/**
	 * delete Adresse by using AdresseDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<AdresseDto> delete(Request<AdresseDto> request, Locale locale)  {
		log.info("----begin delete Adresse-----");

		Response<AdresseDto> response = new Response<AdresseDto>();
		List<Adresse>        items    = new ArrayList<Adresse>();

		for (AdresseDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la adresse existe
			Adresse existingEntity = null;
			existingEntity = adresseRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("adresse -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			adresseRepository.saveAll((Iterable<Adresse>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Adresse-----");
		return response;
	}

	/**
	 * forceDelete Adresse by using AdresseDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<AdresseDto> forceDelete(Request<AdresseDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Adresse-----");

		Response<AdresseDto> response = new Response<AdresseDto>();
		List<Adresse>        items    = new ArrayList<Adresse>();

		for (AdresseDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la adresse existe
			Adresse existingEntity = null;
			existingEntity = adresseRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("adresse -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			adresseRepository.saveAll((Iterable<Adresse>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Adresse-----");
		return response;
	}

	/**
	 * get Adresse by using AdresseDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<AdresseDto> getByCriteria(Request<AdresseDto> request, Locale locale)  throws Exception {
		log.info("----begin get Adresse-----");

		Response<AdresseDto> response = new Response<AdresseDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Adresse> items 			 = adresseRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("adresse", locale));
			response.setHasError(false);
			return response;
		}

		List<AdresseDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AdresseTransformer.INSTANCE.toLiteDtos(items) : AdresseTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(adresseRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Adresse-----");
		return response;
	}

	/**
	 * get full AdresseDto by using Adresse as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private AdresseDto getFullInfos(AdresseDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
