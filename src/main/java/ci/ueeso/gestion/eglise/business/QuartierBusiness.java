/*
 * Java business for entity table quartier 
 * Created on 2020-03-15 ( Time 00:33:36 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.ueeso.gestion.eglise.dao.entity.Adresse;
import ci.ueeso.gestion.eglise.dao.entity.Commune;
import ci.ueeso.gestion.eglise.dao.entity.Quartier;
import ci.ueeso.gestion.eglise.dao.entity.Village;
import ci.ueeso.gestion.eglise.dao.repository.AdresseRepository;
import ci.ueeso.gestion.eglise.dao.repository.CommuneRepository;
import ci.ueeso.gestion.eglise.dao.repository.QuartierRepository;
import ci.ueeso.gestion.eglise.dao.repository.VillageRepository;
import ci.ueeso.gestion.eglise.helper.ExceptionUtils;
import ci.ueeso.gestion.eglise.helper.FunctionalError;
import ci.ueeso.gestion.eglise.helper.TechnicalError;
import ci.ueeso.gestion.eglise.helper.Utilities;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.AdresseDto;
import ci.ueeso.gestion.eglise.helper.dto.QuartierDto;
import ci.ueeso.gestion.eglise.helper.dto.transformer.AdresseTransformer;
import ci.ueeso.gestion.eglise.helper.dto.transformer.QuartierTransformer;
import lombok.extern.java.Log;

/**
BUSINESS for table "quartier"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class QuartierBusiness implements IBasicBusiness<Request<QuartierDto>, Response<QuartierDto>> {

	private Response<QuartierDto> response;
	@Autowired
	private QuartierRepository quartierRepository;
	@Autowired
	private VillageRepository villageRepository;
	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;


	@Autowired
	private AdresseBusiness adresseBusiness;


	@Autowired
	private UserBusiness userBusiness;

	public QuartierBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	/**
	 * create Quartier by using QuartierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<QuartierDto> create(Request<QuartierDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Quartier-----");

		Response<QuartierDto> response = new Response<QuartierDto>();
		List<Quartier>        items    = new ArrayList<Quartier>();

		for (QuartierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			//fieldsToVerify.put("code", dto.getCode());
			fieldsToVerify.put("libelle", dto.getLibelle());
			//fieldsToVerify.put("longitude", dto.getLongitude());
			//fieldsToVerify.put("latitude", dto.getLatitude());
			//fieldsToVerify.put("superficie", dto.getSuperficie());
			fieldsToVerify.put("idCommune/idVillage", Arrays.asList(dto.getIdCommune(), dto.getIdVillage()));
			//fieldsToVerify.put("idVillage", dto.getIdVillage());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			if(Utilities.isValidID(dto.getIdCommune()) && Utilities.isValidID(dto.getIdVillage())) {
				response.setStatus(functionalError.INVALID_DATA("Un quartier doit être lié soit à une commune soit à un village.", locale));
				response.setHasError(true);
				return response;
			}

			boolean codeFromLibelle = false;
			if(Utilities.isBlank(dto.getCode())) {
				dto.setCode(Utilities.getCodeFromLibelle(dto.getLibelle()));
				codeFromLibelle = true;
			}
			
			Commune existingCommune = null;
			if (Utilities.isValidID(dto.getIdCommune())){
				existingCommune = communeRepository.findOne(dto.getIdCommune(), false);
				if (existingCommune == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("commune idCommune -> " + dto.getIdCommune(), locale));
					response.setHasError(true);
					return response;
				}
				Integer existingCommuneId = existingCommune.getId();
				
				Quartier existingEntity = quartierRepository.findByLibelleAndIdCommune(dto.getLibelle(), existingCommune.getId(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("La commune '"+existingCommune.getLibelle()+"' possède déjà un quartier '"+dto.getLibelle()+"'.", locale));
					response.setHasError(true);
					return response;
				}
				// verif unique libelle in items to save
				if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && a.getCommune().getId().equals(existingCommuneId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du quartier '"+dto.getLibelle()+"' la commune '"+existingCommune.getLibelle()+"'.", locale));
					response.setHasError(true);
					return response;
				}
				
				// verif unique code in db
				existingEntity = quartierRepository.findByCodeAndIdCommune(dto.getCode(), existingCommune.getId(), false);
				if (existingEntity != null) {
					String message = "";
					if(codeFromLibelle) {
						message = "Une quatier similaire existe déjà dans la commune '"+existingCommune.getLibelle()+"': '"+existingEntity.getLibelle()+"'.";
					}
					else {
						message = "La quartier de code '" + dto.getCode()+"' existe déjà dans la commune '"+existingCommune.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_EXIST(message, locale));
					response.setHasError(true);
					return response;
				}
				// verif unique code in items to save
				if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()) && a.getCommune().getId().equals(existingCommuneId))) {
					String message = "";
					if(codeFromLibelle) {
						message = "Tentative d'enregistrement de quartiers quasiment similaire dans la commune '"+existingCommune.getLibelle()+"': '"+dto.getLibelle()+"'.";
					}
					else {
						message = "Tentative de duplication du quartier de code '" + dto.getCode()+"' dans la commune '"+existingCommune.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_DUPLICATE(message, locale));
					response.setHasError(true);
					return response;
				}
			}
			
			// Verify if village exist
			Village existingVillage = null;
			if (Utilities.isValidID(dto.getIdVillage())){
				existingVillage = villageRepository.findOne(dto.getIdVillage(), false);
				if (existingVillage == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("village idVillage -> " + dto.getIdVillage(), locale));
					response.setHasError(true);
					return response;
				}
				Integer existingVillageId = existingVillage.getId();
				
				Quartier existingEntity = quartierRepository.findByLibelleAndIdVillage(dto.getLibelle(), existingVillage.getId(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("Le village '"+existingVillage.getLibelle()+"' possède déjà un quartier '"+dto.getLibelle()+"'.", locale));
					response.setHasError(true);
					return response;
				}
				// verif unique libelle in items to save
				if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()) && a.getVillage().getId().equals(existingVillageId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du quartier '"+dto.getLibelle()+"' le village '"+existingVillage.getLibelle()+"'.", locale));
					response.setHasError(true);
					return response;
				}
				
				// verif unique code in db
				existingEntity = quartierRepository.findByCodeAndIdVillage(dto.getCode(), existingVillage.getId(), false);
				if (existingEntity != null) {
					String message = "";
					if(codeFromLibelle) {
						message = "Une quatier similaire existe déjà dans le village '"+existingVillage.getLibelle()+"': '"+existingEntity.getLibelle()+"'.";
					}
					else {
						message = "La quartier de code '" + dto.getCode()+"' existe déjà dans le village '"+existingVillage.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_EXIST(message, locale));
					response.setHasError(true);
					return response;
				}
				// verif unique code in items to save
				if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(dto.getCode()) && a.getVillage().getId().equals(existingVillageId))) {
					String message = "";
					if(codeFromLibelle) {
						message = "Tentative d'enregistrement de quartiers quasiment similaire dans le village '"+existingVillage.getLibelle()+"': '"+dto.getLibelle()+"'.";
					}
					else {
						message = "Tentative de duplication du quartier de code '" + dto.getCode()+"' dans le village '"+existingVillage.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_DUPLICATE(message, locale));
					response.setHasError(true);
					return response;
				}
			}
			
			
			// Verify if commune exist
			
			Quartier entityToSave = QuartierTransformer.INSTANCE.toEntity(dto, existingVillage, existingCommune);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Quartier> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = quartierRepository.saveAll((Iterable<Quartier>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("quartier", locale));
				response.setHasError(true);
				return response;
			}
			List<QuartierDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? QuartierTransformer.INSTANCE.toLiteDtos(itemsSaved) : QuartierTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Quartier-----");
		return response;
	}

	/**
	 * update Quartier by using QuartierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<QuartierDto> update(Request<QuartierDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Quartier-----");

		Response<QuartierDto> response = new Response<QuartierDto>();
		List<Quartier>        items    = new ArrayList<Quartier>();

		for (QuartierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la quartier existe
			Quartier entityToSave = quartierRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("quartier id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();
			
			if((!Utilities.isValidID(dto.getIdCommune()) && !Utilities.isValidID(dto.getIdVillage())) || (Utilities.isValidID(dto.getIdCommune()) && Utilities.isValidID(dto.getIdVillage()))) {
				response.setStatus(functionalError.INVALID_DATA("Un quartier doit être lié soit à une commune soit à un village.", locale));
				response.setHasError(true);
				return response;
			}
			
			String libelle = entityToSave.getLibelle();
			if (Utilities.isNotBlank(dto.getLibelle()) && !dto.getLibelle().equals(entityToSave.getLibelle())) {
				libelle = dto.getLibelle();
			}
			
			boolean codeFromLibelle = false;
			String code = entityToSave.getCode();
			if (Utilities.isNotBlank(dto.getCode())) {
				if (!dto.getCode().equals(entityToSave.getCode())) {
					code = dto.getCode();
				}
			}
			else {
				dto.setCode(Utilities.getCodeFromLibelle(libelle));
				codeFromLibelle = true;
			}
			
			String finalLibelle = libelle;
			String finalCode = code;

			Village existingVillage = entityToSave.getVillage();
			Commune existingCommune = entityToSave.getCommune();
			
			// Verify if village exist
			if (Utilities.isValidID(dto.getIdVillage())){
				if (entityToSave.getVillage() == null || !entityToSave.getVillage().getId().equals(dto.getIdVillage())){
					existingVillage = villageRepository.findOne(dto.getIdVillage(), false);
					if (existingVillage == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("village idVillage -> " + dto.getIdVillage(), locale));
						response.setHasError(true);
						return response;
					}
				}

				Integer existingVillageId = existingVillage.getId();
				
				Quartier existingEntity = quartierRepository.findByLibelleAndIdVillage(finalLibelle, existingVillage.getId(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
					response.setStatus(functionalError.DATA_EXIST("Le village '"+existingVillage.getLibelle()+"' possède déjà un quartier '"+finalLibelle+"'.", locale));
					response.setHasError(true);
					return response;
				}
				// verif unique libelle in items to save
				if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(finalLibelle) && a.getVillage().getId().equals(existingVillageId) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du quartier '"+finalLibelle+"' le village '"+existingVillage.getLibelle()+"'.", locale));
					response.setHasError(true);
					return response;
				}
				
				// verif unique code in db
				existingEntity = quartierRepository.findByCodeAndIdVillage(finalCode, existingVillage.getId(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
					String message = "";
					if(codeFromLibelle) {
						message = "Une quatier similaire existe déjà dans le village '"+existingVillage.getLibelle()+"': '"+finalLibelle+"'.";
					}
					else {
						message = "La quartier de code '"+finalCode+"' existe déjà dans le village '"+existingVillage.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_EXIST(message, locale));
					response.setHasError(true);
					return response;
				}
				// verif unique code in items to save
				if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(finalCode) && a.getVillage().getId().equals(existingVillageId) && !a.getId().equals(entityToSaveId))) {
					String message = "";
					if(codeFromLibelle) {
						message = "Tentative d'enregistrement de quartiers quasiment similaire dans le village '"+existingVillage.getLibelle()+"': '"+finalLibelle+"'.";
					}
					else {
						message = "Tentative de duplication du quartier de code '"+finalCode+"' dans le village '"+existingVillage.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_DUPLICATE(message, locale));
					response.setHasError(true);
					return response;
				}
				
				existingCommune = null;
			}
			else {
				if (entityToSave.getCommune() == null || !entityToSave.getCommune().getId().equals(dto.getIdCommune())){
					existingCommune = communeRepository.findOne(dto.getIdCommune(), false);
					if (existingCommune == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("commune idCommune -> " + dto.getIdCommune(), locale));
						response.setHasError(true);
						return response;
					}
				}

				Integer existingCommuneId = existingCommune.getId();
				
				Quartier existingEntity = quartierRepository.findByLibelleAndIdCommune(finalLibelle, existingCommune.getId(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
					response.setStatus(functionalError.DATA_EXIST("La commune '"+existingCommune.getLibelle()+"' possède déjà un quartier '"+finalLibelle+"'.", locale));
					response.setHasError(true);
					return response;
				}
				// verif unique libelle in items to save
				if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(finalLibelle) && a.getCommune().getId().equals(existingCommuneId) && !a.getId().equals(entityToSaveId))) {
					response.setStatus(functionalError.DATA_DUPLICATE("Tentative de duplication du quartier '"+finalLibelle+"' la commune '"+existingCommune.getLibelle()+"'.", locale));
					response.setHasError(true);
					return response;
				}
				
				// verif unique code in db
				existingEntity = quartierRepository.findByCodeAndIdCommune(finalCode, existingCommune.getId(), false);
				if (existingEntity != null && !existingEntity.getId().equals(entityToSaveId)) {
					String message = "";
					if(codeFromLibelle) {
						message = "Une quatier similaire existe déjà dans la commune '"+existingCommune.getLibelle()+"': '"+finalLibelle+"'.";
					}
					else {
						message = "La quartier de code '"+finalCode+"' existe déjà dans la commune '"+existingCommune.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_EXIST(message, locale));
					response.setHasError(true);
					return response;
				}
				// verif unique code in items to save
				if (items.stream().anyMatch(a -> a.getCode().equalsIgnoreCase(finalCode) && a.getCommune().getId().equals(existingCommuneId) && !a.getId().equals(entityToSaveId))) {
					String message = "";
					if(codeFromLibelle) {
						message = "Tentative d'enregistrement de quartiers quasiment similaire dans la commune '"+existingCommune.getLibelle()+"': '"+finalLibelle+"'.";
					}
					else {
						message = "Tentative de duplication du quartier de code '"+finalCode+"' dans la commune '"+existingCommune.getLibelle()+"'.";
					}
					response.setStatus(functionalError.DATA_DUPLICATE(message, locale));
					response.setHasError(true);
					return response;
				}
				
				existingVillage = null;
			}
			
			if (dto.getLongitude() != null && dto.getLongitude() > 0) {
				entityToSave.setLongitude(dto.getLongitude());
			}
			if (dto.getLatitude() != null && dto.getLatitude() > 0) {
				entityToSave.setLatitude(dto.getLatitude());
			}
			if (dto.getSuperficie() != null && dto.getSuperficie() > 0) {
				entityToSave.setSuperficie(dto.getSuperficie());
			}
			
			entityToSave.setLibelle(finalLibelle);
			entityToSave.setCode(finalCode);
			entityToSave.setVillage(existingVillage);
			entityToSave.setCommune(existingCommune);
			
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Quartier> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = quartierRepository.saveAll((Iterable<Quartier>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("quartier", locale));
				response.setHasError(true);
				return response;
			}
			List<QuartierDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? QuartierTransformer.INSTANCE.toLiteDtos(itemsSaved) : QuartierTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Quartier-----");
		return response;
	}

	/**
	 * delete Quartier by using QuartierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<QuartierDto> delete(Request<QuartierDto> request, Locale locale)  {
		log.info("----begin delete Quartier-----");

		Response<QuartierDto> response = new Response<QuartierDto>();
		List<Quartier>        items    = new ArrayList<Quartier>();

		for (QuartierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la quartier existe
			Quartier existingEntity = quartierRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("quartier -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdQuartier(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAdresse.size() + ")", locale));
				response.setHasError(true);
				return response;
			}


			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			quartierRepository.saveAll((Iterable<Quartier>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Quartier-----");
		return response;
	}

	/**
	 * forceDelete Quartier by using QuartierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<QuartierDto> forceDelete(Request<QuartierDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Quartier-----");

		Response<QuartierDto> response = new Response<QuartierDto>();
		List<Quartier>        items    = new ArrayList<Quartier>();

		for (QuartierDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la quartier existe
			Quartier existingEntity = quartierRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("quartier -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------

			// adresse
			List<Adresse> listOfAdresse = adresseRepository.findByIdQuartier(existingEntity.getId(), false);
			if (listOfAdresse != null && !listOfAdresse.isEmpty()){
				Request<AdresseDto> deleteRequest = new Request<AdresseDto>();
				deleteRequest.setDatas(AdresseTransformer.INSTANCE.toDtos(listOfAdresse));
				deleteRequest.setUser(request.getUser());
				Response<AdresseDto> deleteResponse = adresseBusiness.delete(deleteRequest, locale);
				if(deleteResponse.isHasError()){
					response.setStatus(deleteResponse.getStatus());
					response.setHasError(true);
					return response;
				}
			}
			
			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			quartierRepository.saveAll((Iterable<Quartier>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Quartier-----");
		return response;
	}

	/**
	 * get Quartier by using QuartierDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<QuartierDto> getByCriteria(Request<QuartierDto> request, Locale locale)  throws Exception {
		log.info("----begin get Quartier-----");

		Response<QuartierDto> response = new Response<QuartierDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("libelle");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Quartier> items 			 = quartierRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("quartier", locale));
			response.setHasError(false);
			return response;
		}

		List<QuartierDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? QuartierTransformer.INSTANCE.toLiteDtos(items) : QuartierTransformer.INSTANCE.toDtos(items);

		final int size = items.size();
		List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
		itemsDto.parallelStream().forEach(dto -> {
			try {
				dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
			} catch (Exception e) {
				listOfError.add(e.getMessage());
				e.printStackTrace();
			}
		});
		if (Utilities.isNotEmpty(listOfError)) {
			Object[] objArray = listOfError.stream().distinct().toArray();
			throw new RuntimeException(StringUtils.join(objArray, ", "));
		}

		response.setItems(itemsDto);
		response.setCount(quartierRepository.count(request, em, locale));
		response.setHasError(false);
		response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Quartier-----");
		return response;
	}

	/**
	 * get full QuartierDto by using Quartier as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private QuartierDto getFullInfos(QuartierDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
