                                                            													

/*
 * Java business for entity table paiement 
 * Created on 2019-12-02 ( Time 00:15:34 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Smile Bakend generator. All Rights Reserved.
 */

package ci.ueeso.gestion.eglise.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ci.ueeso.gestion.eglise.helper.*;
import ci.ueeso.gestion.eglise.helper.dto.*;
import ci.ueeso.gestion.eglise.helper.enums.*;
import ci.ueeso.gestion.eglise.helper.contrat.*;
import ci.ueeso.gestion.eglise.helper.contrat.IBasicBusiness;
import ci.ueeso.gestion.eglise.helper.contrat.Request;
import ci.ueeso.gestion.eglise.helper.contrat.Response;
import ci.ueeso.gestion.eglise.helper.dto.transformer.*;
import ci.ueeso.gestion.eglise.helper.Validate;
import ci.ueeso.gestion.eglise.dao.entity.Paiement;
import ci.ueeso.gestion.eglise.dao.entity.Engagement;
import ci.ueeso.gestion.eglise.dao.entity.Periode;
import ci.ueeso.gestion.eglise.dao.entity.Tresorier;
import ci.ueeso.gestion.eglise.dao.entity.*;
import ci.ueeso.gestion.eglise.dao.repository.*;

/**
BUSINESS for table "paiement"
 * 
 * @author Smile Back-End generator
 *
 */
@Log
@Component
public class PaiementBusiness implements IBasicBusiness<Request<PaiementDto>, Response<PaiementDto>> {

	private Response<PaiementDto> response;
	@Autowired
	private PaiementRepository paiementRepository;
	@Autowired
	private EngagementRepository engagementRepository;
	@Autowired
	private PeriodeRepository periodeRepository;
	@Autowired
	private TresorierRepository tresorierRepository;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

              
          @Autowired
    private UserBusiness userBusiness;
    
	public PaiementBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
	
	/**
	 * create Paiement by using PaiementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaiementDto> create(Request<PaiementDto> request, Locale locale)  throws ParseException {
		log.info("----begin create Paiement-----");

		Response<PaiementDto> response = new Response<PaiementDto>();
		List<Paiement>        items    = new ArrayList<Paiement>();
			
		for (PaiementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("montant", dto.getMontant());
			fieldsToVerify.put("idEngagement", dto.getIdEngagement());
			fieldsToVerify.put("idDatePaiement", dto.getIdDatePaiement());
			fieldsToVerify.put("idTresorier", dto.getIdTresorier());
			fieldsToVerify.put("deletedAt", dto.getDeletedAt());
			fieldsToVerify.put("deletedBy", dto.getDeletedBy());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if paiement to insert do not exist
			Paiement existingEntity = null;
			if (existingEntity != null) {
				response.setStatus(functionalError.DATA_EXIST("paiement id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if engagement exist
			Engagement existingEngagement = null;
			if (Utilities.isValidID(dto.getIdEngagement())){
				existingEngagement = engagementRepository.findOne(dto.getIdEngagement(), false);
				if (existingEngagement == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("engagement idEngagement -> " + dto.getIdEngagement(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if periode exist
			Periode existingPeriode = null;
			if (Utilities.isValidID(dto.getIdDatePaiement())){
				existingPeriode = periodeRepository.findOne(dto.getIdDatePaiement(), false);
				if (existingPeriode == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("periode idDatePaiement -> " + dto.getIdDatePaiement(), locale));
					response.setHasError(true);
					return response;
				}
			}
			// Verify if tresorier exist
			Tresorier existingTresorier = null;
			if (Utilities.isValidID(dto.getIdTresorier())){
				existingTresorier = tresorierRepository.findOne(dto.getIdTresorier(), false);
				if (existingTresorier == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("tresorier idTresorier -> " + dto.getIdTresorier(), locale));
					response.setHasError(true);
					return response;
				}
			}
				Paiement entityToSave = PaiementTransformer.INSTANCE.toEntity(dto, existingEngagement, existingPeriode, existingTresorier);
			entityToSave.setCreatedAt(Utilities.getCurrentDate());
			entityToSave.setCreatedBy(request.getUser());
			entityToSave.setIsDeleted(false);
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Paiement> itemsSaved = null;
			// inserer les donnees en base de donnees
			itemsSaved = paiementRepository.saveAll((Iterable<Paiement>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("paiement", locale));
				response.setHasError(true);
				return response;
			}
			List<PaiementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PaiementTransformer.INSTANCE.toLiteDtos(itemsSaved) : PaiementTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end create Paiement-----");
		return response;
	}

	/**
	 * update Paiement by using PaiementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaiementDto> update(Request<PaiementDto> request, Locale locale)  throws ParseException {
		log.info("----begin update Paiement-----");

		Response<PaiementDto> response = new Response<PaiementDto>();
		List<Paiement>        items    = new ArrayList<Paiement>();
			
		for (PaiementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la paiement existe
			Paiement entityToSave = null;
			entityToSave = paiementRepository.findOne(dto.getId(), false);
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("paiement id -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			Integer entityToSaveId = entityToSave.getId();

			// Verify if engagement exist
			if (Utilities.isValidID(dto.getIdEngagement()) && !entityToSave.getEngagement().getId().equals(dto.getIdEngagement())){
				Engagement existingEngagement = engagementRepository.findOne(dto.getIdEngagement(), false);
				if (existingEngagement == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("engagement idEngagement -> " + dto.getIdEngagement(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setEngagement(existingEngagement);
			}
			// Verify if periode exist
			if (Utilities.isValidID(dto.getIdDatePaiement()) && !entityToSave.getPeriode().getId().equals(dto.getIdDatePaiement())){
				Periode existingPeriode = periodeRepository.findOne(dto.getIdDatePaiement(), false);
				if (existingPeriode == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("periode idDatePaiement -> " + dto.getIdDatePaiement(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setPeriode(existingPeriode);
			}
			// Verify if tresorier exist
			if (Utilities.isValidID(dto.getIdTresorier()) && !entityToSave.getTresorier().getId().equals(dto.getIdTresorier())){
				Tresorier existingTresorier = tresorierRepository.findOne(dto.getIdTresorier(), false);
				if (existingTresorier == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("tresorier idTresorier -> " + dto.getIdTresorier(), locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setTresorier(existingTresorier);
			}
			if (dto.getMontant() != null && dto.getMontant() > 0) {
				entityToSave.setMontant(dto.getMontant());
			}
			if (Utilities.notBlank(dto.getDeletedAt())) {
				entityToSave.setDeletedAt(dateFormat.parse(dto.getDeletedAt()));
			}
			if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
				entityToSave.setCreatedBy(dto.getCreatedBy());
			}
			if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
				entityToSave.setUpdatedBy(dto.getUpdatedBy());
			}
			if (dto.getDeletedBy() != null && dto.getDeletedBy() > 0) {
				entityToSave.setDeletedBy(dto.getDeletedBy());
			}
			entityToSave.setUpdatedAt(Utilities.getCurrentDate());
			entityToSave.setUpdatedBy(request.getUser());
			items.add(entityToSave);
		}

		if (!items.isEmpty()) {
			List<Paiement> itemsSaved = null;
			// maj les donnees en base
			itemsSaved = paiementRepository.saveAll((Iterable<Paiement>) items);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("paiement", locale));
				response.setHasError(true);
				return response;
			}
			List<PaiementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PaiementTransformer.INSTANCE.toLiteDtos(itemsSaved) : PaiementTransformer.INSTANCE.toDtos(itemsSaved);

			final int size = itemsSaved.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end update Paiement-----");
		return response;
	}

	/**
	 * delete Paiement by using PaiementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaiementDto> delete(Request<PaiementDto> request, Locale locale)  {
		log.info("----begin delete Paiement-----");

		Response<PaiementDto> response = new Response<PaiementDto>();
		List<Paiement>        items    = new ArrayList<Paiement>();
			
		for (PaiementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la paiement existe
			Paiement existingEntity = null;
			existingEntity = paiementRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("paiement -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			paiementRepository.saveAll((Iterable<Paiement>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end delete Paiement-----");
		return response;
	}

	/**
	 * forceDelete Paiement by using PaiementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaiementDto> forceDelete(Request<PaiementDto> request, Locale locale) throws ParseException {
		log.info("----begin forceDelete Paiement-----");

		Response<PaiementDto> response = new Response<PaiementDto>();
		List<Paiement>        items    = new ArrayList<Paiement>();
			
		for (PaiementDto dto : request.getDatas()) {
			// Definir les parametres obligatoires
			Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la paiement existe
			Paiement existingEntity = null;
			existingEntity = paiementRepository.findOne(dto.getId(), false);
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("paiement -> " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			// -----------------------------------------------------------------------
			// ----------- CHECK IF DATA IS USED
			// -----------------------------------------------------------------------



			existingEntity.setDeletedAt(Utilities.getCurrentDate());
			existingEntity.setDeletedBy(request.getUser());
			existingEntity.setIsDeleted(true);
			items.add(existingEntity);
		}

		if (!items.isEmpty()) {
			// supprimer les donnees en base
			paiementRepository.saveAll((Iterable<Paiement>) items);

			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));
		}

		log.info("----end forceDelete Paiement-----");
		return response;
	}

	/**
	 * get Paiement by using PaiementDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@Override
	public Response<PaiementDto> getByCriteria(Request<PaiementDto> request, Locale locale)  throws Exception {
		log.info("----begin get Paiement-----");

		Response<PaiementDto> response = new Response<PaiementDto>();

		if(Utilities.blank(request.getData().getOrderField())) {
			request.getData().setOrderField("");
		}
		if(Utilities.blank(request.getData().getOrderDirection())) {
			request.getData().setOrderDirection("asc");
		}

		List<Paiement> items 			 = paiementRepository.getByCriteria(request, em, locale);

		if(Utilities.isEmpty(items)){
			response.setStatus(functionalError.DATA_EMPTY("paiement", locale));
			response.setHasError(false);
			return response;
		}

		List<PaiementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PaiementTransformer.INSTANCE.toLiteDtos(items) : PaiementTransformer.INSTANCE.toDtos(items);

			final int size = items.size();
			List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
			itemsDto.parallelStream().forEach(dto -> {
				try {
					dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
				} catch (Exception e) {
					listOfError.add(e.getMessage());
					e.printStackTrace();
				}
			});
			if (Utilities.isNotEmpty(listOfError)) {
				Object[] objArray = listOfError.stream().distinct().toArray();
				throw new RuntimeException(StringUtils.join(objArray, ", "));
			}

			response.setItems(itemsDto);
			response.setCount(paiementRepository.count(request, em, locale));
			response.setHasError(false);
			response.setStatus(functionalError.SUCCESS("", locale));

		log.info("----end get Paiement-----");
		return response;
	}

	/**
	 * get full PaiementDto by using Paiement as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private PaiementDto getFullInfos(PaiementDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}
}
