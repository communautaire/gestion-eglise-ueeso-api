
/*
 * Created on $today.date ( Time $today.time )
 * Generator tool : $generator.name ( version $generator.version )
 * Copyright 2017 Smile Backend generator. All Rights Reserved.
 */

package ${target.javaPackageFromFolder(${SRC})};


import java.util.Arrays;
import java.util.List;

public class OperatorEnum {
	public static final String	EQUAL				= "=";
	public static final String	NOT_EQUAL_1			= "<>";
	public static final String	NOT_EQUAL_2			= "!=";
	public static final String	LESS_OR_EQUAL		= "<=";
	public static final String	LESS				= "<";
	public static final String	MORE_OR_EQUAL		= ">=";
	public static final String	MORE				= ">";
	public static final String	BETWEEN				= "[]";
	public static final String	BETWEEN_OUT			= "][";
	public static final String	BETWEEN_LEFT_OUT	= "]]";
	public static final String	BETWEEN_RIGHT_OUT	= "[[";
	public static final String	CONTAINS			= "%%";
	public static final String	START_WTIH			= "_%";
	public static final String	END_WTIH			= "%_";
	public static final String	IN					= "IN";
	public static final String	NOT_IN				= "!IN";
	public static final String	EXISTS				= "EXISTS";
	public static final String	NOT_EXISTS			= "!EXISTS";
	
		
	private static final List<String> LIST_OF_ALL_OPERATORS = Arrays.asList(EQUAL, NOT_EQUAL_1, NOT_EQUAL_2, LESS_OR_EQUAL, LESS, MORE_OR_EQUAL, MORE, BETWEEN, BETWEEN_OUT, BETWEEN_LEFT_OUT, BETWEEN_RIGHT_OUT, CONTAINS, START_WTIH, END_WTIH, IN, NOT_IN, EXISTS, NOT_EXISTS);
	
	private static final List<String> LIST_OF_BETWEEN = Arrays.asList(BETWEEN, BETWEEN_OUT, BETWEEN_LEFT_OUT, BETWEEN_RIGHT_OUT);
	private static final List<String> LIST_OF_IN = Arrays.asList(IN, NOT_IN);
	private static final List<String> LIST_OF_OPERATORS_NEEDING_FIELD_VALUE = Arrays.asList(EQUAL, NOT_EQUAL_1, NOT_EQUAL_2, LESS_OR_EQUAL, LESS, MORE_OR_EQUAL, MORE, CONTAINS, START_WTIH, END_WTIH);
	private static final List<String> LIST_OF_OPERATORS_NOT_NEEDING_ANY_VALUES = Arrays.asList(EXISTS, NOT_EXISTS);




	
	public static final boolean	IS_VALID_OPERATOR (String operator){
		return LIST_OF_ALL_OPERATORS.stream().anyMatch(s -> operator.equals(s));
	}
	
	public static final boolean	IS_BETWEEN_OPERATOR (String operator){
		return LIST_OF_BETWEEN.stream().anyMatch(s -> operator.equals(s));
	}
	
	public static final boolean	IS_IN_OPERATOR (String operator){
		return LIST_OF_IN.stream().anyMatch(s -> operator.equals(s));
	}
	
	public static final boolean	OPERATOR_NEEDS_FIELD_VALUE (String operator){
		return LIST_OF_OPERATORS_NEEDING_FIELD_VALUE.stream().anyMatch(s -> operator.equals(s));
	}
	
	public static final boolean	OPERATOR_NOT_NEEDS_ANY_VALUE (String operator){
		return LIST_OF_OPERATORS_NOT_NEEDING_ANY_VALUES.stream().anyMatch(s -> operator.equals(s));
	}
	
	public static final String getOperatorNeedingFieldValueWording(String operator) {
		if(OPERATOR_NEEDS_FIELD_VALUE(operator)) {
			if(operator.equals(EQUAL)) {
				return "Egal à";
			}
			else if(operator.equals(NOT_EQUAL_1) || operator.equals(NOT_EQUAL_2)) {
				return "Différent de";
			}
			else if(operator.equals(LESS_OR_EQUAL)) {
				return "Inférieur ou égal à";
			}
			else if(operator.equals(LESS)) {
				return "Strictement inférieur à";
			}
			else if(operator.equals(MORE_OR_EQUAL)) {
				return "Supérieur ou égal à";
			}
			else if(operator.equals(MORE)) {
				return "Strictement supérieur à";
			}
			else if(operator.equals(CONTAINS)) {
				return "Contient";
			}
			else if(operator.equals(START_WTIH)) {
				return "Commence par";
			}
			else if(operator.equals(END_WTIH)) {
				return "Se termine par";
			}
		}
		return null;
	}
	
	public static final String getOperatorNotNeedingAnyValueWording(String operator) {
		if(OPERATOR_NOT_NEEDS_ANY_VALUE(operator)) {
			if(operator.equals(EXISTS)) {
				return "Est null";
			}
			else if(operator.equals(NOT_EXISTS)) {
				return "Est non null";
			}
		}
		return null;
	}
}
