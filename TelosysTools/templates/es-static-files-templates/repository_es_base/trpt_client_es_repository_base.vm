/*
 * Created on $today.date ( Time $today.time )
 * Generator tool : $generator.name ( version $generator.version )
 * Copyright 2019 Smile CI. All Rights Reserved.
 */

package ${target.javaPackageFromFolder(${SRC})};

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ${ROOT_PKG}.helper.ExceptionUtils;
import ${ROOT_PKG}.helper.FunctionalError;
import ${ROOT_PKG}.helper.TechnicalError;
import ${ROOT_PKG}.helper.Utilities;
import ${ROOT_PKG}.helper.Validate;
import ${ROOT_PKG}.helper.contrat.Response;
import ${ROOT_PKG}.helper.dto.customize._AggregationDto;
import ${ROOT_PKG}.helper.dto.customize._AggregationResponseDto;
import ${ROOT_PKG}.helper.dto.transformer.es.EsTransformer;
import ${ROOT_PKG}.helper.es.EsUtils;
import ${ROOT_PKG}.helper.es.TrptClientEsUtils;
import ${ROOT_PKG}.helper.es.TrptClientFactory;


/**
 * CRUD query for elasticSearch using Transport Client
 * 
 * @author Lazare yao, Smile Back-End developper
 *
 */
public class TrptClientEsRepositoryBase<T> {
	@Autowired
	private TrptClientFactory trptClientFactory;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;

	private Class<T> type ;

	private EsTransformer<T> esTransformer ;

	public TrptClientEsRepositoryBase(Class<T> type) {
		this.type = type;
		this.esTransformer = new EsTransformer<T>(type);
	}

	/**
	 * 
	 * @param ids
	 * @param trptClientFactory
	 * @param indexName
	 * @return List<T>
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws InstantiationException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByIds(String [] ids, TrptClientFactory trptClientFactory, String... indexName) throws IllegalAccessException, InvocationTargetException, InstantiationException, JsonParseException, JsonMappingException, IOException {
		List<T> items = null;

		// prepare the client
		SearchRequestBuilder searchRequestBuilder = TrptClientEsUtils.getSearchRequestBuilder(trptClientFactory, indexName);
		searchRequestBuilder.setQuery(QueryBuilders.idsQuery().addIds(ids));

		//execute query
		SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();
		if (searchResponse != null) {
			items = (List<T>) esTransformer.getIntance(type).toDtos(searchResponse.getHits());
		}
		return items;
	}

	/**
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param indexName
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws InstantiationException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@SuppressWarnings("unchecked")
	public T findOneByField(String fieldName, Object fieldValue, String... indexName) throws IllegalAccessException, InvocationTargetException, InstantiationException, JsonParseException, JsonMappingException, IOException {
		T item = null;
		
		// prepare the client
		SearchRequestBuilder searchRequestBuilder = TrptClientEsUtils.getSearchRequestBuilder(trptClientFactory, indexName);
		BoolQueryBuilder boolQuery = boolQuery();
		boolQuery.filter(termQuery(fieldName, fieldValue));

		searchRequestBuilder.setQuery(boolQuery);
		searchRequestBuilder.setSize(1);

		//execute query
		SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();
		if (searchResponse != null) {
			for (SearchHit hit : searchResponse.getHits()) {
				item = (T) esTransformer.getIntance(type).toDto(hit);
			}
		}
		return item;
	}

	/**
	 * 
	 * @param indexName
	 * @param type
	 * @param templateName
	 * @param trptClientFactory
	 * @param maxClauseCount
	 * @param itemToSave
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public T insert(String indexName, String type, String templateName, TrptClientFactory trptClientFactory, Integer maxClauseCount, T itemToSave) throws Exception {
		T dto = null;
		IndexRequestBuilder requestBuilder = null;
		// si l'index existe
		if (TrptClientEsUtils.getIndicesExistsResponse(indexName, trptClientFactory).isExists()) {
			requestBuilder = TrptClientEsUtils.getIndexRequestBuilder(indexName, type, trptClientFactory);
		} else {
			requestBuilder = TrptClientEsUtils.createIndex(indexName, type, templateName, maxClauseCount, trptClientFactory);
		}

		Gson gson = new Gson();
		requestBuilder.setSource(gson.toJson(itemToSave));
		IndexResponse searchResponse = requestBuilder.execute().actionGet();
		if (searchResponse != null) {
			dto = itemToSave;
		}
		return dto;
	}

	/**
	 * 
	 * @param indexName
	 * @param type
	 * @param trptClientFactory
	 * @param itemToSave
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public T update(String indexName, String type, TrptClientFactory trptClientFactory, T itemToSave, Object id) throws Exception {
		T dto = null;

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.serializeSpecialFloatingPointValues();

		UpdateRequestBuilder requestBuilder = TrptClientEsUtils.getUpdateRequestBuilder(indexName, type, (String) id, trptClientFactory);
		requestBuilder.setRefreshPolicy(RefreshPolicy.IMMEDIATE);
		//requestBuilder.setDoc(new Gson().toJson(itemToSave), XContentType.JSON);
		requestBuilder.setDoc(gsonBuilder.create().toJson(itemToSave), XContentType.JSON);

		UpdateResponse updateResponse = requestBuilder.execute().actionGet();
		if (updateResponse != null) {
			dto = itemToSave;
		}
		return dto;
	}


	/**
	 * 
	 * Get list of value for a given elastic serch field
	 * 
	 * @param locale
	 * @param exceptionUtils
	 * @param functionalError
	 * @param trptClientFactory
	 * @param datasAggregation
	 * @param boolQuery
	 * @param indexType
	 * @param sortTag
	 * @param orderDirection
	 * @param indexName
	 * @return
	 * @throws UnknownHostException
	 * @throws ParseException
	 * @throws CloneNotSupportedException
	 */

	/**
	 * 
	 * Get list of value for a given elastic serch field
	 * 
	 * @param locale
	 * @param exceptionUtils
	 * @param functionalError
	 * @param trptClientFactory
	 * @param datasAggregation
	 * @param boolQuery
	 * @param indexType
	 * @param sortTag
	 * @param orderDirection
	 * @param indexName
	 * @return
	 * @throws UnknownHostException
	 * @throws ParseException
	 * @throws CloneNotSupportedException
	 */
	public Response<_AggregationResponseDto> getList(Locale locale, ExceptionUtils exceptionUtils, FunctionalError functionalError, TrptClientFactory trptClientFactory, List<_AggregationDto> datasAggregation, BoolQueryBuilder boolQuery, String indexType, String... indexName) throws UnknownHostException, ParseException, CloneNotSupportedException {
		Response<_AggregationResponseDto> response = new Response<_AggregationResponseDto>();
		try {
			SearchRequestBuilder requestBuilder = TrptClientEsUtils.getSearchRequestBuilder(indexType, trptClientFactory, indexName);
			requestBuilder.setQuery(boolQuery);
			requestBuilder.setSize(0); 

			for (_AggregationDto dto : datasAggregation) {
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("Règle de filtrage", dto.getField());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// BUILD AGGREGATION
				// --------------------------------------------------------------------------------------------

				AggregationBuilder aggs = EsUtils.buildAggs(dto);
				if(aggs != null) {
					requestBuilder.addAggregation(aggs);
				}
			}

			//System.out.println("requestBuilder = "+requestBuilder);
			// --------------------------------------------------------------------------------------------
			// EXECUTE THE QUERY AND GET THE RESPONSE
			// --------------------------------------------------------------------------------------------
			SearchResponse searchResponse = requestBuilder.execute().actionGet();
			if (searchResponse == null || (searchResponse.getAggregations() == null && searchResponse.getHits() == null)) {
				response.setStatus(technicalError.INTERN_ERROR("Echec de la requête vers ElasticSearch", locale));
				response.setHasError(true);
				return response;
			}

			List<_AggregationResponseDto> items = EsUtils.getAggs(datasAggregation, searchResponse.getAggregations());
			if(Utilities.isEmpty(items)) {
				response.setStatus(functionalError.DATA_EMPTY("Aucune donnée trouvée.", locale));
				response.setHasError(true);
				return response;
			}

			response.setItems(items);
			response.setStatus(functionalError.SUCCESS("", locale));
			response.setHasError(false);
		} catch (Exception e) {
			e.printStackTrace();
			exceptionUtils.EXCEPTION(response, locale, e);
		}
		return response;
	}

}

