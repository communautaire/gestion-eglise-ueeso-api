/*
 * Created on $today.date ( Time $today.time )
 * Generator tool : $generator.name ( version $generator.version )
 * Copyright 2019 Smile CI. All Rights Reserved.
 */


package ${target.javaPackageFromFolder(${SRC})};

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ElasticSearch Transport Client factory
 * 
 * @author Lazare yao, Smile Back-End developper
 *
 */
@Service
public class TrptClientFactory {

	@Autowired
	private ClientConfig	clientConfig;

	private TransportClient	client	= null;

	public TransportClient getClient() throws UnknownHostException {
		System.setProperty("es.set.netty.runtime.available.processors", "false");
		if (client == null) {
			initClient();
		}
		return client;
	}

	private void initClient() throws UnknownHostException {
		Settings settings = this.getSettings();

		client = new PreBuiltTransportClient(settings);

		for (String host : clientConfig.getHosts()) {
			String[] hostParams = host.split(":");
			InetAddress address = InetAddress.getByName(hostParams[0]);
			//Integer port = new Integer(hostParams[1]);
			Integer port = 9300;
			client.addTransportAddress(new InetSocketTransportAddress(address, port));
	
		}
	}

	private Settings getSettings() {
		Settings settings = Settings.builder().put("cluster.name", clientConfig.getClusterName()).build();
		return settings;
	}
}
